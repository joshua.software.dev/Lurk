# Athena Overlay

A plugin based OpenGL/Vulkan ImGui overlay, written with love in Zig.

Dedicated to my dog, Athena. Rest in Peace, my sweet baby girl.

# Currently Available Plugins

## Lurk

![vkcube - Vulkan](https://gitlab.com/joshua.software.dev/lurk-files/-/raw/main/public/vkcube.mp4)
![Cave Story - OpenGL](https://gitlab.com/joshua.software.dev/lurk-files/-/raw/main/public/cave_story.mp4)

The Lurk plugin displays members in your current Discord voice chat in an overlay on top of whatever game you are playing, in a manner similar to Discord's default overlay included with Window's builds of Discord.

## Combat

![Combat Plugin Preview 1](https://gitlab.com/joshua.software.dev/lurk-files/-/raw/main/public/combat_plugin1.png)
![Combat Plugin Preview 2](https://gitlab.com/joshua.software.dev/lurk-files/-/raw/main/public/combat_plugin2.png)

The Combat plugin (accepting suggestions for a better name) is a Final Fantasy 14 ACT WebSocket/Cactbot overlay. For Cactbot functionality, [TotallyNotCef](https://gitlab.com/joshua.software.dev/TotallyNotCef) must be running in the background and separately connected to the ACT WebSocket. More customizability will be available in future updates, at the moment it is functional but quite barebones.

# Technical Details

The Athena Overlay is primarily written in Zig, a system level programming language akin to C/C++, for maximum performance. It makes use of various C/C++ libraries, such as [ImGui](https://github.com/ocornut/imgui/) to do the rendering of the interface, and [FreeType](https://gitlab.freedesktop.org/freetype/freetype) for font rendering. This is to enable non-english text support, such as emojis and asian characters. The Athena Overlay uses the [Vulkan Layer Interface](https://vulkan.lunarg.com/doc/view/1.2.189.0/linux/loader_and_layer_interface.html) to dynamically load itself into running Vulkan applications. For OpenGL applications, LD_PRELOAD is used instead.

# Installing

The Athena Overlay is still alpha software, and requires building from source at the moment to use. At the time of writing, it targets Zig master and Zig 0.11.0, either can be used to build it. As such, you must install either Zig 0.11.0 or the latest master build before attempting to compile the Athena Overlay.

Do to so, first clone the repo:
```sh
git clone https://gitlab.com/joshua.software.dev/AthenaOverlay.git
cd AthenaOverlay
```

Next, you need to choose a build type. There are three supported build type configurations:
* Build as an Arch Linux Package: set `-Done_folder=false` (by default, "one_folder" is set to true)
* Build for XDG Spec compatible installation: `-Dxdg_install=true` (by default, "xdg_install" is set to false)
* Build as a self-contained folder: `-Done_folder=true` (or don't supply a build flag)
    * This option has poor default integration with your system, and may require additional steps to make into a useful installation

In addition to your build type, you will also have to choose which architecture(s) you wish to build for. If you wish to play only 64-bit games on a 64-bit system, you may only need to build for 64-bit. Unfortunately, if you wish to use the Athena Overlay in Windows games with wine, many Windows games are 32-bit, and you will either need to use a WOW64 only build of wine, or more likely, you will need to build both 32-bit and 64-bit versions of the Athena Overlay.

## Building as a Self-Contained Folder Example

If you intend to run the Athena Overlay on the same machine you are compiling it on:

```
# 32 bit
zig build -Dcpu=native -Dtarget=x86-linux-gnu -Doptimize=ReleaseSafe

# 64 bit
zig build -Dcpu=native -Dtarget=x86_64-linux-gnu -Doptimize=ReleaseSafe
```

if you intend to build a generic build for running on processors different than your exact model, instead run:

```
# 32 bit
zig build -Dcpu=baseline -Dtarget=x86-linux-gnu -Doptimize=ReleaseSafe

# 64 bit
zig build -Dcpu=baseline -Dtarget=x86_64-linux-gnu -Doptimize=ReleaseSafe
```

The above commands will generate output into the `zig-out` folder, as a self contained folder. If you wish for the Athena Overlay to integrate into your system more easily, you likely want to build either for an XDG Compatible installation, or if you're on Arch Linux, as a system package.

## Building for XDG Example

```
# 32 bit
zig build -Dcpu=baseline -Dtarget=x86-linux-gnu -Doptimize=ReleaseSafe -Dxdg_install=true

# 64 bit
zig build -Dcpu=baseline -Dtarget=x86_64-linux-gnu -Doptimize=ReleaseSafe -Dxdg_install=true
```

A script called `xdg_install.sh` can be found in the project root, to aid with copying the output to the appropriate location for an XDG install. This will install the Athena Overlay into your `$XDG_DATA_HOME` directory (that being `$HOME/.local/share/` if not set manually.)

## Building for Arch Linux Example

```
# 32 bit
zig build -Dcpu=baseline -Dtarget=x86-linux-gnu -Doptimize=ReleaseSafe -Done_folder=false

# 64 bit
zig build -Dcpu=baseline -Dtarget=x86_64-linux-gnu -Doptimize=ReleaseSafe -Done_folder=false
```

Note, this does not create a `.pkg.zst` for use with pacman on its own, you can see an example of how you might do that [here](https://gitlab.com/joshua.software.dev/AthenaOverlay/-/raw/master/scripts/arch_linux_build.sh).

## How to run

When launching a Vulkan application, ensure the application is run with the environment variable `ENABLE_ATHENA_OVERLAY` set to `1`. Similarly, it can be forcibly disabled with `DISABLE_ATHENA_OVERLAY` set to `1`. Example:

`env ENABLE_ATHENA_OVERLAY=1 vkcube`

When launching an OpenGL application, the environment variable `LD_PRELOAD` must be set to point the appropriate Athena Overlay library file. Example:

`env LD_PRELOAD=$XDG_DATA_HOME/athena_overlay/lib/64/libathena_overlay_opengl.so glxgears`

For more complicated setups, see the [Vulkan](https://gitlab.com/joshua.software.dev/AthenaOverlay/-/raw/master/vulkan_layer/scripts/athenavk) and [OpenGL](https://gitlab.com/joshua.software.dev/AthenaOverlay/-/raw/master/opengl_layer/scripts/athenagl) helper scripts for inspiration.

# Configuration

The Athena Overlay tries to be [XDG Base Directory Spec](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) compliant, and as such its configuration files can be found at: `$XDG_CONFIG_HOME/athena_overlay/`. For an example of the options available, see the default config file [here](https://gitlab.com/joshua.software.dev/AthenaOverlay/-/raw/master/athena_overlay/src/default_config.jsonc).

## Known Issues

* Likely does not work in at least some games. I do not own or play every game on the planet, and as such cannot test all of them. Incompatible games are a high priority to fix, and I intend for the Athena Overlay to work in as many as possible.
* Does not (and likely never will) support right to left text (see https://github.com/ocornut/imgui/issues/1228)
* The project primarily supports only Linux. It technically can be built for MacOS/Windows, but support is untested
* On Windows, the OpenGL layer cannot work as it depends on the Linux specific LD_PRELOAD
* On MacOS, the OpenGL layer does not work, as it depends on the [elfhacks](https://gitlab.com/joshua.software.dev/AthenaOverlay/-/raw/master/opengl_layer/deps/elfhacks/elfhacks.cpp) library for loading the layer, which does not support MacOS. An alternative may exist, but integrating it is low priority as MacOS is low priority.

# Thanks
Thank you so much to the variety of library developers upon which the Athena Overlay is built, it wouldn't have been possible without you. A non-exhaustive list of individual thanks:

* Thank you to [nikneym](https://github.com/nikneym) for the [ws](https://github.com/nikneym/ws) library that I based my [branch](https://gitlab.com/joshua.software.dev/ws) on. Learning Zig nearly became another vaporware side project before I found your WebSocket library to start experimenting with.
* Thank you to [Snektron](https://github.com/Snektron/) for the [vulkan-zig](https://github.com/Snektron/vulkan-zig/) bindings. I never would have gotten this project working without a helping hand in understanding Vulkan that the bindings so wonderfully made clear.
* Thank you to [Mesa's Vulkan Overlay Layer](https://gitlab.freedesktop.org/mesa/mesa/-/tree/main/src/vulkan/overlay-layer) and [MangoHud](https://github.com/flightlessmango/MangoHud) for serving as a base for reimplementing in Zig.
* Thank you to [SpexGuy](https://github.com/SpexGuy) for the [initial](https://github.com/SpexGuy/Zig-ImGui) bindings to ImGui that I based my [updated](https://gitlab.com/joshua.software.dev/Zig-ImGui) bindings on
* Thank you to [slimsag](https://github.com/slimsag) for packaging [brotli](https://github.com/hexops/brotli) and [FreeType](https://github.com/hexops/freetype) for Zig
* Thank you to the authors/maintainers/updaters of [glslang](https://github.com/KhronosGroup/glslang), [iguanaTLS](https://github.com/joshua-software-dev/iguanaTLS/), [known_folders](https://github.com/ziglibs/known-folders/), [uuid-zig](https://github.com/r4gus/uuid-zig/), [zgl](https://github.com/ziglibs/zgl/), kubkon's [zig_yaml](https://github.com/kubkon/zig-yaml/), nektro's [zig-yaml](https://github.com/nektro/zig-yaml), and [zstbi](https://github.com/michal-z/zig-gamedev/tree/main/libs/zstbi) for saving me a bunch of time and making their code open source so I could use it in my project.
* Thank you to the Zig Foundation for making Zig, it is a pleasure to work in, and I wish you great success in taking over the world
