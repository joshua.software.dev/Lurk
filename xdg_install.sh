#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH/"

if [[ -z "$XDG_DATA_HOME" ]]; then
    XDG_DATA_HOME="$HOME/.local/share/"
fi

mkdir -p "$XDG_DATA_HOME/athena_overlay/" && \
cp -r zig-out/athena_overlay/* "$XDG_DATA_HOME/athena_overlay/" && \
mkdir -p "$XDG_DATA_HOME/vulkan/implicit_layer.d/" && \
cp zig-out/vulkan/implicit_layer.d/* "$XDG_DATA_HOME/vulkan/implicit_layer.d/"
