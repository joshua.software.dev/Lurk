const builtin = @import("builtin");
const std = @import("std");

const simple_std_http = @import("athena_overlay/deps/simple/src/http/std_http/std_http.zig");


const vk_xml_uri = std.Uri.parse
(
    "https://github.com/KhronosGroup/Vulkan-Docs/raw/463f8c616f49fb83ae4736de0d84b0048a7c76e2/xml/vk.xml"
)
    catch unreachable;

fn get_filename_without_extension(allocator: std.mem.Allocator, input: []const u8) []const u8
{
    var storage = std.ArrayList([]const u8).init(allocator);
    defer storage.deinit();

    var size: usize = 0;
    var iter = std.mem.splitScalar(u8, input, '.');
    while (iter.next()) |it|
    {
        size += it.len;
        storage.append(it) catch @panic("oom");
    }

    var filename = std.ArrayList(u8).init(allocator);
    defer filename.deinit();
    for (storage.items[0..storage.items.len - 1]) |part|
    {
        filename.appendSlice(part) catch @panic("oom");
    }

    return filename.toOwnedSlice() catch @panic("oom");
}

fn write_plugins_to_file
(
    builder: *std.Build,
    plugins: *const std.ArrayList(std.Build.ModuleDependency),
    overwrite: bool,
)
!void
{
    const out_path = builder.pathFromRoot("athena_overlay/src/build_with_plugins.zig");
    const output_fd = blk: {
        break :blk std.fs.createFileAbsolute(out_path, .{ .exclusive = true, })
            catch |err| switch (err)
            {
                error.PathAlreadyExists =>
                {
                    if (!overwrite) return;
                    break :blk try std.fs.createFileAbsolute(out_path, .{});
                },
                else => return err,
            };
    };

    var writer = output_fd.writer();

    try writer.writeAll("pub const plugins = &.{\n");
    for (plugins.items) |plugin|
    {
        try writer.print("    @import(\"{}\"),\n", .{ std.zig.fmtEscapes(plugin.name), });
    }
    try writer.writeAll("};\n");
    try writer.print("pub const plugins_count = {d};\n", .{ plugins.items.len, });
}

fn build_opengl_layer
(
    builder: *std.Build,
    allow_any_arch: bool,
    libathena_overlay: *std.Build.Step.Compile,
    athena_overlay_module: *std.Build.Module,
    simple_mod: *std.Build.Module,
    zgl_dep: *std.Build.Dependency,
    self_contained: bool,
    extract_debug_info: bool,
    args: anytype,
)
void
{
    if (!allow_any_arch)
    {
        switch (args.target.getCpuArch())
        {
            .arm, .aarch64, .x86, .x86_64, => {},
            else => @panic("Unsupported CPU architecture.")
        }
    }

    const opengl_layer = builder.addSharedLibrary
    (
        .{
            .name = "athena_overlay_opengl",
            .root_source_file = .{ .path = "opengl_layer/src/main.zig" },
            .target = args.target,
            .optimize = args.optimize,
        }
    );
    opengl_layer.force_pic = true;
    opengl_layer.link_z_relro = true;
    if (args.target.getCpuArch() == .x86)
    {
        opengl_layer.link_z_notext = true;
    }

    opengl_layer.linkLibrary(libathena_overlay);
    opengl_layer.addCSourceFile
    (
        .{
            .file = .{ .path = "opengl_layer/deps/elfhacks/elfhacks.cpp" },
            .flags = &.{
                "-std=c++17",
                "-fno-sanitize=undefined",
                "-fvisibility=hidden",
            },
        }
    );

    opengl_layer.addModule("athena_overlay", athena_overlay_module);
    opengl_layer.addModule("simple", simple_mod);
    opengl_layer.addModule("zgl", zgl_dep.module("zgl"));

    const target_info = std.zig.system.NativeTargetInfo.detect(args.target)
        catch @panic("Failed to get target info");

    const layer_objcopy = builder.addObjCopy
    (
        opengl_layer.getEmittedBin(),
        if
        (
            @hasDecl(std.Build.Step.ObjCopy, "getOutputSeparatedDebug") and
            args.target.getObjectFormat() == .elf
        )
            .{
                .basename = opengl_layer.out_filename,
                .compress_debug = false,
                .strip = switch (args.optimize)
                {
                    .Debug =>
                        if (extract_debug_info)
                            .debug_and_symbols
                        else
                            .none,
                    else => .debug_and_symbols,
                },
                .extract_to_separate_file = extract_debug_info,
            }
        else
            .{ .basename = opengl_layer.out_filename },
    );

    const build_install_step = builder.getInstallStep();
    if (self_contained)
    {
        const install_gl_layer = builder.addInstallBinFile
        (
            if (args.target.getObjectFormat() == .elf)
                layer_objcopy.getOutput()
            else
                opengl_layer.getEmittedBin(),
            builder.fmt
            (
                "athena_overlay/lib/{s}/{s}",
                .{
                    switch (target_info.target.ptrBitWidth())
                    {
                        32 => "32",
                        64 => "64",
                        else => @panic("Unsupported CPU architecture."),
                    },
                    opengl_layer.out_filename,
                }
            ),
        );
        install_gl_layer.dir = .prefix;
        build_install_step.dependOn(&install_gl_layer.step);

        if
        (
            @hasDecl(std.Build.Step.ObjCopy, "getOutputSeparatedDebug") and
            args.target.getObjectFormat() == .elf and
            extract_debug_info
        )
        {
            const install_debug_symbols = builder.addInstallBinFile
            (
                layer_objcopy.getOutputSeparatedDebug().?,
                builder.fmt
                (
                    "athena_overlay/lib/{s}/{s}.debug",
                    .{
                        switch (target_info.target.ptrBitWidth())
                        {
                            32 => "32",
                            64 => "64",
                            else => @panic("Unsupported CPU architecture."),
                        },
                        get_filename_without_extension(builder.allocator, opengl_layer.out_filename),
                    }
                ),
            );
            install_debug_symbols.dir = .prefix;
            build_install_step.dependOn(&install_debug_symbols.step);
        }

        builder.installDirectory
        (
            .{
                .source_dir = .{ .path = "opengl_layer/third_party" },
                .install_dir = .prefix,
                .install_subdir = "athena_overlay/docs/third_party"
            }
        );

        builder.installFile("opengl_layer/scripts/athenagl", "athena_overlay/bin/athenagl");
    }
    else
    {
        const install_gl_layer = builder.addInstallBinFile
        (
            if (args.target.getObjectFormat() == .elf)
                layer_objcopy.getOutput()
            else
                opengl_layer.getEmittedBin(),
            builder.fmt
            (
                "lib{s}/{s}",
                .{
                    switch (target_info.target.ptrBitWidth())
                    {
                        32 => "32",
                        64 => "",
                        else => @panic("Unsupported CPU architecture."),
                    },
                    opengl_layer.out_filename,
                }
            ),
        );
        install_gl_layer.dir = .prefix;
        build_install_step.dependOn(&install_gl_layer.step);

        if
        (
            @hasDecl(std.Build.Step.ObjCopy, "getOutputSeparatedDebug") and
            args.target.getObjectFormat() == .elf and
            extract_debug_info
        )
        {
            const install_debug_symbols = builder.addInstallBinFile
            (
                layer_objcopy.getOutputSeparatedDebug().?,
                builder.fmt
                (
                    "lib{s}/{s}.debug",
                    .{
                        switch (target_info.target.ptrBitWidth())
                        {
                            32 => "32",
                            64 => "",
                            else => @panic("Unsupported CPU architecture."),
                        },
                        get_filename_without_extension(builder.allocator, opengl_layer.out_filename),
                    }
                ),
            );
            install_debug_symbols.dir = .prefix;
            build_install_step.dependOn(&install_debug_symbols.step);
        }

        builder.installDirectory
        (
            .{
                .source_dir = .{ .path = "opengl_layer/third_party" },
                .install_dir = .prefix,
                .install_subdir = "share/licenses/athena_overlay/third_party"
            }
        );
        builder.installFile("opengl_layer/scripts/athenagl", "bin/athenagl");
    }
}

fn build_vulkan_layer
(
    builder: *std.Build,
    allow_any_arch: bool,
    libathena_overlay: *std.Build.Step.Compile,
    athena_overlay_module: *std.Build.Module,
    simple_mod: *std.Build.Module,
    vk_zig_dep: *std.Build.Dependency,
    glslang_dep: *std.Build.Dependency,
    use_system_vulkan: bool,
    self_contained: bool,
    xdg_install: bool,
    extract_debug_info: bool,
    args: anytype,
)
void
{
    if (!allow_any_arch)
    {
        switch (args.target.getCpuArch())
        {
            .arm, .aarch64, .x86, .x86_64, => {},
            else => @panic("Unsupported CPU architecture.")
        }
    }

    const target_info = std.zig.system.NativeTargetInfo.detect(args.target)
        catch @panic("Failed to get target info");

    const options = builder.addOptions();
    options.addOption(bool, "allow_any_arch", allow_any_arch);

    const glslang_exe = glslang_dep.artifact("glslang");

    const found_frag_output = blk: {
        std.fs.accessAbsolute(builder.pathFromRoot("vulkan_layer/src/shaders/imgui.frag.spv"), .{})
            catch break :blk false;
        break :blk true;
    };

    const found_vert_output = blk: {
        std.fs.accessAbsolute(builder.pathFromRoot("vulkan_layer/src/shaders/imgui.vert.spv"), .{})
            catch break :blk false;
        break :blk true;
    };

    const vulkan_layer = builder.addSharedLibrary
    (
        .{
            .name = "athena_overlay_vulkan",
            .root_source_file = .{ .path = "vulkan_layer/src/main.zig" },
            .target = args.target,
            .optimize = args.optimize,
        }
    );
    vulkan_layer.force_pic = true;
    vulkan_layer.link_z_relro = true;
    if (args.target.getCpuArch() == .x86)
    {
        vulkan_layer.link_z_notext = true;
    }

    if (!found_frag_output)
    {
        const frag_shader_compile = builder.addRunArtifact(glslang_exe);
        frag_shader_compile.addCheck(.{ .expect_stderr_exact = "" }); // silence stdout
        frag_shader_compile.addCheck(.{ .expect_term = .{ .Exited = 0 }});

        frag_shader_compile.addArgs
        (
            &.{
                "-V",
                "-o",
                builder.pathFromRoot("vulkan_layer/src/shaders/imgui.frag.spv"),
                builder.pathFromRoot("vulkan_layer/src/shaders/imgui.frag.glsl"),
            }
        );

        vulkan_layer.step.dependOn(&frag_shader_compile.step);
    }

    if (!found_vert_output)
    {
        const vert_shader_compile = builder.addRunArtifact(glslang_exe);
        vert_shader_compile.addCheck(.{ .expect_stderr_exact = "" }); // silence stdout
        vert_shader_compile.addCheck(.{ .expect_term = .{ .Exited = 0 }});

        vert_shader_compile.addArgs
        (
            &.{
                "-V",
                "-o",
                builder.pathFromRoot("vulkan_layer/src/shaders/imgui.vert.spv"),
                builder.pathFromRoot("vulkan_layer/src/shaders/imgui.vert.glsl"),
            }
        );

        vulkan_layer.step.dependOn(&vert_shader_compile.step);
    }

    vulkan_layer.linkLibC();
    vulkan_layer.linkLibrary(libathena_overlay);

    vulkan_layer.addModule("athena_overlay", athena_overlay_module);
    vulkan_layer.addModule("simple", simple_mod);
    vulkan_layer.addModule("vulkan_layer_build_options", options.createModule());

    const vk_zig_exists = blk: {
        std.fs.accessAbsolute(builder.pathFromRoot("zig-cache/vk.zig"), .{})
            catch break :blk false;
        break :blk true;
    };
    if (!vk_zig_exists)
    {
        const LOCAL_VULKAN_PATH = builder.pathFromRoot("zig-cache/vk.xml");
        const SYSTEM_VULKAN_PATH = "/usr/share/vulkan/registry/vk.xml";

        const download_step = builder.step("download_xml", "Download vk.xml file");
        download_step.makeFn = struct
        {
            fn f(step: *std.build.Step, progress: *std.Progress.Node) !void
            {
                _ = progress;
                var client = try simple_std_http.init(.{
                    .bundle = .dynamically_generate,
                    .http_allocator = step.owner.allocator,
                });
                var simple_http_client = client.as_simple_client();
                defer simple_http_client.deinit();

                try simple_http_client.download_file
                (
                    &.{},
                    vk_xml_uri,
                    step.owner.pathFromRoot("zig-cache/vk.xml"),
                    null,
                );
            }
        }.f;

        const vk_gen = vk_zig_dep.artifact("generator");
        const gen_cmd = builder.addRunArtifact(vk_gen);
        gen_cmd.step.dependOn(download_step);

        var found_system_vulkan = true;
        std.fs.accessAbsolute(SYSTEM_VULKAN_PATH, .{})
            catch { found_system_vulkan = false; };
        gen_cmd.addFileArg(.{
            .path = if (use_system_vulkan and found_system_vulkan)
                SYSTEM_VULKAN_PATH
            else
                LOCAL_VULKAN_PATH
        });

        // the "../" in "../zig-cache/vk.zig" is very important, otherwise it ends
        // up in zig-out/zig-cache/vk.zig
        const gen_install = builder.addInstallFile(gen_cmd.addOutputFileArg("vk.zig"), "../zig-cache/vk.zig");
        gen_install.step.dependOn(&gen_cmd.step);
        vulkan_layer.step.dependOn(&gen_install.step);
    }
    const vulkan_mod = builder.addModule("vk", .{ .source_file = .{ .path = "zig-cache/vk.zig" } });
    vulkan_layer.addModule("vk", vulkan_mod);

    const layer_objcopy = builder.addObjCopy
    (
        vulkan_layer.getEmittedBin(),
        if
        (
            @hasDecl(std.Build.Step.ObjCopy, "getOutputSeparatedDebug") and
            args.target.getObjectFormat() == .elf
        )
            .{
                .basename = vulkan_layer.out_filename,
                .compress_debug = false,
                .strip = switch (args.optimize)
                {
                    .Debug =>
                        if (extract_debug_info)
                            .debug_and_symbols
                        else
                            .none,
                    else => .debug_and_symbols,
                },
                .extract_to_separate_file = extract_debug_info,
            }
        else
            .{ .basename = vulkan_layer.out_filename },
    );

    const build_install_step = builder.getInstallStep();
    if (self_contained)
    {
        const install_vk_layer = builder.addInstallBinFile
        (
            if (args.target.getObjectFormat() == .elf)
                layer_objcopy.getOutput()
            else
                vulkan_layer.getEmittedBin(),
            builder.fmt
            (
                "athena_overlay/lib/{d}/{s}",
                .{
                    target_info.target.ptrBitWidth(),
                    vulkan_layer.out_filename,
                }
            ),
        );
        install_vk_layer.dir = .prefix;
        build_install_step.dependOn(&install_vk_layer.step);

        if
        (
            @hasDecl(std.Build.Step.ObjCopy, "getOutputSeparatedDebug") and
            args.target.getObjectFormat() == .elf and
            extract_debug_info
        )
        {
            const install_debug_symbols = builder.addInstallBinFile
            (
                layer_objcopy.getOutputSeparatedDebug().?,
                builder.fmt
                (
                    "athena_overlay/lib/{d}/{s}.debug",
                    .{
                        target_info.target.ptrBitWidth(),
                        get_filename_without_extension(builder.allocator, vulkan_layer.out_filename),
                    }
                ),
            );
            install_debug_symbols.dir = .prefix;
            build_install_step.dependOn(&install_debug_symbols.step);
        }

        switch (args.target.getCpuArch())
        {
            .arm =>
            {
                switch (args.target.getOsTag())
                {
                    .macos, .windows => @panic("Unsupported CPU architecture."),
                    else =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/linux/one_folder/athena_vulkan_layer_linux_arm_32.json",
                            if (xdg_install)
                                "vulkan/implicit_layer.d/athena_vulkan_layer_linux_arm_32.json"
                            else
                                "athena_overlay/manifests/athena_vulkan_layer_linux_arm_32.json"
                        );
                    },
                }
            },
            .aarch64 =>
            {
                switch (args.target.getOsTag())
                {
                    .macos =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/macos/athena_vulkan_layer_macos_arm_64.json",
                            "athena_overlay/manifests/athena_vulkan_layer_arm_64.json"
                        );
                    },
                    .windows =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/windows/athena_vulkan_layer_windows_arm_64.json",
                            "athena_overlay/manifests/vk_layer_windows_arm_64.json"
                        );
                    },
                    else =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/linux/one_folder/athena_vulkan_layer_linux_arm_64.json",
                            if (xdg_install)
                                "vulkan/implicit_layer.d/athena_vulkan_layer_linux_arm_64.json"
                            else
                                "athena_overlay/manifests/athena_vulkan_layer_linux_arm_64.json"
                        );
                    },
                }
            },
            .x86 =>
            {
                switch (args.target.getOsTag())
                {
                    .macos => @panic("Unsupported CPU architecture."),
                    .windows =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/windows/athena_vulkan_layer_windows_x86_32.json",
                            "athena_overlay/manifests/athena_vulkan_layer_windows_x86_32.json"
                        );
                    },
                    else =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/linux/one_folder/athena_vulkan_layer_linux_x86_32.json",
                            if (xdg_install)
                                "vulkan/implicit_layer.d/athena_vulkan_layer_linux_x86_32.json"
                            else
                                "athena_overlay/manifests/athena_vulkan_layer_linux_x86_32.json"
                        );
                    },
                }
            },
            .x86_64 =>
            {
                switch (args.target.getOsTag())
                {
                    .macos =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/macos/athena_vulkan_layer_macos_x86_64.json",
                            "athena_overlay/manifests/athena_vulkan_layer_x86_64.json"
                        );
                    },
                    .windows =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/windows/athena_vulkan_layer_windows_x86_64.json",
                            "athena_overlay/manifests/athena_vulkan_layer_windows_x86_64.json"
                        );
                    },
                    else =>
                    {
                        builder.installFile
                        (
                            "vulkan_layer/manifests/linux/one_folder/athena_vulkan_layer_linux_x86_64.json",
                            if (xdg_install)
                                "vulkan/implicit_layer.d/athena_vulkan_layer_linux_x86_64.json"
                            else
                                "athena_overlay/manifests/athena_vulkan_layer_linux_x86_64.json"
                        );
                    },
                }
            },
            else =>
            {
                if(!allow_any_arch) @panic("Unsupported CPU architecture.");
            },
        }

        builder.installDirectory
        (
            .{
                .source_dir = .{ .path = "vulkan_layer/third_party" },
                .install_dir = .prefix,
                .install_subdir = "athena_overlay/docs/third_party"
            }
        );

        if (!args.target.isWindows())
        {
            builder.installFile("vulkan_layer/scripts/athenavk", "athena_overlay/bin/athenavk");
        }
    }
    else
    {
        const install_vk_layer = builder.addInstallBinFile
        (
            if (args.target.getObjectFormat() == .elf)
                layer_objcopy.getOutput()
            else
                vulkan_layer.getEmittedBin(),
            builder.fmt
            (
                "lib{s}/{s}",
                .{
                    switch (target_info.target.ptrBitWidth())
                    {
                        32 => "32",
                        64 => "",
                        else => @panic("Unsupported CPU architecture.")
                    },
                    vulkan_layer.out_filename,
                }
            ),
        );
        install_vk_layer.dir = .prefix;
        build_install_step.dependOn(&install_vk_layer.step);

        if
        (
            @hasDecl(std.Build.Step.ObjCopy, "getOutputSeparatedDebug") and
            args.target.getObjectFormat() == .elf and
            extract_debug_info
        )
        {
            const install_debug_symbols = builder.addInstallBinFile
            (
                layer_objcopy.getOutputSeparatedDebug().?,
                builder.fmt
                (
                    "lib{s}/{s}.debug",
                    .{
                        switch (target_info.target.ptrBitWidth())
                        {
                            32 => "32",
                            64 => "",
                            else => @panic("Unsupported CPU architecture.")
                        },
                        get_filename_without_extension(builder.allocator, vulkan_layer.out_filename),
                    }
                ),
            );
            install_debug_symbols.dir = .prefix;
            build_install_step.dependOn(&install_debug_symbols.step);
        }

        switch (args.target.getCpuArch())
        {
            .arm =>
            {
                builder.installFile
                (
                    "vulkan_layer/manifests/linux/sys_install/athena_vulkan_layer_linux_arm_32.json",
                    "share/vulkan/implicit_layer.d/athena_vulkan_layer_linux_arm_32.json"
                );
            },
            .aarch64 =>
            {
                builder.installFile
                (
                    "vulkan_layer/manifests/linux/sys_install/athena_vulkan_layer_linux_arm_64.json",
                    "share/vulkan/implicit_layer.d/athena_vulkan_layer_linux_arm_64.json"
                );
            },
            .x86 =>
            {
                builder.installFile
                (
                    "vulkan_layer/manifests/linux/sys_install/athena_vulkan_layer_linux_x86_32.json",
                    "share/vulkan/implicit_layer.d/athena_vulkan_layer_linux_x86_32.json"
                );
            },
            .x86_64 =>
            {
                builder.installFile
                (
                    "vulkan_layer/manifests/linux/sys_install/athena_vulkan_layer_linux_x86_64.json",
                    "share/vulkan/implicit_layer.d/athena_vulkan_layer_linux_x86_64.json"
                );
            },
            else =>
            {
                if(!allow_any_arch) @panic("Unsupported CPU architecture.");
            },
        }

        builder.installDirectory
        (
            .{
                .source_dir = .{ .path = "vulkan_layer/third_party" },
                .install_dir = .prefix,
                .install_subdir = "share/licenses/athena_overlay/third_party"
            }
        );
        builder.installFile("vulkan_layer/scripts/athenavk", "bin/athenavk");
    }
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    var target = b.standardTargetOptions(.{});
    switch (target.getCpuArch())
    {
        .arm =>
        {
            target.abi = switch (target.abi.?)
            {
                // defaults to soft float only, which some of the C libs do not
                // like, could maybe fixed with great effort, but what arm
                // board without hard float support has a decent enough GPU to
                // run vulkan or opengl games reasonably?
                .gnu => .gnueabihf,
                else => target.abi
            };
        },
        else => {},
    }
    const native_target = (std.Build.StandardTargetOptionsArgs{}).default_target;

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const should_build_opengl =
        if (target.isDarwin() or target.isWindows())
            false
        else if (b.option(bool, "build_gl", "Build the opengl layer, default=true")) |opt|
            opt
        else
            true;

    const should_build_vulkan =
        if (b.option(bool, "build_vk", "Build the vulkan layer, default=true")) |opt|
            opt
        else
            true;

    if (!should_build_opengl and !should_build_vulkan) return;

    const xdg_install =
        if
        (
            b.option
            (
                bool,
                "xdg_install",
                "Build with intent to copy files from zig-out to $XDG_DATA_HOME " ++
                "after compiling, instead of to system directories. Has no effect " ++
                "when one_folder=false. When one_folder is true or not set, this " ++
                "prefers generating an XDG compatible layout to generating exactly " ++
                "one_folder. default=false"
            )
        ) |opt|
            opt
        else
            false;

    const one_folder =
        if (target.isDarwin() or target.isWindows())
            true
        else if
        (
            b.option
            (
                bool,
                "one_folder",
                "Build with intent to copy files from zig-out to a single folder such as /opt/athena_overlay " ++
                "after compiling, instead of to system directories, default=true"
            )
        ) |opt|
            opt
        else
            true;

    const allow_any_arch =
        if
        (
            b.option
            (
                bool,
                "any_arch",
                "Allow building for any architecture, supported explicitly or not, default=false"
            )
        ) |opt|
            opt
        else
            false;

    const include_debug_plugins =
        if
        (
            b.option
            (
                bool,
                "include_debug_plugins",
                "Include debug rendering plugins, default=true if optimize==Debug, else false"
            )
        ) |opt|
            opt
        else
            optimize == .Debug;

    const include_release_plugins =
        if
        (
            b.option
            (
                bool,
                "include_release_plugins",
                "Include release rendering plugins, default=true if optimize!=Debug, else false"
            )
        ) |opt|
            opt
        else
            optimize != .Debug;

    const use_system_vulkan = b.option
    (
        bool,
        "sysvk",
        "Use /usr/share/vulkan/registry/vk.xml to generate Vulkan bindings " ++
        "instead of downloading the latest bindings from the Vulkan SDK."
    ) orelse false;

    const english_only =
        if
        (
            b.option(bool, "english_only", "Build without support for non-english text and emojis, default=false")
        ) |opt|
            opt
        else
            false;

    const extract_debug_info =
        if
        (
            b.option
            (
                bool,
                "extract_debug_info",
                "Strip debug info from libraries into a secondary .debug file, " ++
                "default=true if optimize!=Debug, else false"
            )
        ) |opt|
            opt
        else
            optimize != .Debug;

    const build_args = .{ .target = target, .optimize = optimize, };

    const glslang_dep: ?*std.Build.Dependency =
        if (should_build_vulkan)
            b.dependency("glslang", .{ .optimize = .ReleaseFast, .target = native_target })
        else
            null;
    const iguana_tls_dep = b.dependency("iguanaTLS", build_args);
    const known_folders_dep = b.dependency("known_folders", .{});
    const libyaml_zig_dep = b.dependency("libyaml_zig", build_args);
    const uuid_dep = b.dependency("uuid", build_args);
    const vk_dep: ?*std.Build.Dependency =
        if (should_build_vulkan)
            b.dependency("vulkan_zig", .{})
        else
            null;
    const ws_dep = b.dependency("ws", build_args);
    const zgl_dep: ?*std.Build.Dependency =
        if (should_build_opengl)
            b.dependency("zgl", .{})
        else
            null;
    const ZigImGui_dep: ?*std.Build.Dependency =
        if (should_build_opengl or should_build_vulkan)
            b.dependency
            (
                "ZigImGui",
                .{
                    .target = target,
                    .optimize = optimize,
                    .enable_freetype = !english_only,
                    .enable_lunasvg = false,
                    .enable_opengl = should_build_opengl,
                },
            )
        else
            null;
    const zig_xml_dep = b.dependency("zig_xml", build_args);
    const zstbi_dep = b.dependency("zstbi", build_args);

    const simple = b.addModule
    (
        "simple",
        .{
            .source_file = .{ .path = "athena_overlay/deps/simple/src/main.zig" },
            .dependencies = &.{
                .{ .name = "iguanaTLS", .module = iguana_tls_dep.module("iguanaTLS") },
            },
        },
    );

    const disc = b.addModule
    (
        "discord_ws_conn",
        .{
            .source_file = .{ .path = "athena_overlay/deps/discord_ws_conn/src/main.zig" },
            .dependencies = &.{
                .{ .name = "simple", .module = simple, },
                .{ .name = "uuid", .module = uuid_dep.module("uuid") },
                .{ .name = "ws", .module = ws_dep.module("ws") },
            },
        }
    );

    b.installFile
    (
        "LICENSE",
        if (one_folder)
            "athena_overlay/docs/LICENSE"
        else
            "share/licenses/athena_overlay/LICENSE",
    );

    var plugins = std.ArrayList(std.Build.ModuleDependency).init(b.allocator);
    defer plugins.deinit();

    var dir =
        if (@hasDecl(std.fs, "openIterableDirAbsolute"))
            try std.fs.openIterableDirAbsolute(b.pathFromRoot("athena_overlay/plugins"), .{})
        else
            try std.fs.openDirAbsolute(b.pathFromRoot("athena_overlay/plugins"), .{ .iterate = true, });
    var iter = dir.iterateAssumeFirstIteration();
    while (try iter.next()) |entry|
    {
        if (std.mem.containsAtLeast(u8, entry.name, 1, "debug"))
        {
            if (!include_debug_plugins) continue;
        }
        else if (!include_release_plugins)
        {
            continue;
        }

        try plugins.append
        (
            .{
                .name = entry.name,
                .module = b.addModule
                (
                    entry.name,
                    .{
                        .source_file = .{
                            .path = b.pathFromRoot(b.pathJoin(&.{
                                "athena_overlay",
                                "plugins",
                                entry.name,
                                "src",
                                "main.zig",
                            })),
                        },
                        .dependencies = &.{
                            .{ .name = "discord_ws_conn", .module = disc, },
                            .{ .name = "known-folders", .module = known_folders_dep.module("known-folders") },
                            .{ .name = "libyaml_zig", .module = libyaml_zig_dep.module("libyaml_zig") },
                            .{ .name = "simple", .module = simple, },
                            .{ .name = "ws", .module = ws_dep.module("ws") },
                            .{ .name = "Zig-ImGui", .module = ZigImGui_dep.?.module("Zig-ImGui") },
                            .{ .name = "zig-xml", .module = zig_xml_dep.module("xml") },
                            .{ .name = "zstbi", .module = zstbi_dep.module("zstbi"), },
                        },
                    },
                ),
            }
        );

        if (std.mem.eql(u8, entry.name, "lurk_plugin"))
        {
            b.installDirectory
            (
                .{
                    .source_dir = .{ .path = "athena_overlay/deps/discord_ws_conn/third_party" },
                    .install_dir = .prefix,
                    .install_subdir =
                        if (one_folder)
                            "athena_overlay/docs/third_party"
                        else
                            "share/licenses/athena_overlay/third_party"
                }
            );
        }
    }

    const libathena_overlay = b.addStaticLibrary
    (
        .{
            .name = "athena_overlay",
            .root_source_file = .{ .path = "athena_overlay/src/main.zig" },
            .target = target,
            .optimize = optimize,
        }
    );
    libathena_overlay.force_pic = true;
    libathena_overlay.link_z_relro = true;
    if (target.getCpuArch() == .x86)
    {
        libathena_overlay.link_z_notext = true;
    }

    libathena_overlay.linkLibrary(libyaml_zig_dep.artifact("libyaml"));
    libathena_overlay.linkLibrary(ZigImGui_dep.?.artifact("cimgui"));
    libathena_overlay.linkLibrary(zstbi_dep.artifact("zstbi"));

    const overlay_options = b.addOptions();
    overlay_options.addOption(bool, "english_only", english_only);
    const athena_overlay_module = b.addModule
    (
        "athena_overlay",
        .{
            .source_file = .{ .path = "athena_overlay/src/main.zig" },
            .dependencies = &.{
                .{ .name = "known-folders", .module = known_folders_dep.module("known-folders") },
                .{ .name = "libyaml_zig", .module = libyaml_zig_dep.module("libyaml_zig") },
                .{ .name = "overlay_opts", .module = overlay_options.createModule() },
                .{ .name = "simple", .module = simple },
                .{ .name = "Zig-ImGui", .module = ZigImGui_dep.?.module("Zig-ImGui") },
                .{ .name = "zig-xml", .module = zig_xml_dep.module("xml") },
                .{ .name = "zstbi", .module = zstbi_dep.module("zstbi"), },
            }
        }
    );

    for (plugins.items) |plugin|
    {
        try athena_overlay_module.dependencies.put(b.dupe(plugin.name), plugin.module);
    }
    try write_plugins_to_file(b, &plugins, true);

    if (should_build_opengl)
    {
        build_opengl_layer
        (
            b,
            allow_any_arch,
            libathena_overlay,
            athena_overlay_module,
            simple,
            zgl_dep.?,
            one_folder or xdg_install,
            extract_debug_info,
            build_args,
        );
    }
    if (should_build_vulkan)
    {
        build_vulkan_layer
        (
            b,
            allow_any_arch,
            libathena_overlay,
            athena_overlay_module,
            simple,
            vk_dep.?,
            glslang_dep.?,
            use_system_vulkan,
            one_folder,
            xdg_install,
            extract_debug_info,
            build_args,
        );
    }
}
