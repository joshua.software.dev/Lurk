const builtin = @import("builtin");
const std = @import("std");

const imgui_ogl = @import("ogl/imgui_ogl.zig");
const layer_global_state = @import("layer_global_state.zig");

const athena_overlay = @import("athena_overlay");
const simple = @import("simple");
const zgl = @import("zgl");


pub fn process_is_blacklisted() !bool
{
    if (layer_global_state.ogl_vtable != null and layer_global_state.is_using_zink == null)
    {
        const renderer = zgl.getString(.renderer);
        if (renderer == null)
        {
            std.log.scoped(.ATHENAGL).warn("Failed to get opengl renderer name, continuing may fail.", .{});
            layer_global_state.is_using_zink = false;
        }
        else if (std.mem.indexOf(u8, renderer.?, "zink") != null)
        {
            // Prefer using the vulkan layer instead of gl when running under zink
            layer_global_state.is_using_zink = true;
        }
        else
        {
            layer_global_state.is_using_zink = false;
        }
    }

    if (layer_global_state.is_using_zink != null and layer_global_state.is_using_zink.?) return true;
    var proc_info: simple.BlacklistProcess = undefined;
    const result_was_cached = try simple.is_this_process_blacklisted(&proc_info);
    if (!result_was_cached)
    {
        std.log.scoped(.ATHENAGL).debug
        (
            "proc_name: {s} | is blacklisted: {} | is wine: {}",
            .{
                proc_info.name.constSlice(),
                proc_info.is_blacklisted,
                proc_info.is_wine,
            },
        );
    }
    return proc_info.is_blacklisted;
}

fn load_image_ogl
(
    user_id: []const u8,
    width: u32,
    height: u32,
    pixel_data: ?[]const u8,
)
anyerror!?athena_overlay.TextureID
{
    if (pixel_data == null)
    {
        return
            if (layer_global_state.gpu_image_map.?.map.getPtr(user_id)) |existing|
                existing.*
            else
                null;
    }

    const result = layer_global_state.gpu_image_map.?.map.getOrPutAssumeCapacity(user_id);
    if (!result.found_existing)
    {
        const texture = zgl.genTexture();
        zgl.bindTexture(texture, .@"2d");
        zgl.texParameter(.@"2d", .min_filter, .linear);
        zgl.texParameter(.@"2d", .mag_filter, .linear);
        zgl.texParameter(.@"2d", .wrap_s, .clamp_to_edge);
        zgl.texParameter(.@"2d", .wrap_t, .clamp_to_edge);
        zgl.textureImage2D(.@"2d", 0, .rgba, width, height, .rgba, .unsigned_byte, pixel_data.?[0..].ptr);
        result.value_ptr.* = @enumFromInt(@intFromEnum(texture));
    }

    return result.value_ptr.*;
}

pub fn create_imgui_context() !void
{
    if (!(try process_is_blacklisted()))
    {
        layer_global_state.imgui_ref_count = @truncate(layer_global_state.imgui_ref_count + 1);

        if (builtin.mode == .Debug) athena_overlay.set_allocator_for_imgui(null);

        const heap_allocator = layer_global_state.get_unbounded_heap_backed_allocator();
        const short_allocator = heap_allocator;
        const long_allocator = heap_allocator;
        const font_allocator = layer_global_state.get_font_allocator();

        _ = try athena_overlay.make_or_fetch_config(short_allocator);
        try athena_overlay.load_fonts(font_allocator);

        layer_global_state.gpu_image_map = try layer_global_state.GpuImageHashMap.init(long_allocator);
        try athena_overlay.init_plugins
        (
            false,
            .{
                .short_lived_allocator = short_allocator,
                .long_lived_allocator = long_allocator,
                .unbounded_heap_backed_allocator = heap_allocator,
                .load_image_hook_fn = &load_image_ogl,
            },
        );

        var viewport: [4]i32 = undefined;
        zgl.binding.getIntegerv(zgl.binding.VIEWPORT, viewport[0..]);

        athena_overlay.create_overlay_context();

        const old_ctx = athena_overlay.use_overlay_context()
            catch unreachable;
        defer athena_overlay.restore_old_context(old_ctx);
    }
}

pub fn do_imgui_swap() !void
{
    if (layer_global_state.imgui_ref_count < 1) return;

    const old_ctx = athena_overlay.use_overlay_context()
        catch return;
    defer athena_overlay.restore_old_context(old_ctx);

    athena_overlay.is_draw_ready()
        catch |err| switch (err)
        {
            error.FontNotLoaded => return,
            error.FontTextureRequiresReload =>
            {
                layer_global_state.free_font_allocator();
                _ = imgui_ogl.ImGui_ImplOpenGL3_Init(null);
                return;
            },
        };

    var viewport: [4]i32 = undefined;
    zgl.binding.getIntegerv(zgl.binding.VIEWPORT, viewport[0..]);

    imgui_ogl.ImGui_ImplOpenGL3_NewFrame();
    try athena_overlay.draw_frame(@intCast(viewport[2]), @intCast(viewport[3]));

    imgui_ogl.ImGui_ImplOpenGL3_RenderDrawData(athena_overlay.get_draw_data().?);
    layer_global_state.first_draw_complete = true;
}
