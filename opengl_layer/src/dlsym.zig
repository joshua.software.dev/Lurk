const std = @import("std");

const layer_global_state = @import("layer_global_state.zig");
const locate_dl = @import("elfhacks/locate_dlopen_dlsym.zig");


pub fn dlsym
(
    comptime get_hooked_function: ?fn ([]const u8) ??*anyopaque,
    handle: ?*anyopaque,
    name: ?[*:0]const u8,
)
!?*anyopaque
{
    if (layer_global_state.dl_symbol_vtable == null)
    {
        layer_global_state.dl_symbol_vtable = try locate_dl.get_dl_symbols_vtable();
    }

    const span_name = std.mem.span(name.?);
    std.log.scoped(.ATHENAGL).debug("dlsym: {s}", .{ span_name });
    if (get_hooked_function) |hook_lookup|
    {
        if (hook_lookup(span_name)) |hook| return hook;
    }

    return layer_global_state.dl_symbol_vtable.?.dlsym(handle, name);
}
