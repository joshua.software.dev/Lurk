const builtin = @import("builtin");
const std = @import("std");

const dlsym_impl = @import("dlsym.zig");
const glx_get_proc_address = @import("ogl/glx_get_proc_address.zig");
const glx_get_proc_address_arb = @import("ogl/glx_get_proc_address_arb.zig");
const glx_create_context = @import("ogl/glx_create_context.zig");
const glx_create_context_attribs = @import("ogl/glx_create_context_attribs.zig");
const glx_create_context_attribs_arb = @import("ogl/glx_create_context_attribs_arb.zig");
const glx_destroy_context = @import("ogl/glx_destroy_context.zig");
const glx_swap_buffers = @import("ogl/glx_swap_buffers.zig");
const glx_swap_buffers_msc_oml = @import("ogl/glx_swap_buffers_msc_oml.zig");


const stacktrace = switch (builtin.mode)
{
    .Debug => true,
    else => false,
};

const HookedFunctionMap = std.ComptimeStringMap
(
    ?*anyopaque,
    .{
        .{
            "glXGetProcAddressARB",
            @as(?*anyopaque, @ptrCast(@constCast(&glXGetProcAddressARB))),
        },
        .{
            "glXGetProcAddress",
            @as(?*anyopaque, @ptrCast(@constCast(&glXGetProcAddress))),
        },
        .{
            "glXCreateContext",
            @as(?*anyopaque, @ptrCast(@constCast(&glXCreateContext))),
        },
        .{
            "glXCreateContextAttribs",
            @as(?*anyopaque, @ptrCast(@constCast(&glXCreateContextAttribs))),
        },
        .{
            "glXCreateContextAttribsARB",
            @as(?*anyopaque, @ptrCast(@constCast(&glXCreateContextAttribsARB))),
        },
        .{
            "glXDestroyContext",
            @as(?*anyopaque, @ptrCast(@constCast(&glXDestroyContext))),
        },
        .{
            "glXSwapBuffers",
            @as(?*anyopaque, @ptrCast(@constCast(&glXSwapBuffers))),
        },
        .{
            "glXSwapBuffersMscOML",
            @as(?*anyopaque, @ptrCast(@constCast(&glXSwapBuffersMscOML))),
        },
    },
);

fn get_hooked_func(name: []const u8) ??*anyopaque
{
    return HookedFunctionMap.get(name);
}

export fn dlsym(handle: ?*anyopaque, name: ?[*:0]const u8) ?*anyopaque
{
    return dlsym_impl.dlsym(get_hooked_func, handle, name)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXGetProcAddress(procedure_name: ?[*:0]const u8) ?*anyopaque
{
    return glx_get_proc_address.glx_get_proc_address(get_hooked_func, procedure_name)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXGetProcAddressARB(procedure_name: ?[*:0]const u8) ?*anyopaque
{
    return glx_get_proc_address_arb.glx_get_proc_address_arb(get_hooked_func, procedure_name)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXCreateContext(dpy: ?*anyopaque, vis: ?*anyopaque, share_list: ?*anyopaque, arg_direct: i32) ?*anyopaque
{
    return glx_create_context.glx_create_context(dpy, vis, share_list, arg_direct)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXCreateContextAttribs
(
    dpy: ?*anyopaque,
    config: ?*anyopaque,
    share_context: ?*anyopaque,
    direct: i32,
    attrib_list: ?[*:0]const i32
)
?*anyopaque
{
    return glx_create_context_attribs.glx_create_context_attribs(dpy, config, share_context, direct, attrib_list)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXCreateContextAttribsARB
(
    dpy: ?*anyopaque,
    config: ?*anyopaque,
    share_context: ?*anyopaque,
    direct: i32,
    arg_attrib_list: ?[*:0]const i32
)
?*anyopaque
{
    return glx_create_context_attribs_arb.glx_create_context_attribs_arb
    (
        dpy,
        config,
        share_context,
        direct,
        arg_attrib_list,
    )
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXDestroyContext(dpy: ?*anyopaque, ctx: ?*anyopaque) void
{
    return glx_destroy_context.glx_destroy_context(dpy, ctx)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXSwapBuffers(dpy: ?*anyopaque, drawable: ?*anyopaque) void
{
    return glx_swap_buffers.glx_swap_buffers(dpy, drawable)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

export fn glXSwapBuffersMscOML
(
    dpy: ?*anyopaque,
    drawable: ?*anyopaque,
    target_msc: i64,
    divisor: i64,
    remainder: i64
)
i64
{
    return glx_swap_buffers_msc_oml.glx_swap_buffers_msc_oml(dpy, drawable, target_msc, divisor, remainder)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}
