const std = @import("std");

const dynamic_load_ogl = @import("dynamic_load_ogl.zig");
const overlay_swap = @import("../overlay_swap.zig");
const layer_global_state = @import("../layer_global_state.zig");


pub fn glx_swap_buffers(dpy: ?*anyopaque, drawable: ?*anyopaque) !void
{
    try dynamic_load_ogl.ensure_opengl_loaded(.standard);
    if (layer_global_state.ogl_vtable.?.GetCurrentContext() != null and layer_global_state.imgui_ref_count < 1)
    {
        try overlay_swap.create_imgui_context();
    }

    try overlay_swap.do_imgui_swap();
    layer_global_state.ogl_vtable.?.SwapBuffers(dpy, drawable);
}
