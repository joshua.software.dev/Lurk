const std = @import("std");

const dynamic_load_ogl = @import("dynamic_load_ogl.zig");
const imgui_ogl = @import("imgui_ogl.zig");
const layer_global_state = @import("../layer_global_state.zig");

const athena_overlay = @import("athena_overlay");


pub fn glx_destroy_context(dpy: ?*anyopaque, ctx: ?*anyopaque) !void
{
    try dynamic_load_ogl.ensure_opengl_loaded(.standard);
    layer_global_state.ogl_vtable.?.DestroyContext(dpy, ctx);

    const temp_ref_count: i64 = layer_global_state.imgui_ref_count - 1;
    // (abs(x) + x) / 2
    // branchless negative number to 0 formula
    layer_global_state.imgui_ref_count = @truncate
    (
        @divExact
        (
            // at least it would be, if I didn't need to do this compat step
            // for 0.11.0 and 0.12.0
            (if (temp_ref_count < 0) -temp_ref_count else temp_ref_count) + temp_ref_count,
            2
        )
    );

    if (layer_global_state.imgui_ref_count == 0)
    {
        if (layer_global_state.gpu_image_map != null)
        {
            layer_global_state.gpu_image_map.?.map.deinit();
            layer_global_state.gpu_image_map = null;
        }

        athena_overlay.deinit_plugins();

        if (layer_global_state.first_draw_complete)
        {
            blk: {
                const old_ctx = athena_overlay.use_overlay_context()
                    catch break :blk; // nothing to do
                defer athena_overlay.restore_old_context(old_ctx);

                imgui_ogl.ImGui_ImplOpenGL3_Shutdown();
                athena_overlay.destroy_overlay_context();
            }

            layer_global_state.free_short_lived_allocator();
            layer_global_state.free_long_lived_allocator();
            layer_global_state.free_unbounded_heap_backed_allocator();
        }
    }
}