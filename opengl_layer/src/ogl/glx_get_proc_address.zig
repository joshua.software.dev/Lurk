const std = @import("std");

const layer_global_state = @import("../layer_global_state.zig");
const dynamic_load_ogl = @import("dynamic_load_ogl.zig");


pub fn glx_get_proc_address
(
    comptime get_hooked_function: ?fn ([]const u8) ??*anyopaque,
    procedure_name: ?[*:0]const u8,
)
!?*anyopaque
{
    try dynamic_load_ogl.ensure_opengl_loaded(.standard);

    const span_name = std.mem.span(procedure_name.?);
    if (get_hooked_function) |get_hooked_func|
    {
        if (get_hooked_func(span_name)) |hook| return hook;
    }

    return @constCast(layer_global_state.ogl_vtable.?.GetProcAddress(procedure_name));
}
