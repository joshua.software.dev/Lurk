const std = @import("std");

const layer_global_state = @import("../layer_global_state.zig");
const locate_dl = @import("../elfhacks/locate_dlopen_dlsym.zig");
const ogl_types = @import("ogl_types.zig");

const zgl = @import("zgl");


const LoadHelper = struct
{
    loader_func: *ogl_types.OpenGlLoadFunction,

    fn load_bindings(self: @This(), name: [:0]const u8) ?zgl.binding.FunctionPointer
    {
        return self.loader_func(name);
    }
};

fn dynamic_load_opengl(loader_type: ogl_types.loader_types) !void
{
    @setCold(true);
    std.log.scoped(.ATHENAGL).debug("Something loaded our OpenGL functions", .{});

    if (layer_global_state.dl_symbol_vtable == null)
    {
        std.log.scoped(.ATHENAGL).warn("dlsym was not hooked, trying to hook from opengl loader", .{});
        layer_global_state.dl_symbol_vtable = try locate_dl.get_dl_symbols_vtable();
    }

    const opengl_lib = layer_global_state.dl_symbol_vtable.?.dlopen("libGL.so.1", std.os.system.RTLD.LAZY);
    if (opengl_lib == null) return error.OpenGlNotFound;

    const load_helper: LoadHelper = .{
        .loader_func = @as
        (
            ?*ogl_types.OpenGlLoadFunction,
            @ptrCast
            (
                @alignCast
                (
                    layer_global_state.dl_symbol_vtable.?.dlsym
                    (
                        opengl_lib.?,
                        switch (loader_type)
                        {
                            .standard => "glXGetProcAddress",
                            .arb => "glXGetProcAddressARB",
                        },
                    ),
                ),
            ),
        ).?,
    };

    try zgl.loadExtensions(load_helper, LoadHelper.load_bindings);

    layer_global_state.ogl_vtable = ogl_types.OpenGlVtable.init(load_helper.loader_func);

    std.log.scoped(.ATHENAGL).debug("Finished loading our OpenGL functions", .{});
}

pub fn ensure_opengl_loaded(loader_type: ogl_types.loader_types) !void
{
    if (layer_global_state.ogl_vtable != null) return;
    try dynamic_load_opengl(loader_type);
}
