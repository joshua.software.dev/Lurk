const std = @import("std");

const dynamic_load_ogl = @import("dynamic_load_ogl.zig");
const overlay_swap = @import("../overlay_swap.zig");
const layer_global_state = @import("../layer_global_state.zig");


pub fn glx_create_context(dpy: ?*anyopaque, vis: ?*anyopaque, share_list: ?*anyopaque, arg_direct: i32) !?*anyopaque
{
    try dynamic_load_ogl.ensure_opengl_loaded(.standard);

    const result = layer_global_state.ogl_vtable.?.CreateContext(dpy, vis, share_list, arg_direct);
    if (result != null and layer_global_state.imgui_ref_count < 1) try overlay_swap.create_imgui_context();
    return result;
}
