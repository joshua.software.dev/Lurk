const zgl = @import("zgl");


pub const loader_types = enum
{
    standard,
    arb,
};

pub const OpenGlLoadFunction = fn (name: ?[*:0]const u8) ?zgl.binding.FunctionPointer;
pub const Pfn_glXCreateContext = fn(?*anyopaque, ?*anyopaque, ?*anyopaque, i32) callconv(.C) ?*anyopaque;
pub const Pfn_glXCreateContextAttribs = fn(?*anyopaque, ?*anyopaque, ?*anyopaque, i32, ?[*:0]const i32) callconv(.C) ?*anyopaque;
pub const Pfn_glXCreateContextAttribsARB = fn(?*anyopaque, ?*anyopaque, ?*anyopaque, i32, ?[*:0]const i32) callconv(.C) ?*anyopaque;
pub const Pfn_glXDestroyContext = fn(?*anyopaque, ?*anyopaque) callconv(.C) void;
pub const Pfn_glXGetCurrentContext = fn() callconv(.C) ?*anyopaque;
pub const Pfn_glXSwapBuffers = fn(?*anyopaque, ?*anyopaque) callconv(.C) void;
pub const Pfn_glXSwapBuffersMscOML = fn(?*anyopaque, ?*anyopaque, i64, i64, i64) callconv(.C) i64;

pub const OpenGlVtable = struct
{
    CreateContext: *const Pfn_glXCreateContext,
    CreateContextAttribs: ?*const Pfn_glXCreateContextAttribs,
    CreateContextAttribsARB: ?*const Pfn_glXCreateContextAttribsARB,
    DestroyContext: *const Pfn_glXDestroyContext,
    GetCurrentContext: *const Pfn_glXGetCurrentContext,
    GetProcAddress: *const OpenGlLoadFunction,
    GetProcAddressARB: *const OpenGlLoadFunction,
    SwapBuffers: *const Pfn_glXSwapBuffers,
    SwapBuffersMscOML: ?*const Pfn_glXSwapBuffersMscOML,

    pub fn init(loader_func: *const OpenGlLoadFunction) @This()
    {
        return .{
            .CreateContext =
                @as(?*const Pfn_glXCreateContext, @ptrCast(loader_func("glXCreateContext"))).?,
            .CreateContextAttribs =
                @as(?*const Pfn_glXCreateContextAttribs, @ptrCast(loader_func("glXCreateContextAttribs"))),
            .CreateContextAttribsARB =
                @as(?*const Pfn_glXCreateContextAttribsARB, @ptrCast(loader_func("glXCreateContextAttribsARB"))),
            .DestroyContext =
                @as(?*const Pfn_glXDestroyContext, @ptrCast(loader_func("glXDestroyContext"))).?,
            .GetCurrentContext =
                @as(?*const Pfn_glXGetCurrentContext, @ptrCast(loader_func("glXGetCurrentContext"))).?,
            .GetProcAddress =
                @as(?*const OpenGlLoadFunction, @ptrCast(loader_func("glXGetProcAddress"))).?,
            .GetProcAddressARB =
                @as(?*const OpenGlLoadFunction, @ptrCast(loader_func("glXGetProcAddressARB"))).?,
            .SwapBuffers =
                @as(?*const Pfn_glXSwapBuffers, @ptrCast(loader_func("glXSwapBuffers"))).?,
            .SwapBuffersMscOML =
                @as(?*const Pfn_glXSwapBuffersMscOML, @ptrCast(loader_func("glXSwapBuffersMscOML"))),
        };
    }
};
