const std = @import("std");

const dynamic_load_ogl = @import("dynamic_load_ogl.zig");
const overlay_swap = @import("../overlay_swap.zig");
const layer_global_state = @import("../layer_global_state.zig");


pub fn glx_create_context_attribs_arb
(
    dpy: ?*anyopaque,
    config: ?*anyopaque,
    share_context: ?*anyopaque,
    direct: i32,
    arg_attrib_list: ?[*:0]const i32
)
!?*anyopaque
{
    try dynamic_load_ogl.ensure_opengl_loaded(.arb);

    const result = layer_global_state.ogl_vtable.?.CreateContextAttribsARB.?
    (
        dpy,
        config,
        share_context,
        direct,
        arg_attrib_list,
    );
    if (result != null and layer_global_state.imgui_ref_count < 1) try overlay_swap.create_imgui_context();
    return result;
}
