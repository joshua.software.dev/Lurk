const builtin = @import("builtin");


const Elf_Phdr = extern struct {
    p_type: u32,
    p_flags: u32,
    p_offset: usize,
    p_vaddr: usize,
    p_paddr: usize,
    p_filesz: u64,
    p_memsz: u64,
    p_align: u64,
};

const Elf_Dyn_Union = extern union {
    d_val: usize,
    d_ptr: usize,
};
const Elf_Dyn = extern struct {
    d_tag: usize,
    d_un: Elf_Dyn_Union,
};

const Elf32_Sym = extern struct {
    st_name: u32,
    st_value: u32,
    st_size: u32,
    st_info: u8,
    st_other: u8,
    st_shndx: u16,
};
const Elf64_Sym = extern struct {
    st_name: u32,
    st_info: u8,
    st_other: u8,
    st_shndx: u16,
    st_value: u64,
    st_size: u64,
};

pub const eh_obj_t = extern struct {
    name: [*:0]const u8,
    addr: usize,
    phdr: ?*const Elf_Phdr,
    phnum: u16,
    dynamic: ?*Elf_Dyn,
    symtab: if (builtin.target.ptrBitWidth() == 32) ?*Elf32_Sym else ?*Elf64_Sym,
    strtab: [*:0]const u8,
    hash: ?*u32,
    gnu_hash: ?*u32,
};
