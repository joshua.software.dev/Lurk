const std = @import("std");

const elf_types = @import("elf_types.zig");


extern fn eh_destroy_obj(obj: ?*elf_types.eh_obj_t) i32;
extern fn eh_find_obj(obj: ?*elf_types.eh_obj_t, soname: [*:0]const u8) i32;
extern fn eh_find_sym(obj: ?*elf_types.eh_obj_t, name: [*:0]const u8, to: ?*?*anyopaque) i32;

pub const ElfhacksObject = struct
{
    object: elf_types.eh_obj_t,

    pub fn deinit(self: *@This()) void
    {
        _ = eh_destroy_obj(&self.object);
    }

    pub fn find_symbol(self: *@This(), name: [:0]const u8, comptime T: type, ) !T
    {
        var symbol: ?*anyopaque = std.mem.zeroes(?*anyopaque);

        const return_code = eh_find_sym(&self.object, name.ptr, &symbol);
        switch (@as(std.os.system.E, @enumFromInt(return_code)))
        {
            .SUCCESS => return @as(T, @ptrCast(@alignCast(symbol))),
            .AGAIN => return error.SymbolNotFound,
            else => return error.UnexpectedError,
        }
    }
};

pub fn find_object(object_name: [:0]const u8) !ElfhacksObject
{
    var object: elf_types.eh_obj_t = std.mem.zeroes(elf_types.eh_obj_t);

    const return_code = eh_find_obj(&object, object_name.ptr);
    switch (@as(std.os.system.E, @enumFromInt(return_code)))
    {
        .SUCCESS => return .{ .object = object, },
        .AGAIN => return error.ObjectNotFound,
        .OPNOTSUPP => return error.NotSupported,
        else => return error.UnexpectedError,
    }
}
