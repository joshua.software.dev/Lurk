const std = @import("std");

const elfhacks = @import("elfhacks.zig");


pub const dlopen_func = fn (?[*:0]const u8, i32) align(8) callconv(.C) ?*anyopaque;
pub const dlsym_func = fn (?*anyopaque, ?[*:0]const u8) align(8) callconv(.C) ?*anyopaque;
pub const DlSymbolPtrs = struct
{
    dlopen: *dlopen_func,
    dlsym: *dlsym_func,
};

pub fn get_dl_symbols_vtable() !DlSymbolPtrs
{
    std.log.scoped(.ATHENAGL).debug("Hooking dlopen and dlsym...", .{});

    var dlopen_func_ptr: ?*dlopen_func = null;
    var dlsym_func_ptr: ?*dlsym_func = null;

    const dlls_to_try: []const [:0]const u8 = &.{ "*libdl.so*", "*libc.so*", "*libc.*.so*" };
    for (dlls_to_try) |dll|
    {
        dlopen_func_ptr = null;
        dlsym_func_ptr = null;

        var object = elfhacks.find_object(dll)
            catch |err| switch (err)
            {
                error.ObjectNotFound => continue,
                else => return err,
            };
        defer object.deinit();

        dlopen_func_ptr = object.find_symbol("dlopen", @TypeOf(dlopen_func_ptr))
            catch |err| switch (err)
            {
                error.SymbolNotFound => continue,
                else => return err,
            };
        dlsym_func_ptr = object.find_symbol("dlsym", @TypeOf(dlsym_func_ptr))
            catch |err| switch (err)
            {
                error.SymbolNotFound => continue,
                else => return err,
            };

        std.log.scoped(.ATHENAGL).debug("Found symbols in object: {s}", .{ object.object.name, });
        break; // exit early only when both symbols are found
    }

    if (dlopen_func_ptr == null) return error.FailedToFindDlopen;
    if (dlsym_func_ptr == null) return error.FailedToFindDlsym;

    return .{
        .dlopen = dlopen_func_ptr.?,
        .dlsym = dlsym_func_ptr.?,
    };
}
