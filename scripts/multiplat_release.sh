#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

optimize="safe"
if [ "$1" = "debug" ]; then optimize="debug";
elif [ "$1" = "fast" ]; then optimize="fast";
elif [ "$1" = "small" ]; then optimize="small";
fi

./arch_linux_build.sh "$optimize" true true
./xdg_linux_build.sh "$optimize" true
./macos_build.sh "$optimize"
./windows_build.sh "$optimize" true
