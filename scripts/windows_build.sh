#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH/.."

optimize="Debug"
if [ "$1" = "fast" ]; then optimize="ReleaseFast";
elif [ "$1" = "safe" ]; then optimize="ReleaseSafe";
elif [ "$1" = "small" ]; then optimize="ReleaseSmall";
fi

build_arm=0
if [ "$2" = "true" ]; then build_arm=1;
fi

ver=$(grep ".version" build.zig.zon | awk -F '"' '{print $2}')

rm -rf zig-out/
printf "zig build -Dcpu=baseline -Dtarget=x86-windows-gnu -Doptimize=$optimize -Dinclude_release_plugins=true\n"
zig build -Dcpu=baseline -Dtarget=x86-windows-gnu -Doptimize=$optimize -Dinclude_release_plugins=true
printf "tar -cf "athena_overlay-$ver-windows-i686.pkg.tar.gz" -C zig-out .\n\n"
tar -cf "athena_overlay-$ver-windows-i686.pkg.tar.gz" -C zig-out .

rm -rf zig-out/athena_overlay/bin
printf "zig build -Dcpu=baseline -Dtarget=x86_64-windows-gnu -Doptimize=$optimize -Dinclude_release_plugins=true\n"
zig build -Dcpu=baseline -Dtarget=x86_64-windows-gnu -Doptimize=$optimize -Dinclude_release_plugins=true
printf "tar -cf "athena_overlay-$ver-windows-x86_64.pkg.tar.gz" -C zig-out .\n\n"
tar -cf "athena_overlay-$ver-windows-x86_64.pkg.tar.gz" -C zig-out .

if [ "$build_arm" = 1 ]; then
    rm -rf zig-out/
    printf "zig build -Dcpu=baseline -Dtarget=aarch64-windows-gnu -Doptimize=$optimize -Dinclude_release_plugins=true\n"
    zig build -Dcpu=baseline -Dtarget=aarch64-windows-gnu -Doptimize=$optimize -Dinclude_release_plugins=true
    printf "tar -cf "athena_overlay-$ver-windows-aarch64.pkg.tar.gz" -C zig-out .\n\n"
    tar -cf "athena_overlay-$ver-windows-aarch64.pkg.tar.gz" -C zig-out .
fi
