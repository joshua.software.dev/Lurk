#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH/.."

abi_target="gnu.2.17"
optimize="Debug"
if [ "$1" = "fast" ]; then optimize="ReleaseFast";
elif [ "$1" = "safe" ]; then optimize="ReleaseSafe";
elif [ "$1" = "small" ]; then optimize="ReleaseSmall";
fi

build_arm=0
if [ "$2" = "true" ]; then build_arm=1;
fi

build_32primary=0
if [ "$3" = "true" ]; then build_32primary=1;
fi

ver=$(grep ".version" build.zig.zon | awk -F '"' '{print $2}')

rm -rf zig-out/
printf "zig build -Dcpu=baseline -Dtarget=\"x86_64-linux-$abi_target\" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true\n"
zig build -Dcpu=baseline -Dtarget="x86_64-linux-$abi_target" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true
fpm -f -t pacman -a x86_64 -p "athena_overlay-$ver-archlinux-x86_64.pkg.tar.zst" --version "$ver" lib=/usr/ bin=/usr/

rm -rf zig-out/
if [ "$build_32primary" = 1 ]; then
    printf "zig build -Dcpu=baseline -Dtarget=\"x86-linux-$abi_target\" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true\n"
    zig build -Dcpu=baseline -Dtarget="x86-linux-$abi_target" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true
    mv zig-out/lib32 zig-out/lib
    fpm -f -t pacman -a i686 -p "athena_overlay-$ver-archlinux-i686.pkg.tar.zst" --version "$ver" lib=/usr/ bin=/usr/
    mv zig-out/lib zig-out/lib32
else
    printf "zig build -Dcpu=baseline -Dtarget=\"x86-linux-$abi_target\" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true\n"
    zig build -Dcpu=baseline -Dtarget="x86-linux-$abi_target" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true
fi
rm -rf zig-out/bin/
rm -rf zig-out/share/licenses
fpm -f -t pacman -a x86_64 -p "athena_overlay-$ver-archlinux-x86_32.pkg.tar.zst" --version "$ver" --name lib32-athena_overlay lib32=/usr/

if [ "$build_arm" = 1 ]; then
    rm -rf zig-out/
    printf "zig build -Dcpu=baseline -Dtarget=\"aarch64-linux-$abi_target\" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true\n"
    zig build -Dcpu=baseline -Dtarget="aarch64-linux-$abi_target" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true
    fpm -f -t pacman -a aarch64 -p "athena_overlay-$ver-archlinux-aarch64.pkg.tar.zst" --version "$ver" lib=/usr/ bin=/usr/

    rm -rf zig-out/
    if [ "$build_32primary" = 1 ]; then
        printf "zig build -Dcpu=baseline -Dtarget=\"arm-linux-$abi_target\" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true\n"
        zig build -Dcpu=baseline -Dtarget="arm-linux-$abi_target" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true
        mv zig-out/lib32 zig-out/lib
        fpm -f -t pacman -a armv7hf -p "athena_overlay-$ver-archlinux-armv7hf.pkg.tar.zst" --version "$ver" lib=/usr/ bin=/usr/
        mv zig-out/lib zig-out/lib32
    else
        printf "zig build -Dcpu=baseline -Dtarget=\"arm-linux-$abi_target\" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true\n"
        zig build -Dcpu=baseline -Dtarget="arm-linux-$abi_target" -Doptimize=$optimize -Done_folder=false -Dextract_debug_info=false -Dinclude_release_plugins=true
    fi
    rm -rf zig-out/bin/
    rm -rf zig-out/share/licenses
    fpm -f -t pacman -a aarch64 -p "athena_overlay-$ver-archlinux-aarch64_armv7hf.pkg.tar.zst" --version "$ver" --name lib32-athena_overlay lib32=/usr/
fi
