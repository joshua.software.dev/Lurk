#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH/.."

optimize="Debug"
if [ "$1" = "fast" ]; then optimize="ReleaseFast";
elif [ "$1" = "safe" ]; then optimize="ReleaseSafe";
elif [ "$1" = "small" ]; then optimize="ReleaseSmall";
fi

ver=$(grep ".version" build.zig.zon | awk -F '"' '{print $2}')

rm -rf zig-out/
printf "zig build -Dcpu=baseline -Dtarget=x86_64-macos-none -Doptimize=$optimize -Dinclude_release_plugins=true\n"
zig build -Dcpu=baseline -Dtarget=x86_64-macos-none -Doptimize=$optimize -Dinclude_release_plugins=true
printf "tar -cf "athena_overlay-$ver-macos-x86_64.tar.gz" -C zig-out .\n\n"
tar -cf "athena_overlay-$ver-macos-x86_64.tar.gz" -C zig-out .

printf "zig build -Dcpu=baseline -Dtarget=aarch64-macos-none -Doptimize=$optimize -Dinclude_release_plugins=true\n"
zig build -Dcpu=baseline -Dtarget=aarch64-macos-none -Doptimize=$optimize -Dinclude_release_plugins=true
printf "tar -cf "athena_overlay-$ver-macos-aarch64.tar.gz" -C zig-out .\n\n"
tar -cf "athena_overlay-$ver-macos-aarch64.tar.gz" -C zig-out .
