#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

declare -a projects=("athena_overlay" "opengl_layer" "vulkan_layer" "vulkan_layer/..")

for dir in "${projects[@]}"
do
    rm -rf "$dir/zig-cache"
    rm -rf "$dir/zig-out"
done

# rm -f vulkan_layer/src/shaders/*.spv
rm -rf athena_overlay-*.zst athena_overlay-*.gz
rm -rf "vulkan_layer/../logs/"*
