#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

printf "zig\n" && \
env \
    ENABLE_ATHENA_OVERLAY=1 \
    VK_ADD_LAYER_PATH="$(realpath ../zig-out/athena_overlay/manifests)" \
    VK_LOADER_LAYERS_ENABLE="ATHENA_OVERLAY_VULKAN_*" \
    VK_LOADER_DEBUG=all \
    vkcube
