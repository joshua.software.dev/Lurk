const std = @import("std");

const info = @import("info.zig");
const layer_global_state = @import("layer_global_state.zig");
const vklct = @import("vk_layer_custom_types.zig");

const vk = @import("vk");


pub fn destroy_avatar_image
(
    device: vk.Device,
    device_wrapper: vklct.LayerDeviceWrapper,
    descriptor_pool: vk.DescriptorPool,
    avatar_image: *const vklct.AvatarImage,
)
!void
{
    device_wrapper.freeMemory(device, avatar_image.upload_buffer_memory, null);
    device_wrapper.destroyBuffer(device, avatar_image.upload_buffer, null);
    device_wrapper.destroySampler(device, avatar_image.image_sampler, null);
    device_wrapper.destroyImageView(device, avatar_image.image_view, null);
    device_wrapper.destroyImage(device, avatar_image.image, null);
    device_wrapper.freeMemory(device, avatar_image.image_memory, null);
    try device_wrapper.freeDescriptorSets
    (
        device,
        descriptor_pool,
        1,
        &@as([1]vk.DescriptorSet, .{ avatar_image.descriptor_set, }),
    );
}

pub fn destroy_swapchain_internal
(
    device: vk.Device,
    device_wrapper: vklct.LayerDeviceWrapper,
    swapchain_data: *vklct.SwapchainData,
    device_data: *vklct.DeviceData,
)
!void
{
    if (device_data.most_recent_draw_data) |draw_data|
    {
        device_wrapper.destroySemaphore(device, draw_data.cross_engine_semaphore, null);
        device_wrapper.destroySemaphore(device, draw_data.semaphore, null);
        device_wrapper.destroyFence(device, draw_data.fence, null);

        if (draw_data.vertex_buffer) |vtx_buf|
        {
            device_wrapper.destroyBuffer(device, vtx_buf, null);
        }

        if (draw_data.index_buffer) |idx_buf|
        {
            device_wrapper.destroyBuffer(device, idx_buf, null);
        }

        if (draw_data.vertex_buffer_mem) |vtx_mem|
        {
            device_wrapper.freeMemory(device, vtx_mem, null);
        }

        if (draw_data.index_buffer_mem) |idx_mem|
        {
            device_wrapper.freeMemory(device, idx_mem, null);
        }
    }
    device_data.most_recent_draw_data = null;

    for (swapchain_data.image_views.slice()) |iv|
    {
        device_wrapper.destroyImageView(device, iv, null);
    }
    swapchain_data.image_views.len = 0;

    for (swapchain_data.framebuffers.slice()) |fb|
    {
        device_wrapper.destroyFramebuffer(device, fb, null);
    }
    swapchain_data.framebuffers.len = 0;

    if (swapchain_data.avatar_images) |*avatar_images|
    {
        for (avatar_images.constSlice()) |avatar_image|
        {
            try destroy_avatar_image(device, device_wrapper, swapchain_data.descriptor_pool.?, &avatar_image);
        }
        avatar_images.len = 0;
    }

    if (swapchain_data.render_pass) |render_pass|
    {
        device_wrapper.destroyRenderPass(device, render_pass, null);
    }
    swapchain_data.render_pass = null;

    if (swapchain_data.command_pool) |command_pool|
    {
        device_wrapper.destroyCommandPool(device, command_pool, null);
    }
    swapchain_data.command_pool = null;

    if (swapchain_data.pipeline) |pipeline|
    {
        device_wrapper.destroyPipeline(device, pipeline, null);
    }
    swapchain_data.pipeline = null;

    if (swapchain_data.pipeline_layout) |pipeline_layout|
    {
        device_wrapper.destroyPipelineLayout(device, pipeline_layout, null);
    }
    swapchain_data.pipeline_layout = null;

    if (swapchain_data.descriptor_pool) |descriptor_pool|
    {
        device_wrapper.destroyDescriptorPool(device, descriptor_pool, null);
    }
    swapchain_data.descriptor_pool = null;

    if (swapchain_data.descriptor_layout) |descriptor_layout|
    {
        device_wrapper.destroyDescriptorSetLayout(device, descriptor_layout, null);
    }
    swapchain_data.descriptor_layout = null;

    if (swapchain_data.font_sampler) |font_sampler|
    {
        device_wrapper.destroySampler(device, font_sampler, null);
    }
    swapchain_data.font_sampler = null;

    if (swapchain_data.font_image_view) |font_image_view|
    {
        device_wrapper.destroyImageView(device, font_image_view, null);
    }
    swapchain_data.font_image_view = null;

    if (swapchain_data.font_image) |font_image|
    {
        device_wrapper.destroyImage(device, font_image, null);
    }
    swapchain_data.font_image = null;

    if (swapchain_data.font_mem) |font_mem|
    {
        device_wrapper.freeMemory(device, font_mem, null);
    }
    swapchain_data.font_mem = null;

    if (swapchain_data.upload_font_buffer) |upload_font_buffer|
    {
        device_wrapper.destroyBuffer(device, upload_font_buffer, null);
    }
    swapchain_data.upload_font_buffer = null;

    if (swapchain_data.upload_font_buffer_mem) |upload_font_buffer_mem|
    {
        device_wrapper.freeMemory(device, upload_font_buffer_mem, null);
    }
    swapchain_data.upload_font_buffer_mem = null;
}

pub fn DestroySwapchainKHR
(
    device: vk.Device,
    swapchain: vk.SwapchainKHR,
    p_allocator: ?*const vk.AllocationCallbacks,
)
!void
{
    const lock_success = layer_global_state.wrappers_global_lock.tryLock();
    if (!lock_success)
    {
        std.log.scoped(.ATHENAVK).err("Failed to get global vulkan vtable lock", .{});
    }
    defer if (lock_success) layer_global_state.wrappers_global_lock.unlock();

    std.log.scoped(.ATHENAVK).debug("Destroy Swapchain: " ++ info.LAYER_NAME, .{});

    if (swapchain == .null_handle)
    {
        var device_data: *vklct.DeviceData = layer_global_state.device_backing.?.map.getPtr(device).?;
        device_data.device_wrapper.destroySwapchainKHR(device, swapchain, p_allocator);
        return;
    }

    var swapchain_data = layer_global_state.swapchain_backing.?.map.fetchOrderedRemove(swapchain).?;
    std.log.scoped(.ATHENAVK).debug
    (
        "Destroyed Swapchain ID: {d}|{d}",
        .{
            swapchain_data.value.swapchain_id,
            swapchain_data.value.swapchain.?,
        },
    );
    var device_data = layer_global_state.device_backing.?.map.getPtr(swapchain_data.value.device).?;
    try destroy_swapchain_internal
    (
        device,
        device_data.device_wrapper,
        &swapchain_data.value,
        device_data,
    );
    device_data.device_wrapper.destroySwapchainKHR(device, swapchain, p_allocator);
}
