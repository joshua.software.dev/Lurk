const vk = @import("vk");

const layer_global_state = @import("layer_global_state.zig");
const vklct = @import("vk_layer_custom_types.zig");


pub fn vk_memory_type(device: vk.Device, properties: vk.MemoryPropertyFlags, type_bits: u32) !u32
{
    const device_data = layer_global_state.device_backing.?.map.getPtr(device).?;
    const instance_data = layer_global_state.instance_backing.?.map.getPtr(device_data.instance).?;
    const physical_mem_props = instance_data.instance_wrapper.getPhysicalDeviceMemoryProperties
    (
        device_data.physical_device
    );

    var i: u32 = 0;
    var supported_mem_type: u32 = 1;
    while (i < physical_mem_props.memory_type_count) : ({i += 1; supported_mem_type += supported_mem_type;})
    {
        if
        (
            physical_mem_props.memory_types[i].property_flags.contains(properties)
            and ((type_bits & supported_mem_type) > 0)
        )
        {
            return i;
        }
    }

    return error.UnknownMemoryType;
}
