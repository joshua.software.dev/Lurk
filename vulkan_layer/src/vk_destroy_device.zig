const std = @import("std");

const info = @import("info.zig");
const layer_global_state = @import("layer_global_state.zig");
const vklct = @import("vk_layer_custom_types.zig");

const vk = @import("vk");


pub fn DestroyDevice
(
    device: vk.Device,
    p_allocator: ?*const vk.AllocationCallbacks
)
!void
{
    std.log.scoped(.ATHENAVK).debug("Destroy Device: " ++ info.LAYER_NAME, .{});

    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    const device_data = layer_global_state.device_backing.?.map.fetchOrderedRemove(device).?;
    device_data.value.device_wrapper.destroyDevice(device, p_allocator);
    std.log.scoped(.ATHENAVK).debug
    (
        "Destroyed Device ID: {d}|{d}",
        .{
            device_data.value.device_id,
            device_data.value.device,
        }
    );
}
