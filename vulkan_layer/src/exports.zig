const builtin = @import("builtin");
const std = @import("std");

const create_instance = @import("vk_create_instance.zig");
const destroy_instance = @import("vk_destroy_instance.zig");
const create_device = @import("vk_create_device.zig");
const destroy_device = @import("vk_destroy_device.zig");
const create_swapchain_khr = @import("vk_create_swapchain_khr.zig");
const destroy_swapchain_khr = @import("vk_destroy_swapchain_khr.zig");
const queue_present_khr = @import("vk_queue_present_khr.zig");
const get_device_proc_addr = @import("vk_get_device_proc_addr.zig");
const get_instance_proc_addr = @import("vk_get_instance_proc_addr.zig");

const vk = @import("vk");


const stacktrace = switch (builtin.mode)
{
    .Debug => true,
    else => false,
};

// Create compile time hashmaps that specify which functions this layer intends
// to hook into
const BlacklistRegistionFunctionMap = std.ComptimeStringMap
(
    vk.PfnVoidFunction,
    .{
        .{
            vk.BaseCommandFlags.cmdName(.createInstance),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_CreateInstance)))
        },
        .{
            vk.InstanceCommandFlags.cmdName(.destroyInstance),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_DestroyInstance)))
        },
        .{
            vk.InstanceCommandFlags.cmdName(.createDevice),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_CreateDevice)))
        },
        .{
            vk.DeviceCommandFlags.cmdName(.destroyDevice),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_DestroyDevice)))
        },
    },
);

const DeviceRegistionFunctionMap = std.ComptimeStringMap
(
    vk.PfnVoidFunction,
    .{
        .{
            vk.InstanceCommandFlags.cmdName(.getDeviceProcAddr),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_GetDeviceProcAddr)))
        },
        .{
            vk.InstanceCommandFlags.cmdName(.createDevice),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_CreateDevice)))
        },
        .{
            vk.DeviceCommandFlags.cmdName(.destroyDevice),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_DestroyDevice)))
        },
        .{
            vk.DeviceCommandFlags.cmdName(.createSwapchainKHR),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_CreateSwapchainKHR)))
        },
        .{
            vk.DeviceCommandFlags.cmdName(.destroySwapchainKHR),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_DestroySwapchainKHR)))
        },
        .{
            vk.DeviceCommandFlags.cmdName(.queuePresentKHR),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_QueuePresentKHR)))
        },
    },
);

const InstanceRegistionFunctionMap = std.ComptimeStringMap
(
    vk.PfnVoidFunction,
    .{
        .{
            vk.BaseCommandFlags.cmdName(.getInstanceProcAddr),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_GetInstanceProcAddr)))
        },
        .{
            vk.BaseCommandFlags.cmdName(.createInstance),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_CreateInstance)))
        },
        .{
            vk.InstanceCommandFlags.cmdName(.destroyInstance),
            @as(vk.PfnVoidFunction, @ptrCast(@alignCast(&AthenaOverlayVulkan_DestroyInstance)))
        },
    },
);

fn get_blacklist_func(name: []const u8) ?vk.PfnVoidFunction
{
    return BlacklistRegistionFunctionMap.get(name);
}

fn get_device_func(name: []const u8) ?vk.PfnVoidFunction
{
    return DeviceRegistionFunctionMap.get(name);
}

fn get_instance_func(name: []const u8) ?vk.PfnVoidFunction
{
    return InstanceRegistionFunctionMap.get(name);
}

pub export fn AthenaOverlayVulkan_CreateInstance
(
    p_create_info: *const vk.InstanceCreateInfo,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_instance: *vk.Instance,
)
callconv(vk.vulkan_call_conv) vk.Result
{
    return create_instance.CreateInstance(p_create_info, p_allocator, p_instance)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_DestroyInstance
(
    instance: vk.Instance,
    p_allocator: ?*const vk.AllocationCallbacks,
)
callconv(vk.vulkan_call_conv) void
{
    return destroy_instance.DestroyInstance(instance, p_allocator)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_CreateDevice
(
    physical_device: vk.PhysicalDevice,
    p_create_info: *const vk.DeviceCreateInfo,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_device: *vk.Device,
)
callconv(vk.vulkan_call_conv) vk.Result
{
    return create_device.CreateDevice(physical_device, p_create_info, p_allocator, p_device)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_DestroyDevice
(
    device: vk.Device,
    p_allocator: ?*const vk.AllocationCallbacks,
)
callconv(vk.vulkan_call_conv) void
{
    return destroy_device.DestroyDevice(device, p_allocator)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_CreateSwapchainKHR
(
    device: vk.Device,
    p_create_info: *const vk.SwapchainCreateInfoKHR,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_swapchain: *vk.SwapchainKHR,
)
callconv(vk.vulkan_call_conv) vk.Result
{
    return create_swapchain_khr.CreateSwapchainKHR(device, p_create_info, p_allocator, p_swapchain)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_DestroySwapchainKHR
(
    device: vk.Device,
    swapchain: vk.SwapchainKHR,
    p_allocator: ?*const vk.AllocationCallbacks,
)
callconv(vk.vulkan_call_conv) void
{
    return destroy_swapchain_khr.DestroySwapchainKHR(device, swapchain, p_allocator)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_QueuePresentKHR
(
    queue: vk.Queue,
    p_present_info: *const vk.PresentInfoKHR,
)
callconv(vk.vulkan_call_conv) vk.Result
{
    return queue_present_khr.QueuePresentKHR(queue, p_present_info)
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_GetDeviceProcAddr
(
    device: vk.Device,
    p_name: [*:0]const u8,
)
callconv(vk.vulkan_call_conv) vk.PfnVoidFunction
{
    return get_device_proc_addr.GetDeviceProcAddr
    (
        get_blacklist_func,
        get_device_func,
        device,
        p_name,
    )
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}

pub export fn AthenaOverlayVulkan_GetInstanceProcAddr
(
    instance: vk.Instance,
    p_name: [*:0]const u8,
)
callconv(vk.vulkan_call_conv) vk.PfnVoidFunction
{
    return get_instance_proc_addr.GetInstanceProcAddr
    (
        get_blacklist_func,
        get_device_func,
        get_instance_func,
        instance,
        p_name,
    )
        catch |err|
        {
            std.log.err("{s}", .{ @errorName(err) });
            if (stacktrace)
            {
                if (@errorReturnTrace()) |trace|
                {
                    std.debug.dumpStackTrace(trace.*);
                }
            }

            @panic("Unexpected error, aborting...");
        };
}
