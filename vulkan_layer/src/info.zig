const builtin = @import("builtin");

const build_options = @import("vulkan_layer_build_options");


pub const LAYER_NAME = "ATHENA_OVERLAY_VULKAN_" ++ switch (builtin.cpu.arch)
{
    .x86 => "x86_32",
    .x86_64 => "x86_64",
    .arm, .armeb => "arm32",
    .aarch64, .aarch64_be, .aarch64_32 => "arm64",
    else => if (!build_options.allow_any_arch) @compileError("Unsupported CPU architecture"),
};
pub const LAYER_DESC =
    "A plugin based OpenGL/Vulkan ImGui overlay written in Zig! " ++
    "https://github.com/joshua-software-dev/AthenaOverlay";
