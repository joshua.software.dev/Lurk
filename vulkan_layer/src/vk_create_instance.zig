const builtin = @import("builtin");
const std = @import("std");

const info = @import("info.zig");
const layer_global_state = @import("layer_global_state.zig");
const vkl = @import("vulkan_layer_api_stubs.zig");
const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const vk = @import("vk");


fn search_instance_create_info(p_create_info: *const vk.InstanceCreateInfo, func_type: c_int) !*vkl.LayerInstanceCreateInfo
{
    var instance_create_info: ?*vkl.LayerInstanceCreateInfo = @ptrCast(@alignCast(@constCast(p_create_info.p_next)));
    while (instance_create_info) |ici|
    {
        if
        (
            ici.s_type == .loader_instance_create_info and
            ici.function == func_type
        )
        {
            return ici;
        }

        instance_create_info = @ptrCast(@alignCast(@constCast(ici.p_next)));
    }

    return error.MissingVulkanInstanceCreateInfo;
}

fn create_instance_data
(
    p_create_info: *const vk.InstanceCreateInfo,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_instance: *vk.Instance,
)
!vklct.InstanceData
{
    var layer_create_info = try search_instance_create_info(p_create_info, vkl.LayerFunction_LAYER_LINK_INFO);
    if (layer_create_info.u.p_layer_info == null) return error.InvalidVulkanInstanceLayerInfo;

    const get_inst_proc_addr_func_ptr = layer_create_info.u.p_layer_info.?.pfn_next_get_instance_proc_addr;

    const create_instance_func_ptr = @as
    (
        ?vk.PfnCreateInstance,
        @constCast
        (
            @ptrCast
            (
                @alignCast
                (
                    get_inst_proc_addr_func_ptr
                    (
                        .null_handle,
                        vk.BaseCommandFlags.cmdName(.createInstance),
                    ),
                ),
            ),
        ),
    )
        orelse return error.InitializationFailed;

    // move chain on for next layer
    layer_create_info.u.p_layer_info = layer_create_info.u.p_layer_info.?.p_next;

    // Create instance before loading instance function table
    const create_instance_result = create_instance_func_ptr
    (
        p_create_info,
        p_allocator,
        p_instance,
    );
    switch (create_instance_result) {
        .success => {},
        .error_out_of_host_memory => return error.OutOfHostMemory,
        .error_out_of_device_memory => return error.OutOfDeviceMemory,
        .error_initialization_failed => return error.InitializationFailed,
        .error_layer_not_present => return error.LayerNotPresent,
        .error_extension_not_present => return error.ExtensionNotPresent,
        .error_incompatible_driver => return error.IncompatibleDriver,
        else => return error.Unknown,
    }

    const instance = p_instance.*;
    const instance_wrapper = try vklct.LayerInstanceWrapper.load(instance, get_inst_proc_addr_func_ptr);

    std.log.scoped(.ATHENAVK).debug("Current Instance Ref: {d}", .{ layer_global_state.instance_ref_count });
    layer_global_state.instance_ref_count += 1;
    std.log.scoped(.ATHENAVK).debug("New Instance Ref: {d}|{d}", .{ layer_global_state.instance_ref_count, instance });
    return .{
        .instance_id = layer_global_state.instance_ref_count,
        .instance = instance,
        .get_inst_proc_addr_func_ptr = get_inst_proc_addr_func_ptr,
        .instance_wrapper = instance_wrapper,
        .physical_devices = try vklct.PhysicalDeviceBacking.init(0),
    };
}

fn map_physical_devices_to_instance(instance_data: *vklct.InstanceData) !void
{
    var phy_device_count: u32 = 0;
    _ = try instance_data.instance_wrapper.enumeratePhysicalDevices(instance_data.instance, &phy_device_count, null);

    std.log.scoped(.ATHENAVK).debug("PhysicalDevice Count: {d}", .{ phy_device_count });
    try instance_data.physical_devices.resize(phy_device_count);

    _ = try instance_data.instance_wrapper.enumeratePhysicalDevices
    (
        instance_data.instance,
        &phy_device_count,
        instance_data.physical_devices.slice().ptr,
    );
}

pub fn CreateInstance
(
    p_create_info: *const vk.InstanceCreateInfo,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_instance: *vk.Instance
)
!vk.Result
{
    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    if (layer_global_state.instance_backing == null)
    {
        if (builtin.mode == .Debug) athena_overlay.set_allocator_for_imgui(null);

        const heap_allocator = layer_global_state.get_unbounded_heap_backed_allocator();
        const short_allocator = heap_allocator;
        // const long_allocator = layer_global_state.get_long_lived_allocator();
        _ = layer_global_state.get_font_allocator();

        _ = try athena_overlay.make_or_fetch_config(short_allocator);

        layer_global_state.instance_backing = undefined;
        try layer_global_state.instance_backing.?.init();
        layer_global_state.device_backing = undefined;
        try layer_global_state.device_backing.?.init();
        layer_global_state.swapchain_backing = undefined;
        try layer_global_state.swapchain_backing.?.init();
    }

    std.log.scoped(.ATHENAVK).debug("Create Instance: {d}" ++ info.LAYER_NAME, .{ p_instance.* });
    const instance_data = try create_instance_data(p_create_info, p_allocator, p_instance);

    const backing = layer_global_state.instance_backing.?.map.getOrPutAssumeCapacity(instance_data.instance);
    if (backing.found_existing) return error.DuplicateVulkanInstance;
    backing.value_ptr.* = instance_data;

    std.log.scoped(.ATHENAVK).debug("Mapping \"PhysicalDevice\"s to Instance...", .{});
    try map_physical_devices_to_instance(backing.value_ptr);
    return .success;
}
