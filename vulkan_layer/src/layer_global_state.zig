const builtin = @import("builtin");
const std = @import("std");

const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const simple = @import("simple");
const vk = @import("vk");


const MemoryLimitedGPA = std.heap.GeneralPurposeAllocator
(
    .{
        .enable_memory_limit = true,
        .never_unmap = false,
        .retain_metadata = false,
        .verbose_log = false,
    }
);
const UnboundedGPA = std.heap.GeneralPurposeAllocator
(
    .{
        .enable_memory_limit = false,
        .never_unmap = false,
        .retain_metadata = false,
        .verbose_log = false,
    }
);

const MAX_MEMORY_ALLOCATION_FONTS = 1024 * 512; // bytes
const MAX_MEMORY_ALLOCATION_LONG_LIVED = ((1024 * 1024 * 8) * 128) + 4096; // bytes
const MAX_MEMORY_ALLOCATION_SHORT_LIVED = 1024 * 128; // bytes

pub var active_swapchain: ?*vklct.SwapchainData = null;

// single global lock, for simplicity
pub var wrappers_global_lock: std.Thread.Mutex = .{};
pub var instance_backing: ?vklct.InstanceBackingHashMap = null;
pub var instance_ref_count: u32 = 0;
pub var device_backing: ?vklct.DeviceBackingHashMap = null;
pub var device_ref_count: u32 = 0;
pub var swapchain_backing: ?vklct.SwapchainBackingHashMap = null;
pub var swapchain_ref_count: u32 = 0;

var short_lived_gpa: ?MemoryLimitedGPA = null;
var short_lived_heap_buf: ?[]u8 = null;
var short_lived_heap_fba: ?std.heap.FixedBufferAllocator = null;
var long_lived_gpa: ?MemoryLimitedGPA = null;
var unbounded_heap_backed_gpa: ?UnboundedGPA = null;
var font_gpa: ?MemoryLimitedGPA = null;

pub fn get_short_lived_allocator() !std.mem.Allocator
{
    switch (builtin.mode)
    {
        .Debug =>
        {
            if (short_lived_gpa == null)
            {
                short_lived_gpa = .{};
                short_lived_gpa.?.setRequestedMemoryLimit(MAX_MEMORY_ALLOCATION_SHORT_LIVED);
            }

            return short_lived_gpa.?.allocator();
        },
        else =>
        {
            if (short_lived_heap_fba == null)
            {
                short_lived_heap_buf = try std.heap.c_allocator.alloc(u8, MAX_MEMORY_ALLOCATION_SHORT_LIVED);
                short_lived_heap_fba = std.heap.FixedBufferAllocator.init(short_lived_heap_buf.?);
            }

            return short_lived_heap_fba.?.allocator();
        },
    }
}

pub fn free_short_lived_allocator() void
{
    switch (builtin.mode)
    {
        .Debug =>
        {
            if (short_lived_gpa != null)
            {
                _ = short_lived_gpa.?.deinit();
                short_lived_gpa = null;
            }
        },
        else =>
        {
            if (short_lived_heap_buf != null)
            {
                std.heap.c_allocator.free(short_lived_heap_buf.?);
                short_lived_heap_buf = null;
                short_lived_heap_fba = null;
            }
        },
    }
}

pub fn get_long_lived_allocator() std.mem.Allocator
{
    if (long_lived_gpa == null)
    {
        long_lived_gpa = .{};
        long_lived_gpa.?.setRequestedMemoryLimit(MAX_MEMORY_ALLOCATION_LONG_LIVED);
    }

    return long_lived_gpa.?.allocator();
}

pub fn free_long_lived_allocator() void
{
    if (long_lived_gpa != null)
    {
        _ = long_lived_gpa.?.deinit();
        long_lived_gpa = null;
    }
}

pub fn get_unbounded_heap_backed_allocator() std.mem.Allocator
{
    if (unbounded_heap_backed_gpa == null)
    {
        unbounded_heap_backed_gpa = .{};
    }

    return unbounded_heap_backed_gpa.?.allocator();
}

pub fn free_unbounded_heap_backed_allocator() void
{
    if (unbounded_heap_backed_gpa != null)
    {
        _ = unbounded_heap_backed_gpa.?.deinit();
        unbounded_heap_backed_gpa = null;
    }
}

pub fn get_font_allocator() std.mem.Allocator
{
    if (font_gpa == null)
    {
        font_gpa = .{};
        font_gpa.?.setRequestedMemoryLimit(MAX_MEMORY_ALLOCATION_FONTS);
    }

    return font_gpa.?.allocator();
}

pub fn free_font_allocator() void
{
    if (font_gpa != null)
    {
        _ = font_gpa.?.deinit();
        font_gpa = null;
    }
}
