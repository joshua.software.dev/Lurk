const std = @import("std");

const info = @import("info.zig");
const layer_global_state = @import("layer_global_state.zig");
const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const vk = @import("vk");


pub fn DestroyInstance
(
    instance: vk.Instance,
    p_allocator: ?*const vk.AllocationCallbacks
)
!void
{
    std.log.scoped(.ATHENAVK).debug("Destroy Instance: " ++ info.LAYER_NAME, .{});

    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    var instance_data = layer_global_state.instance_backing.?.map.fetchOrderedRemove(instance).?;
    instance_data.value.instance_wrapper.destroyInstance(instance, p_allocator);
    std.log.scoped(.ATHENAVK).debug
    (
        "Destroyed Instance ID: {d}|{d}",
        .{
            instance_data.value.instance_id,
            instance_data.value.instance
        }
    );

    if (layer_global_state.instance_backing.?.map.count() == 0)
    {
        athena_overlay.deinit_plugins();
        athena_overlay.destroy_overlay_context();
        layer_global_state.instance_backing = null;
        layer_global_state.device_backing = null;
        layer_global_state.swapchain_backing = null;
        layer_global_state.free_short_lived_allocator();
        layer_global_state.free_long_lived_allocator();
        layer_global_state.free_unbounded_heap_backed_allocator();
    }
}
