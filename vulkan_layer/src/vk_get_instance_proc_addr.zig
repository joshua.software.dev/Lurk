const builtin = @import("builtin");
const std = @import("std");

const layer_global_state = @import("layer_global_state.zig");
const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const simple = @import("simple");
const vk = @import("vk");


pub fn GetInstanceProcAddr
(
    comptime get_blacklist_func: fn ([]const u8) ?vk.PfnVoidFunction,
    comptime get_device_func: fn ([]const u8) ?vk.PfnVoidFunction,
    comptime get_instance_func: fn ([]const u8) ?vk.PfnVoidFunction,
    instance: vk.Instance,
    p_name: [*:0]const u8,
)
!vk.PfnVoidFunction
{
    const span_name = std.mem.span(p_name);
    const proc_is_blacklisted = blk: {
        var proc_info: simple.BlacklistProcess = undefined;
        const result_was_cached = try simple.is_this_process_blacklisted(&proc_info);
        if (!result_was_cached)
        {
            std.log.scoped(.ATHENAVK).debug
            (
                "proc_name: {s} | is blacklisted: {} | is wine: {}",
                .{
                    proc_info.name.constSlice(),
                    proc_info.is_blacklisted,
                    proc_info.is_wine,
                },
            );
        }
        break :blk proc_info.is_blacklisted;
    };

    if (proc_is_blacklisted)
    {
        if (get_blacklist_func(span_name)) |bl_func| return bl_func;
    }
    else
    {
        if (get_instance_func(span_name)) |inst_func|
        {
            return inst_func;
        }
        else if (get_device_func(span_name)) |device_func|
        {
            return device_func;
        }
    }

    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    const instance_data = layer_global_state.instance_backing.?.map.getPtr(instance).?;
    return @ptrCast(@alignCast(instance_data.get_inst_proc_addr_func_ptr(instance, p_name)));
}
