const std = @import("std");

const layer_global_state = @import("layer_global_state.zig");
const vklct = @import("vk_layer_custom_types.zig");

const simple = @import("simple");
const vk = @import("vk");


pub fn GetDeviceProcAddr
(
    comptime get_blacklist_func: fn ([]const u8) ?vk.PfnVoidFunction,
    comptime get_device_func: fn ([]const u8) ?vk.PfnVoidFunction,
    device: vk.Device,
    p_name: [*:0]const u8
)
!vk.PfnVoidFunction
{
    const span_name = std.mem.span(p_name);
    const proc_is_blacklisted = blk: {
        var proc_info: simple.BlacklistProcess = undefined;
        _ = try simple.is_this_process_blacklisted(&proc_info);
        break :blk proc_info.is_blacklisted;
    };

    if (proc_is_blacklisted)
    {
        if (get_blacklist_func(span_name)) |bl_func| return bl_func;
    }
    else
    {
        if (get_device_func(span_name)) |device_func| return device_func;
    }

    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    const device_data = layer_global_state.device_backing.?.map.getPtr(device).?;
    return @ptrCast(@alignCast(device_data.get_device_proc_addr_func(device, p_name)));
}
