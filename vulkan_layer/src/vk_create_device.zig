const std = @import("std");

const info = @import("info.zig");
const layer_global_state = @import("layer_global_state.zig");
const queue_present_khr = @import("vk_queue_present_khr.zig");
const vkl = @import("vulkan_layer_api_stubs.zig");
const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const simple = @import("simple");
const vk = @import("vk");


fn search_device_create_info(p_create_info: *const vk.DeviceCreateInfo, func_type: c_int) !*vkl.LayerDeviceCreateInfo
{
    var device_create_info: ?*vkl.LayerDeviceCreateInfo = @ptrCast(@alignCast(@constCast(p_create_info.p_next)));
    while (device_create_info) |dci|
    {
        if
        (
            dci.s_type == .loader_device_create_info and
            dci.function == func_type
        )
        {
            return dci;
        }

        device_create_info = @ptrCast(@alignCast(@constCast(dci.p_next)));
    }

    return error.MissingVulkanDeviceCreateInfo;
}

fn create_device_data
(
    instance: vk.Instance,
    physical_device: vk.PhysicalDevice,
    p_create_info: *const vk.DeviceCreateInfo,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_device: *vk.Device
)
!vklct.DeviceData
{
    var layer_create_info = try search_device_create_info(p_create_info, vkl.LayerFunction_LAYER_LINK_INFO);
    if (layer_create_info.u.p_layer_info == null) return error.InvalidVulkanDeviceLayerInfo;

    const get_device_proc_addr = layer_create_info.u.p_layer_info.?.pfn_next_get_device_proc_addr;
    const get_inst_proc_addr = layer_create_info.u.p_layer_info.?.pfn_next_get_instance_proc_addr;

    const create_device_func_ptr = @as
    (
        ?vk.PfnCreateDevice,
        @constCast
        (
            @ptrCast
            (
                @alignCast
                (
                    get_inst_proc_addr
                    (
                        .null_handle,
                        vk.InstanceCommandFlags.cmdName(.createDevice),
                    )
                )
            )
        )
    )
        orelse return error.InitializationFailed;

    layer_create_info.u.p_layer_info = layer_create_info.u.p_layer_info.?.p_next;

    const create_device_result = create_device_func_ptr
    (
        physical_device,
        p_create_info,
        p_allocator,
        p_device,
    );
    switch (create_device_result) {
        .success => {},
        .error_out_of_host_memory => return error.OutOfHostMemory,
        .error_out_of_device_memory => return error.OutOfDeviceMemory,
        .error_initialization_failed => return error.InitializationFailed,
        .error_extension_not_present => return error.ExtensionNotPresent,
        .error_feature_not_present => return error.FeatureNotPresent,
        .error_too_many_objects => return error.TooManyObjects,
        .error_device_lost => return error.DeviceLost,
        else => return error.Unknown,
    }

    const device = p_device.*;
    const device_wrapper = try vklct.LayerDeviceWrapper.load(device, get_device_proc_addr);

    const device_loader = try search_device_create_info(p_create_info, vkl.LayerFunction_LOADER_DATA_CALLBACK);

    std.log.scoped(.ATHENAVK).debug("Current Device Ref: {d}", .{ layer_global_state.device_ref_count });
    layer_global_state.device_ref_count += 1;
    std.log.scoped(.ATHENAVK).debug("New Device Ref: {d}|{d}", .{ layer_global_state.device_ref_count, device });
    return .{
        .device_id = layer_global_state.device_ref_count,
        .device = device,
        .physical_device = physical_device,
        .instance = instance,
        .get_device_proc_addr_func = get_device_proc_addr,
        .set_device_loader_data_func = device_loader.u.pfn_set_device_loader_data
            orelse return error.InvalidVulkanDeviceLayerInfo,
        .graphic_queue = null,
        .most_recent_draw_data = null,
        .device_wrapper = device_wrapper,
        .queues = try vklct.QueueDataBacking.init(0),
    };
}

fn device_map_queues
(
    p_create_info: *const vk.DeviceCreateInfo,
    instance_wrapper: vklct.LayerInstanceWrapper,
    device_data: *vklct.DeviceData,
)
!void
{
    var queue_family_props_count: u32 = 0;
    instance_wrapper.getPhysicalDeviceQueueFamilyProperties
    (
        device_data.physical_device,
        &queue_family_props_count,
        null,
    );

    var family_props = try vklct.QueueFamilyPropsBacking.init(0);
    try family_props.resize(queue_family_props_count);

    instance_wrapper.getPhysicalDeviceQueueFamilyProperties
    (
        device_data.physical_device,
        &queue_family_props_count,
        family_props.slice().ptr,
    );

    var i: u32 = 0;
    while (i < p_create_info.queue_create_info_count) : (i += 1)
    {
        const queue_family_index = p_create_info.p_queue_create_infos[i].queue_family_index;
        var j: u32 = 0;
        while (j < p_create_info.p_queue_create_infos[i].queue_count) : (j += 1)
        {
            try device_data.queues.resize(device_data.queues.len + 1);
            var data = &device_data.queues.slice()[device_data.queues.len - 1];

            const queue = device_data.device_wrapper.getDeviceQueue(device_data.device, queue_family_index, j);

            const set_dvc_loader_result = device_data.set_device_loader_data_func
            (
                device_data.device,
                @intFromEnum(queue),
            );
            switch (set_dvc_loader_result) {
                .success => {},
                .error_initialization_failed => return error.InitializationFailed,
                else => return error.Unknown,
            }

            // Fence synchronizing access to queries on that queue.
            const fence_info: vk.FenceCreateInfo = .{ .flags = .{ .signaled_bit = true, }, };
            const fence = try device_data.device_wrapper.createFence(device_data.device, &fence_info, null);

            data.* = .{
                .device = device_data.device,
                .queue_family_index = queue_family_index,
                .queue_flags = family_props.buffer[queue_family_index].queue_flags,
                .queue = queue,
                .fence = fence,
            };

            if (data.queue_flags.contains(.{ .graphics_bit = true, }))
            {
                device_data.graphic_queue = data;
            }
        }
    }
}

fn load_image_vk
(
    user_id: []const u8,
    width: u32,
    height: u32,
    pixel_data: ?[]const u8,
)
anyerror!?athena_overlay.TextureID
{
    var swapchain_data = layer_global_state.active_swapchain.?;
    var maybe_image: ?*vklct.AvatarImage = blk: {
        for (swapchain_data.avatar_images.?.slice()) |*avatar_image|
        {
            if (std.mem.eql(u8, user_id, avatar_image.hashmap_key.constSlice()))
            {
                break :blk avatar_image;
            }
        }

        break :blk null;
    };
    if (maybe_image == null and pixel_data != null)
    {
        const device_data = layer_global_state.device_backing.?.map.getPtr(swapchain_data.device).?;

        maybe_image = try swapchain_data.avatar_images.?.addOne();
        maybe_image.?.* = try queue_present_khr.load_one_avatar_image
        (
            device_data,
            swapchain_data.command_pool.?,
            swapchain_data.descriptor_layout.?,
            swapchain_data.descriptor_pool.?,
            width,
            height,
            user_id,
            pixel_data.?,
        );
    }

    return
        if (maybe_image) |image|
            @enumFromInt(@intFromEnum(image.*.descriptor_set))
        else
            null;
}

pub fn CreateDevice
(
    physical_device: vk.PhysicalDevice,
    p_create_info: *const vk.DeviceCreateInfo,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_device: *vk.Device
)
!vk.Result
{
    const proc_is_blacklisted = blk: {
        var proc_info: simple.BlacklistProcess = undefined;
        _ = try simple.is_this_process_blacklisted(&proc_info);
        break :blk proc_info.is_blacklisted;
    };

    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    if (!proc_is_blacklisted and layer_global_state.device_backing.?.map.count() < 1)
    {
        const heap_allocator = layer_global_state.get_unbounded_heap_backed_allocator();
        const short_allocator = heap_allocator;
        const long_allocator = heap_allocator;
        const font_allocator = layer_global_state.get_font_allocator();

        try athena_overlay.load_fonts(font_allocator);

        try athena_overlay.init_plugins(
            proc_is_blacklisted,
            .{
                .short_lived_allocator = short_allocator,
                .long_lived_allocator = long_allocator,
                .unbounded_heap_backed_allocator = heap_allocator,
                .load_image_hook_fn = &load_image_vk,
            }
        );

        // calling this should be idempotent
        athena_overlay.create_overlay_context();
    }

    std.log.scoped(.ATHENAVK).debug("Create Device: {d}" ++ info.LAYER_NAME, .{ p_device.* });

    const instance_data = blk: {
        for (layer_global_state.instance_backing.?.map.values()) |*v|
        {
            for (v.physical_devices.constSlice()) |dev|
            {
                if (dev == physical_device) break :blk v;
            }
        }

        return error.FailedMappingPhysicalDeviceToInstance;
    };

    const device_data = try create_device_data
    (
        instance_data.instance,
        physical_device,
        p_create_info,
        p_allocator,
        p_device
    );

    const backing = layer_global_state.device_backing.?.map.getOrPutAssumeCapacity(device_data.device);
    if (backing.found_existing) return error.DuplicateVulkanDevice;
    backing.value_ptr.* = device_data;

    try device_map_queues(p_create_info, instance_data.instance_wrapper, backing.value_ptr);
    return .success;
}