const std = @import("std");

const destroy_swapchain_khr = @import("vk_destroy_swapchain_khr.zig");
const info = @import("info.zig");
const layer_global_state = @import("layer_global_state.zig");
const mem_type_helper = @import("vk_memory_type_helper.zig");
const shaders = @import("shaders/vk_shaders.zig");
const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const simple = @import("simple");
const vk = @import("vk");


fn setup_swapchain_data_pipeline
(
    device: vk.Device,
    device_wrapper: vklct.LayerDeviceWrapper,
    swapchain_data: *vklct.SwapchainData,
)
!void
{
    const frag_module = try device_wrapper.createShaderModule
    (
        device,
        &.{
            .code_size = shaders.overlay_frag_spv.len * @sizeOf(u32),
            .p_code = shaders.overlay_frag_spv.ptr,
        },
        null,
    );

    const vert_module = try device_wrapper.createShaderModule
    (
        device,
        &.{
            .code_size = shaders.overlay_vert_spv.len * @sizeOf(u32),
            .p_code = shaders.overlay_vert_spv.ptr,
        },
        null,
    );

    swapchain_data.font_sampler = try device_wrapper.createSampler
    (
        device,
        &.{
            .mag_filter = .linear,
            .min_filter = .linear,
            .mipmap_mode = .linear,
            .address_mode_u = .repeat,
            .address_mode_v = .repeat,
            .address_mode_w = .repeat,
            .min_lod = -1000,
            .max_lod = 1000,
            .max_anisotropy = 1.0,
            // zero init
            .anisotropy_enable = 0,
            .border_color = .float_transparent_black,
            .compare_enable = 0,
            .compare_op = .never,
            .mip_lod_bias = 0,
            .unnormalized_coordinates = 0,
        },
        null,
    );

    swapchain_data.descriptor_pool = try device_wrapper.createDescriptorPool
    (
        device,
        &.{
            .max_sets = 256,
            .pool_size_count = 1,
            .p_pool_sizes = &@as
            (
                [1]vk.DescriptorPoolSize,
                .{
                    .{
                        .type = .combined_image_sampler,
                        .descriptor_count = 256,
                    },
                },
            ),
        },
        null,
    );

    swapchain_data.descriptor_layout = try device_wrapper.createDescriptorSetLayout
    (
        device,
        &.{
            .binding_count = 1,
            .p_bindings = &@as
            (
                [1]vk.DescriptorSetLayoutBinding,
                .{
                    .{
                        .descriptor_type = .combined_image_sampler,
                        .descriptor_count = 1,
                        .stage_flags = vk.ShaderStageFlags{ .fragment_bit = true, },
                        .p_immutable_samplers = &@as([1]vk.Sampler, .{ swapchain_data.font_sampler.?, }),
                        // zero init
                        .binding = 0,
                    },
                },
            ),
        },
        null,
    );

    {
        var descriptor_set_container: [1]vk.DescriptorSet = .{ .null_handle, };
        try device_wrapper.allocateDescriptorSets
        (
            device,
            &.{
                .descriptor_pool = swapchain_data.descriptor_pool.?,
                .descriptor_set_count = 1,
                .p_set_layouts = &@as([1]vk.DescriptorSetLayout, .{ swapchain_data.descriptor_layout.?, }),
            },
            &descriptor_set_container,
        );
        swapchain_data.descriptor_set = descriptor_set_container[0];
    }

    swapchain_data.pipeline_layout = try device_wrapper.createPipelineLayout
    (
        device,
        &.{
            .set_layout_count = 1,
            .p_set_layouts = &@as([1]vk.DescriptorSetLayout, .{ swapchain_data.descriptor_layout.?, }),
            .push_constant_range_count = 1,
            .p_push_constant_ranges = &@as
            (
                [1]vk.PushConstantRange,
                .{
                    .{
                        .stage_flags = .{ .vertex_bit = true, },
                        .offset = @sizeOf(f32) * 0, // can't this just be simplified to 0?
                        .size = @sizeOf(f32) * 4,
                    },
                },
            ),
        },
        null,
    );

    {
        var pipeline_container: [1]vk.Pipeline = .{ .null_handle, };
        _ = try device_wrapper.createGraphicsPipelines
        (
            device,
            .null_handle,
            1,
            &@as
            (
                [1]vk.GraphicsPipelineCreateInfo,
                .{
                    .{
                        .stage_count = 2,
                        .p_stages = &@as
                        (
                            [2]vk.PipelineShaderStageCreateInfo,
                            .{
                                .{
                                    .stage = .{ .vertex_bit = true, },
                                    .module = vert_module,
                                    .p_name = "main",
                                },
                                .{
                                    .stage = .{ .fragment_bit = true, },
                                    .module = frag_module,
                                    .p_name = "main",
                                },
                            },
                        ),
                        .p_vertex_input_state = &.{
                            .vertex_binding_description_count = 1,
                            .p_vertex_binding_descriptions = &@as
                            (
                                [1]vk.VertexInputBindingDescription,
                                .{
                                    .{
                                        .input_rate = .vertex,
                                        .stride = @sizeOf(athena_overlay.DrawVert),
                                        // zero init
                                        .binding = 0,
                                    },
                                },
                            ),
                            .vertex_attribute_description_count = 3,
                            .p_vertex_attribute_descriptions = &@as
                            (
                                [3]vk.VertexInputAttributeDescription,
                                .{
                                    .{
                                        .location = 0,
                                        .binding = 0,
                                        .format = .r32g32_sfloat,
                                        .offset = @offsetOf(athena_overlay.DrawVert, "pos"),
                                    },
                                    .{
                                        .location = 1,
                                        .binding = 0,
                                        .format = .r32g32_sfloat,
                                        .offset = @offsetOf(athena_overlay.DrawVert, "uv"),
                                    },
                                    .{
                                        .location = 2,
                                        .binding = 0,
                                        .format = .r8g8b8a8_unorm,
                                        .offset = @offsetOf(athena_overlay.DrawVert, "col"),
                                    },
                                },
                            ),
                        },
                        .p_input_assembly_state = &.{
                            .topology = .triangle_list,
                            // zero init
                            .primitive_restart_enable = 0,
                        },
                        .p_viewport_state = &.{
                            .viewport_count = 1,
                            .scissor_count = 1,
                        },
                        .p_rasterization_state = &.{
                            .polygon_mode = .fill,
                            .cull_mode = .{},
                            .front_face = .counter_clockwise,
                            .line_width = 1.0,
                            // zero init
                            .depth_bias_clamp = 0,
                            .depth_bias_constant_factor = 0,
                            .depth_bias_enable = 0,
                            .depth_bias_slope_factor = 0,
                            .depth_clamp_enable = 0,
                            .rasterizer_discard_enable = 0,
                        },
                        .p_multisample_state = &.{
                            .rasterization_samples = .{ .@"1_bit" = true, },
                            // zero init
                            .alpha_to_coverage_enable = 0,
                            .alpha_to_one_enable = 0,
                            .min_sample_shading = 0,
                            .sample_shading_enable = 0,
                        },
                        .p_depth_stencil_state = &.{
                            // zero init
                            .back = .{
                                .compare_mask = 0,
                                .compare_op = .never,
                                .depth_fail_op = .keep,
                                .fail_op = .keep,
                                .pass_op = .keep,
                                .reference = 0,
                                .write_mask = 0,
                            },
                            .depth_bounds_test_enable = 0,
                            .depth_compare_op = .never,
                            .depth_test_enable = 0,
                            .depth_write_enable = 0,
                            .front = .{
                                .compare_mask = 0,
                                .compare_op = .never,
                                .depth_fail_op = .keep,
                                .fail_op = .keep,
                                .pass_op = .keep,
                                .reference = 0,
                                .write_mask = 0,
                            },
                            .min_depth_bounds = 0,
                            .stencil_test_enable = 0,
                            .max_depth_bounds = 0,
                        },
                        .p_color_blend_state = &.{
                            .attachment_count = 1,
                            .p_attachments = &@as
                            (
                                [1]vk.PipelineColorBlendAttachmentState,
                                .{
                                    .{
                                        .blend_enable = 1,
                                        .src_color_blend_factor = .src_alpha,
                                        .dst_color_blend_factor = .one_minus_src_alpha,
                                        .color_blend_op = .add,
                                        .src_alpha_blend_factor = .one_minus_src_alpha,
                                        .dst_alpha_blend_factor = .zero,
                                        .alpha_blend_op = .add,
                                        .color_write_mask = .{
                                            .r_bit = true,
                                            .g_bit = true,
                                            .b_bit = true,
                                            .a_bit = true,
                                        },
                                    },
                                },
                            ),
                            // zero init
                            .blend_constants = .{ 0.0, 0.0, 0.0, 0.0 },
                            .logic_op = .clear,
                            .logic_op_enable = 0,
                        },
                        .p_dynamic_state = &.{
                            .dynamic_state_count = 2,
                            .p_dynamic_states = &@as
                            (
                                [2]vk.DynamicState,
                                .{
                                    .viewport,
                                    .scissor,
                                },
                            ),
                        },
                        .layout = swapchain_data.pipeline_layout.?,
                        .render_pass = swapchain_data.render_pass.?,
                        // zero init
                        .base_pipeline_index = 0,
                        .subpass = 0,
                    },
                },
            ),
            null,
            &pipeline_container,
        );
        swapchain_data.pipeline = pipeline_container[0];
    }

    device_wrapper.destroyShaderModule(device, vert_module, null);
    device_wrapper.destroyShaderModule(device, frag_module, null);

    {
        const font_pixel_data = try athena_overlay.get_font_atlas_as_pixels(.rgba32);

        swapchain_data.font_image = try device_wrapper.createImage
        (
            device,
            &.{
                .image_type = .@"2d",
                .format = .r8g8b8a8_unorm,
                .extent = .{
                    .depth = 1,
                    .height = font_pixel_data.y_height,
                    .width = font_pixel_data.x_width,
                },
                .mip_levels = 1,
                .array_layers = 1,
                .samples = .{ .@"1_bit" = true, },
                .tiling = .optimal,
                .usage = .{ .sampled_bit = true, .transfer_dst_bit = true, },
                .sharing_mode = .exclusive,
                .initial_layout = .undefined,
            },
            null,
        );
    }

    {
        const font_image_req = device_wrapper.getImageMemoryRequirements(device, swapchain_data.font_image.?);
        swapchain_data.font_mem = try device_wrapper.allocateMemory
        (
            device,
            &.{
                .allocation_size = font_image_req.size,
                .memory_type_index = try mem_type_helper.vk_memory_type
                (
                    device,
                    .{ .device_local_bit = true, },
                    font_image_req.memory_type_bits
                ),
            },
            null,
        );
    }

    try device_wrapper.bindImageMemory(device, swapchain_data.font_image.?, swapchain_data.font_mem.?, 0);

    swapchain_data.font_image_view = try device_wrapper.createImageView
    (
        device,
        &.{
            .image = swapchain_data.font_image.?,
            .view_type = .@"2d",
            .format = .r8g8b8a8_unorm,
            .subresource_range = .{
                .aspect_mask = .{ .color_bit = true, },
                .level_count = 1,
                .layer_count = 1,
                // zero init
                .base_array_layer = 0,
                .base_mip_level = 0,
            },
            // zero init
            .components = .{
                .r = .identity,
                .g = .identity,
                .b = .identity,
                .a = .identity,
            }
        },
        null,
    );

    device_wrapper.updateDescriptorSets
    (
        device,
        1,
        &@as
        (
            [1]vk.WriteDescriptorSet,
            .{
                .{
                    .dst_set = swapchain_data.descriptor_set.?,
                    .descriptor_count = 1,
                    .descriptor_type = .combined_image_sampler,
                    .p_image_info = &@as
                    (
                        [1]vk.DescriptorImageInfo,
                        .{
                            .{
                                .sampler = swapchain_data.font_sampler.?,
                                .image_view = swapchain_data.font_image_view.?,
                                .image_layout = .shader_read_only_optimal,
                            },
                        },
                    ),
                    .p_buffer_info = &@as([1]vk.DescriptorBufferInfo, .{ .{ .offset = 0, .range = 0, } }),
                    .p_texel_buffer_view = &@as([1]vk.BufferView, .{ vk.BufferView.null_handle, }),
                    // zero init
                    .dst_array_element = 0,
                    .dst_binding = 0,
                },
            }
        ),
        0,
        null,
    );
}

pub fn setup_swapchain
(
    device: vk.Device,
    device_wrapper: vklct.LayerDeviceWrapper,
    swapchain_data: *vklct.SwapchainData,
    queue_family_index: u32,
)
!void
{
    const old_ctx = athena_overlay.use_overlay_context()
        catch unreachable;
    defer athena_overlay.restore_old_context(old_ctx);

    swapchain_data.render_pass = try device_wrapper.createRenderPass
    (
        device,
        &.{
            .s_type = .render_pass_create_info,
            .attachment_count = 1,
            .p_attachments = &@as
            (
                [1]vk.AttachmentDescription,
                .{
                    .{
                        .format = swapchain_data.format,
                        .samples = .{ .@"1_bit" = true, },
                        .load_op = .load,
                        .store_op = .store,
                        .stencil_load_op = .dont_care,
                        .stencil_store_op = .dont_care,
                        .initial_layout = .color_attachment_optimal,
                        .final_layout = .present_src_khr,
                    },
                },
            ),
            .subpass_count = 1,
            .p_subpasses = &@as
            (
                [1]vk.SubpassDescription,
                .{
                    .{
                        .pipeline_bind_point = .graphics,
                        .color_attachment_count = 1,
                        .p_color_attachments = &@as
                        (
                            [1]vk.AttachmentReference,
                            .{
                                .{
                                    .attachment = 0,
                                    .layout = .color_attachment_optimal,
                                },
                            },
                        ),
                    },
                },
            ),
            .dependency_count = 1,
            .p_dependencies = &@as
            (
                [1]vk.SubpassDependency,
                .{
                    .{
                        .src_subpass = vk.SUBPASS_EXTERNAL,
                        .dst_subpass = 0,
                        .src_stage_mask = .{ .color_attachment_output_bit = true, },
                        .dst_stage_mask = .{ .color_attachment_output_bit = true, },
                        .src_access_mask = .{},
                        .dst_access_mask = .{ .color_attachment_write_bit = true, },
                    },
                },
            ),
        },
        null,
    );

    try setup_swapchain_data_pipeline
    (
        device,
        device_wrapper,
        swapchain_data,
    );

    swapchain_data.image_count = 0;
    _ = try device_wrapper.getSwapchainImagesKHR
    (
        device,
        swapchain_data.swapchain.?,
        &swapchain_data.image_count.?,
        null,
    );

    try swapchain_data.framebuffers.resize(swapchain_data.image_count.?);
    try swapchain_data.image_views.resize(swapchain_data.image_count.?);
    try swapchain_data.images.resize(swapchain_data.image_count.?);

    _ = try device_wrapper.getSwapchainImagesKHR
    (
        device,
        swapchain_data.swapchain.?,
        &swapchain_data.image_count.?,
        swapchain_data.images.slice().ptr,
    );

    for (0..swapchain_data.image_count.?) |i|
    {
        swapchain_data.image_views.set
        (
            i,
            try device_wrapper.createImageView
            (
                device,
                &.{
                    .image = swapchain_data.images.get(i),
                    .view_type = .@"2d",
                    .format = swapchain_data.format,
                    .components = .{
                        .r = .r,
                        .g = .g,
                        .b = .b,
                        .a = .a,
                    },
                    .subresource_range = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .base_mip_level = 0,
                        .level_count = 1,
                        .base_array_layer = 0,
                        .layer_count = 1,
                    },
                },
                null,
            ),
        );

        swapchain_data.framebuffers.set
        (
            i,
            try device_wrapper.createFramebuffer
            (
                device,
                &.{
                    .render_pass = swapchain_data.render_pass.?,
                    .attachment_count = 1,
                    .p_attachments = &@as([1]vk.ImageView, .{ swapchain_data.image_views.get(i), }),
                    .width = swapchain_data.width,
                    .height = swapchain_data.height,
                    .layers = 1,
                },
                null,
            ),
        );
    }

    swapchain_data.command_pool = try device_wrapper.createCommandPool
    (
        device,
        &.{
            .flags = .{ .reset_command_buffer_bit = true, },
            .queue_family_index = queue_family_index,
        },
        null,
    );

    swapchain_data.avatar_images = try vklct.AvatarImageBacking.init(0);
}

pub fn CreateSwapchainKHR
(
    device: vk.Device,
    p_create_info: *const vk.SwapchainCreateInfoKHR,
    p_allocator: ?*const vk.AllocationCallbacks,
    p_swapchain: *vk.SwapchainKHR,
)
!vk.Result
{
    if (!layer_global_state.wrappers_global_lock.tryLock()) return error.FailedVulkanVTableLock;
    defer layer_global_state.wrappers_global_lock.unlock();

    std.log.scoped(.ATHENAVK).debug("Create Swapchain: {d}" ++ info.LAYER_NAME, .{ p_swapchain.* });
    var device_data: *vklct.DeviceData = layer_global_state.device_backing.?.map.getPtr(device).?;

    const result: vk.Result = device_data.device_wrapper.dispatch.vkCreateSwapchainKHR
    (
        device,
        p_create_info,
        p_allocator,
        p_swapchain,
    );
    switch (result) {
        .success => {},
        .error_out_of_host_memory => return error.OutOfHostMemory,
        .error_out_of_device_memory => return error.OutOfDeviceMemory,
        .error_device_lost => return error.DeviceLost,
        .error_surface_lost_khr => return error.SurfaceLostKHR,
        .error_native_window_in_use_khr => return error.NativeWindowInUseKHR,
        .error_initialization_failed => return error.InitializationFailed,
        .error_compression_exhausted_ext => return error.CompressionExhaustedEXT,
        else => return error.Unknown,
    }

    const swapchain = p_swapchain.*;
    const backing = layer_global_state.swapchain_backing.?.map.getOrPutAssumeCapacity(swapchain);
    if (backing.found_existing)
    {
        std.log.scoped(.ATHENAVK).warn
        (
            "A new instance was requested with the same memory address as an existing one.",
            .{}
        );
        try destroy_swapchain_khr.destroy_swapchain_internal
        (
            device,
            device_data.device_wrapper,
            backing.value_ptr,
            device_data,
        );
    }

    std.log.scoped(.ATHENAVK).debug("Current Swapchain Ref: {d}", .{ layer_global_state.swapchain_ref_count });
    layer_global_state.swapchain_ref_count += 1;
    std.log.scoped(.ATHENAVK).debug
    (
        "New Swapchain Ref: {d}|{d}",
        .{
            layer_global_state.swapchain_ref_count,
            swapchain,
        },
    );
    backing.value_ptr.* = vklct.SwapchainData
    {
        .swapchain_id = layer_global_state.swapchain_ref_count,
        .command_pool = null,
        .descriptor_layout = null,
        .descriptor_pool = null,
        .descriptor_set = null,
        .device = device,
        .font_image_view = null,
        .font_image = null,
        .font_mem = null,
        .font_sampler = null,
        .font_uploaded = false,
        .format = p_create_info.image_format,
        .height = p_create_info.image_extent.height,
        .image_count = null,
        .pipeline_layout = null,
        .pipeline = null,
        .render_pass = null,
        .swapchain = swapchain,
        .upload_font_buffer_mem = null,
        .upload_font_buffer = null,
        .width = p_create_info.image_extent.width,
        .framebuffers = try vklct.FramebufferBacking.init(0),
        .image_views = try vklct.ImageViewBacking.init(0),
        .images = try vklct.ImageBacking.init(0),
        .avatar_images = null,
    };

    return result;
}
