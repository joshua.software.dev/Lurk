const std = @import("std");

const create_swapchain_khr = @import("vk_create_swapchain_khr.zig");
const destroy_swapchain_khr = @import("vk_destroy_swapchain_khr.zig");
const layer_global_state = @import("layer_global_state.zig");
const mem_type_helper = @import("vk_memory_type_helper.zig");
const vkl = @import("vulkan_layer_api_stubs.zig");
const vklct = @import("vk_layer_custom_types.zig");

const athena_overlay = @import("athena_overlay");
const simple = @import("simple");
const vk = @import("vk");


fn get_overlay_draw
(
    device_data: *vklct.DeviceData,
    swapchain_data: *vklct.SwapchainData,
)
!*vklct.DrawBufferData
{
    if (device_data.most_recent_draw_data) |*draw_buf_data|
    {
        if (try device_data.device_wrapper.getFenceStatus(device_data.device, draw_buf_data.fence) == .success)
        {
            try device_data.device_wrapper.resetFences
            (
                device_data.device,
                1,
                &@as([1]vk.Fence, .{ draw_buf_data.fence, }),
            );

            return draw_buf_data;
        }
    }

    var command_buf_container: [1]vk.CommandBuffer = .{ .null_handle, };
    try device_data.device_wrapper.allocateCommandBuffers
    (
        device_data.device,
        &.{
            .command_pool = swapchain_data.command_pool.?,
            .level = .primary,
            .command_buffer_count = 1,
        },
        &command_buf_container,
    );

    const set_dvc_loader_result = device_data.set_device_loader_data_func
    (
        device_data.device,
        @intFromEnum(command_buf_container[0]),
    );
    switch (set_dvc_loader_result) {
        .success => {},
        .error_initialization_failed => return error.InitializationFailed,
        else => return error.Unknown,
    }

    const fence = try device_data.device_wrapper.createFence
    (
        device_data.device,
        &.{},
        null,
    );

    const semaphore = try device_data.device_wrapper.createSemaphore
    (
        device_data.device,
        &.{},
        null,
    );
    const cross_engine_semaphore = try device_data.device_wrapper.createSemaphore
    (
        device_data.device,
        &.{},
        null,
    );

    device_data.most_recent_draw_data = .{
        .command_buffer = command_buf_container[0],
        .cross_engine_semaphore = cross_engine_semaphore,
        .semaphore = semaphore,
        .fence = fence,
        .vertex_buffer = null,
        .vertex_buffer_mem = null,
        .vertex_buffer_size = null,
        .index_buffer = null,
        .index_buffer_mem = null,
        .index_buffer_size = null,
    };

    return &device_data.most_recent_draw_data.?;
}

fn ensure_swapchain_fonts
(
    device_data: *vklct.DeviceData,
    swapchain_data: *vklct.SwapchainData,
)
!void
{
    if (swapchain_data.font_uploaded != null and swapchain_data.font_uploaded.?) return;

    const font_pixel_data = try athena_overlay.get_font_atlas_as_pixels(.rgba32);

    const command_buffer = blk: {
        var command_buf_container: [1]vk.CommandBuffer = .{ .null_handle, };
        try device_data.device_wrapper.allocateCommandBuffers
        (
            device_data.device,
            &.{
                .command_pool = swapchain_data.command_pool.?,
                .level = .primary,
                .command_buffer_count = 1,
            },
            &command_buf_container,
        );
        break :blk command_buf_container[0];
    };

    try device_data.device_wrapper.beginCommandBuffer(command_buffer, &.{});

    swapchain_data.upload_font_buffer = try device_data.device_wrapper.createBuffer
    (
        device_data.device,
        &.{
            .size = font_pixel_data.data.len,
            .usage = .{ .transfer_src_bit = true, },
            .sharing_mode = .exclusive,
        },
        null,
    );

    const upload_buffer_req = device_data.device_wrapper.getBufferMemoryRequirements
    (
        device_data.device,
        swapchain_data.upload_font_buffer.?,
    );

    swapchain_data.upload_font_buffer_mem = try device_data.device_wrapper.allocateMemory
    (
        device_data.device,
        &.{
            .allocation_size = upload_buffer_req.size,
            .memory_type_index = try mem_type_helper.vk_memory_type
            (
                device_data.device,
                .{ .host_visible_bit = true, },
                upload_buffer_req.memory_type_bits
            ),
        },
        null,
    );

    try device_data.device_wrapper.bindBufferMemory
    (
        device_data.device,
        swapchain_data.upload_font_buffer.?,
        swapchain_data.upload_font_buffer_mem.?,
        0,
    );

    var map: [*]u8 = @ptrCast
    (
        try device_data.device_wrapper.mapMemory
        (
            device_data.device,
            swapchain_data.upload_font_buffer_mem.?,
            0,
            font_pixel_data.data.len,
            .{},
        )
    );

    @memcpy(map[0..font_pixel_data.data.len], font_pixel_data.data[0..]);
    try device_data.device_wrapper.flushMappedMemoryRanges
    (
        device_data.device,
        1,
        &@as
        (
            [1]vk.MappedMemoryRange,
            .{
                .{
                    .memory = swapchain_data.upload_font_buffer_mem.?,
                    .size = font_pixel_data.data.len,
                    // zero init
                    .offset = 0,
                },
            },
        ),
    );
    device_data.device_wrapper.unmapMemory(device_data.device, swapchain_data.upload_font_buffer_mem.?);

    device_data.device_wrapper.cmdPipelineBarrier
    (
        command_buffer,
        .{ .host_bit = true, },
        .{ .transfer_bit = true, },
        .{},
        0,
        null,
        0,
        null,
        1,
        &@as
        (
            [1]vk.ImageMemoryBarrier,
            .{
                .{
                    .dst_access_mask = .{ .transfer_write_bit = true, },
                    .old_layout = .undefined,
                    .new_layout = .transfer_dst_optimal,
                    .src_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .dst_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .image = swapchain_data.font_image.?,
                    .subresource_range = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .level_count = 1,
                        .layer_count = 1,
                        // zero init
                        .base_array_layer = 0,
                        .base_mip_level = 0,
                    },
                    // zero init
                    .src_access_mask = .{},
                },
            },
        ),
    );

    device_data.device_wrapper.cmdCopyBufferToImage
    (
        command_buffer,
        swapchain_data.upload_font_buffer.?,
        swapchain_data.font_image.?,
        .transfer_dst_optimal,
        1,
        &@as
        (
            [1]vk.BufferImageCopy,
            .{
                .{
                    .image_subresource = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .layer_count = 1,
                        // zero init
                        .mip_level = 0,
                        .base_array_layer = 0,
                    },
                    .image_extent = .{
                        .width = font_pixel_data.x_width,
                        .height = font_pixel_data.y_height,
                        .depth = @as(u32, 1),
                    },
                    // zero init
                    .buffer_image_height = 0,
                    .buffer_offset = 0,
                    .buffer_row_length = 0,
                    .image_offset = .{
                        .x = 0,
                        .y = 0,
                        .z = 0,
                    },
                },
            },
        ),
    );

    device_data.device_wrapper.cmdPipelineBarrier
    (
        command_buffer,
        .{ .transfer_bit = true, },
        .{ .fragment_shader_bit = true, },
        .{},
        0,
        null,
        0,
        null,
        1,
        &@as
        (
            [1]vk.ImageMemoryBarrier,
            .{
                .{
                    .src_access_mask = .{ .transfer_write_bit = true, },
                    .dst_access_mask = .{ .shader_read_bit = true, },
                    .old_layout = .transfer_dst_optimal,
                    .new_layout = .shader_read_only_optimal,
                    .src_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .dst_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .image = swapchain_data.font_image.?,
                    .subresource_range = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .level_count = 1,
                        .layer_count = 1,
                        // zero init
                        .base_array_layer = 0,
                        .base_mip_level = 0,
                    },
                },
            },
        ),
    );

    try device_data.device_wrapper.endCommandBuffer(command_buffer);
    try device_data.device_wrapper.queueSubmit
    (
        device_data.graphic_queue.?.queue,
        1,
        &@as
        (
            [1]vk.SubmitInfo,
            .{
                .{
                    .command_buffer_count = 1,
                    .p_command_buffers = &@as([1]vk.CommandBuffer, .{ command_buffer, }),
                },
            },
        ),
        .null_handle,
    );
    try device_data.device_wrapper.queueWaitIdle(device_data.graphic_queue.?.queue);
    device_data.device_wrapper.freeCommandBuffers
    (
        device_data.device,
        swapchain_data.command_pool.?,
        1,
        &@as([1]vk.CommandBuffer, .{ command_buffer, }),
    );

    athena_overlay.set_font_atlas_gpu_texture_id(@enumFromInt(@intFromEnum(swapchain_data.descriptor_set.?)));
    swapchain_data.font_uploaded = true;
}

pub fn load_one_avatar_image
(
    device_data: *vklct.DeviceData,
    command_pool: vk.CommandPool,
    descriptor_layout: vk.DescriptorSetLayout,
    descriptor_pool: vk.DescriptorPool,
    image_width: u32,
    image_height: u32,
    hashmap_key: []const u8,
    texture_data: []const u8,
)
!vklct.AvatarImage
{
    const command_buffer = blk: {
        var command_buf_container: [1]vk.CommandBuffer = .{ .null_handle, };
        try device_data.device_wrapper.allocateCommandBuffers
        (
            device_data.device,
            &.{
                .command_pool = command_pool,
                .level = .primary,
                .command_buffer_count = 1,
            },
            &command_buf_container,
        );
        break :blk command_buf_container[0];
    };

    try device_data.device_wrapper.beginCommandBuffer(command_buffer, &.{});

    const image = try device_data.device_wrapper.createImage
    (
        device_data.device,
        &.{
            .image_type = .@"2d",
            .format = .r8g8b8a8_unorm,
            .extent = .{
                .depth = 1,
                .height = image_height,
                .width = image_width,
            },
            .mip_levels = 1,
            .array_layers = 1,
            .samples = .{ .@"1_bit" = true, },
            .tiling = .optimal,
            .usage = .{ .sampled_bit = true, .transfer_dst_bit = true, },
            .sharing_mode = .exclusive,
            .initial_layout = .undefined,
        },
        null,
    );

    const image_memory = blk: {
        const image_mem_req = device_data.device_wrapper.getImageMemoryRequirements(device_data.device, image);
        break :blk try device_data.device_wrapper.allocateMemory
        (
            device_data.device,
            &.{
                .allocation_size = image_mem_req.size,
                .memory_type_index = try mem_type_helper.vk_memory_type
                (
                    device_data.device,
                    .{ .device_local_bit = true, },
                    image_mem_req.memory_type_bits
                ),
            },
            null,
        );
    };

    try device_data.device_wrapper.bindImageMemory(device_data.device, image, image_memory, 0);

    const image_view = try device_data.device_wrapper.createImageView
    (
        device_data.device,
        &.{
            .image = image,
            .view_type = .@"2d",
            .format = .r8g8b8a8_unorm,
            .subresource_range = .{
                .aspect_mask = .{ .color_bit = true, },
                .level_count = 1,
                .layer_count = 1,
                // zero init
                .base_array_layer = 0,
                .base_mip_level = 0,
            },
            // zero init
            .components = .{
                .r = .identity,
                .g = .identity,
                .b = .identity,
                .a = .identity,
            }
        },
        null,
    );

    const image_sampler = try device_data.device_wrapper.createSampler
    (
        device_data.device,
        &.{
            .mag_filter = .linear,
            .min_filter = .linear,
            .mipmap_mode = .linear,
            .address_mode_u = .repeat,
            .address_mode_v = .repeat,
            .address_mode_w = .repeat,
            .min_lod = -1000,
            .max_lod = 1000,
            .max_anisotropy = 1.0,
            // zero init
            .anisotropy_enable = 0,
            .border_color = .float_transparent_black,
            .compare_enable = 0,
            .compare_op = .never,
            .mip_lod_bias = 0,
            .unnormalized_coordinates = 0,
        },
        null,
    );

    const descriptor_set = blk: {
        var descriptor_set_container: [1]vk.DescriptorSet = .{ .null_handle, };
        try device_data.device_wrapper.allocateDescriptorSets
        (
            device_data.device,
            &.{
                .descriptor_pool = descriptor_pool,
                .descriptor_set_count = 1,
                .p_set_layouts = &@as([1]vk.DescriptorSetLayout, .{ descriptor_layout, }),
            },
            &descriptor_set_container,
        );
        break :blk descriptor_set_container[0];
    };

    device_data.device_wrapper.updateDescriptorSets
    (
        device_data.device,
        1,
        &@as
        (
            [1]vk.WriteDescriptorSet,
            .{
                .{
                    .dst_set = descriptor_set,
                    .descriptor_count = 1,
                    .descriptor_type = .combined_image_sampler,
                    .p_image_info = &@as
                    (
                        [1]vk.DescriptorImageInfo,
                        .{
                            .{
                                .sampler = image_sampler,
                                .image_view = image_view,
                                .image_layout = .shader_read_only_optimal,
                            },
                        },
                    ),
                    .p_buffer_info = &@as([1]vk.DescriptorBufferInfo, .{ .{ .offset = 0, .range = 0, } }),
                    .p_texel_buffer_view = &@as([1]vk.BufferView, .{ vk.BufferView.null_handle, }),
                    // zero init
                    .dst_array_element = 0,
                    .dst_binding = 0,
                },
            }
        ),
        0,
        null,
    );

    const upload_buffer = try device_data.device_wrapper.createBuffer
    (
        device_data.device,
        &.{
            .size = texture_data.len,
            .usage = .{ .transfer_src_bit = true, },
            .sharing_mode = .exclusive,
        },
        null,
    );

    const upload_buffer_req = device_data.device_wrapper.getBufferMemoryRequirements
    (
        device_data.device,
        upload_buffer,
    );

    const upload_buffer_memory = try device_data.device_wrapper.allocateMemory
    (
        device_data.device,
        &.{
            .allocation_size = upload_buffer_req.size,
            .memory_type_index = try mem_type_helper.vk_memory_type
            (
                device_data.device,
                .{ .host_visible_bit = true, },
                upload_buffer_req.memory_type_bits
            ),
        },
        null,
    );

    try device_data.device_wrapper.bindBufferMemory
    (
        device_data.device,
        upload_buffer,
        upload_buffer_memory,
        0,
    );

    var map: [*]u8 = @ptrCast
    (
        try device_data.device_wrapper.mapMemory
        (
            device_data.device,
            upload_buffer_memory,
            0,
            texture_data.len,
            .{},
        )
    );

    @memcpy(map[0..texture_data.len], texture_data[0..]);
    try device_data.device_wrapper.flushMappedMemoryRanges
    (
        device_data.device,
        1,
        &@as
        (
            [1]vk.MappedMemoryRange,
            .{
                .{
                    .memory = upload_buffer_memory,
                    .size = texture_data.len,
                    // zero init
                    .offset = 0,
                },
            },
        ),
    );
    device_data.device_wrapper.unmapMemory(device_data.device, upload_buffer_memory);

    device_data.device_wrapper.cmdPipelineBarrier
    (
        command_buffer,
        .{ .host_bit = true, },
        .{ .transfer_bit = true, },
        .{},
        0,
        null,
        0,
        null,
        1,
        &@as
        (
            [1]vk.ImageMemoryBarrier,
            .{
                .{
                    .dst_access_mask = .{ .transfer_write_bit = true, },
                    .old_layout = .undefined,
                    .new_layout = .transfer_dst_optimal,
                    .src_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .dst_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .image = image,
                    .subresource_range = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .level_count = 1,
                        .layer_count = 1,
                        // zero init
                        .base_array_layer = 0,
                        .base_mip_level = 0,
                    },
                    // zero init
                    .src_access_mask = .{},
                },
            },
        ),
    );

    device_data.device_wrapper.cmdCopyBufferToImage
    (
        command_buffer,
        upload_buffer,
        image,
        .transfer_dst_optimal,
        1,
        &@as
        (
            [1]vk.BufferImageCopy,
            .{
                .{
                    .image_subresource = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .layer_count = 1,
                        // zero init
                        .mip_level = 0,
                        .base_array_layer = 0,
                    },
                    .image_extent = .{
                        .width = image_width,
                        .height = image_height,
                        .depth = @as(u32, 1),
                    },
                    // zero init
                    .buffer_image_height = 0,
                    .buffer_offset = 0,
                    .buffer_row_length = 0,
                    .image_offset = .{
                        .x = 0,
                        .y = 0,
                        .z = 0,
                    },
                },
            },
        ),
    );

    device_data.device_wrapper.cmdPipelineBarrier
    (
        command_buffer,
        .{ .transfer_bit = true, },
        .{ .fragment_shader_bit = true, },
        .{},
        0,
        null,
        0,
        null,
        1,
        &@as
        (
            [1]vk.ImageMemoryBarrier,
            .{
                .{
                    .src_access_mask = .{ .transfer_write_bit = true, },
                    .dst_access_mask = .{ .shader_read_bit = true, },
                    .old_layout = .transfer_dst_optimal,
                    .new_layout = .shader_read_only_optimal,
                    .src_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .dst_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
                    .image = image,
                    .subresource_range = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .level_count = 1,
                        .layer_count = 1,
                        // zero init
                        .base_array_layer = 0,
                        .base_mip_level = 0,
                    },
                },
            },
        ),
    );

    try device_data.device_wrapper.endCommandBuffer(command_buffer);
    try device_data.device_wrapper.queueSubmit
    (
        device_data.graphic_queue.?.queue,
        1,
        &@as
        (
            [1]vk.SubmitInfo,
            .{
                .{
                    .command_buffer_count = 1,
                    .p_command_buffers = &@as([1]vk.CommandBuffer, .{ command_buffer, }),
                },
            },
        ),
        .null_handle,
    );
    try device_data.device_wrapper.queueWaitIdle(device_data.graphic_queue.?.queue);
    device_data.device_wrapper.freeCommandBuffers
    (
        device_data.device,
        command_pool,
        1,
        &@as([1]vk.CommandBuffer, .{ command_buffer, }),
    );

    return .{
        .hashmap_key = try simple.BoundedArray(u8, 255).fromSlice(hashmap_key),
        .descriptor_set = descriptor_set,
        .image = image,
        .image_memory = image_memory,
        .image_view = image_view,
        .image_sampler = image_sampler,
        .upload_buffer = upload_buffer,
        .upload_buffer_memory = upload_buffer_memory,
    };
}

fn create_or_resize_buffer
(
    device_data: *vklct.DeviceData,
    buffer: *vk.Buffer,
    buffer_mem: *vk.DeviceMemory,
    new_size: u64,
    usage: vk.BufferUsageFlags,
)
!void
{
    if (buffer.* != .null_handle)
    {
        device_data.device_wrapper.destroyBuffer(device_data.device, buffer.*, null);
    }

    if (buffer_mem.* != .null_handle)
    {
        device_data.device_wrapper.freeMemory(device_data.device, buffer_mem.*, null);
    }

    buffer.* = try device_data.device_wrapper.createBuffer
    (
        device_data.device,
        &.{
            .size = new_size,
            .usage = usage,
            .sharing_mode = .exclusive,
        },
        null,
    );

    {
        const req = device_data.device_wrapper.getBufferMemoryRequirements(device_data.device, buffer.*);
        buffer_mem.* = try device_data.device_wrapper.allocateMemory
        (
            device_data.device,
            &.{
                .allocation_size = req.size,
                .memory_type_index = try mem_type_helper.vk_memory_type
                (
                    device_data.device,
                    .{ .host_visible_bit = true, },
                    req.memory_type_bits,
                ),
            },
            null,
        );
    }

    try device_data.device_wrapper.bindBufferMemory(device_data.device, buffer.*, buffer_mem.*, 0);
}

fn render_swapchain_display
(
    queue: vk.Queue,
    p_wait_semaphores: ?[*]const vk.Semaphore,
    wait_semaphore_count: u32,
    image_index: u32,
    device_data: *vklct.DeviceData,
    swapchain_data: *vklct.SwapchainData,
)
!bool
{
    const queue_data: *vklct.QueueData = blk: {
        for (device_data.queues.slice()) |*queue_data|
        {
            if (queue_data.queue == queue)
            {
                break :blk queue_data;
            }
        }

        return error.DataMissingForVulkanQueue;
    };

    _ = try device_data.device_wrapper.resetFences(device_data.device, 1, &@as([1]vk.Fence, .{ queue_data.fence, }));
    try device_data.device_wrapper.queueSubmit(queue, 0, null, queue_data.fence);
    _ = try device_data.device_wrapper.waitForFences
    (
        device_data.device,
        1,
        &@as([1]vk.Fence, .{ queue_data.fence, }),
        0,
        std.math.maxInt(u64),
    );

    try ensure_swapchain_fonts
    (
        device_data,
        swapchain_data,
    );

    {
        layer_global_state.active_swapchain = swapchain_data;
        defer layer_global_state.active_swapchain = null;

        try athena_overlay.draw_frame
        (
            swapchain_data.width,
            swapchain_data.height,
        );
    }

    const imgui_draw_data = blk: {
        const data = athena_overlay.get_draw_data();
        if (data == null or data.?.TotalVtxCount < 1) return false;
        break :blk data.?;
    };

    var draw_buf_data = try get_overlay_draw(device_data, swapchain_data);
    try device_data.device_wrapper.resetCommandBuffer(draw_buf_data.command_buffer, .{});
    try device_data.device_wrapper.beginCommandBuffer(draw_buf_data.command_buffer, &.{});

    device_data.device_wrapper.cmdPipelineBarrier
    (
        draw_buf_data.command_buffer,
        .{ .all_graphics_bit = true, },
        .{ .all_graphics_bit = true, },
        .{},
        0,
        null,
        0,
        null,
        1,
        &@as
        (
            [1]vk.ImageMemoryBarrier,
            .{
                .{
                    .src_access_mask = .{ .color_attachment_write_bit = true, },
                    .dst_access_mask = .{ .color_attachment_write_bit = true, },
                    .old_layout = .present_src_khr,
                    .new_layout = .color_attachment_optimal,
                    .src_queue_family_index = queue_data.queue_family_index,
                    .dst_queue_family_index = device_data.graphic_queue.?.queue_family_index,
                    .image = swapchain_data.images.get(image_index),
                    .subresource_range = .{
                        .aspect_mask = .{ .color_bit = true, },
                        .base_mip_level = 0,
                        .level_count = 1,
                        .base_array_layer = 0,
                        .layer_count = 1,
                    },
                },
            },
        ),
    );

    device_data.device_wrapper.cmdBeginRenderPass
    (
        draw_buf_data.command_buffer,
        &.{
            .render_pass = swapchain_data.render_pass.?,
            .framebuffer = swapchain_data.framebuffers.get(image_index),
            .render_area = .{
                .extent = .{
                    .width = swapchain_data.width,
                    .height = swapchain_data.height,
                },
                // zero init
                .offset = .{
                    .x = 0,
                    .y = 0,
                },
            },
        },
        .@"inline",
    );

    const vertex_size: u64 = @as(u64, @intCast(imgui_draw_data.TotalVtxCount)) * @sizeOf(athena_overlay.DrawVert);
    const index_size: u64 = @as(u64, @intCast(imgui_draw_data.TotalIdxCount)) * @sizeOf(athena_overlay.DrawIdx);

    if (draw_buf_data.vertex_buffer_size orelse 0 < vertex_size)
    {
        if (draw_buf_data.vertex_buffer == null)
        {
            draw_buf_data.vertex_buffer = .null_handle;
        }

        if (draw_buf_data.vertex_buffer_mem == null)
        {
            draw_buf_data.vertex_buffer_mem = .null_handle;
        }

        try create_or_resize_buffer
        (
            device_data,
            &draw_buf_data.vertex_buffer.?,
            &draw_buf_data.vertex_buffer_mem.?,
            vertex_size,
            .{ .vertex_buffer_bit = true, },
        );

        draw_buf_data.vertex_buffer_size = vertex_size;
    }

    if (draw_buf_data.index_buffer_size orelse 0 < index_size)
    {
        if (draw_buf_data.index_buffer == null)
        {
            draw_buf_data.index_buffer = .null_handle;
        }

        if (draw_buf_data.index_buffer_mem == null)
        {
            draw_buf_data.index_buffer_mem = .null_handle;
        }

        try create_or_resize_buffer
        (
            device_data,
            &draw_buf_data.index_buffer.?,
            &draw_buf_data.index_buffer_mem.?,
            index_size,
            .{ .index_buffer_bit = true, },
        );

        draw_buf_data.index_buffer_size = index_size;
    }

    var vertex_dst: [*]athena_overlay.DrawVert = @ptrCast
    (
        @alignCast
        (
            try device_data.device_wrapper.mapMemory
            (
                device_data.device,
                draw_buf_data.vertex_buffer_mem.?,
                0,
                vertex_size,
                .{},
            )
        ),
    );

    var index_dst: [*]athena_overlay.DrawIdx = @ptrCast
    (
        @alignCast
        (
            try device_data.device_wrapper.mapMemory
            (
                device_data.device,
                draw_buf_data.index_buffer_mem.?,
                0,
                index_size,
                .{},
            )
        ),
    );

    for (athena_overlay.get_draw_data_draw_list(imgui_draw_data)) |cmd_list|
    {
        const vertex_buf = athena_overlay.get_draw_list_vertex_buffer(cmd_list.?);
        const index_buf = athena_overlay.get_draw_list_index_buffer(cmd_list.?);
        @memcpy(vertex_dst[0..vertex_buf.len], vertex_buf);
        @memcpy(index_dst[0..index_buf.len], index_buf);
        vertex_dst += vertex_buf.len;
        index_dst += index_buf.len;
    }

    try device_data.device_wrapper.flushMappedMemoryRanges
    (
        device_data.device,
        2,
        &@as
        (
            [2]vk.MappedMemoryRange,
            .{
                .{
                    .memory = draw_buf_data.vertex_buffer_mem.?,
                    .size = vk.WHOLE_SIZE,
                    // zero init
                    .offset = 0,
                },
                .{
                    .memory = draw_buf_data.index_buffer_mem.?,
                    .size = vk.WHOLE_SIZE,
                    // zero init
                    .offset = 0,
                },
            },
        ),
    );
    device_data.device_wrapper.unmapMemory(device_data.device, draw_buf_data.vertex_buffer_mem.?);
    device_data.device_wrapper.unmapMemory(device_data.device, draw_buf_data.index_buffer_mem.?);

    device_data.device_wrapper.cmdBindPipeline
    (
        draw_buf_data.command_buffer,
        .graphics,
        swapchain_data.pipeline.?,
    );
    device_data.device_wrapper.cmdBindDescriptorSets
    (
        draw_buf_data.command_buffer,
        .graphics,
        swapchain_data.pipeline_layout.?,
        0,
        1,
        &@as([1]vk.DescriptorSet, .{ swapchain_data.descriptor_set.?, }),
        0,
        null,
    );

    device_data.device_wrapper.cmdBindVertexBuffers
    (
        draw_buf_data.command_buffer,
        0,
        1,
        &@as([1]vk.Buffer, .{ draw_buf_data.vertex_buffer.?, }),
        &@as([1]vk.DeviceSize, .{ 0, }),
    );
    device_data.device_wrapper.cmdBindIndexBuffer
    (
        draw_buf_data.command_buffer,
        draw_buf_data.index_buffer.?,
        0,
        .uint16,
    );

    device_data.device_wrapper.cmdSetViewport
    (
        draw_buf_data.command_buffer,
        0,
        1,
        &@as
        (
            [1]vk.Viewport,
            .{
                .{
                    .x = 0,
                    .y = 0,
                    .width = imgui_draw_data.DisplaySize.x,
                    .height = imgui_draw_data.DisplaySize.y,
                    .min_depth = 0.0,
                    .max_depth = 1.0,
                },
            }
        ),
    );

    const scale: [2]f32 = .{
        2.0 / imgui_draw_data.DisplaySize.x,
        2.0 / imgui_draw_data.DisplaySize.y,
    };
    device_data.device_wrapper.cmdPushConstants
    (
        draw_buf_data.command_buffer,
        swapchain_data.pipeline_layout.?,
        .{ .vertex_bit = true, },
        @sizeOf(f32) * 0, // can't this just be 0?
        @sizeOf(f32) * 2,
        std.mem.asBytes(&scale),
    );

    const translate: [2]f32 = .{ -1.0, -1.0 };
    device_data.device_wrapper.cmdPushConstants
    (
        draw_buf_data.command_buffer,
        swapchain_data.pipeline_layout.?,
        .{ .vertex_bit = true, },
        @sizeOf(f32) * 2,
        @sizeOf(f32) * 2,
        std.mem.asBytes(&translate),
    );

    {
        var referenced_textures = try simple.BoundedArray(vk.DescriptorSet, 255).init(0);
        var vertex_offset: u32 = 0;
        var index_offset: u32 = 0;
        for (athena_overlay.get_draw_data_draw_list(imgui_draw_data)) |cmd_list|
        {
            for (athena_overlay.get_draw_list_command_buffer(cmd_list.?)) |cmd|
            {
                const x_pos: i32 = @intFromFloat(cmd.ClipRect.x - imgui_draw_data.DisplayPos.x);
                const y_pos: i32 = @intFromFloat(cmd.ClipRect.y - imgui_draw_data.DisplayPos.y);
                device_data.device_wrapper.cmdSetScissor
                (
                    draw_buf_data.command_buffer,
                    0,
                    1,
                    &@as
                    (
                        [1]vk.Rect2D,
                        .{
                            .{
                                .offset = .{
                                    .x = if (x_pos > 0) x_pos else 0,
                                    .y = if (y_pos > 0) y_pos else 0,
                                },
                                .extent = .{
                                    .width = @as(u32, @intFromFloat(cmd.ClipRect.z - cmd.ClipRect.x)),
                                    .height = @as(u32, @intFromFloat(cmd.ClipRect.w - cmd.ClipRect.y + 1.0)),
                                },
                            },
                        }
                    ),
                );

                const set = blk: {
                    if (cmd.TextureId != .null_handle)
                    {
                        const tex_ds: vk.DescriptorSet = @enumFromInt(@intFromEnum(cmd.TextureId));
                        for (swapchain_data.avatar_images.?.constSlice()) |avatar_image|
                        {
                            if (tex_ds == avatar_image.descriptor_set)
                            {
                                try referenced_textures.append(tex_ds);
                                break :blk tex_ds;
                            }
                        }
                    }

                    break :blk swapchain_data.descriptor_set.?;
                };
                device_data.device_wrapper.cmdBindDescriptorSets
                (
                    draw_buf_data.command_buffer,
                    .graphics,
                    swapchain_data.pipeline_layout.?,
                    0,
                    1,
                    &@as([1]vk.DescriptorSet, .{ set, }),
                    0,
                    null,
                );

                device_data.device_wrapper.cmdDrawIndexed
                (
                    draw_buf_data.command_buffer,
                    cmd.ElemCount,
                    1,
                    index_offset,
                    @intCast(vertex_offset),
                    0,
                );

                index_offset += cmd.ElemCount;
            }

            vertex_offset += @intCast(athena_overlay.get_draw_list_vertex_buffer(cmd_list.?).len);
        }

        if (referenced_textures.len != swapchain_data.avatar_images.?.len)
        {
            // collect list of indexes to unused textures for removal
            var unused_texture_indexes = try simple.BoundedArray(usize, 255).init(0);
            for (swapchain_data.avatar_images.?.constSlice(), 0..) |avatar_image, i|
            {
                const was_referenced = blk: {
                    for (referenced_textures.constSlice()) |ds|
                    {
                        if (ds == avatar_image.descriptor_set) break :blk true;
                    }

                    break :blk false;
                };

                if (!was_referenced) try unused_texture_indexes.append(i);
            }

            // order by largest to smallest index to allow for iterative removal
            std.sort.heap(usize, unused_texture_indexes.slice(), {}, std.sort.desc(usize));
            // free final list of unused avatar textures
            for (unused_texture_indexes.constSlice()) |it|
            {
                try destroy_swapchain_khr.destroy_avatar_image
                (
                    device_data.device,
                    device_data.device_wrapper,
                    swapchain_data.descriptor_pool.?,
                    &swapchain_data.avatar_images.?.slice()[it],
                );
                _ = swapchain_data.avatar_images.?.orderedRemove(it);
            }
        }
    }

    device_data.device_wrapper.cmdEndRenderPass(draw_buf_data.command_buffer);

    if (device_data.graphic_queue.?.queue_family_index != queue_data.queue_family_index)
    {
        device_data.device_wrapper.cmdPipelineBarrier
        (
            draw_buf_data.command_buffer,
            .{ .all_graphics_bit = true, },
            .{ .all_graphics_bit = true, },
            .{},
            0,
            null,
            0,
            null,
            1,
            &@as
            (
                [1]vk.ImageMemoryBarrier,
                .{
                    .{
                        .src_access_mask = .{ .color_attachment_write_bit = true, },
                        .dst_access_mask = .{ .color_attachment_write_bit = true, },
                        .old_layout = .present_src_khr,
                        .new_layout = .present_src_khr,
                        .src_queue_family_index = device_data.graphic_queue.?.queue_family_index,
                        .dst_queue_family_index = queue_data.queue_family_index,
                        .image = swapchain_data.images.get(image_index),
                        .subresource_range = .{
                            .aspect_mask = .{ .color_bit = true, },
                            .base_mip_level = 0,
                            .level_count = 1,
                            .base_array_layer = 0,
                            .layer_count = 1,
                        },
                    },
                },
            ),
        );
    }

    try device_data.device_wrapper.endCommandBuffer(draw_buf_data.command_buffer);

    if (wait_semaphore_count == 0 and device_data.graphic_queue.?.queue != queue_data.queue)
    {
        try device_data.device_wrapper.queueSubmit
        (
            queue_data.queue,
            1,
            &@as
            (
                [1]vk.SubmitInfo,
                .{
                    .{
                        .wait_semaphore_count = 0,
                        .p_wait_dst_stage_mask = &@as([1]vk.PipelineStageFlags, .{ .{ .all_commands_bit = true, }, }),
                        .command_buffer_count = 0,
                        .signal_semaphore_count = 1,
                        .p_signal_semaphores = &@as([1]vk.Semaphore, .{ draw_buf_data.cross_engine_semaphore, }),
                    },
                },
            ),
            .null_handle,
        );

        try device_data.device_wrapper.queueSubmit
        (
            device_data.graphic_queue.?.queue,
            1,
            &@as
            (
                [1]vk.SubmitInfo,
                .{
                    .{
                        .wait_semaphore_count = 1,
                        .p_wait_semaphores = &@as([1]vk.Semaphore, .{ draw_buf_data.cross_engine_semaphore, }),
                        .p_wait_dst_stage_mask = &@as([1]vk.PipelineStageFlags, .{ .{ .all_commands_bit = true, }, }),
                        .command_buffer_count = 1,
                        .p_command_buffers = &@as([1]vk.CommandBuffer, .{ draw_buf_data.command_buffer, }),
                        .signal_semaphore_count = 1,
                        .p_signal_semaphores = &@as([1]vk.Semaphore, .{ draw_buf_data.semaphore, }),
                    },
                },
            ),
            draw_buf_data.fence,
        );
    }
    else
    {
        var stages_wait_backing = try vklct.PipelineStageFlagsBacking.init(wait_semaphore_count);

        for (0..wait_semaphore_count) |i|
        {
            stages_wait_backing.buffer[i] = .{ .fragment_shader_bit = true, };
        }

        try device_data.device_wrapper.queueSubmit
        (
            device_data.graphic_queue.?.queue,
            1,
            &@as
            (
                [1]vk.SubmitInfo,
                .{
                    .{
                        .wait_semaphore_count = wait_semaphore_count,
                        .p_wait_semaphores = p_wait_semaphores,
                        .p_wait_dst_stage_mask = stages_wait_backing.constSlice().ptr,
                        .command_buffer_count = 1,
                        .p_command_buffers = &@as([1]vk.CommandBuffer, .{ draw_buf_data.command_buffer, }),
                        .signal_semaphore_count = 1,
                        .p_signal_semaphores = &@as([1]vk.Semaphore, .{ draw_buf_data.semaphore, }),
                    },
                },
            ),
            draw_buf_data.fence,
        );
    }

    return true;
}

fn before_present
(
    queue: vk.Queue,
    p_wait_semaphores: ?[*]const vk.Semaphore,
    wait_semaphore_count: u32,
    image_index: u32,
    device_data: *vklct.DeviceData,
    swapchain_data: *vklct.SwapchainData,
)
!bool
{
    const old_ctx = athena_overlay.use_overlay_context()
        catch unreachable;
    defer athena_overlay.restore_old_context(old_ctx);

    athena_overlay.is_draw_ready()
        catch |err| switch (err)
        {
            error.FontNotLoaded => return false,
            error.FontTextureRequiresReload =>
            {
                layer_global_state.free_font_allocator();
                swapchain_data.font_uploaded = false;
            },
        };

    if (swapchain_data.font_uploaded != null and !swapchain_data.font_uploaded.?)
    {
        try destroy_swapchain_khr.destroy_swapchain_internal
        (
            device_data.device,
            device_data.device_wrapper,
            swapchain_data,
            device_data,
        );

        device_data.most_recent_draw_data = null;
        swapchain_data.command_pool = null;
        swapchain_data.descriptor_layout = null;
        swapchain_data.descriptor_pool = null;
        swapchain_data.descriptor_set = null;
        swapchain_data.font_image_view = null;
        swapchain_data.font_image = null;
        swapchain_data.font_mem = null;
        swapchain_data.font_sampler = null;
        swapchain_data.font_uploaded = null;
        swapchain_data.image_count = null;
        swapchain_data.pipeline_layout = null;
        swapchain_data.pipeline = null;
        swapchain_data.render_pass = null;
        swapchain_data.upload_font_buffer_mem = null;
        swapchain_data.upload_font_buffer = null;
        swapchain_data.framebuffers.len = 0;
        swapchain_data.image_views.len = 0;
        swapchain_data.images.len = 0;

        try create_swapchain_khr.setup_swapchain
        (
            device_data.device,
            device_data.device_wrapper,
            swapchain_data,
            device_data.graphic_queue.?.queue_family_index,
        );
        return false;
    }

    return render_swapchain_display
    (
        queue,
        p_wait_semaphores,
        wait_semaphore_count,
        image_index,
        device_data,
        swapchain_data,
    );
}

pub fn QueuePresentKHR
(
    queue: vk.Queue,
    p_present_info: *const vk.PresentInfoKHR,
)
!vk.Result
{
    if (!layer_global_state.wrappers_global_lock.tryLock()) return .error_unknown;
    defer layer_global_state.wrappers_global_lock.unlock();

    var device_data = blk: {
        var ref_count: u32 = 0;
        var last_device_data: ?*vklct.DeviceData = null;
        for (layer_global_state.device_backing.?.map.values()) |*v|
        {
            for (v.queues.slice()) |*queue_data|
            {
                if (queue_data.queue == queue)
                {
                    ref_count += 1;
                    last_device_data = v;
                }
            }
        }

        switch (ref_count)
        {
            0 => return error.CouldNotFindQueueDevice,
            1 => break :blk last_device_data.?,
            else => return error.VulkanQueueRefCountMismanagement
        }
    };

    var final_result: vk.Result = .success;
    for (0..p_present_info.swapchain_count) |i|
    {
        const maybe_swapchain_data = layer_global_state.swapchain_backing.?.map.getPtr(p_present_info.p_swapchains[i]);
        if (maybe_swapchain_data == null)
        {
            final_result = .error_unknown;
            continue;
        }
        const swapchain_data = maybe_swapchain_data.?;

        const draw_necessary = try before_present
        (
            queue,
            p_present_info.p_wait_semaphores,
            p_present_info.wait_semaphore_count,
            p_present_info.p_image_indices[i],
            device_data,
            swapchain_data,
        );

        const swapchain_container: [1]vk.SwapchainKHR = .{ swapchain_data.swapchain.?, };
        const image_index_container: [1]u32 = .{ p_present_info.p_image_indices[i], };
        var semaphore_container: [1]vk.Semaphore = .{ .null_handle, };
        var present_info = p_present_info.*;
        if (draw_necessary)
        {
            present_info.swapchain_count = 1;
            present_info.p_swapchains = &swapchain_container;
            present_info.p_image_indices = &image_index_container;

            semaphore_container[0] = device_data.most_recent_draw_data.?.semaphore;
            present_info.p_wait_semaphores = &semaphore_container;
            present_info.wait_semaphore_count = 1;
        }

        const chain_result = device_data.device_wrapper.dispatch.vkQueuePresentKHR
        (
            queue,
            &present_info,
        );

        if (p_present_info.p_results) |p_results|
        {
            p_results[i] = chain_result;
        }

        if (chain_result != .success and final_result == .success)
        {
            final_result = chain_result;
        }
    }

    return final_result;
}
