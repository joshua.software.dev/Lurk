const builtin = @import("builtin");
const std = @import("std");


const native_endian = builtin.cpu.arch.endian();

const read_int_native = struct
{
    fn f(comptime T: type, buffer: *const [@divExact(@typeInfo(T).Int.bits, 8)]u8) T
    {
        return
            if (@hasDecl(std.mem, "readIntNative"))
                std.mem.readIntNative(T, buffer)
            else
                std.mem.readInt(T, buffer, native_endian);
    }
}.f;

fn bytes_to_u32_array(comptime string: []const u8) []const u32
{
    @setEvalBranchQuota(2000);
    comptime var out: [@divFloor(string.len, 4) + if (@mod(string.len, 4) > 0) 1 else 0]u32 = undefined;
    var stream = std.io.fixedBufferStream(string);
    var reader = stream.reader();

    var i = 0;
    while (true)
    {
        var buf: [4]u8 = undefined;
        switch (reader.read(&buf) catch unreachable)
        {
            0 => break,
            1 =>
            {
                buf[1] = 0;
                buf[2] = 0;
                buf[3] = 0;
            },
            2 =>
            {
                buf[2] = 0;
                buf[3] = 0;
            },
            3 =>
            {
                buf[3] = 0;
            },
            4 => {},
            else => unreachable
        }

        out[i] = read_int_native(u32, &buf);
        i += 1;
    }

    return &out;
}

pub const overlay_frag_spv: []const u32 = bytes_to_u32_array(@embedFile("imgui.frag.spv"));
pub const overlay_vert_spv: []const u32 = bytes_to_u32_array(@embedFile("imgui.vert.spv"));
