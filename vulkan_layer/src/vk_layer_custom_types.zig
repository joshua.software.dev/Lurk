const std = @import("std");

const vkl = @import("vulkan_layer_api_stubs.zig");

const simple = @import("simple");
const vk = @import("vk");


pub const DeviceData = struct
{
    device_id: u32,
    device: vk.Device,
    physical_device: vk.PhysicalDevice,
    instance: vk.Instance,
    get_device_proc_addr_func: vk.PfnGetDeviceProcAddr,
    set_device_loader_data_func: vkl.PfnSetDeviceLoaderData,
    graphic_queue: ?*QueueData,
    most_recent_draw_data: ?DrawBufferData,
    device_wrapper: LayerDeviceWrapper,
    queues: QueueDataBacking,
};
pub const DrawBufferData = struct
{
    command_buffer: vk.CommandBuffer,
    cross_engine_semaphore: vk.Semaphore,
    semaphore: vk.Semaphore,
    fence: vk.Fence,
    vertex_buffer: ?vk.Buffer,
    vertex_buffer_mem: ?vk.DeviceMemory,
    vertex_buffer_size: ?vk.DeviceSize,
    index_buffer: ?vk.Buffer,
    index_buffer_mem: ?vk.DeviceMemory,
    index_buffer_size: ?vk.DeviceSize,
};
pub const InstanceData = struct
{
    instance_id: u32,
    instance: vk.Instance,
    get_inst_proc_addr_func_ptr: vk.PfnGetInstanceProcAddr,
    instance_wrapper: LayerInstanceWrapper,
    physical_devices: PhysicalDeviceBacking,
};
pub const AvatarImage = struct
{
    hashmap_key: simple.BoundedArray(u8, 255),
    descriptor_set: vk.DescriptorSet,
    image: vk.Image,
    image_memory: vk.DeviceMemory,
    image_view: vk.ImageView,
    image_sampler: vk.Sampler,
    upload_buffer: vk.Buffer,
    upload_buffer_memory: vk.DeviceMemory,
};
pub const SwapchainData = struct
{
    swapchain_id: u32,
    command_pool: ?vk.CommandPool,
    descriptor_layout: ?vk.DescriptorSetLayout,
    descriptor_pool: ?vk.DescriptorPool,
    descriptor_set: ?vk.DescriptorSet,
    device: vk.Device,
    font_image_view: ?vk.ImageView,
    font_image: ?vk.Image,
    font_mem: ?vk.DeviceMemory,
    font_sampler: ?vk.Sampler,
    font_uploaded: ?bool,
    format: vk.Format,
    height: u32,
    image_count: ?u32,
    pipeline_layout: ?vk.PipelineLayout,
    pipeline: ?vk.Pipeline,
    render_pass: ?vk.RenderPass,
    swapchain: ?vk.SwapchainKHR,
    upload_font_buffer_mem: ?vk.DeviceMemory,
    upload_font_buffer: ?vk.Buffer,
    width: u32,
    framebuffers: FramebufferBacking,
    image_views: ImageViewBacking,
    images: ImageBacking,
    avatar_images: ?AvatarImageBacking,
};
pub const QueueData = struct
{
    device: vk.Device,
    queue_family_index: u32,
    queue_flags: vk.QueueFlags,
    queue: vk.Queue,
    fence: vk.Fence,
};

pub const LayerDeviceWrapper = vk.DeviceWrapper
(
    .{
        .allocateCommandBuffers = true,
        .allocateDescriptorSets = true,
        .allocateMemory = true,
        .beginCommandBuffer = true,
        .bindBufferMemory = true,
        .bindImageMemory = true,
        .cmdBeginRenderPass = true,
        .cmdBindDescriptorSets = true,
        .cmdBindIndexBuffer = true,
        .cmdBindPipeline = true,
        .cmdBindVertexBuffers = true,
        .cmdCopyBufferToImage = true,
        .cmdDraw = true,
        .cmdDrawIndexed = true,
        .cmdEndRenderPass = true,
        .cmdPipelineBarrier = true,
        .cmdPushConstants = true,
        .cmdSetScissor = true,
        .cmdSetViewport = true,
        .createBuffer = true,
        .createCommandPool = true,
        .createDescriptorPool = true,
        .createDescriptorSetLayout = true,
        .createFence = true,
        .createFramebuffer = true,
        .createGraphicsPipelines = true,
        .createImage = true,
        .createImageView = true,
        .createPipelineLayout = true,
        .createRenderPass = true,
        .createSampler = true,
        .createSemaphore = true,
        .createShaderModule = true,
        .createSwapchainKHR = true,
        .destroyBuffer = true,
        .destroyCommandPool = true,
        .destroyDescriptorPool = true,
        .destroyDescriptorSetLayout = true,
        .destroyDevice = true,
        .destroyFence = true,
        .destroyFramebuffer = true,
        .destroyImage = true,
        .destroyImageView = true,
        .destroyPipeline = true,
        .destroyPipelineLayout = true,
        .destroyRenderPass = true,
        .destroySampler = true,
        .destroySemaphore = true,
        .destroyShaderModule = true,
        .destroySwapchainKHR = true,
        .endCommandBuffer = true,
        .flushMappedMemoryRanges = true,
        .freeCommandBuffers = true,
        .freeDescriptorSets = true,
        .freeMemory = true,
        .getBufferMemoryRequirements = true,
        .getDeviceQueue = true,
        .getFenceStatus = true,
        .getImageMemoryRequirements = true,
        .getSwapchainImagesKHR = true,
        .mapMemory = true,
        .queuePresentKHR = true,
        .queueSubmit = true,
        .queueWaitIdle = true,
        .resetCommandBuffer = true,
        .resetFences = true,
        .unmapMemory = true,
        .updateDescriptorSets = true,
        .waitForFences = true,
    },
);
pub const LayerInstanceWrapper = vk.InstanceWrapper
(
    .{
        .destroyInstance = true,
        .enumerateDeviceExtensionProperties = true,
        .enumeratePhysicalDevices = true,
        .getPhysicalDeviceMemoryProperties = true,
        .getPhysicalDeviceQueueFamilyProperties = true,
    },
);

pub const AvatarImageBacking = simple.BoundedArray(AvatarImage, 255);
pub const FramebufferBacking = simple.BoundedArray(vk.Framebuffer, 32);
pub const ImageBacking = simple.BoundedArray(vk.Image, 32);
pub const ImageViewBacking = simple.BoundedArray(vk.ImageView, 32);
pub const PhysicalDeviceBacking = simple.BoundedArray(vk.PhysicalDevice, 8);
pub const PipelineStageFlagsBacking = simple.BoundedArray(vk.PipelineStageFlags, 32);
pub const QueueDataBacking = simple.BoundedArray(QueueData, 8);
pub const QueueFamilyPropsBacking = simple.BoundedArray(vk.QueueFamilyProperties, 256);

pub const InstanceBackingHashMap = simple.FixedSizeArrayHashMap
(
    std.AutoArrayHashMapUnmanaged(vk.Instance, InstanceData),
    8,
);
pub const DeviceBackingHashMap = simple.FixedSizeArrayHashMap
(
    std.AutoArrayHashMapUnmanaged(vk.Device, DeviceData),
    8,
);
pub const SwapchainBackingHashMap = simple.FixedSizeArrayHashMap
(
    std.AutoArrayHashMapUnmanaged(vk.SwapchainKHR, SwapchainData),
    8,
);
