const std = @import("std");


pub const AuthCodeData = struct { code: []const u8 };
pub const EventReadyData = struct { v: f64 };
pub const VoiceSpeakingStartStopData = struct { user_id: []const u8 };
pub const SubscribeEventData = struct { evt: []const u8 };
pub const VoiceChannelSelectData = struct
{
    channel_id: ?[]const u8,
    guild_id: ?[]const u8,
};

pub const AuthSuccessId = struct { id: []const u8 };
pub const AuthSuccessData = struct { user: AuthSuccessId };

pub const UserVoiceState = struct
{
    deaf: ?bool,
    mute: ?bool,
    self_deaf: ?bool,
    self_mute: ?bool,
    suppress: ?bool,
};
pub const UserInfo = struct
{
    avatar: ?[]const u8,
    id: []const u8,
};
pub const UserInfoAndVoiceState = struct
{
    nick: []const u8,
    volume: f64,
    user: UserInfo,
    voice_state: UserVoiceState,
};
pub const VoiceStateData = struct
{
    guild_id: ?[]const u8,
    id: ?[]const u8,
    name: []const u8,
    voice_states: []UserInfoAndVoiceState,
};

pub const AnyData = union(enum)
{
    AuthCodeData: AuthCodeData,
    AuthSuccessData: AuthSuccessData,
    EventReadyData: EventReadyData,
    SubscribeEventData: SubscribeEventData,
    UserInfoAndVoiceStateData: UserInfoAndVoiceState,
    VoiceChannelSelectData: VoiceChannelSelectData,
    VoiceSpeakingStartStopData: VoiceSpeakingStartStopData,
    VoiceStateData: VoiceStateData,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        const start_height = source.stackHeight();
        if (try source.next() != .object_begin) return error.UnexpectedToken;

        while ((try source.peekNextTokenType()) != .end_of_document)
        {
            const token: std.json.Token = try source.next();
            if (source.stackHeight() > (start_height + 1)) continue;

            switch (token)
            {
                .string, .allocated_string => |str|
                {
                    if (std.mem.eql(u8, str, "v"))
                    {
                        const out: @This() = .{
                            .EventReadyData = .{
                                .v = try std.json.innerParse(f64, allocator, source, options)
                            },
                        };
                        try source.skipUntilStackHeight(start_height);
                        return out;
                    }
                    else if (std.mem.eql(u8, str, "evt"))
                    {
                        const out: @This() = .{
                            .SubscribeEventData = .{
                                .evt = try std.json.innerParse([]const u8, allocator, source, options),
                            },
                        };
                        try source.skipUntilStackHeight(start_height);
                        return out;
                    }
                    else if (std.mem.eql(u8, str, "code"))
                    {
                        const out: @This() = .{
                            .AuthCodeData = .{
                                .code = std.json.innerParse([]const u8, allocator, source, options) catch "",
                            },
                        };
                        try source.skipUntilStackHeight(start_height);
                        return out;
                    }
                    else if (std.mem.eql(u8, str, "user"))
                    {
                        const out: @This() = .{
                            .AuthSuccessData = .{
                                .user = try std.json.innerParse(AuthSuccessId, allocator, source, options),
                            }
                        };
                        try source.skipUntilStackHeight(start_height);
                        return out;
                    }
                    else if (std.mem.eql(u8, str, "user_id"))
                    {
                        const out: @This() = .{
                            .VoiceSpeakingStartStopData = .{
                                .user_id = try std.json.innerParse([]const u8, allocator, source, options)
                            }
                        };
                        try source.skipUntilStackHeight(start_height);
                        return out;
                    }
                    else if
                    (
                        std.mem.eql(u8, str, "nick") or
                        std.mem.eql(u8, str, "volume") or
                        std.mem.eql(u8, str, "voice_state")
                    )
                    {
                        var nick: ?[]const u8 =
                            if (std.mem.eql(u8, str, "nick"))
                                try std.json.innerParse([]const u8, allocator, source, options)
                            else
                                null;
                        var volume: ?f64 =
                            if (std.mem.eql(u8, str, "volume"))
                                try std.json.innerParse(f64, allocator, source, options)
                            else
                                null;
                        var voice_state: ?UserVoiceState =
                            if (std.mem.eql(u8, str, "voice_state"))
                                try std.json.innerParse(UserVoiceState, allocator, source, options)
                            else
                                null;
                        var user_info: ?UserInfo = null;

                        while ((try source.peekNextTokenType()) != .end_of_document)
                        {
                            const obj_token = try source.next();
                            if (source.stackHeight() > (start_height + 1)) continue;

                            switch (obj_token)
                            {
                                .string, .allocated_string => |obj_str|
                                {
                                    if (std.mem.eql(u8, obj_str, "nick"))
                                    {
                                        nick = try std.json.innerParse(?[]const u8, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "volume"))
                                    {
                                        volume = try std.json.innerParse(f64, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "user"))
                                    {
                                        user_info = try std.json.innerParse(UserInfo, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "voice_state"))
                                    {
                                        voice_state = try std.json.innerParse(UserVoiceState, allocator, source, options);
                                    }
                                },
                                .object_end => if (source.stackHeight() == start_height) break,
                                else => {},
                            }
                        }

                        return .{
                            .UserInfoAndVoiceStateData = .{
                                .nick = nick.?,
                                .volume = volume.?,
                                .user = user_info.?,
                                .voice_state = voice_state.?
                            }
                        };
                    }
                    else if
                    (
                        std.mem.eql(u8, str, "channel_id") or
                        std.mem.eql(u8, str, "guild_id") or
                        std.mem.eql(u8, str, "id") or
                        std.mem.eql(u8, str, "name") or
                        std.mem.eql(u8, str, "voice_states")
                    )
                    {
                        var found_channel_id = std.mem.eql(u8, str, "channel_id");

                        var channel_id: ?[]const u8 =
                            if (std.mem.eql(u8, str, "channel_id"))
                                try std.json.innerParse(?[]const u8, allocator, source, options)
                            else
                                null;
                        var user_id: ?[]const u8 =
                            if (std.mem.eql(u8, str, "user_id"))
                                try std.json.innerParse([]const u8, allocator, source, options)
                            else
                                null;
                        var guild_id: ?[]const u8 =
                            if (std.mem.eql(u8, str, "guild_id"))
                                try std.json.innerParse(?[]const u8, allocator, source, options)
                            else
                                null;
                        var id: ?[]const u8 =
                            if (std.mem.eql(u8, str, "id"))
                                try std.json.innerParse(?[]const u8, allocator, source, options)
                            else
                                null;
                        var name: ?[]const u8 =
                            if (std.mem.eql(u8, str, "name"))
                                try std.json.innerParse([]const u8, allocator, source, options)
                            else
                                null;
                        var voice_states: ?[]UserInfoAndVoiceState =
                            if (std.mem.eql(u8, str, "voice_states"))
                                try std.json.innerParse([]UserInfoAndVoiceState, allocator, source, options)
                            else
                                null;

                        while ((try source.peekNextTokenType()) != .end_of_document)
                        {
                            const obj_token = try source.next();
                            if (source.stackHeight() > (start_height + 1)) continue;

                            switch (obj_token)
                            {
                                .string, .allocated_string => |obj_str|
                                {
                                    if (std.mem.eql(u8, obj_str, "channel_id"))
                                    {
                                        channel_id = try std.json.innerParse(?[]const u8, allocator, source, options);
                                        found_channel_id = true;
                                    }
                                    else if (std.mem.eql(u8, obj_str, "user_id"))
                                    {
                                        user_id = try std.json.innerParse([]const u8, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "guild_id"))
                                    {
                                        guild_id = try std.json.innerParse(?[]const u8, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "id"))
                                    {
                                        id = try std.json.innerParse(?[]const u8, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "name"))
                                    {
                                        name = try std.json.innerParse([]const u8, allocator, source, options);
                                    }
                                    else if (std.mem.eql(u8, obj_str, "voice_states"))
                                    {
                                        voice_states = try std.json.innerParse
                                        (
                                            []UserInfoAndVoiceState,
                                            allocator,
                                            source,
                                            options,
                                        );
                                    }
                                },
                                .object_end => if (source.stackHeight() == start_height) break,
                                else => {},
                            }
                        }

                        if (user_id != null)
                        {
                            return .{
                                .VoiceSpeakingStartStopData = .{ .user_id = user_id.? }
                            };
                        }
                        else if (found_channel_id)
                        {
                            return .{
                                .VoiceChannelSelectData = .{
                                    .channel_id = channel_id,
                                    .guild_id = guild_id,
                                }
                            };
                        }

                        return .{
                            .VoiceStateData = .{
                                .guild_id = guild_id,
                                .id = id,
                                .name = name.?,
                                .voice_states = voice_states.?,
                            }
                        };
                    }
                },
                .object_end => if (source.stackHeight() == start_height) break,
                else => {},
            }
        }

        return error.MissingField;
    }
};

pub const Command = enum
{
    DISPATCH,
    AUTHORIZE,
    AUTHENTICATE,
    GET_GUILD,
    GET_GUILDS,
    GET_CHANNEL,
    GET_CHANNELS,
    SUBSCRIBE,
    UNSUBSCRIBE,
    SET_USER_VOICE_SETTINGS,
    SELECT_VOICE_CHANNEL,
    GET_SELECTED_VOICE_CHANNEL,
    SELECT_TEXT_CHANNEL,
    GET_VOICE_SETTINGS,
    SET_VOICE_SETTINGS,
    SET_CERTIFIED_DEVICES,
    SET_ACTIVITY,
    SEND_ACTIVITY_JOIN_INVITE,
    CLOSE_ACTIVITY_REQUEST,
};

pub const Event = enum
{
    READY,
    ERROR,
    GUILD_STATUS,
    GUILD_CREATE,
    CHANNEL_CREATE,
    VOICE_CHANNEL_SELECT,
    VOICE_STATE_CREATE,
    VOICE_STATE_UPDATE,
    VOICE_STATE_DELETE,
    VOICE_SETTINGS_UPDATE,
    VOICE_CONNECTION_STATUS,
    SPEAKING_START,
    SPEAKING_STOP,
    MESSAGE_CREATE,
    MESSAGE_UPDATE,
    MESSAGE_DELETE,
    NOTIFICATION_CREATE,
    ACTIVITY_JOIN,
    ACTIVITY_SPECTATE,
    ACTIVITY_JOIN_REQUEST,
};

pub const Message = struct
{
    cmd: Command,
    evt: ?Event,
    nonce: ?[]const u8,
    data: ?AnyData,

    pub fn format
    (
        message_fmt: Message,
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    )
    !void
    {
        _ = options;
        _ = fmt_str;
        _ = try writer.write("Message");
        try std.json.stringify(message_fmt, .{}, writer);
    }
};

pub const AccessTokenHolder = struct
{
    access_token: []const u8,
};
