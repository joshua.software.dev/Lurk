const std = @import("std");

const msgt = @import("message_types.zig");

const simple = @import("simple");


pub const MAX_AVATAR_ID_LENGTH = 256;
pub const MAX_CHANNEL_ID_LENGTH = 256;
pub const MAX_GUILD_ID_LENGTH = 256;
pub const MAX_NICKNAME_LENGTH = 256;
pub const MAX_USER_ID_LENGTH = 256;

pub const AvatarId = simple.BoundedArray(u8, MAX_AVATAR_ID_LENGTH);
pub const ChannelId = simple.BoundedArray(u8, MAX_CHANNEL_ID_LENGTH);
pub const GuildId = simple.BoundedArray(u8, MAX_GUILD_ID_LENGTH);
pub const Nickname = simple.BoundedArray(u8, MAX_NICKNAME_LENGTH);
pub const UserId = simple.BoundedArray(u8, MAX_USER_ID_LENGTH);

pub const DiscordChannel = struct
{
    channel_id: ChannelId,
    guild_id: ?GuildId,

    pub fn same_channel_id(lhs: @This(), rhs: @This()) bool
    {
        return
            lhs.channel_id.len > 0 and
            rhs.channel_id.len > 0 and
            std.mem.eql(u8, lhs.channel_id.constSlice(), rhs.channel_id.constSlice());
    }

    pub fn same_guild_id(lhs: @This(), rhs: @This()) bool
    {
        return
            (lhs.guild_id == null and rhs.guild_id == null) or
            (
                lhs.guild_id != null and
                rhs.guild_id != null and
                std.mem.eql(u8, lhs.guild_id.?.constSlice(), rhs.guild_id.?.constSlice())
            );
    }

    pub fn reset(self: *@This()) void
    {
        self.channel_id.len = 0;
        self.guild_id = null;
    }
};

pub const AvatarImage = struct
{
    avatar_id: AvatarId,
    image_bytes: []const u8,
};

pub const DiscordUser = struct
{
    speaking: bool,
    muted: bool,
    deafened: bool,
    volume: u32,
    nickname: ?Nickname,
    user_id: UserId,
    avatar_id: ?AvatarId,
    avatar_image: ?*AvatarImage,
};
pub const UsersHashMap = simple.FixedSizeArrayHashMapAllocator(std.StringArrayHashMapUnmanaged(DiscordUser), 128);

pub const DiscordState = struct
{
    self_user_id: UserId,
    current_channel: DiscordChannel,
    image_backing_allocator: ?std.mem.Allocator,
    users_map_lock: std.Thread.Mutex,
    users_map_arena: std.heap.ArenaAllocator,
    users_map: UsersHashMap,
    avatar_map: ?std.StringArrayHashMap(AvatarImage),

    pub fn init(state_allocator: std.mem.Allocator, image_allocator: ?std.mem.Allocator) !@This()
    {
        var state: @This() = .{
            .self_user_id = try UserId.init(0),
            .current_channel = DiscordChannel
            {
                .channel_id = try ChannelId.init(0),
                .guild_id = try GuildId.init(0),
            },
            .image_backing_allocator = image_allocator,
            .users_map_lock = .{},
            .users_map_arena = std.heap.ArenaAllocator.init(state_allocator),
            .users_map = undefined,
            .avatar_map =
                if (image_allocator == null)
                    null
                else
                    std.StringArrayHashMap(AvatarImage).init(image_allocator.?)
        };
        state.users_map = try UsersHashMap.init(state.users_map_arena.allocator());
        return state;
    }

    pub fn free_avatar_images(self: *@This()) void
    {
        if (self.image_backing_allocator != null)
        {
            for (self.users_map.map.values()) |*user|
            {
                if (user.avatar_image) |avatar_image|
                {
                    self.image_backing_allocator.?.free(avatar_image.image_bytes);
                }
            }
        }

        self.avatar_map.?.clearAndFree();
    }

    pub fn deinit(self: *@This()) void
    {
        self.free_avatar_images();
        self.users_map_arena.deinit();
    }

    pub fn free_user_hashmap(self: *@This()) void
    {
        self.users_map.map.clearRetainingCapacity();
    }

    pub fn set_channel(self: *@This(), discord_conn: anytype, new_channel: ?DiscordChannel) !void
    {
        std.log.scoped(.WS).debug
        (
            "Channel/Guild change from ({s}/{s}) to ({s}/{s})",
            .{
                (
                    if (self.current_channel.channel_id.len > 0)
                        self.current_channel.channel_id.constSlice()
                    else
                        "null"
                ),
                (
                    if (self.current_channel.guild_id) |g|
                        if (g.len > 0)
                            g.constSlice()
                        else
                            "null"
                    else
                        "null"
                ),
                (
                    if (new_channel) |new|
                        new.channel_id.constSlice()
                    else
                        "null"
                ),
                (
                    if (new_channel) |new|
                        if (new.guild_id) |g|
                            g.constSlice()
                        else
                            "null"
                    else
                        "null"
                ),
            },
        );

        if
        (
            (new_channel == null) or
            (
                self.current_channel.same_channel_id(new_channel.?) and
                self.current_channel.same_guild_id(new_channel.?)
            )
        )
        {
            return;
        }

        if (self.current_channel.channel_id.len > 0)
        {
            try discord_conn.unsubscribe(.VOICE_STATE_CREATE, self.current_channel);
            try discord_conn.unsubscribe(.VOICE_STATE_UPDATE, self.current_channel);
            try discord_conn.unsubscribe(.VOICE_STATE_DELETE, self.current_channel);
            try discord_conn.unsubscribe(.SPEAKING_START, self.current_channel);
            try discord_conn.unsubscribe(.SPEAKING_STOP, self.current_channel);
        }

        try discord_conn.subscribe(.VOICE_STATE_CREATE, new_channel.?);
        try discord_conn.subscribe(.VOICE_STATE_UPDATE, new_channel.?);
        try discord_conn.subscribe(.VOICE_STATE_DELETE, new_channel.?);
        try discord_conn.subscribe(.SPEAKING_START, new_channel.?);
        try discord_conn.subscribe(.SPEAKING_STOP, new_channel.?);

        self.current_channel.channel_id.len = 0;
        try self.current_channel.channel_id.appendSlice(new_channel.?.channel_id.constSlice());

        if (new_channel.?.guild_id == null)
        {
            self.current_channel.guild_id = null;
            return;
        }
        self.current_channel.guild_id = try GuildId.init(0);
        try self.current_channel.guild_id.?.appendSlice(new_channel.?.guild_id.?.constSlice());
    }

    pub fn get_user_self(self: *@This()) ?DiscordUser
    {
        if (self.self_user_id == null) return null;
        if (self.users_map.get(self.self_user_id)) |user| return user;

        return null;
    }

    pub fn parse_or_update_one_voice_state(self: *@This(), voice_state: msgt.UserInfoAndVoiceState) !*DiscordUser
    {
        const result = self.users_map.map.getOrPutAssumeCapacity(voice_state.user.id);
        if (!result.found_existing)
        {
            result.value_ptr.* = .{
                .speaking = false,
                .muted = false,
                .deafened = false,
                .volume = 0,
                .nickname = null,
                .user_id = try UserId.init(0),
                .avatar_id = null,
                .avatar_image = null,
            };

            result.value_ptr.*.user_id.len = 0;
            try result.value_ptr.*.user_id.appendSlice(voice_state.user.id);
            result.key_ptr.* = result.value_ptr.*.user_id.constSlice();
        }

        if (voice_state.voice_state.self_mute != null)
        {
            result.value_ptr.*.muted = voice_state.voice_state.self_mute.?;
        }
        else if (voice_state.voice_state.mute != null)
        {
            result.value_ptr.*.muted = voice_state.voice_state.mute.?;
        }
        else if (voice_state.voice_state.suppress != null)
        {
            result.value_ptr.*.muted = voice_state.voice_state.suppress.?;
        }
        else
        {
            result.value_ptr.*.muted = false;
        }

        if (voice_state.voice_state.self_deaf != null)
        {
            result.value_ptr.*.deafened = voice_state.voice_state.self_deaf.?;
        }
        else if (voice_state.voice_state.deaf != null)
        {
            result.value_ptr.*.deafened = voice_state.voice_state.deaf.?;
        }
        else
        {
            result.value_ptr.*.deafened = false;
        }

        result.value_ptr.*.volume = @intFromFloat(voice_state.volume);

        result.value_ptr.*.nickname = try Nickname.init(0);
        try result.value_ptr.*.nickname.?.appendSlice(voice_state.nick);

        result.value_ptr.*.avatar_id = null;
        if (voice_state.user.avatar) |avatar|
        {
            result.value_ptr.*.avatar_id = try AvatarId.init(0);
            try result.value_ptr.*.avatar_id.?.appendSlice(avatar);
        }

        return result.value_ptr;
    }

    pub fn parse_voice_state_data(self: *@This(), dataMsg: msgt.VoiceStateData) !void
    {
        for (dataMsg.voice_states) |voice_state|
        {
            _ = try self.parse_or_update_one_voice_state(voice_state);
        }
    }

    pub fn write_users_data_to_write_stream_ascii(self: *@This(), writer: anytype) !void
    {
        try writer.print("nickname                         | muted | speaking\n", .{});

        for (self.users_map.value_store.constSlice()) |user|
        {
            var nickname_buffer = try Nickname.init(0);
            if (user.nickname) |nick|
            {
                nickname_buffer.len = 0;

                for (nick.constSlice()) |character|
                {
                    if (character < 128) // is ascii
                    {
                        try nickname_buffer.append(character);
                    }
                }
            }

            try writer.print
            (
                "{s: <32} | {: <5} | {}\n",
                .{
                    nickname_buffer.constSlice(),
                    user.muted,
                    user.speaking,
                }
            );
        }
    }
};
