const conn = @import("discord_conn.zig");
const state = @import("discord_state.zig");


pub const DiscordUser = state.DiscordUser;
pub const UsersHashMap = state.UsersHashMap;
pub const DiscordWsConn = conn.DiscordWsConn;
