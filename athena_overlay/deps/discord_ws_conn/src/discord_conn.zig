const builtin = @import("builtin");
const std = @import("std");

const mmr = @import("multi_message_reader.zig");
const msgt = @import("message_types.zig");
const state = @import("discord_state.zig");

const simple = @import("simple");
const uuid = @import("uuid");
const ws = @import("ws");


const AVATAR_API_URL = "https://cdn.discordapp.com/avatars/{s}/{s}.png"; // user_id, avatar_id
const AVATAR_MAX_SIZE = 1024 * 1024 * 8; // 8 Megabytes
const CLIENT_ID = "207646673902501888";
const EXPECTED_API_VERSION = 1;
const HOST = "127.0.0.1";
const HTTP_API_URL = "https://streamkit.discord.com/overlay/token";
const JSON_BUFFER_SIZE = 1024;
const PORT_RANGE: []const u16 = &.{ 6463, 6464, 6465, 6466, 6467, 6468, 6469, 6470, 6471, 6472, };

const HTTP_API_URI =
    std.Uri.parse(HTTP_API_URL)
    catch unreachable;
const WS_API_URI =
    std.Uri.parse(std.fmt.comptimePrint("ws://{s}:{d}/?v=1&client_id={s}", .{ HOST, PORT_RANGE[0], CLIENT_ID }))
    catch unreachable;
const ws_logger = std.log.scoped(.WS);


pub const LoggingSettings = struct
{
    output_path: []const u8,
    current_msg_index: u32 = 0,
    timestamp: i64 = 0,
};
pub const LoadImageFn = fn (std.mem.Allocator, []const u8) anyerror![]const u8;

pub const DiscordWsConn = struct
{
    connection_closed: bool = false,
    logging_settings: ?LoggingSettings,
    access_token: simple.BoundedArray(u8, 32),
    simple_http_client: simple.SimpleHttpClient,
    ws_conn: ?ws.UnbufferedConnection,
    state: state.DiscordState,
    load_image_fn: *const LoadImageFn,

    pub fn init
    (
        simple_http_client: simple.SimpleHttpClient,
        state_allocator: std.mem.Allocator,
        image_allocator: ?std.mem.Allocator,
        load_img_fn: *const LoadImageFn,
        settings: ?LoggingSettings,
    )
    !@This()
    {
        return .{
            .logging_settings =
                if (settings) |s|
                    .{
                        .output_path = s.output_path,
                        .current_msg_index = s.current_msg_index,
                        .timestamp = std.time.timestamp(),
                    }
                else
                    null,
            .access_token = try simple.BoundedArray(u8, 32).init(0),
            .simple_http_client = simple_http_client,
            .ws_conn = null,
            .state = try state.DiscordState.init(state_allocator, image_allocator),
            .load_image_fn = load_img_fn,
        };
    }

    pub fn connect(self: *@This()) !?std.Uri
    {
        var current_uri = WS_API_URI;

        for (PORT_RANGE) |current_port|
        {
            current_uri.port.? = current_port;
            var buf: [256]u8 = undefined;
            const conn = ws.connect_unbuffered
            (
                null,
                current_uri,
                &.{
                    .{ "Host", try std.fmt.bufPrint(&buf, "{s}:{d}", .{ HOST, current_port, }) },
                    .{ "Origin", "https://streamkit.discord.com" }
                },
            )
            catch |err|
            {
                if (err == error.ConnectionRefused)
                {
                    std.log.scoped(.WS).warn
                    (
                        "Connection Failed: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                        .{ current_uri },
                    );
                }

                if (err == error.ConnectionRefused and current_port < PORT_RANGE[PORT_RANGE.len - 2]) continue;
                return err;
            };

            self.ws_conn = conn;
            return current_uri;
        }

        return null;
    }

    pub fn close(self: *@This()) void
    {
        if (!self.connection_closed)
        {
            self.connection_closed = true;

            self.simple_http_client.deinit();
            if (self.ws_conn != null) self.ws_conn.?.deinit();
            self.state.deinit();
        }
    }

    pub fn send_ws_message(self: *@This(), object: anytype, options: std.json.StringifyOptions) !void
    {
        var buf: [JSON_BUFFER_SIZE]u8 = undefined;
        var stream = std.io.fixedBufferStream(&buf);
        const writer = stream.writer();

        try std.json.stringify(object, options, writer);

        const msg = stream.getWritten();
        try self.ws_conn.?.send(.text, msg);
        ws_logger.debug("sent: {s}", .{msg});
    }

    pub fn authenticate(self: *@This()) !void
    {
        try self.send_ws_message
        (
            .{
                .cmd = @tagName(msgt.Command.AUTHENTICATE),
                .args =
                .{ .access_token = self.access_token.constSlice() },
                .nonce = uuid.urn.serialize(uuid.v4.new())
            },
            .{ .emit_null_optional_fields = true }
        );
    }

    pub fn authorize_stage_1(self: *@This()) !void
    {
        try self.send_ws_message
        (
            .{
                .cmd = @tagName(msgt.Command.AUTHORIZE),
                .args =
                .{
                    .client_id = CLIENT_ID,
                    .scopes = &.{ "rpc", "messages.read", "rpc.notifications.read" },
                    .prompt = "none"
                },
                .nonce = uuid.urn.serialize(uuid.v4.new())
            },
            .{}
        );
    }

    pub fn authorize_stage_2(self: *@This(), auth_code: []const u8) !void
    {
        std.log.scoped(.WS).debug("Using HTTP Backend: {s}", .{ self.simple_http_client.mode });

        {
            var auth_buf: [64]u8 = undefined;
            var auth_stream = std.io.fixedBufferStream(&auth_buf);
            try std.json.stringify(.{ .code = auth_code, }, .{}, auth_stream.writer());
            const json_code = auth_stream.getWritten();

            var response_buf: [JSON_BUFFER_SIZE]u8 = undefined;
            var fba = std.heap.FixedBufferAllocator.init(&response_buf);

            const response = try self.simple_http_client.post_access_token
            (
                HTTP_API_URI,
                fba.allocator(),
                256,
                json_code,
            );
            defer fba.allocator().free(response);

            const token_holder = try std.json.parseFromSlice(msgt.AccessTokenHolder, fba.allocator(), response, .{});
            defer token_holder.deinit();

            self.access_token.len = 0;
            try self.access_token.appendSlice(token_holder.value.access_token);
        }

        try self.authenticate();
    }

    pub fn subscribe(self: *@This(), event: msgt.Event, channel: ?state.DiscordChannel) !void
    {
        if (channel == null)
        {
            try self.send_ws_message
            (
                .{
                    .args = @as(struct {}, .{}),
                    .cmd = @tagName(msgt.Command.SUBSCRIBE),
                    .evt = @tagName(event),
                    .nonce = &uuid.urn.serialize(uuid.v4.new())
                },
                .{ .emit_null_optional_fields = true }
            );
        }
        else
        {
            try self.send_ws_message
            (
                .{
                    .args = .{ .channel_id = channel.?.channel_id.constSlice() },
                    .cmd = @tagName(msgt.Command.SUBSCRIBE),
                    .evt = @tagName(event),
                    .nonce = &uuid.urn.serialize(uuid.v4.new())
                },
                .{ .emit_null_optional_fields = true }
            );
        }
    }

    pub fn unsubscribe(self: *@This(), event: msgt.Event, channel: ?state.DiscordChannel) !void
    {
        if (channel == null)
        {
            try self.send_ws_message
            (
                .{
                    .args = @as(struct {}, .{}),
                    .cmd = @tagName(msgt.Command.UNSUBSCRIBE),
                    .evt = @tagName(event),
                    .nonce = &uuid.urn.serialize(uuid.v4.new())
                },
                .{ .emit_null_optional_fields = true },
            );
        }
        else
        {
            try self.send_ws_message
            (
                .{
                    .args = .{ .channel_id = channel.?.channel_id.constSlice() },
                    .cmd = @tagName(msgt.Command.UNSUBSCRIBE),
                    .evt = @tagName(event),
                    .nonce = &uuid.urn.serialize(uuid.v4.new())
                },
                .{ .emit_null_optional_fields = true },
            );
        }
    }

    pub fn send_get_selected_voice_channel(self: *@This()) !void
    {
        try self.send_ws_message
        (
            .{
                .cmd = @tagName(msgt.Command.GET_SELECTED_VOICE_CHANNEL),
                .nonce = uuid.urn.serialize(uuid.v4.new())
            },
            .{ .emit_null_optional_fields = true },
        );
    }

    pub fn fetch_user_avatars(self: *@This()) !void
    {
        if (self.state.image_backing_allocator == null) return;

        ws_logger.debug("Fetching user avatars...", .{});

        var api_url_buf: [512]u8 = undefined;
        var inactive_ids = try simple.BoundedArray([]const u8, state.UsersHashMap.Capacity * 2).init(0);
        for (self.state.avatar_map.?.values()) |avatar_image|
        {
            try inactive_ids.append(avatar_image.avatar_id.constSlice());
        }

        for (self.state.users_map.map.values()) |*user|
        {
            const existing_avatar = blk: {
                if (user.avatar_id) |avatar_id|
                {
                    const result = try self.state.avatar_map.?.getOrPut(avatar_id.constSlice());
                    if (!result.found_existing)
                    {
                        result.value_ptr.*.avatar_id = try state.AvatarId.fromSlice(avatar_id.constSlice());
                        result.key_ptr.* = result.value_ptr.avatar_id.constSlice();
                    }
                    break :blk result;
                }

                const result = try self.state.avatar_map.?.getOrPut("0");
                if (!result.found_existing)
                {
                    result.value_ptr.*.avatar_id = try state.AvatarId.fromSlice("0");
                    result.key_ptr.* = result.value_ptr.avatar_id.constSlice();
                }
                break :blk result;
            };
            user.avatar_image = existing_avatar.value_ptr;
            for (inactive_ids.constSlice(), 0..) |avatar_id, i|
            {
                if (std.mem.eql(u8, existing_avatar.value_ptr.avatar_id.constSlice(), avatar_id))
                {
                    _ = inactive_ids.orderedRemove(i);
                    break;
                }
            }
            if (existing_avatar.found_existing) continue;

            const user_avatar_url =
                if (user.avatar_id) |avatar_id|
                    try std.fmt.bufPrint
                    (
                        &api_url_buf,
                        AVATAR_API_URL,
                        .{
                            user.user_id.constSlice(),
                            avatar_id.constSlice(),
                        }
                    )
                else
                    "https://cdn.discordapp.com/embed/avatars/0.png";

            ws_logger.debug("Fetching avatar for user: {s}|{s}", .{ user.nickname.?.constSlice(), user_avatar_url });
            const image = try self.simple_http_client.download_bytes
            (
                &.{
                    .{ "Referer", "https://streamkit.discord.com/overlay/voice", },
                },
                try std.Uri.parse(user_avatar_url),
                self.state.image_backing_allocator.?,
                AVATAR_MAX_SIZE,
            );

            existing_avatar.value_ptr.image_bytes = try self.load_image_fn
            (
                self.state.image_backing_allocator.?,
                image,
            );
        }

        // any ids remaining in this array after loading images now refer to
        // users not in the call anymore, and are thus invalid and ok to free
        for (inactive_ids.constSlice()) |leftover_id|
        {
            const result = self.state.avatar_map.?.fetchOrderedRemove(leftover_id);
            if (result != null)
            {
                self.state.image_backing_allocator.?.free(result.?.value.image_bytes);
            }
        }

        ws_logger.debug("Finished fetching user avatars", .{});
    }

    pub fn handle_message(self: *@This(), parsed_msg: msgt.Message) !bool
    {
        self.state.users_map_lock.lock();
        defer self.state.users_map_lock.unlock();

        switch (parsed_msg.cmd)
        {
            .DISPATCH =>
            {
                switch (parsed_msg.evt.?)
                {
                    .READY =>
                    {
                        const version_found: u32 = @intFromFloat(parsed_msg.data.?.EventReadyData.v);
                        if (version_found != EXPECTED_API_VERSION)
                        {
                            ws_logger.err
                            (
                                "Unexpected API Version: {d}, expected {d}",
                                .{ version_found, EXPECTED_API_VERSION }
                            );
                            return false;
                        }

                        try self.authenticate();
                    },
                    .VOICE_CHANNEL_SELECT =>
                    {
                        if (parsed_msg.data == null)
                        {
                            self.state.free_user_hashmap();
                            try self.state.set_channel(self, null);
                        }
                        else
                        {
                            const channel_id = parsed_msg.data.?.VoiceChannelSelectData.channel_id;
                            const guild_id = parsed_msg.data.?.VoiceChannelSelectData.guild_id;

                            try self.state.set_channel
                            (
                                self,
                                if (channel_id == null)
                                    null
                                else
                                    .{
                                        .channel_id = try state.ChannelId.fromSlice(channel_id.?),
                                        .guild_id =
                                            if (guild_id == null)
                                                null
                                            else
                                                try state.GuildId.fromSlice(guild_id.?),
                                    }
                            );
                        }
                    },
                    .VOICE_STATE_CREATE, .VOICE_STATE_UPDATE => |evt|
                    {
                        if (parsed_msg.data == null) return false;

                        const new_user = try self.state.parse_or_update_one_voice_state
                        (
                            parsed_msg.data.?.UserInfoAndVoiceStateData
                        );

                        if
                        (
                            evt == .VOICE_STATE_CREATE and
                            self.state.self_user_id.len > 0 and
                            std.mem.eql(u8, self.state.self_user_id.constSlice(), new_user.user_id.constSlice())
                        )
                        {
                            try self.send_get_selected_voice_channel();
                        }

                        try self.fetch_user_avatars();
                    },
                    .VOICE_STATE_DELETE =>
                    {
                        self.state.free_user_hashmap();
                        try self.send_get_selected_voice_channel();
                    },
                    .SPEAKING_START, .SPEAKING_STOP => |evt|
                    {
                        const user_id = parsed_msg.data.?.VoiceSpeakingStartStopData.user_id;

                        if (self.state.users_map.map.getPtr(user_id)) |user|
                        {
                            user.*.speaking = (evt == .SPEAKING_START);
                        }
                        else
                        {
                            ws_logger.warn("Could not find user with id: {d}", .{ user_id });
                        }
                    },
                    else => return error.UnexpectedEvent,
                }
            },
            .AUTHENTICATE =>
            {
                switch (parsed_msg.evt orelse .READY)
                {
                    .ERROR =>
                    {
                        self.access_token.len = 0;
                        self.state.free_user_hashmap();
                        try self.authorize_stage_1();
                    },
                    else =>
                    {
                        self.state.self_user_id.len = 0;
                        try self.state.self_user_id.appendSlice(parsed_msg.data.?.AuthSuccessData.user.id);

                        try self.subscribe(.VOICE_CHANNEL_SELECT, null);
                        try self.send_get_selected_voice_channel();
                    }
                }

            },
            .AUTHORIZE =>
            {
                ws_logger.debug("Attempting stage 2 auth...", .{});
                try self.authorize_stage_2(parsed_msg.data.?.AuthCodeData.code);
                ws_logger.debug("Stage 2 auth attempt finished.", .{});
            },
            .GET_SELECTED_VOICE_CHANNEL =>
            {
                if (parsed_msg.data == null)
                {
                    self.state.free_user_hashmap();
                    try self.state.set_channel(self, null);
                    return true;
                }

                try self.state.set_channel
                (
                    self,
                    if
                    (
                        parsed_msg.data.?.VoiceStateData.id == null or
                        parsed_msg.data.?.VoiceStateData.guild_id == null
                    )
                        null
                    else
                        .{
                            .channel_id = try state.ChannelId.fromSlice(parsed_msg.data.?.VoiceStateData.id.?),
                            .guild_id = try state.GuildId.fromSlice(parsed_msg.data.?.VoiceStateData.guild_id.?),
                        },
                );

                self.state.free_user_hashmap();
                if (parsed_msg.data != null)
                {
                    try self.state.parse_voice_state_data(parsed_msg.data.?.VoiceStateData);
                }

                try self.fetch_user_avatars();
            },
            .SUBSCRIBE => {},
            .UNSUBSCRIBE => {},
            else => return error.UnexpectedCommand,
        }

        return true;
    }

    pub fn receive_next_msg_buffered(self: *@This(), timeout_ns: u64, buffer: []u8) !bool
    {
        var parse_buf: [JSON_BUFFER_SIZE]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&parse_buf);

        const msg = try self.ws_conn.receiveIntoBuffer(buffer, timeout_ns);
        switch (msg.type)
        {
            .binary, .text =>
            {
                switch (msg.data)
                {
                    .slice => |slice|
                    {
                        const parsed = try std.json.parseFromSlice
                        (
                            msgt.Message,
                            fba.allocator(),
                            slice,
                            .{ .ignore_unknown_fields = true },
                        );
                        defer parsed.deinit();
                        ws_logger.debug("parsed message: {}", .{ parsed.value });

                        return self.handle_message(parsed.value);
                    },
                    .written => |amount_written|
                    {
                        const parsed = try std.json.parseFromSlice
                        (
                            msgt.Message,
                            fba.allocator(),
                            buffer[0..amount_written],
                            .{ .ignore_unknown_fields = true },
                        );
                        defer parsed.deinit();
                        ws_logger.debug("parsed message: {}", .{ parsed.value });

                        return self.handle_message(parsed.value);
                    },
                    else => return false,
                }
            },
            .close => return false,
            else => return false,
        }
    }

    pub fn receive_next_msg(self: *@This(), timeout_ns: u64) !bool
    {
        var parse_buf: [JSON_BUFFER_SIZE * 4]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&parse_buf);

        var msg_reader = try mmr.MultiMessageReader.init(&self.ws_conn.?, timeout_ns);
        const buf_reader = std.io.bufferedReader(msg_reader.reader());
        var msg_json_reader = std.json.reader(fba.allocator(), buf_reader);
        defer msg_json_reader.deinit();

        const raw_parsed_msg = try std.json.parseFromTokenSource
        (
            msgt.Message,
            fba.allocator(),
            &msg_json_reader,
            .{ .ignore_unknown_fields = true },
        );
        defer raw_parsed_msg.deinit();

        const parsed_msg = raw_parsed_msg.value;
        try msg_reader.ensure_finished();
        ws_logger.debug("parsed message: {}", .{ parsed_msg });

        if (self.logging_settings) |*ls|
        {
            var pathbuf: [std.os.PATH_MAX + 256]u8 = undefined;
            var pathfba = std.heap.FixedBufferAllocator.init(&pathbuf);

            const output_path = blk: {
                const out_filename = try std.fmt.allocPrint
                (
                    pathfba.allocator(),
                    "{}/{d}.json",
                    .{ ls.timestamp, ls.current_msg_index },
                );
                defer pathfba.allocator().free(out_filename);

                break :blk try std.fs.path.join(pathfba.allocator(), &.{ ls.output_path, out_filename, });
            };
            defer pathfba.allocator().free(output_path);

            std.fs.makeDirAbsolute(std.fs.path.dirname(output_path).?)
                catch |err| switch (err)
                {
                    error.PathAlreadyExists => {},
                    else => return err,
                };
            const out_fd = try std.fs.createFileAbsolute(output_path, .{ .truncate = false, .exclusive = true, });
            const writer = out_fd.writer();

            try std.json.stringify
            (
                parsed_msg,
                .{
                    .emit_null_optional_fields = true,
                    .escape_unicode = true,
                    .whitespace = .indent_4,
                },
                writer,
            );

            ls.current_msg_index += 1;
        }

        return try self.handle_message(parsed_msg);
    }
};
