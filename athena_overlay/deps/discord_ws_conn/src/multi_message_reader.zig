const std = @import("std");

const ws = @import("ws");


pub const MultiMessageReader = struct
{
    conn: *ws.UnbufferedConnection,
    message: ws.UnbufferedMessage,
    bytes_left: ?u64,
    message_complete: bool,
    timeout_ns: u64,

    pub fn init(conn: *ws.UnbufferedConnection, timeout_ns: u64) !@This()
    {
        return .{
            .conn = conn,
            .message = try conn.receiveUnbuffered(0, timeout_ns),
            .bytes_left = null,
            .message_complete = false,
            .timeout_ns = timeout_ns,
        };
    }

    pub fn read(self: *@This(), dest: []u8) anyerror!usize
    {
        if (self.message_complete) return 0;

        switch (self.message.type)
        {
            .binary, .text =>
            {
                switch (self.message.data)
                {
                    .slice => |slice|
                    {
                        if (slice.len > dest.len) return error.OutOfMemory;

                        @memcpy(dest[0..slice.len], slice[0..]);
                        self.message_complete = true;
                        return slice.len;
                    },
                    .reader => |r|
                    {
                        if (self.bytes_left == null) self.bytes_left = r.message_length;

                        const max_read = @min(self.bytes_left.?, dest.len);
                        const num =
                            if (self.bytes_left.? > 0)
                                try r.stream_reader.read(dest[0..max_read])
                            else
                                0;

                        if (self.bytes_left.? > 0)
                        {
                            // do nothing
                        }
                        else if (!self.message.data.reader.message_complete)
                        {
                            self.bytes_left = null;
                            self.message = try self.conn.receiveUnbuffered(0, self.timeout_ns);
                        }
                        else
                        {
                            self.message_complete = true;
                        }

                        self.bytes_left = self.bytes_left.? - num;
                        return num;
                    },
                    else => return error.UnexpectedMessageDataType,
                }
            },
            .close =>
            {
                self.message_complete = true;
                return 0;
            },
            else => return error.UnexpectedMessageDataType,
        }
    }

    pub fn ensure_finished(self: *@This()) !void
    {
        switch (self.message.type)
        {
            .binary, .text =>
            {
                switch (self.message.data)
                {
                    .reader => |r|
                    {
                        if (self.bytes_left != null and self.bytes_left.? > 0)
                        {
                            try r.stream_reader.skipBytes(self.bytes_left.?, .{});
                        }
                    },
                    else => {},
                }
            },
            else => {},
        }
    }

    pub fn reader(self: *@This()) std.io.Reader(*@This(), anyerror, read)
    {
        return .{ .context = self };
    }
};
