const blacklist = @import("blacklist_processes.zig");
pub const BlacklistProcess = blacklist.BlacklistProcess;
pub const does_process_name_match = blacklist.does_process_name_match;
pub const is_this_process_blacklisted = blacklist.is_this_process_blacklisted;

const bounded_array = @import("bounded_array.zig");
pub const BoundedArray = bounded_array.BoundedArray;
pub const BoundedArrayAligned = bounded_array.BoundedArrayAligned;

const bounded_array_hash_map = @import("bounded_array_hash_map.zig");
pub const AutoBoundedArrayHashMap = bounded_array_hash_map.AutoBoundedArrayHashMap;
pub const BoundedArrayHashMap = bounded_array_hash_map.BoundedArrayHashMap;
pub const StringBoundedArrayHashMap = bounded_array_hash_map.StringBoundedArrayHashMap;

pub const comptime_allocator = @import("comptime_allocator.zig").comptime_allocator;

const expand = @import("expand_path.zig");
pub const RealPath = expand.RealPath;
pub const expand_path = expand.expand_path;
pub const expand_realpath = expand.expand_realpath;

const fixed_size_array_hashmap = @import("fixed_size_array_hashmap.zig");
pub const FixedSizeArrayHashMap = fixed_size_array_hashmap.FixedSizeArrayHashMap;
pub const FixedSizeArrayHashMapAllocator = fixed_size_array_hashmap.FixedSizeArrayHashMapAllocator;

pub const http_settings = @import("http/settings.zig");
pub const SimpleHttpClient = @import("http/simple_http_client.zig").SimpleHttpClient;
const curl_client = @import("http/curl/curl_client.zig");
pub const CurlClient = curl_client.CurlClient;
pub const init_curl_client = curl_client.init;
const iguana_tls_client = @import("http/iguanaTLS/iguana_tls_client.zig");
pub const IguanaTlsClient = iguana_tls_client.IguanaTlsClient;
pub const init_iguana_tls_client = iguana_tls_client.init;
const std_http_client = @import("http/std_http/std_http.zig");
pub const StdHttpClient = std_http_client.StdHttpClient;
pub const init_std_http_client = std_http_client.init;

pub const jsonc = @import("jsonc.zig");

const sha256 = @import("sha256.zig");
pub const sha256sum_to_array = sha256.sha256sum_to_array;

const builtin = @import("builtin");
const std = @import("std");
pub const URL_FORMAT_TEMPLATE =
    if (builtin.zig_version.order(std.SemanticVersion.parse("0.11.0") catch unreachable) == .gt)
        "{+/?}"
    else
        "{+/}";
pub const URL_FORMAT_TEMPLATE_WITH_SCHEME =
    if (builtin.zig_version.order(std.SemanticVersion.parse("0.11.0") catch unreachable) == .gt)
        "{}"
    else
        "{+/}";
