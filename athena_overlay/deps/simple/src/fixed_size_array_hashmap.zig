const std = @import("std");


/// A single entry in the lookup acceleration structure.  These structs
/// are found in an array after the IndexHeader.  Hashes index into this
/// array, and linear probing is used for collisions.
fn Index(comptime I: type) type {
    return extern struct {
        const Self = @This();

        /// The index of this entry in the backing store.  If the index is
        /// empty, this is empty_sentinel.
        entry_index: I,

        /// The distance between this slot and its ideal placement.  This is
        /// used to keep maximum scan length small.  This value is undefined
        /// if the index is empty.
        distance_from_start_index: I,

        /// The special entry_index value marking an empty slot.
        const empty_sentinel = ~@as(I, 0);

        /// A constant empty index
        const empty = Self{
            .entry_index = empty_sentinel,
            .distance_from_start_index = undefined,
        };

        /// Checks if a slot is empty
        fn isEmpty(idx: Self) bool {
            return idx.entry_index == empty_sentinel;
        }

        /// Sets a slot to empty
        fn setEmpty(idx: *Self) void {
            idx.entry_index = empty_sentinel;
            idx.distance_from_start_index = undefined;
        }
    };
}

const CapacityIndexType = enum { u8, u16, u32 };

fn capacityIndexType(bit_index: u8) CapacityIndexType {
    if (bit_index <= 8)
        return .u8;
    if (bit_index <= 16)
        return .u16;
    std.debug.assert(bit_index <= 32);
    return .u32;
}

fn capacityIndexSize(bit_index: u8) usize {
    switch (capacityIndexType(bit_index)) {
        .u8 => return @sizeOf(Index(u8)),
        .u16 => return @sizeOf(Index(u16)),
        .u32 => return @sizeOf(Index(u32)),
    }
}

/// the byte size of the index must fit in a usize.  This is a power of two
/// length * the size of an Index(u32).  The index is 8 bytes (3 bits repr)
/// and max_usize + 1 is not representable, so we need to subtract out 4 bits.
const max_representable_index_len = @bitSizeOf(usize) - 4;
const max_bit_index =
    if (max_representable_index_len < 32)
        max_representable_index_len
    else
        32;
const min_bit_index = 5;
const max_capacity = (1 << max_bit_index) - 1;
const index_capacities = blk: {
    var caps: [max_bit_index + 1]u32 = undefined;
    for (caps[0..max_bit_index], 0..) |*item, i| {
        item.* = (1 << i) * 3 / 5;
    }
    caps[max_bit_index] = max_capacity;
    break :blk caps;
};

/// This struct is trailed by two arrays of length indexes_len
/// of integers, whose integer size is determined by indexes_len.
/// These arrays are indexed by constrainIndex(hash).  The
/// entryIndexes array contains the index in the dense backing store
/// where the entry's data can be found.  Entries which are not in
/// use have their index value set to emptySentinel(I).
/// The entryDistances array stores the distance between an entry
/// and its ideal hash bucket.  This is used when adding elements
/// to balance the maximum scan length.
const IndexHeader = struct {
    /// This field tracks the total number of items in the arrays following
    /// this header.  It is the bit index of the power of two number of indices.
    /// This value is between min_bit_index and max_bit_index, inclusive.
    bit_index: u8 align(@alignOf(u32)),

    fn findBitIndex(comptime desired_capacity: usize) u8 {
        if (desired_capacity > max_capacity) @compileError("HashMap requested is too large to allocate");
        var new_bit_index = @as(u8, @intCast(std.math.log2_int_ceil(usize, desired_capacity)));
        if (desired_capacity > index_capacities[new_bit_index]) new_bit_index += 1;
        if (new_bit_index < min_bit_index) new_bit_index = min_bit_index;
        std.debug.assert(desired_capacity <= index_capacities[new_bit_index]);
        return new_bit_index;
    }

    fn getRequiredAllocationSize(new_bit_index: u8) usize
    {
        const len = @as(usize, 1) << @as(std.math.Log2Int(usize), @intCast(new_bit_index));
        const index_size = capacityIndexSize(new_bit_index);
        const nbytes = @sizeOf(IndexHeader) + index_size * len;
        return nbytes;
    }

    // Verify that the header has sufficient alignment to produce aligned arrays.
    comptime {
        if (@alignOf(u32) > @alignOf(IndexHeader))
            @compileError("IndexHeader must have a larger alignment than its indexes!");
    }
};

pub fn array_hashmap_calculate_approximate_memory
(
    comptime array_hash_map_type: anytype,
    comptime desired_capacity: comptime_int
)
usize
{
    const fields = std.meta.fields(array_hash_map_type.Data);
    const sizes = blk: {
        const Data = struct {
            size: usize,
            size_index: usize,
            alignment: usize,
        };
        var data: [fields.len]Data = undefined;
        for (fields, 0..) |field_info, i| {
            data[i] = .{
                .size = @sizeOf(field_info.type),
                .size_index = i,
                .alignment = if (@sizeOf(field_info.type) == 0) 1 else field_info.alignment,
            };
        }
        const Sort = struct {
            fn lessThan(context: void, lhs: Data, rhs: Data) bool {
                _ = context;
                return lhs.alignment > rhs.alignment;
            }
        };
        std.mem.sort(Data, &data, {}, Sort.lessThan);
        var sizes_bytes: [fields.len]usize = undefined;
        var field_indexes: [fields.len]usize = undefined;
        for (data, 0..) |elem, i| {
            sizes_bytes[i] = elem.size;
            field_indexes[i] = elem.size_index;
        }
        break :blk .{
            .bytes = sizes_bytes,
            .fields = field_indexes,
        };
    };

    comptime var elem_bytes: usize = 0;
    inline for (sizes.bytes) |size| elem_bytes += size;
    const required_element_space: usize = elem_bytes * desired_capacity;
    const required_header_space = IndexHeader.getRequiredAllocationSize(IndexHeader.findBitIndex(desired_capacity));

    return
        // header is not needed when below linear scan threshold (8 by default)
        (required_header_space * @intFromBool(desired_capacity > 8)) +
        // excess padding in case calculations are wrong
        8 +
        // this is mostly right, I think.
        required_element_space;
}

pub fn FixedSizeArrayHashMap
(
    comptime array_hash_map_type: type,
    comptime desired_capacity: comptime_int,
)
type
{
    return extern struct
    {
        pub const HashMapType = array_hash_map_type;
        pub const Capacity: usize = desired_capacity;

        fba: *std.heap.FixedBufferAllocator,
        map: *HashMapType,
        buffer: [
            @sizeOf(std.heap.FixedBufferAllocator) +
            @sizeOf(HashMapType) +
            array_hashmap_calculate_approximate_memory(HashMapType, Capacity)
        ]u8,

        pub fn init(self: *@This()) !void
        {
            {
                var temp_fba = std.heap.FixedBufferAllocator.init(&self.buffer);
                self.fba = try temp_fba.allocator().create(std.heap.FixedBufferAllocator);
                self.fba.* = std.heap.FixedBufferAllocator.init(&self.buffer);
                self.fba.*.end_index = temp_fba.end_index;
            }

            self.map = try self.fba.allocator().create(HashMapType);

            if (@hasField(HashMapType, "unmanaged"))
            {
                self.map.* = HashMapType.init(self.fba.*.allocator());
                try self.map.*.unmanaged.entries.setCapacity(self.map.*.allocator, Capacity);
                try self.map.*.unmanaged.reIndex(self.map.*.allocator);
            }
            else
            {
                self.map.* = .{};
                try self.map.*.entries.setCapacity(self.fba.*.allocator(), Capacity);
                try self.map.*.reIndex(self.fba.*.allocator());
            }
        }
    };
}

pub fn FixedSizeArrayHashMapAllocator
(
    comptime array_hash_map_type: type,
    comptime desired_capacity: comptime_int,
)
type
{
    return extern struct
    {
        pub const HashMapType = array_hash_map_type;
        pub const Capacity: usize = desired_capacity;
        pub const MemoryRequirements =
            @sizeOf(HashMapType) +
            array_hashmap_calculate_approximate_memory(HashMapType, Capacity);

        map: *HashMapType,

        pub fn init(allocator: std.mem.Allocator) !@This()
        {
            const state: @This() = .{
                .map = try allocator.create(HashMapType),
            };

            if (@hasField(HashMapType, "unmanaged"))
            {
                state.map.* = HashMapType.init(allocator);
                try state.map.*.unmanaged.entries.setCapacity(state.map.*.allocator, Capacity);
                try state.map.*.unmanaged.reIndex(state.map.*.allocator);
            }
            else
            {
                state.map.* = .{};
                try state.map.*.entries.setCapacity(allocator, Capacity);
                try state.map.*.reIndex(allocator);
            }

            return state;
        }
    };
}
