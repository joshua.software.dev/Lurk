const std = @import("std");
const BoundedArray = @import("bounded_array.zig").BoundedArray;


// API Parity with std.array_hash_map.ArrayHashMap
// ----
// pub fn init() !@This()                                                              | pub fn init(allocator: Allocator) Self
// pub fn initContext(ctx: Context) !@This()                                           | pub fn initContext(allocator: Allocator, ctx: Context) Self
// pub fn deinit(self: *@This()) void                                                  | pub fn deinit(self: *Self) void
// pub fn clearRetainingCapacity(self: *@This()) void                                  | pub fn clearRetainingCapacity(self: *Self) void
// pub fn clearAndFree(self: *@This()) void                                            | pub fn clearAndFree(self: *Self) void
// pub fn count(self: @This()) usize                                                   | pub fn count(self: Self) usize
// pub fn keys(self: *@This()) []K                                                     | pub fn keys(self: Self) []K
// pub fn values(self: *@This()) []V                                                   | pub fn values(self: Self) []V
// pub fn iterator(self: *@This()) Iterator                                            | pub fn iterator(self: *const Self) Iterator
// pub fn getOrPut(self: *@This(), key: K) !GetOrPutResult                             | pub fn getOrPut(self: *Self, key: K) !GetOrPutResult
//                                                                                     | pub fn getOrPutAdapted(self: *Self, key: anytype, ctx: anytype) !GetOrPutResult
// pub fn getOrPutAssumeCapacity(self: *@This(), key: K) GetOrPutResult                | pub fn getOrPutAssumeCapacity(self: *Self, key: K) GetOrPutResult
//                                                                                     | pub fn getOrPutAssumeCapacityAdapted(self: *Self, key: anytype, ctx: anytype) GetOrPutResult
// pub fn getOrPutValue(self: *@This(), key: K, value: V) !GetOrPutResult              | pub fn getOrPutValue(self: *Self, key: K, value: V) !GetOrPutResult
// pub fn getOrPutValueAssumeCapacity(self: *@This(), key: K, value: V) GetOrPutResult |
// pub fn capacity(self: @This()) usize                                                | pub fn capacity(self: Self) usize
// pub fn put(self: *@This(), key: K, value: V) !void                                  | pub fn put(self: *Self, key: K, value: V) !void
// pub fn putNoClobber(self: *@This(), key: K, value: V) !void                         | pub fn putNoClobber(self: *Self, key: K, value: V) !void
// pub fn putAssumeCapacity(self: *@This(), key: K, value: V) void                     | pub fn putAssumeCapacity(self: *Self, key: K, value: V) void
// pub fn putAssumeCapacityNoClobber(self: *@This(), key: K, value: V) void            | pub fn putAssumeCapacityNoClobber(self: *Self, key: K, value: V) void
// pub fn fetchPut(self: *@This(), key: K, value: V) !?KV                              | pub fn fetchPut(self: *Self, key: K, value: V) !?KV
// pub fn fetchPutAssumeCapacity(self: *@This(), key: K, value: V) ?KV                 | pub fn fetchPutAssumeCapacity(self: *Self, key: K, value: V) ?KV
// pub fn getEntry(self: *@This(), key: K) ?Entry                                      | pub fn getEntry(self: Self, key: K) ?Entry
//                                                                                     | pub fn getEntryAdapted(self: Self, key: anytype, ctx: anytype) ?Entry
// pub fn getIndex(self: @This(), key: K) ?usize                                       | pub fn getIndex(self: Self, key: K) ?usize
//                                                                                     | pub fn getIndexAdapted(self: Self, key: anytype, ctx: anytype) ?usize
// pub fn get(self: @This(), key: K) ?V                                                | pub fn get(self: Self, key: K) ?V
//                                                                                     | pub fn getAdapted(self: Self, key: anytype, ctx: anytype) ?V
// pub fn getPtr(self: *@This(), key: K) ?*V                                           | pub fn getPtr(self: Self, key: K) ?*V
//                                                                                     | pub fn getPtrAdapted(self: Self, key: anytype, ctx: anytype) ?*V
// pub fn getKey(self: @This(), key: K) ?K                                             | pub fn getKey(self: Self, key: K) ?K
//                                                                                     | pub fn getKeyAdapted(self: Self, key: anytype, ctx: anytype) ?K
// pub fn getKeyPtr(self: *@This(), key: K) ?*K                                        | pub fn getKeyPtr(self: Self, key: K) ?*K
//                                                                                     | pub fn getKeyPtrAdapted(self: Self, key: anytype, ctx: anytype) ?*K
// pub fn contains(self: @This(), key: K) bool                                         | pub fn contains(self: Self, key: K) bool
//                                                                                     | pub fn containsAdapted(self: Self, key: anytype, ctx: anytype) bool
// pub fn fetchSwapRemove(self: *@This(), key: K) ?KV                                  | pub fn fetchSwapRemove(self: *Self, key: K) ?KV
//                                                                                     | pub fn fetchSwapRemoveAdapted(self: *Self, key: anytype, ctx: anytype) ?KV
// pub fn fetchOrderedRemove(self: *@This(), key: K) ?KV                               | pub fn fetchOrderedRemove(self: *Self, key: K) ?KV
//                                                                                     | pub fn fetchOrderedRemoveAdapted(self: *Self, key: anytype, ctx: anytype) ?KV
// pub fn swapRemove(self: *@This(), key: K) bool                                      | pub fn swapRemove(self: *Self, key: K) bool
//                                                                                     | pub fn swapRemoveAdapted(self: *Self, key: anytype, ctx: anytype) bool
// pub fn orderedRemove(self: *Self, key: K) bool                                      | pub fn orderedRemove(self: *Self, key: K) bool
//                                                                                     | pub fn orderedRemoveAdapted(self: *Self, key: anytype, ctx: anytype) bool
// pub fn swapRemoveAt(self: *@This(), index: usize) void                              | pub fn swapRemoveAt(self: *Self, index: usize) void
// pub fn orderedRemoveAt(self: *@This(), index: usize) void                           | pub fn orderedRemoveAt(self: *Self, index: usize) void
// pub fn clone(self: @This()) !@This()                                                | pub fn clone(self: Self) !Self
// pub fn cloneWithAllocator(self: @This(), allocator: std.mem.Allocator) !@This()     | pub fn cloneWithAllocator(self: Self, allocator: Allocator) !Self
//                                                                                     | pub fn cloneWithContext(self: Self, ctx: anytype) !ArrayHashMap(K, V, @TypeOf(ctx), store_hash)
//                                                                                     | pub fn cloneWithAllocatorAndContext(self: Self, allocator: Allocator, ctx: anytype) !ArrayHashMap(K, V, @TypeOf(ctx), store_hash)
// pub fn move(self: *@This()) @This()                                                 | pub fn move(self: *Self) Self
//                                                                                     | pub fn reIndex(self: *Self) !void
//                                                                                     | pub fn sort(self: *Self, sort_ctx: anytype) void
// pub fn shrinkRetainingCapacity(self: *@This(), new_len: usize) void                 | pub fn shrinkRetainingCapacity(self: *Self, new_len: usize) void
// pub fn shrinkAndFree(self: *@This(), new_len: usize) void                           | pub fn shrinkAndFree(self: *Self, new_len: usize) void
// pub fn pop(self: *@This()) KV                                                       | pub fn pop(self: *Self) KV
// pub fn popOrNull(self: *@This()) ?KV                                                | pub fn popOrNull(self: *Self) ?KV
// ----

pub fn AutoBoundedArrayHashMap(comptime K: type, comptime V: type, comptime capacity: comptime_int) type
{
    return BoundedArrayHashMap
    (
        K,
        V,
        std.array_hash_map.AutoContext(K),
        !std.array_hash_map.autoEqlIsCheap(K),
        capacity,
    );
}

pub fn StringBoundedArrayHashMap(comptime V: type, comptime capacity: comptime_int) type
{
    return BoundedArrayHashMap([]const u8, V, std.array_hash_map.StringContext, true, capacity);
}

pub fn BoundedArrayHashMap
(
    comptime K: type,
    comptime V: type,
    comptime Context: type,
    comptime store_hash: bool,
    comptime desired_capacity: comptime_int,
)
type
{
    return struct
    {
        ctx: Context,
        hash_store: if (store_hash) BoundedArray(u32, desired_capacity) else void,
        key_store: BoundedArray(K, desired_capacity),
        value_store: BoundedArray(V, desired_capacity),

        comptime
        {
            std.hash_map.verifyContext(Context, K, K, u32, true);
        }

        /// Modifying the key is allowed only if it does not change the hash.
        /// Modifying the value is allowed.
        /// Entry pointers become invalid whenever this ArrayHashMap is modified,
        pub const Entry = struct
        {
            key_ptr: *K,
            value_ptr: *V,
        };

        pub const GetOrPutResult = struct
        {
            key_ptr: *K,
            value_ptr: *V,
            found_existing: bool,
            index: usize,
        };

        pub const KV = struct
        {
            key: K,
            value: V,
        };

        pub fn init() !@This()
        {
            return initContext(undefined);
        }

        pub fn initContext(ctx: Context) !@This()
        {
            return .{
                .ctx = ctx,
                .hash_store = if (store_hash) try BoundedArray(u32, desired_capacity).init(0) else {},
                .key_store = try BoundedArray(K, desired_capacity).init(0),
                .value_store = try BoundedArray(V, desired_capacity).init(0),
            };
        }

        pub fn deinit(self: *@This()) void
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
                self.hash_store.len = 0;
            }

            self.key_store.len = 0;
            self.value_store.len = 0;
        }

        pub fn clearRetainingCapacity(self: *@This()) void
        {
            self.deinit();
        }

        pub fn clearAndFree(self: *@This()) void
        {
            self.deinit();
        }

        pub fn count(self: @This()) usize
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
            }

            return self.key_store.len;
        }

        pub fn keys(self: *@This()) []K
        {
            return self.key_store.slice();
        }

        pub fn values(self: *@This()) []V
        {
            return self.value_store.slice();
        }

        pub const Iterator = struct {
            keys: [*]K,
            values: [*]V,
            len: u32,
            index: u32 = 0,

            pub fn next(it: *Iterator) ?Entry
            {
                if (it.index >= it.len) return null;
                const result: Entry = .{
                    .key_ptr = &it.keys[it.index],
                    // workaround for #6974
                    .value_ptr = if (@sizeOf(*V) == 0) undefined else &it.values[it.index],
                };
                it.index += 1;
                return result;
            }

            /// Reset the iterator to the initial index
            pub fn reset(it: *Iterator) void
            {
                it.index = 0;
            }
        };

        /// Returns an iterator over the pairs in this map.
        /// Modifying the map may invalidate this iterator.
        pub fn iterator(self: *@This()) Iterator
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
            }

            return .{
                .keys = self.key_store.slice().ptr,
                .values = self.value_store.slice().ptr,
                .len = @as(u32, @intCast(self.key_store.len)),
            };
        }

        pub fn getOrPut(self: *@This(), key: K) !GetOrPutResult
        {
            if (self.capacity() < 1) return error.OutOfMemory;
            return self.getOrPutAssumeCapacity(key);
        }

        pub fn getOrPutAssumeCapacity(self: *@This(), key: K) GetOrPutResult
        {
            if (self.getIndex(key)) |index|
            {
                return .{
                    .key_ptr = &self.key_store.slice()[index],
                    // workaround for #6974
                    .value_ptr = if (@sizeOf(*V) == 0) undefined else &self.value_store.slice()[index],
                    .found_existing = true,
                    .index = index,
                };
            }

            if (store_hash) self.hash_store.appendAssumeCapacity(self.checkedHash(key));
            self.key_store.appendAssumeCapacity(key);
            self.value_store.appendAssumeCapacity(undefined);

            return .{
                .key_ptr = &self.key_store.slice()[self.key_store.len - 1],
                // workaround for #6974
                .value_ptr = if (@sizeOf(*V) == 0) undefined else &self.value_store.slice()[self.value_store.len - 1],
                .found_existing = false,
                .index = self.key_store.len - 1,
            };
        }

        pub fn getOrPutValue(self: *@This(), key: K, value: V) !GetOrPutResult
        {
            if (self.capacity() < 1) return error.OutOfMemory;
            return self.getOrPutValueAssumeCapacity(key, value);
        }

        pub fn getOrPutValueAssumeCapacity(self: *@This(), key: K, value: V) GetOrPutResult
        {
            const res = self.getOrPutAssumeCapacity(key);
            if (!res.found_existing) {
                res.key_ptr.* = key;
                res.value_ptr.* = value;
            }
            return res;
        }

        pub fn capacity(self: @This()) usize
        {
            return desired_capacity - self.count();
        }

        pub fn put(self: *@This(), key: K, value: V) !void
        {
            if (self.capacity() < 1) return error.OutOfMemory;
            return self.putAssumeCapacity(key, value);
        }

        pub fn putNoClobber(self: *@This(), key: K, value: V) !void
        {
            const result = try self.getOrPut(key);
            std.debug.assert(!result.found_existing);
            result.value_ptr.* = value;
        }

        pub fn putAssumeCapacity(self: *@This(), key: K, value: V) void
        {
            const result = self.getOrPutAssumeCapacity(key);
            result.value_ptr.* = value;
        }

        pub fn putAssumeCapacityNoClobber(self: *@This(), key: K, value: V) void
        {
            const result = try self.getOrPutAssumeCapacity(key);
            std.debug.assert(!result.found_existing);
            result.value_ptr.* = value;
        }

        pub fn fetchPut(self: *@This(), key: K, value: V) !?KV
        {
            if (self.capacity() < 1) return error.OutOfMemory;
            return self.fetchPutAssumeCapacity(key, value);
        }

        pub fn fetchPutAssumeCapacity(self: *@This(), key: K, value: V) ?KV
        {
            const gop = self.getOrPutAssumeCapacity(key);
            var result: ?KV = null;
            if (gop.found_existing) {
                result = .{
                    .key = gop.key_ptr.*,
                    .value = gop.value_ptr.*,
                };
            }
            gop.value_ptr.* = value;
            return result;
        }

        pub fn getEntry(self: *@This(), key: K) ?Entry
        {
            if (self.getIndex(key)) |index|
            {
                return .{
                    .key_ptr = &self.key_store.slice()[index],
                    // workaround for #6974
                    .value_ptr = if (@sizeOf(*V) == 0) undefined else &self.value_store.slice()[index],
                };
            }

            return null;
        }

        pub const getIndex: fn (@This(), K) ?usize =
            if (store_hash)
                getIndexHash
            else
                getIndexEql;

        inline fn checkedEql(self: @This(), lhs: K, rhs: K, b_index: usize) bool {
            comptime std.hash_map.verifyContext(@TypeOf(self.ctx), @TypeOf(lhs), K, u32, true);
            // If you get a compile error on the next line, it means that your
            // generic eql function doesn't accept (self, adapt key, K, index).
            const eql = self.ctx.eql(lhs, rhs, b_index);
            if (@TypeOf(eql) != bool) {
                @compileError
                (
                    "Context " ++
                    @typeName(@TypeOf(self.ctx)) ++
                    " has a generic eql function that returns the wrong type!\n" ++
                    @typeName(bool) ++
                    " was expected, but found " ++
                    @typeName(@TypeOf(eql)),
                );
            }
            return eql;
        }

        inline fn checkedHash(self: @This(), key: K) u32 {
            comptime std.hash_map.verifyContext(@TypeOf(self.ctx), @TypeOf(key), K, u32, true);
            // If you get a compile error on the next line, it means that your
            // hash function doesn't accept your key.
            const hash = self.ctx.hash(key);
            if (@TypeOf(hash) != u32)
            {
                @compileError
                (
                    "Context " ++
                    @typeName(@TypeOf(self.ctx)) ++
                    " has a generic hash function that returns the wrong type!\n" ++
                    @typeName(u32) ++
                    " was expected, but found " ++
                    @typeName(@TypeOf(hash)),
                );
            }
            return hash;
        }

        fn getIndexHash(self: @This(), key: K) ?usize
        {
            const hash_of_key = self.checkedHash(key);
            for (self.hash_store.slice(), self.key_store.slice(), 0..) |hash_it, key_it, i|
            {
                if (hash_it == hash_of_key and self.checkedEql(key, key_it, i))
                {
                    return i;
                }
            }

            return null;
        }

        fn getIndexEql(self: @This(), key: K) ?usize
        {
            for (self.key_store.slice(), 0..) |key_it, i|
            {
                if (self.checkedEql(key, key_it, i))
                {
                    return i;
                }
            }

            return null;
        }

        pub fn get(self: @This(), key: K) ?V
        {
            if (self.getIndex(key)) |index|
            {
                return self.value_store.slice()[index];
            }

            return null;
        }

        pub fn getPtr(self: *@This(), key: K) ?*V
        {
            if (self.getIndex(key)) |index|
            {
                return &self.value_store.slice()[index];
            }

            return null;
        }

        pub fn getKey(self: @This(), key: K) ?K
        {
            if (self.getIndex(key)) |index|
            {
                return self.key_store.slice()[index];
            }

            return null;
        }

        pub fn getKeyPtr(self: *@This(), key: K) ?*K
        {
            if (self.getIndex(key)) |index|
            {
                return &self.key_store.slice()[index];
            }

            return null;
        }

        pub fn contains(self: @This(), key: K) bool
        {
            return self.getIndex(key) != null;
        }

        pub fn fetchSwapRemove(self: *@This(), key: K) ?KV
        {
            if (self.getIndex(key)) |index|
            {
                std.debug.assert(self.key_store.len == self.value_store.len);

                if (store_hash)
                {
                    std.debug.assert(self.key_store.len == self.hash_store.len);
                    _ = self.hash_store.swapRemove(index);
                }

                return .{
                    .key = self.key_store.swapRemove(index),
                    .value = self.value_store.swapRemove(index),
                };
            }

            return null;
        }

        pub fn fetchOrderedRemove(self: *@This(), key: K) ?KV
        {
            if (self.getIndex(key)) |index|
            {
                std.debug.assert(self.key_store.len == self.value_store.len);

                if (store_hash)
                {
                    std.debug.assert(self.key_store.len == self.hash_store.len);
                    _ = self.hash_store.orderedRemove(index);
                }

                return .{
                    .key = self.key_store.orderedRemove(index),
                    .value = self.value_store.orderedRemove(index),
                };
            }

            return null;
        }

        pub fn swapRemove(self: *@This(), key: K) bool
        {
            if (self.getIndex(key)) |index|
            {
                self.swapRemoveAt(index);
                return true;
            }

            return false;
        }

        pub fn orderedRemove(self: *@This(), key: K) bool
        {
            if (self.getIndex(key)) |index|
            {
                self.orderedRemoveAt(index);
                return true;
            }

            return false;
        }

        pub fn swapRemoveAt(self: *@This(), index: usize) void
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
                _ = self.hash_store.swapRemove(index);
            }

            _ = self.key_store.swapRemove(index);
            _ = self.value_store.swapRemove(index);
        }

        pub fn orderedRemoveAt(self: *@This(), index: usize) void
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
                _ = self.hash_store.orderedRemove(index);
            }

            _ = self.key_store.orderedRemove(index);
            _ = self.value_store.orderedRemove(index);
        }

        pub fn clone(self: @This()) !@This()
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            var new = try init();
            @memcpy(new.key_store.buffer, self.key_store.buffer);
            @memcpy(new.value_store.buffer, self.value_store.buffer);
            new.key_store.len = self.key_store.len;
            new.value_store.len = self.value_store.len;

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
                @memcpy(new.hash_store.buffer, self.hash_store.buffer);
                new.hash_store.len = self.hash_store.len;
            }

            return new;
        }

        pub fn cloneWithAllocator(self: @This(), allocator: std.mem.Allocator) !@This()
        {
            const new = try allocator.create(@This());
            new.* = self.clone();
            return new.*;
        }

        pub fn move(self: *@This()) @This()
        {
            return self.*;
        }

        pub fn shrinkRetainingCapacity(self: *@This(), new_len: usize) void
        {
            std.debug.assert(self.key_store.len == self.value_store.len);
            std.debug.assert(self.key_store.len <= new_len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
                self.hash_store.len = new_len;
            }

            self.key_store.len = new_len;
            self.value_store.len = new_len;
        }

        pub fn shrinkAndFree(self: *@This(), new_len: usize) void
        {
            self.shrinkRetainingCapacity(new_len);
        }

        pub fn pop(self: *@This()) KV
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
                _ = self.hash_store.pop();
            }

            return .{
                .key = self.key_store.pop(),
                .value = self.value_store.pop(),
            };
        }

        pub fn popOrNull(self: *@This()) ?KV
        {
            std.debug.assert(self.key_store.len == self.value_store.len);

            if (store_hash)
            {
                std.debug.assert(self.key_store.len == self.hash_store.len);
            }

            if (self.key_store.len == 0) return null;
            if (store_hash) _ = self.hash_store.pop();
            return .{
                .key = self.key_store.pop(),
                .value = self.value_store.pop(),
            };
        }
    };
}
