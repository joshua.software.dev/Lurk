const std = @import("std");

const settings = @import("settings.zig");


pub const SimpleHttpClient = @This();

mode: []const u8,
ptr: *anyopaque,
vtable: *const HttpClientVTable,

pub const HttpClientVTable = struct
{
    deinit: *const fn (ctx: *anyopaque) void,
    download_bytes:
        *const fn
        (
            ctx: *anyopaque,
            req_headers: []const [2][]const u8,
            uri: std.Uri,
            response_allocator: std.mem.Allocator,
            max_size: usize,
        )
        anyerror![]u8,
    download_file:
        *const fn
        (
            ctx: *anyopaque,
            req_headers: []const [2][]const u8,
            uri: std.Uri,
            abs_path: []const u8,
            expected_hash: ?[32]u8,
        )
        anyerror!void,
    post_access_token:
        *const fn
        (
            ctx: *anyopaque,
            uri: std.Uri,
            response_allocator: std.mem.Allocator,
            max_size: usize,
            json_message: []const u8,
        )
        anyerror![]u8,
};

pub fn deinit(self: SimpleHttpClient) void
{
    self.vtable.deinit(self.ptr);
}

pub fn download_bytes
(
    self: SimpleHttpClient,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
)
![]u8
{
    return self.vtable.download_bytes(self.ptr, req_headers, uri, response_allocator, max_size);
}

pub fn download_file
(
    self: SimpleHttpClient,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    abs_path: []const u8,
    expected_hash: ?[32]u8,
)
!void
{
    return self.vtable.download_file(self.ptr, req_headers, uri, abs_path, expected_hash);
}

pub fn post_access_token
(
    self: SimpleHttpClient,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
    json_message: []const u8,
)
![]u8
{
    return self.vtable.post_access_token(self.ptr, uri, response_allocator, max_size, json_message);
}
