const std = @import("std");

const certs = @import("preload_ssl_certs.zig");
const settings = @import("../settings.zig");
const simple_http_client = @import("../simple_http_client.zig");


pub const StdHttpClient = @This();

const request_buffer_size = 1024 * 96;

http_allocator: std.mem.Allocator,
cert_allocator: ?std.mem.Allocator,
cert_bundle: ?std.crypto.Certificate.Bundle,

const req_func =
    if (@hasDecl(std.http.Client, "request"))
        std.http.Client.request
    else
        std.http.Client.open;
const start_func = struct
{
    fn f(req: *std.http.Client.Request, args: anytype) !void
    {
        if (@hasDecl(std.http.Client.Request, "start"))
            try req.start()
        else
            try req.send(args);
    }
}.f;

pub fn init(init_settings: settings.StdHttpSettings) !@This()
{
    var cert_allocator: ?std.mem.Allocator = null;
    var cert_bundle: ?std.crypto.Certificate.Bundle = null;
    switch (init_settings.bundle)
    {
        .allocate_new => |new_allocator|
        {
            cert_allocator = new_allocator.final_cert_allocator;
            cert_bundle = try certs.preload_ssl_certs
            (
                new_allocator.creation_allocator,
                new_allocator.final_cert_allocator,
            );
        },
        .dynamically_generate => {},
        .use_existing => |existing|
        {
            cert_allocator = existing.freeing_allocator;
            cert_bundle = existing.existing_bundle;
        },
    }

    return .{
        .http_allocator = init_settings.http_allocator,
        .cert_allocator = cert_allocator,
        .cert_bundle = cert_bundle,
    };
}

pub fn as_simple_client(self: *@This()) simple_http_client.SimpleHttpClient
{
    return .{
        .mode = "std http",
        .ptr = self,
        .vtable = &.{
            .deinit = deinit,
            .download_bytes = download_bytes,
            .download_file = download_file,
            .post_access_token = post_access_token,
        }
    };
}

pub fn deinit(ctx: *anyopaque) void
{
    const self: *@This() = @ptrCast(@alignCast(ctx));
    if (self.cert_allocator != null and self.cert_bundle != null) self.cert_bundle.?.deinit(self.cert_allocator.?);
}

fn create_client(self: @This(), allocator: std.mem.Allocator) !std.http.Client
{
    return
        if (self.cert_bundle == null)
            .{ .allocator = allocator, }
        else
            blk: {
                // deinit unnecessary due to client.deinit calling it
                var temp_bundle: std.crypto.Certificate.Bundle = .{
                    .bytes = try self.cert_bundle.?.bytes.clone(allocator),
                };

                const now_sec = std.time.timestamp();
                var iter = self.cert_bundle.?.map.iterator();
                while (iter.next()) |it|
                {
                    try temp_bundle.parseCert(allocator, it.value_ptr.*, now_sec);
                }

                break :blk .{
                    .allocator = allocator,
                    .ca_bundle = temp_bundle,
                    .next_https_rescan_certs = false,
                };
            };
}

pub fn download_bytes
(
    ctx: *anyopaque,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
)
anyerror![]u8
{
    const self: *@This() = @ptrCast(@alignCast(ctx));

    var req_buf: ?[]u8 = null;
    defer if (req_buf != null) self.http_allocator.free(req_buf.?);
    var fba: ?std.heap.FixedBufferAllocator = null;
    var request_allocator: ?std.mem.Allocator = null;

    if (self.cert_bundle == null)
    {
        request_allocator = self.http_allocator;
    }
    else
    {
        req_buf = try self.http_allocator.alloc(u8, request_buffer_size);
        fba = std.heap.FixedBufferAllocator.init(req_buf.?);
        request_allocator = fba.?.allocator();
    }

    var client: std.http.Client = try self.create_client(request_allocator.?);
    defer client.deinit();

    var headers: std.http.Headers = .{ .allocator = request_allocator.?, };
    defer headers.deinit();
    for (req_headers) |h|
    {
        try headers.append(h[0], h[1]);
    }

    var req: std.http.Client.Request = try req_func(&client, .GET, uri, headers, .{ .max_redirects = 10 });
    defer req.deinit();

    try start_func(&req, .{});
    try req.finish();
    try req.wait();

    if (req.response.status != .ok)
    {
        return error.DownloadFailed;
    }

    return try req.reader().readAllAlloc(response_allocator, max_size);
}

pub fn download_file
(
    ctx: *anyopaque,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    abs_path: []const u8,
    expected_hash: ?[32]u8,
)
anyerror!void
{
    const self: *@This() = @ptrCast(@alignCast(ctx));

    var req_buf: ?[]u8 = null;
    defer if (req_buf != null) self.http_allocator.free(req_buf.?);
    var fba: ?std.heap.FixedBufferAllocator = null;
    var request_allocator: ?std.mem.Allocator = null;

    if (self.cert_bundle == null)
    {
        request_allocator = self.http_allocator;
    }
    else
    {
        req_buf = try self.http_allocator.alloc(u8, request_buffer_size);
        fba = std.heap.FixedBufferAllocator.init(req_buf.?);
        request_allocator = fba.?.allocator();
    }

    var hash = std.crypto.hash.sha2.Sha256.init(.{});

    {
        var file = try std.fs.createFileAbsolute(abs_path, .{});
        defer file.close();

        var client: std.http.Client = try self.create_client(request_allocator.?);
        defer client.deinit();

        var headers: std.http.Headers = .{ .allocator = request_allocator.?, };
        defer headers.deinit();
        for (req_headers) |h|
        {
            try headers.append(h[0], h[1]);
        }

        var req: std.http.Client.Request = try req_func(&client, .GET, uri, headers, .{ .max_redirects = 10 });
        defer req.deinit();

        try start_func(&req, .{});
        try req.finish();
        try req.wait();

        if (req.response.status != .ok)
        {
            return error.DownloadFailed;
        }

        var reader = req.reader();
        var writer = std.io.bufferedWriter(file.writer());
        var local_buf: [4096]u8 = undefined;

        while (true)
        {
            const bytes_read = try reader.read(&local_buf);
            if (bytes_read == 0) break;

            _ = try writer.write(local_buf[0..bytes_read]);
            hash.update(local_buf[0..bytes_read]);
        }

        try writer.flush();
        try file.sync();
    }

    if (expected_hash) |expected|
    {
        var out: [32]u8 = undefined;
        hash.final(out[0..]);

        if (!std.mem.eql(u8, &out, &expected)) return error.DownloadHashCheckFailed;
    }
}

pub fn post_access_token
(
    ctx: *anyopaque,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
    json_message: []const u8,
)
anyerror![]u8
{
    const self: *@This() = @ptrCast(@alignCast(ctx));

    var req_buf: ?[]u8 = null;
    defer if (req_buf != null) self.http_allocator.free(req_buf.?);
    var fba: ?std.heap.FixedBufferAllocator = null;
    var request_allocator: ?std.mem.Allocator = null;

    if (self.cert_bundle == null)
    {
        request_allocator = self.http_allocator;
    }
    else
    {
        req_buf = try self.http_allocator.alloc(u8, request_buffer_size);
        fba = std.heap.FixedBufferAllocator.init(req_buf.?);
        request_allocator = fba.?.allocator();
    }

    var client: std.http.Client = try self.create_client(request_allocator.?);
    defer client.deinit();

    var headers: std.http.Headers = .{ .allocator = request_allocator.?, };
    defer headers.deinit();
    try headers.append("Content-Type", "application/json");

    var req: std.http.Client.Request = try req_func(&client, .POST, uri, headers, .{ .max_redirects = 10 });
    defer req.deinit();
    req.transfer_encoding = .chunked;

    try start_func(&req, .{});
    try req.writeAll(json_message);
    try req.finish();
    try req.wait();

    if (req.response.status != .ok)
    {
        return error.RequestFailed;
    }

    return req.reader().readAllAlloc(response_allocator, max_size);
}
