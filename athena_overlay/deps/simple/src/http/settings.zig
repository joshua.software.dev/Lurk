const std = @import("std");


pub const AllocateNewBundleSettings = struct
{
    /// This allocator is used to load the full system cert bundle, which will
    /// then be trimmed down to just certs required to communicate with
    /// discord's API. Lots of memory churn, recommend against using a
    /// FixedBufferAllocator. All memory from this allocator will be freed and
    /// just the smaller bundle will be retained using the final_cert_allocator
    creation_allocator: std.mem.Allocator,
    /// The allocator used to allocate and eventually free the final, smaller
    /// certificate bundle
    final_cert_allocator: std.mem.Allocator,
};

pub const UseExistingBundleSettings = struct
{
    existing_bundle: std.crypto.Certificate.Bundle,
    // When the http client is deinit, this allocator will be used to free the
    // existing_bundle.
    freeing_allocator: std.mem.Allocator,
};

pub const CertBundleSettings = union(enum)
{
    allocate_new: AllocateNewBundleSettings,
    // Do not pregenerate a cert bundle, let the std http client load a new,
    // complete bundle from disk each time. Useful when you don't know what
    // certs will be necessary to verify your connections in advance.
    dynamically_generate,
    use_existing: UseExistingBundleSettings,
};

pub const StdHttpSettings = struct
{
    /// The allocator used during http requests to hold temporary data for a
    /// given request
    http_allocator: std.mem.Allocator,
    bundle: CertBundleSettings,
};
