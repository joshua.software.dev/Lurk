const std = @import("std");

const simple_http_client = @import("../simple_http_client.zig");

const iguana = @import("iguanaTLS");


pub const IguanaTlsClient = @This();

pub fn init() !@This()
{
    return .{};
}

pub fn as_simple_client(self: *@This()) simple_http_client.SimpleHttpClient
{
    return .{
        .mode = "iguanaTLS",
        .ptr = self,
        .vtable = &.{
            .deinit = deinit,
            .download_bytes = download_bytes,
            .download_file = download_file,
            .post_access_token = post_access_token,
        }
    };
}

pub fn deinit(ctx: *anyopaque) void
{
    _ = ctx;
}

pub fn download_bytes
(
    ctx: *anyopaque,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
)
anyerror![]u8
{
    _ = max_size;
    _ = response_allocator;
    _ = uri;
    _ = req_headers;
    _ = ctx;
    @panic("TODO: implement");
}

pub fn download_file
(
    ctx: *anyopaque,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    abs_path: []const u8,
    expected_hash: ?[32]u8,
)
anyerror!void
{
    _ = expected_hash;
    _ = abs_path;
    _ = uri;
    _ = req_headers;
    _ = ctx;
    @panic("TODO: implement");
}

pub fn post_access_token
(
    ctx: *anyopaque,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
    json_message: []const u8,
)
anyerror![]u8
{
    _ = ctx;
    var fba_buf: [1024 * 20]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&fba_buf);
    var allocator = fba.allocator();

    const out_buf = try allocator.alloc(u8, 512);
    defer allocator.free(out_buf);
    var message_length: ?usize = null;

    {
        var net_stream: std.net.Stream = try std.net.tcpConnectToHost
        (
            allocator,
            uri.host.?,
            if (std.mem.eql(u8, uri.scheme, "https")) 443 else 80,
        );
        defer net_stream.close();

        var random = blk: {
            var seed: [std.rand.DefaultCsprng.secret_seed_length]u8 = undefined;
            try std.os.getrandom(&seed);
            break :blk std.rand.DefaultCsprng.init(seed);
        };

        var tls_stream = try iguana.client_connect
        (
            .{
                .rand = random.random(),
                .reader = net_stream.reader(),
                .writer = net_stream.writer(),
                .temp_allocator = allocator,
                .cert_verifier = .none, // TODO: Enable this
            },
            uri.host.?,
        );
        defer tls_stream.close_notify()
            catch {};

        {
            const write_msg = try std.fmt.allocPrint
            (
                fba.allocator(),
                "POST {/} HTTP/1.1\r\n" ++
                "HOST: {s}\r\n" ++
                "User-Agent: Athena Overlay (iguanaTLS)\r\n" ++
                "Accept: */*\r\n" ++
                "Content-Type: application/json\r\n" ++
                "Content-Length: {d}\r\n" ++
                "\r\n" ++
                "{s}\r\n" ++
                "\r\n",
                .{
                    uri,
                    uri.host.?,
                    json_message.len,
                    json_message,
                },
            );
            defer allocator.free(write_msg);
            _ = try tls_stream.write(write_msg);
        }

        while (true)
        {
            var read_buf: [1]u8 = undefined;
            if ((try tls_stream.read(&read_buf)) < 1) return error.EndOfStream;

            switch (read_buf[0])
            {
                '\r' =>
                {
                    var secondary_buf: [3]u8 = undefined;
                    if ((try tls_stream.read(&secondary_buf)) < 3) return error.EndOfStream;

                    if (std.mem.eql(u8, &secondary_buf, "\n\r\n")) break;
                },
                else => {}
            }
        }

        message_length = try tls_stream.read(out_buf);
    }

    const read_size = if (message_length.? > max_size) max_size else message_length.?;
    return try response_allocator.dupe(u8, out_buf[0..read_size]);
}
