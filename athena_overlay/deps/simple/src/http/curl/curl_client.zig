const builtin = @import("builtin");
const std = @import("std");

const simple_http_client = @import("../simple_http_client.zig");


pub const CurlClient = @This();

const exec_func = if (@hasDecl(std.ChildProcess, "exec")) std.ChildProcess.exec else std.ChildProcess.run;
const uri_format =
    if (builtin.zig_version.order(std.SemanticVersion.parse("0.11.0") catch unreachable) == .gt)
        "{:}"
    else
        "{+/}";

pub fn init() !@This()
{
    return .{};
}

pub fn as_simple_client(self: *@This()) simple_http_client.SimpleHttpClient
{
    return .{
        .mode = "curl",
        .ptr = self,
        .vtable = &.{
            .deinit = deinit,
            .download_bytes = download_bytes,
            .download_file = download_file,
            .post_access_token = post_access_token,
        }
    };
}

pub fn deinit(ctx: *anyopaque) void
{
    _ = ctx;
}

pub fn download_bytes
(
    ctx: *anyopaque,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
)
anyerror![]u8
{
    _ = max_size;
    _ = response_allocator;
    _ = uri;
    _ = req_headers;
    _ = ctx;
    @panic("TODO: implement");
}

pub fn download_file
(
    ctx: *anyopaque,
    req_headers: []const [2][]const u8,
    uri: std.Uri,
    abs_path: []const u8,
    expected_hash: ?[32]u8,
)
anyerror!void
{
    _ = expected_hash;
    _ = abs_path;
    _ = uri;
    _ = req_headers;
    _ = ctx;
    @panic("TODO: implement");
}

pub fn post_access_token
(
    ctx: *anyopaque,
    uri: std.Uri,
    response_allocator: std.mem.Allocator,
    max_size: usize,
    json_message: []const u8,
)
anyerror![]u8
{
    _ = ctx;
    var fba_buf: [1024 * 32]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&fba_buf);
    var allocator = fba.allocator();

    const url = try std.fmt.allocPrint(allocator, uri_format, .{ uri });
    defer allocator.free(url);

    const result = try exec_func
    (
        .{
            .allocator = allocator,
            .argv =
            &.{
                "curl",
                "-X",
                "POST",
                url,
                "-H",
                "Content-Type: application/json",
                "-d",
                json_message,
            },
            .cwd = null,
            .env_map = null,
            .max_output_bytes = 1024
        }
    );

    switch (result.term)
    {
        .Exited => |code| if (code != 0) return error.CommandFailed,
        else => return error.CommandFailed,
    }

    const read_size = if (result.stdout.len > max_size) max_size else result.stdout.len;
    return try response_allocator.dupe(u8, result.stdout[0..read_size]);
}
