const std = @import("std");


pub fn sha256sum_to_array(comptime sha256sum: []const u8) [32]u8
{
    var expected_bytes: [sha256sum.len / 2]u8 = undefined;
    for (&expected_bytes, 0..) |*r, i| {
        r.* = std.fmt.parseInt(u8, sha256sum[2 * i .. 2 * i + 2], 16) catch unreachable;
    }
    return expected_bytes;
}
