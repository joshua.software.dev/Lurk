const std = @import("std");

const BoundedArray = @import("bounded_array.zig").BoundedArray;


const BlacklistedProcessesMap = std.ComptimeStringMap
(
    void,
    .{
        // shamelessly stolen (under MIT License) from mangohud
        .{ "Amazon Games UI.exe", void, },
        .{ "Battle.net.exe", void, },
        .{ "BethesdaNetLauncher.exe", void, },
        .{ "EABackgroundService.exe", void, },
        .{ "EADesktop.exe", void, },
        .{ "EALauncher.exe", void, },
        .{ "EpicGamesLauncher.exe", void, },
        .{ "explorer.exe", void, },
        .{ "ffxivlauncher.exe", void, },
        .{ "ffxivlauncher64.exe", void, },
        .{ "GalaxyClient.exe", void, },
        .{ "gamescope", void, },
        .{ "gldriverquery", void, },
        .{ "idTechLauncher.exe", void, },
        .{ "iexplore.exe", void, },
        .{ "IGOProxy.exe", void, },
        .{ "IGOProxy64.exe", void, },
        .{ "InsurgencyEAC.exe", void, },
        .{ "LeagueClient.exe", void, },
        .{ "LeagueClientUxRender.exe", void, },
        .{ "Link2EA.exe", void, },
        .{ "Origin.exe", void, },
        .{ "OriginThinSetupInternal.exe", void, },
        .{ "QtWebEngineProcess.exe", void, },
        .{ "REDlauncher.exe", void, },
        .{ "REDprelauncher.exe", void, },
        .{ "RSI Launcher.exe", void, },
        .{ "rundll32.exe", void, },
        .{ "SocialClubHelper.exe", void, },
        .{ "StarCitizen_Launcher.exe", void, },
        .{ "steam.exe", void, },
        .{ "Steam.exe", void, },
        .{ "steam", void, },
        .{ "steamwebhelper", void, },
        .{ "tabtip.exe", void, },
        .{ "vrcompositor", void, },
        .{ "vulkandriverquery", void, },
        .{ "wine-preloader", void, },
        .{ "wine64-preloader", void, },
    },
);

pub const BlacklistProcess = struct
{
    is_blacklisted: bool,
    is_wine: bool,
    name: BoundedArray(u8, std.fs.MAX_PATH_BYTES),
};

fn get_process_name(info: *BlacklistProcess) !void
{
    const proc_name = try std.fs.selfExePath(&info.name.buffer);

    info.is_wine =
        std.mem.endsWith(u8, proc_name, "wine-preloader") or
        std.mem.endsWith(u8, proc_name, "wine64-preloader");
    if (!info.is_wine)
    {
        const result = std.fs.path.basename(proc_name);
        info.name.len = @intCast(result.len);
        std.mem.copyForwards(u8, &info.name.buffer, result);
        return;
    }

    const comm_file = try std.fs.openFileAbsolute("/proc/self/comm", .{});
    defer comm_file.close();
    info.name.len = @intCast(try comm_file.readAll(&info.name.buffer));
    const possible_exe_name = std.mem.trim(u8, info.name.slice(), " \t\r\n");
    if (std.mem.endsWith(u8, possible_exe_name, ".exe"))
    {
        info.name.len = @intCast(possible_exe_name.len);
        std.mem.copyForwards(u8, &info.name.buffer, possible_exe_name);
        return;
    }

    const cmd = try std.fs.openFileAbsolute("/proc/self/cmdline", .{});
    defer cmd.close();

    var reader = cmd.reader();
    while (true)
    {
        var fbs = std.io.fixedBufferStream(&info.name.buffer);
        reader.streamUntilDelimiter(fbs.writer(), '\x00', fbs.buffer.len)
            catch |err| switch (err)
            {
                error.EndOfStream =>
                {
                    if (fbs.getWritten().len == 0)
                    {
                        info.name.len = 0;
                        return;
                    }

                    return err;
                },
                else => return err,
            };

        const output = fbs.getWritten();
        if (std.mem.endsWith(u8, output, ".exe"))
        {
            const result = std.fs.path.basenameWindows(output);
            info.name.len = @intCast(result.len);
            std.mem.copyForwards(u8, &info.name.buffer, result);
            return;
        }
    }
}

var is_blacklisted: ?bool = null;
var is_wine: ?bool = null;
pub fn is_this_process_blacklisted(info: *BlacklistProcess) !bool
{
    info.name = try std.BoundedArray(u8, std.fs.MAX_PATH_BYTES).init(0);
    if (is_blacklisted) |result|
    {
        info.is_blacklisted = result;
        info.is_wine = is_wine orelse false;
        return true;
    }

    try get_process_name(info);
    info.is_blacklisted = BlacklistedProcessesMap.has(info.name.constSlice());
    is_blacklisted = info.is_blacklisted;
    is_wine = info.is_wine;
    return false;
}

pub fn does_process_name_match(input: []const u8) !bool
{
    var proc_info: BlacklistProcess = undefined;
    try get_process_name(&proc_info);
    return std.mem.eql(u8, input, proc_info.name.constSlice());
}
