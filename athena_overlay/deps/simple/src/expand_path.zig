const std = @import("std");

const bounded_array = @import("bounded_array.zig");


pub fn expand_path(allocator: std.mem.Allocator, input: []const u8) ![]const u8
{
    if (input.len < 1 or input[0] != '~') return input;

    const home = try std.process.getEnvVarOwned(allocator, "HOME");
    defer allocator.free(home);

    return try std.fs.path.join
    (
        allocator,
        &.{
            home,
            if (input.len > 1 and input[1] == '/')
                input[2..]
            else
                input[1..],
        }
    );
}

pub const RealPath = struct
{
    exists: bool,
    path: bounded_array.BoundedArray(u8, std.fs.MAX_PATH_BYTES),
};

pub fn expand_realpath(allocator: std.mem.Allocator, input: []const u8) !RealPath
{
    var realpath: RealPath = .{
        .exists = true,
        .path = try bounded_array.BoundedArray(u8, std.fs.MAX_PATH_BYTES).init(0),
    };

    {
        const expand = try expand_path(allocator, input);
        defer allocator.free(expand);

        const result = std.os.realpath(expand, &realpath.path.buffer)
            catch |err| switch (err)
            {
                error.FileNotFound =>
                {
                    realpath.exists = false;
                    realpath.path.len = 0;
                    try realpath.path.appendSlice(expand);
                    return realpath;
                },
                else => return err,
            };
        realpath.path.len = @truncate(result.len);
    }

    return realpath;
}
