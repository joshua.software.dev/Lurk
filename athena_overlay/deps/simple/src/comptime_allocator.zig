const std = @import("std");


pub const comptime_allocator: std.mem.Allocator = .{
    .ptr = undefined,
    .vtable = &.{
        .alloc = struct {
            fn alloc(_: *anyopaque, len: usize, log2_align: u8, ra: usize) ?[*]u8 {
                _ = ra;
                if (!@inComptime()) {
                    @panic("comptime_allocator cannot be used at runtime");
                }
                var buf: [len]u8 align(1 << log2_align) = undefined;
                return &buf;
            }
        }.alloc,
        .resize = &std.mem.Allocator.noResize,
        .free = &std.mem.Allocator.noFree,
    },
};
