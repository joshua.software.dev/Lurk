const builtin = @import("builtin");
const std = @import("std");

const overlay_opts = @import("overlay_opts");
const state = @import("overlay_state.zig");

const simple = @import("simple");
const zimgui = @import("Zig-ImGui");


const primary_font_uri = std.Uri.parse
(
    "https://gitlab.com/joshua.software.dev/Lurk/-/package_files/104229211/download"
)
    catch unreachable;
const primary_font_sha256sum = simple.sha256sum_to_array
(
    "7707f03fdac86c686e715d5e9ec03f4ce38a897ec05fcb973ae84f5d67ffe406"
);

const emoji_font_uri = std.Uri.parse
(
    "https://gitlab.com/joshua.software.dev/Lurk/-/package_files/104229217/download"
)
    catch unreachable;
const emoji_font_sha256sum = simple.sha256sum_to_array
(
    "de10ef1cca0407b83048536b9e27294099df00bf8deb6e429f30aae6011629c4"
);

const monospace_font_uri = std.Uri.parse
(
    "https://gitlab.com/joshua.software.dev/Lurk/-/package_files/104229220/download"
)
    catch unreachable;
const monospace_font_sha256sum = simple.sha256sum_to_array
(
    "f0bb8b80bbc26e27569b4053c4a1588280ded85e8a40433c4009a69cc19f32be"
);

const FontPointers = struct
{
    primary_font_ptr: ?*zimgui.Font,
    emoji_font_ptr: ?*zimgui.Font,
};

fn load_primary_fonts(primary_font_size: f32, emoji_font_size: f32) FontPointers
{
    var font_ptrs: FontPointers = .{
        .primary_font_ptr = null,
        .emoji_font_ptr = null,
    };

    var primary_font_config = zimgui.FontConfig.init_ImFontConfig();
    defer primary_font_config.deinit();
    primary_font_config.EllipsisChar = @as(zimgui.Wchar, 0x0085);
    primary_font_config.GlyphOffset.y = 1.0;
    primary_font_config.OversampleH = 1;
    primary_font_config.OversampleV = 1;
    primary_font_config.PixelSnapH = true;
    primary_font_config.SizePixels = primary_font_size;

    {
        const primary_font_name = std.fs.path.basename(state.config.?.primary_font_path.path.constSlice());
        _ = std.fmt.bufPrint
        (
            &primary_font_config.Name,
            "{s}, {d}px\x00",
            .{
                primary_font_name[0..@min(primary_font_name.len, 32)],
                primary_font_config.SizePixels,
            }
        )
            catch @panic("oom loading primary font name");
    }

    {
        const primary_font_posix_path = std.os.toPosixPath(state.config.?.primary_font_path.path.constSlice())
            catch @panic("Failed to get posix path for primary font.");

        // init using imgui's allocator to allow it to free the memory later
        font_ptrs.primary_font_ptr = state.shared_font_atlas.?.AddFontFromFileTTFExt
        (
            &primary_font_posix_path,
            primary_font_config.SizePixels,
            primary_font_config,
            &@as
            (
                [18:0]zimgui.Wchar,
                .{ // Should contain at least Chinese, English, Japanese, and Korean
                    0x20,
                    0xFF,
                    0x2000,
                    0x206F,
                    0x3000,
                    0x30FF,
                    0x3131,
                    0x3163,
                    0x31F0,
                    0x31FF,
                    0x4E00,
                    0x9FAF,
                    0xAC00,
                    0xD7A3,
                    0xFF00,
                    0xFFEF,
                    0xFFFD,
                    0xFFFD,
                },
            )
        );
    }

    var emoji_font_config = zimgui.FontConfig.init_ImFontConfig();
    defer emoji_font_config.deinit();
    emoji_font_config.FontBuilderFlags = 256; // Allow Color Emoji
    emoji_font_config.MergeMode = true;
    emoji_font_config.OversampleH = 1;
    emoji_font_config.OversampleV = 1;
    emoji_font_config.SizePixels = emoji_font_size;

    {
        const emoji_font_name = std.fs.path.basename(state.config.?.emoji_font_path.path.constSlice());
        _ = std.fmt.bufPrint
        (
            &emoji_font_config.Name,
            "{s}, {d}px\x00",
            .{
                emoji_font_name[0..@min(emoji_font_name.len, 32)],
                emoji_font_config.SizePixels,
            }
        )
            catch @panic("oom loading emoji font name");
    }

    {
        const emoji_font_posix_path = std.os.toPosixPath(state.config.?.emoji_font_path.path.constSlice())
            catch @panic("Failed to get posix path for emoji font.");

        // init using imgui's allocator to allow it to free the memory later
        font_ptrs.emoji_font_ptr = state.shared_font_atlas.?.AddFontFromFileTTFExt
        (
            &emoji_font_posix_path,
            emoji_font_config.SizePixels,
            emoji_font_config,
            &@as([2:0]zimgui.Wchar, .{ 0x1, 0x10FFFF, })
        );
    }

    return font_ptrs;
}

pub fn load_shared_font(allocator: std.mem.Allocator, needs_large_fonts: bool) void
{
    const any_plugin_needs_large_fonts = needs_large_fonts and (builtin.target.ptrBitWidth() == 64);

    if (state.config == null)
    {
        std.log.scoped(.ATHENAOVERLAY).err
        (
            "Could not read config during font loading. Closing thread...\n",
            .{}
        );
        return;
    }
    else if (state.shared_font_atlas == null)
    {
        std.log.scoped(.ATHENAOVERLAY).err
        (
            "Font atlas not initialized before font loading. Closing thread...\n",
            .{}
        );
        return;
    }

    if (state.config.?.english_only)
    {
        _ = state.shared_font_atlas.?.AddFontDefault();
        _ = state.shared_font_atlas.?.Build();
        std.log.scoped(.ATHENAOVERLAY).warn("Using english only mode, expect potential rendering errors", .{});
        @atomicStore(bool, &state.font_thread_finished, true, .Release);
        @atomicStore(bool, &state.font_load_complete, true, .Release);
        return;
    }

    if (state.config.?.download_missing_fonts)
    {
        var client: ?simple.StdHttpClient = null;
        var simple_http_client = blk: {
            if (builtin.zig_backend == .stage2_x86_64)
            {
                var curl = try simple.init_curl_client();
                break :blk curl.as_simple_client();
            }

            client = simple.init_std_http_client(.{
                .bundle = .dynamically_generate,
                .http_allocator = allocator,
            })
                catch unreachable; // cannot error with .dynamically_generate
            break :blk client.?.as_simple_client();
        };
        defer simple_http_client.deinit();

        if (!state.config.?.primary_font_path.exists)
        {
            std.log.scoped(.ATHENAOVERLAY).warn
            (
                "Primary font not found: {s}",
                .{ state.config.?.primary_font_path.path.constSlice() }
            );
            std.log.scoped(.ATHENAOVERLAY).warn
            (
                "Downloading primary font from: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                .{ primary_font_uri },
            );
            simple_http_client.download_file
            (
                &.{},
                primary_font_uri,
                state.config.?.primary_font_path.path.constSlice(),
                primary_font_sha256sum,
            )
                catch |err| std.debug.panic("Failed to download primary font: {any}", .{ err });
        }

        if (!state.config.?.emoji_font_path.exists)
        {
            std.log.scoped(.ATHENAOVERLAY).warn
            (
                "Emoji font not found: {s}",
                .{ state.config.?.emoji_font_path.path.constSlice() }
            );
            std.log.scoped(.ATHENAOVERLAY).warn
            (
                "Downloading emoji font from: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                .{ emoji_font_uri },
            );
            simple_http_client.download_file
            (
                &.{},
                emoji_font_uri,
                state.config.?.emoji_font_path.path.constSlice(),
                emoji_font_sha256sum,
            )
                catch |err| std.debug.panic("Failed to download emoji font: {any}", .{ err });
        }

        if (!state.config.?.monospace_font_path.exists)
        {
            std.log.scoped(.ATHENAOVERLAY).warn
            (
                "Monospace font not found: {s}",
                .{ state.config.?.monospace_font_path.path.constSlice() }
            );
            std.log.scoped(.ATHENAOVERLAY).warn
            (
                "Downloading monospace font from: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                .{ monospace_font_uri },
            );
            simple_http_client.download_file
            (
                &.{},
                monospace_font_uri,
                state.config.?.monospace_font_path.path.constSlice(),
                monospace_font_sha256sum,
            )
                catch |err| std.debug.panic("Failed to download monospace font: {any}", .{ err });
        }
    }

    const small_fonts = load_primary_fonts
    (
        state.config.?.primary_font_size,
        state.config.?.emoji_font_size,
    );
    const large_fonts = blk: {
        if (!any_plugin_needs_large_fonts) break :blk null;

        break :blk load_primary_fonts
        (
            state.config.?.primary_font_size * 2.0,
            state.config.?.emoji_font_size * 2.0,
        );
    };

    var monospace_font_config = zimgui.FontConfig.init_ImFontConfig();
    defer monospace_font_config.deinit();
    monospace_font_config.OversampleH = 1;
    monospace_font_config.OversampleV = 1;
    monospace_font_config.PixelSnapH = true;
    monospace_font_config.SizePixels = state.config.?.monospace_font_size;

    {
        const monospace_font_name = std.fs.path.basename(state.config.?.monospace_font_path.path.constSlice());
        _ = std.fmt.bufPrint
        (
            &monospace_font_config.Name,
            "{s}, {d}px\x00",
            .{
                monospace_font_name[0..@min(monospace_font_name.len, 32)],
                monospace_font_config.SizePixels,
            }
        )
            catch @panic("oom loading monospace font name");
    }

    const monospace_font = blk: {
        const monospace_font_posix_path = std.os.toPosixPath(state.config.?.monospace_font_path.path.constSlice())
            catch @panic("Failed to get posix path for monospace font.");

        // init using imgui's allocator to allow it to free the memory later
        break :blk state.shared_font_atlas.?.AddFontFromFileTTFExt
        (
            &monospace_font_posix_path,
            monospace_font_config.SizePixels,
            monospace_font_config,
            state.shared_font_atlas.?.GetGlyphRangesDefault(),
        );
    };

    state.font_table =
        if (any_plugin_needs_large_fonts)
            .{
                .primary_font_small = small_fonts.primary_font_ptr,
                .primary_font_large = large_fonts.?.primary_font_ptr,
                .emoji_font_small = small_fonts.emoji_font_ptr,
                .emoji_font_large = large_fonts.?.emoji_font_ptr,
                .monospace_font = monospace_font,
            }
        else
            .{
                .primary_font_small = small_fonts.primary_font_ptr,
                .primary_font_large = small_fonts.primary_font_ptr,
                .emoji_font_small = small_fonts.emoji_font_ptr,
                .emoji_font_large = small_fonts.emoji_font_ptr,
                .monospace_font = monospace_font,
            };

    _ = state.shared_font_atlas.?.Build();
    @atomicStore(bool, &state.font_thread_finished, true, .Release);
    @atomicStore(bool, &state.font_load_complete, true, .Release);
    std.log.scoped(.ATHENAOVERLAY).info("Font loading complete", .{});
}

pub fn load_shared_font_background(allocator: std.mem.Allocator, needs_large_fonts: bool) !void
{
    if (state.font_thread == null)
    {
        std.log.scoped(.ATHENAOVERLAY).info("Started background font loading thread", .{});
        state.font_thread = try std.Thread.spawn(.{}, load_shared_font, .{ allocator, needs_large_fonts });
    }
}
