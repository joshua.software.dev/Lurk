const builtin = @import("builtin");
const std = @import("std");

const build_with_plugins = @import("build_with_plugins.zig");
const config = @import("config.zig");
const font = @import("font.zig");
const overlay_types = @import("overlay_types.zig");
const state = @import("overlay_state.zig");

const zimgui = @import("Zig-ImGui");
const zstbi = @import("zstbi");


pub const PeakMemoryAllocator = overlay_types.PeakMemoryAllocator;
pub const DrawIdx = zimgui.DrawIdx;
pub const DrawVert = zimgui.DrawVert;
pub const TextureID = zimgui.TextureID;
pub const LoadImageHookFunction = fn ([]const u8, u32, u32, ?[]const u8) anyerror!?zimgui.TextureID;
pub const PluginsInitConfig = struct
{
    short_lived_allocator: std.mem.Allocator,
    long_lived_allocator: std.mem.Allocator,
    unbounded_heap_backed_allocator: std.mem.Allocator,
    load_image_hook_fn: ?*const LoadImageHookFunction,
};

pub fn init_plugins(blacklisted: bool, init_config: PluginsInitConfig) !void
{
    zstbi.init(init_config.long_lived_allocator);

    if (!blacklisted)
    {
        inline for (build_with_plugins.plugins) |plugin|
        {
            try plugin.init(init_config);
        }
    }

    state.plugin_init_complete = true;
}

pub fn deinit_plugins() void
{
    inline for (build_with_plugins.plugins) |plugin|
    {
        plugin.deinit();
    }

    if (state.plugin_init_complete)
    {
        zstbi.deinit();
        state.plugin_init_complete = false;
    }
}

pub const make_or_fetch_config = config.make_or_fetch_config;
pub const set_allocator_for_imgui = state.set_allocator_for_imgui;

pub fn load_fonts(font_allocator: std.mem.Allocator) !void
{
    if (state.shared_font_atlas == null)
    {
        state.shared_font_atlas = zimgui.FontAtlas.init_ImFontAtlas();

        const any_plugin_needs_large_fonts = blk: {
            var needs_large_fonts = false;

            inline for (build_with_plugins.plugins) |plugin|
            {
                needs_large_fonts = needs_large_fonts or (try plugin.plugin_requires_large_fonts());
            }

            break :blk needs_large_fonts;
        };

        if (!builtin.single_threaded and state.config.?.load_fonts_in_background_thread)
        {
            return font.load_shared_font_background(font_allocator, any_plugin_needs_large_fonts);
        }

        font.load_shared_font(font_allocator, any_plugin_needs_large_fonts);
    }
}

pub fn create_overlay_context() void
{
    if (state.overlay_context == null)
    {
        const old_ctx = zimgui.GetCurrentContext();
        zimgui.SetCurrentContext(null);
        defer zimgui.SetCurrentContext(old_ctx);

        const temp_atlas = zimgui.FontAtlas.init_ImFontAtlas();
        state.overlay_context = zimgui.CreateContextExt(temp_atlas);
        zimgui.SetCurrentContext(state.overlay_context);

        const io = zimgui.GetIO();
        io.Fonts = temp_atlas;
        io.IniFilename = null;
    }
}

pub fn use_overlay_context() !?*zimgui.Context
{
    if (state.overlay_context == null) return error.OverlayContextNotCreated;

    const old_ctx = zimgui.GetCurrentContext();
    zimgui.SetCurrentContext(state.overlay_context);
    return old_ctx;
}

pub fn restore_old_context(old_ctx: ?*zimgui.Context) void
{
    zimgui.SetCurrentContext(old_ctx);
}

pub fn destroy_overlay_context() void
{
    if (state.font_thread != null)
    {
        std.log.scoped(.ATHENAOVERLAY).warn("Waiting for font thread to close...", .{});
        state.font_thread.?.join();
        state.font_thread = null;

        if (state.overlay_context != null)
        {
            const old_ctx = use_overlay_context()
                catch unreachable;
            defer restore_old_context(old_ctx);
            const im_io = zimgui.GetIO();
            if (im_io.Fonts != null and im_io.Fonts != state.shared_font_atlas) im_io.Fonts.?.deinit();
        }
    }

    if (state.overlay_context != null)
    {
        zimgui.DestroyContextExt(state.overlay_context);
        state.shared_font_atlas.?.deinit();
        state.free_custom_allocator();
    }
}

pub fn get_font_atlas_as_pixels(comptime format: overlay_types.pixel_format) !overlay_types.font_texture_info
{
    var width: i32 = 0;
    var height: i32 = 0;
    var pixels: ?[*]u8 = undefined;

    const im_io = zimgui.GetIO();
    switch (format)
    {
        .alpha8 => im_io.Fonts.?.GetTexDataAsAlpha8(&pixels, &width, &height),
        .rgba32 => im_io.Fonts.?.GetTexDataAsRGBA32(&pixels, &width, &height),
    }

    if (pixels == null)
        return error.InvalidFontTextureData
    else if (width < 1 or height < 1)
        return error.InvalidFontTextureSize;

    return .{
        .x_width = @intCast(width),
        .y_height = @intCast(height),
        .data = pixels.?
        [
            0..
            (
                @as(u32, @intCast(width)) * @as(u32, @intCast(height)) * switch (format)
                {
                    .alpha8 => @sizeOf(u8),
                    .rgba32 => @sizeOf(u32),
                }
            )
        ]
    };
}

pub fn set_font_atlas_gpu_texture_id(id: zimgui.TextureID) void
{
    const im_io = zimgui.GetIO();
    im_io.Fonts.?.TexID = id;
}

pub fn get_draw_data() ?*zimgui.DrawData
{
    return zimgui.GetDrawData();
}

pub fn get_draw_data_draw_list(draw_data: *zimgui.DrawData) []const ?*zimgui.DrawList
{
    return draw_data.CmdLists.Data.?[0..draw_data.CmdLists.Size];
}

pub fn get_draw_list_command_buffer(draw_list: *zimgui.DrawList) []const zimgui.DrawCmd
{
    return draw_list.CmdBuffer.Data.?[0..draw_list.CmdBuffer.Size];
}

pub fn get_draw_list_index_buffer(draw_list: *zimgui.DrawList) []const zimgui.DrawIdx
{
    return draw_list.IdxBuffer.Data.?[0..draw_list.IdxBuffer.Size];
}

pub fn get_draw_list_vertex_buffer(draw_list: *zimgui.DrawList) []const zimgui.DrawVert
{
    return draw_list.VtxBuffer.Data.?[0..draw_list.VtxBuffer.Size];
}

pub fn is_draw_ready() !void
{
    if (!@atomicLoad(bool, &state.font_load_complete, .Acquire)) return error.FontNotLoaded;
    if (@atomicLoad(bool, &state.font_thread_finished, .Acquire))
    {
        if (state.font_thread != null)
        {
            std.log.scoped(.ATHENAOVERLAY).debug("Closing font thread before first draw...", .{});
            state.font_thread.?.join();
            state.font_thread = null;
        }
    }

    const im_io = zimgui.GetIO();
    if (im_io.Fonts != state.shared_font_atlas)
    {
        if (im_io.Fonts != null) im_io.Fonts.?.deinit();

        im_io.Fonts = state.shared_font_atlas;
        return error.FontTextureRequiresReload;
    }
}

pub fn draw_frame(display_x: u32, display_y: u32) !void
{
    defer
    {
        inline for (build_with_plugins.plugins) |plugin|
        {
            plugin.post_draw_frame();
        }
    }

    var which_plugins_ready: [build_with_plugins.plugins_count]bool = .{ false, } ** build_with_plugins.plugins_count;
    inline for (build_with_plugins.plugins, 0..) |plugin, i|
    {

        const this_plugin_enabled = plugin.ready_to_draw_frame()
            catch |err| switch (err)
            {
                error.WouldBlock => false,
                else => return err,
            };

        // All enabled plugins must be ready or this frame will instead render
        // the previous rendered frame to prevent flickering between out of
        // sync plugins
        if (this_plugin_enabled) |this_plugin_ready|
        {
            if (!this_plugin_ready) return;
            which_plugins_ready[i] = this_plugin_ready;
        }
    }

    const im_io = zimgui.GetIO();
    im_io.DisplaySize = zimgui.Vec2.init(@floatFromInt(display_x), @floatFromInt(display_y));

    zimgui.NewFrame();

    inline for (build_with_plugins.plugins, which_plugins_ready) |plugin, ready|
    {
        if (ready) try plugin.draw_frame(state.font_table.?, display_x, display_y);
    }

    zimgui.EndFrame();
    zimgui.Render();
}
