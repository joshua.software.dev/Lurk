const builtin = @import("builtin");
const std = @import("std");

const overlay_types = @import("overlay_types.zig");
const state = @import("overlay_state.zig");

const known_folders = @import("known-folders");
const simple = @import("simple");


const emoji_font_name = "Twemoji.Mozilla.v0.7.0.woff2";
const monospace_font_name = "NotoSansMono-Regular.woff2";
const primary_font_name = "GoNotoKurrent-Regular_v7.0.woff2";

const ConfigType = enum
{
    json,
    jsonc,
    yaml,
};

const true_false_map = std.ComptimeStringMap
(
    bool,
    .{
        .{ "true", true, },
        .{ "false", false, },
    }
);

fn parse_jsonc_config(config_file_fd: *const std.fs.File, allocator: std.mem.Allocator) !overlay_types.overlay_config
{
    const file_reader = config_file_fd.reader();
    var jsonc_reader = simple.jsonc.reader(allocator, file_reader);

    const parsed = try std.json.parseFromTokenSource
    (
        overlay_types.raw_config,
        allocator,
        &jsonc_reader,
        .{ .ignore_unknown_fields = true },
    );
    defer parsed.deinit();

    return .{
        .config_version = parsed.value.config_version,

        // Fonts
        .english_only = parsed.value.english_only,
        .download_missing_fonts = parsed.value.download_missing_fonts,
        .load_fonts_in_background_thread = parsed.value.load_fonts_in_background_thread,
        .primary_font_path = try simple.expand_realpath(allocator, parsed.value.primary_font_path),
        .primary_font_size = parsed.value.primary_font_size,
        .emoji_font_path = try simple.expand_realpath(allocator, parsed.value.emoji_font_path),
        .emoji_font_size = parsed.value.emoji_font_size,
        .monospace_font_path = try simple.expand_realpath(allocator, parsed.value.monospace_font_path),
        .monospace_font_size = parsed.value.monospace_font_size,
    };
}

fn parse_yaml_config(config_file_fd: *const std.fs.File, allocator: std.mem.Allocator) !overlay_types.overlay_config
{
    const zig_yaml = @import("zig_yaml");

    var yaml_reader = blk: {
        const config_text = try config_file_fd.readToEndAlloc(allocator, std.math.maxInt(usize));
        defer allocator.free(config_text);

        break :blk try zig_yaml.Yaml.load(allocator, config_text);
    };
    defer yaml_reader.deinit();

    const config_version = yaml_reader.docs.items[0].map.get("config_version")
        orelse return error.MissingField;

    // Fonts
    const english_only = yaml_reader.docs.items[0].map.get("english_only")
        orelse return error.MissingField;
    const download_missing_fonts = yaml_reader.docs.items[0].map.get("download_missing_fonts")
        orelse return error.MissingField;
    const load_fonts_in_background_thread = yaml_reader.docs.items[0].map.get("load_fonts_in_background_thread")
        orelse return error.MissingField;
    const primary_font_path = yaml_reader.docs.items[0].map.get("primary_font_path")
        orelse return error.MissingField;
    const primary_font_size = yaml_reader.docs.items[0].map.get("primary_font_size")
        orelse return error.MissingField;
    const emoji_font_path = yaml_reader.docs.items[0].map.get("emoji_font_path")
        orelse return error.MissingField;
    const emoji_font_size = yaml_reader.docs.items[0].map.get("emoji_font_size")
        orelse return error.MissingField;
    const monospace_font_path = yaml_reader.docs.items[0].map.get("monospace_font_path")
        orelse return error.MissingField;
    const monospace_font_size = yaml_reader.docs.items[0].map.get("monospace_font_size")
        orelse return error.MissingField;

    return .{
        .config_version = @intCast(config_version.int),

        // Fonts
        .english_only = true_false_map.get(english_only.string)
            orelse return error.InvalidValue,
        .download_missing_fonts = true_false_map.get(download_missing_fonts.string)
            orelse return error.InvalidValue,
        .load_fonts_in_background_thread = true_false_map.get(load_fonts_in_background_thread.string)
            orelse return error.InvalidValue,
        .primary_font_path = try simple.expand_realpath(allocator, primary_font_path.string),
        .primary_font_size = @floatCast(primary_font_size.float),
        .emoji_font_path = try simple.expand_realpath(allocator, emoji_font_path.string),
        .emoji_font_size = @floatCast(emoji_font_size.float),
        .monospace_font_path = try simple.expand_realpath(allocator, monospace_font_path.string),
        .monospace_font_size = @floatCast(monospace_font_size.float),
    };
}

fn parse_libyaml_config(config_file_fd: *const std.fs.File, allocator: std.mem.Allocator) !overlay_types.overlay_config
{
    const libyaml_zig = @import("libyaml_zig");

    const config_text = try config_file_fd.readToEndAlloc(allocator, std.math.maxInt(usize));
    defer allocator.free(config_text);
    var document = try libyaml_zig.parse(allocator, config_text);

    const config_version = document.mapping.get("config_version")
        orelse return error.MissingField;

    // Fonts
    const english_only = document.mapping.get("english_only")
        orelse return error.MissingField;
    const download_missing_fonts = document.mapping.get("download_missing_fonts")
        orelse return error.MissingField;
    const load_fonts_in_background_thread = document.mapping.get("load_fonts_in_background_thread")
        orelse return error.MissingField;
    const primary_font_path = document.mapping.get("primary_font_path")
        orelse return error.MissingField;
    const primary_font_size = document.mapping.get("primary_font_size")
        orelse return error.MissingField;
    const emoji_font_path = document.mapping.get("emoji_font_path")
        orelse return error.MissingField;
    const emoji_font_size = document.mapping.get("emoji_font_size")
        orelse return error.MissingField;
    const monospace_font_path = document.mapping.get("monospace_font_path")
        orelse return error.MissingField;
    const monospace_font_size = document.mapping.get("monospace_font_size")
        orelse return error.MissingField;

    return .{
        .config_version = try std.fmt.parseInt(u32, config_version.string, 10),

        // Fonts
        .english_only = true_false_map.get(english_only.string)
            orelse return error.InvalidValue,
        .download_missing_fonts = true_false_map.get(download_missing_fonts.string)
            orelse return error.InvalidValue,
        .load_fonts_in_background_thread = true_false_map.get(load_fonts_in_background_thread.string)
            orelse return error.InvalidValue,
        .primary_font_path = try simple.expand_realpath(allocator, primary_font_path.string),
        .primary_font_size = try std.fmt.parseFloat(f32, primary_font_size.string),
        .emoji_font_path = try simple.expand_realpath(allocator, emoji_font_path.string),
        .emoji_font_size = try std.fmt.parseFloat(f32, emoji_font_size.string),
        .monospace_font_path = try simple.expand_realpath(allocator, monospace_font_path.string),
        .monospace_font_size = try std.fmt.parseFloat(f32, monospace_font_size.string),
    };
}

pub fn make_or_fetch_config(allocator: ?std.mem.Allocator) !overlay_types.overlay_config
{
    if (state.config) |c| return c;

    var arena = std.heap.ArenaAllocator.init(allocator.?);
    defer arena.deinit();

    const athena_overlay_cache_path = blk: {
        const maybe_path = try known_folders.getPath(arena.allocator(), .cache);
        if (maybe_path == null) return error.FailedToFindCacheFolder;
        const cache_folder_path = maybe_path.?;
        defer arena.allocator().free(cache_folder_path);

        const athena_overlay_cache_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                cache_folder_path,
                "athena_overlay",
            },
        );

        std.fs.makeDirAbsolute(athena_overlay_cache_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        break :blk athena_overlay_cache_path;
    };
    defer arena.allocator().free(athena_overlay_cache_path);

    var config_extension: ?ConfigType = null;
    const config_file_fd = blk: {
        const config_folder_path = (
            try known_folders.getPath
            (
                arena.allocator(),
                .local_configuration,
            )
        )
            orelse return error.FailedToFindConfig;
        defer arena.allocator().free(config_folder_path);

        const athena_overlay_config_folder_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                config_folder_path,
                "athena_overlay",
            }
        );
        defer arena.allocator().free(athena_overlay_config_folder_path);

        std.fs.makeDirAbsolute(athena_overlay_config_folder_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        const file_extensions: []const ConfigType = &.{ .json, .jsonc, .yaml, };
        for (file_extensions) |ext|
        {
            config_extension = ext;
            const athena_overlay_config_path = try std.fs.path.join
            (
                arena.allocator(),
                &.{
                    athena_overlay_config_folder_path,
                    switch (ext)
                    {
                        .json => "athena_overlay_config.json",
                        .jsonc => "athena_overlay_config.jsonc",
                        .yaml => "athena_overlay_config.yaml",
                    },
                },
            );
            defer arena.allocator().free(athena_overlay_config_path);

            break :blk std.fs.openFileAbsolute(athena_overlay_config_path, .{})
                catch |err| switch (err)
                {
                    error.FileNotFound => continue,
                    else => return err,
                };
        }

        // create file if not found
        const athena_overlay_config_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                athena_overlay_config_folder_path,
                "athena_overlay_config.jsonc",
            }
        );
        defer arena.allocator().free(athena_overlay_config_path);

        config_extension = .jsonc;
        const fd = try std.fs.createFileAbsolute(athena_overlay_config_path, .{ .read = true, .truncate = false, });
        const file_info = try fd.stat();
        if (file_info.size == 0)
        {
            try fd.writeAll(@embedFile("default_config.jsonc"));
            try fd.seekTo(0);
        }

        break :blk fd;
    };
    defer config_file_fd.close();

    state.config = switch (config_extension.?)
    {
        .json, .jsonc => try parse_jsonc_config(&config_file_fd, arena.allocator()),
        .yaml => try parse_libyaml_config(&config_file_fd, arena.allocator()),
    };

    if (!state.config.?.primary_font_path.exists)
    {
        {
            const primary_font_cache_path = try std.fs.path.join
            (
                arena.allocator(),
                &.{
                    athena_overlay_cache_path,
                    primary_font_name,
                },
            );
            defer arena.allocator().free(primary_font_cache_path);

            state.config.?.primary_font_path.path.len = 0;
            try state.config.?.primary_font_path.path.appendSlice(primary_font_cache_path);
        }

        state.config.?.primary_font_path.exists = blk: {
            std.fs.accessAbsolute(state.config.?.primary_font_path.path.constSlice(), .{})
                catch break :blk false;
            break :blk true;
        };
    }

    if (!state.config.?.emoji_font_path.exists)
    {
        {
            const emoji_font_cache_path = try std.fs.path.join
            (
                arena.allocator(),
                &.{
                    athena_overlay_cache_path,
                    emoji_font_name,
                },
            );
            defer arena.allocator().free(emoji_font_cache_path);

            state.config.?.emoji_font_path.path.len = 0;
            try state.config.?.emoji_font_path.path.appendSlice(emoji_font_cache_path);
        }

        state.config.?.emoji_font_path.exists = blk: {
            std.fs.accessAbsolute(state.config.?.emoji_font_path.path.constSlice(), .{})
                catch break :blk false;
            break :blk true;
        };
    }

    if (!state.config.?.monospace_font_path.exists)
    {
        {
            const monospace_font_cache_path = try std.fs.path.join
            (
                arena.allocator(),
                &.{
                    athena_overlay_cache_path,
                    monospace_font_name,
                },
            );
            defer arena.allocator().free(monospace_font_cache_path);

            state.config.?.monospace_font_path.path.len = 0;
            try state.config.?.monospace_font_path.path.appendSlice(monospace_font_cache_path);
        }

        state.config.?.monospace_font_path.exists = blk: {
            std.fs.accessAbsolute(state.config.?.monospace_font_path.path.constSlice(), .{})
                catch break :blk false;
            break :blk true;
        };
    }

    return state.config.?;
}
