const std = @import("std");

const simple = @import("simple");
const zimgui = @import("Zig-ImGui");


pub const pixel_format = enum
{
    alpha8,
    rgba32,
};

pub const font_texture_info = struct
{
    x_width: u32,
    y_height: u32,
    data: []const u8,
};

pub const FontTable = struct
{
    primary_font_small: ?*zimgui.Font,
    primary_font_large: ?*zimgui.Font,
    emoji_font_small: ?*zimgui.Font,
    emoji_font_large: ?*zimgui.Font,
    monospace_font: ?*zimgui.Font,
};

pub const overlay_config = struct
{
    config_version: u32,

    // Fonts
    english_only: bool,
    download_missing_fonts: bool,
    load_fonts_in_background_thread: bool,
    primary_font_path: simple.RealPath,
    primary_font_size: f32,
    emoji_font_path: simple.RealPath,
    emoji_font_size: f32,
    monospace_font_path: simple.RealPath,
    monospace_font_size: f32,
};

pub const raw_config = struct
{
    config_version: u32,

    // Fonts
    english_only: bool,
    download_missing_fonts: bool,
    load_fonts_in_background_thread: bool,
    primary_font_path: []const u8,
    primary_font_size: f32,
    emoji_font_path: []const u8,
    emoji_font_size: f32,
    monospace_font_path: []const u8,
    monospace_font_size: f32,
};

pub fn PeakMemoryAllocator(comptime gpa_type: anytype) type {
    return struct
    {
        peak_usage: usize,
        backing_allocator: *gpa_type,

        pub fn init(gpa: *gpa_type) @This()
        {
            return .{
                .backing_allocator = gpa,
                .peak_usage = 0,
            };
        }

        pub fn allocator(self: *@This()) std.mem.Allocator
        {
            return .{
                .ptr = self,
                .vtable = &.{
                    .alloc = alloc,
                    .resize = resize,
                    .free = free,
                },
            };
        }

        pub fn set_peak(self: *@This()) void
        {
            const current_peak = self.peak_usage;
            self.peak_usage = @max(self.peak_usage, self.backing_allocator.total_requested_bytes);
            if (current_peak < self.peak_usage)
            {
                std.log.scoped(.ATHENAOVERLAY).err("NEW PEAK MEM: {d}", .{ self.peak_usage });
            }
        }

        pub fn alloc(ctx: *anyopaque, n: usize, log2_ptr_align: u8, ra: usize) ?[*]u8
        {
            const self: *@This() = @ptrCast(@alignCast(ctx));
            const gpa = self.backing_allocator.allocator();
            const result = gpa.vtable.alloc(gpa.ptr, n, log2_ptr_align, ra);
            self.set_peak();
            return result;
        }

        pub fn resize
        (
            ctx: *anyopaque,
            buf: []u8,
            log2_buf_align: u8,
            new_size: usize,
            return_address: usize,
        )
        bool
        {
            const self: *@This() = @ptrCast(@alignCast(ctx));
            const gpa = self.backing_allocator.allocator();
            const result = gpa.vtable.resize(gpa.ptr, buf, log2_buf_align, new_size, return_address);
            self.set_peak();
            return result;
        }

        pub fn free
        (
            ctx: *anyopaque,
            buf: []u8,
            log2_buf_align: u8,
            return_address: usize,
        )
        void
        {
            const self: *@This() = @ptrCast(@alignCast(ctx));
            const gpa = self.backing_allocator.allocator();
            const result = gpa.vtable.free(gpa.ptr, buf, log2_buf_align, return_address);
            self.set_peak();
            return result;
        }
    };
}
