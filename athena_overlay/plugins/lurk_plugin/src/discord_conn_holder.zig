const builtin = @import("builtin");
const std = @import("std");

const plugin_config = @import("plugin_config.zig");

const disc = @import("discord_ws_conn");
const simple = @import("simple");
const zstbi = @import("zstbi");


const sleep_interval: u64 = 0.5 * std.time.ns_per_s;
const stacktrace = switch (builtin.mode)
{
    .Debug => true,
    else => false,
};
const timeout: u64 = 0.5 * std.time.ns_per_ms;

pub var conn: ?disc.DiscordWsConn = null;
var http_client: ?simple.StdHttpClient = null;
var message_thread: ?std.Thread = null;
var thread_should_run: bool = false;

fn load_smol_image(image_allocator: std.mem.Allocator, data: []const u8) anyerror![]const u8
{
    var smol = blk: {
        var image_data = try zstbi.Image.loadFromMemory(data, 4);
        defer image_data.deinit();
        defer image_allocator.free(data);

        const config = try plugin_config.make_or_fetch_config(null);
        break :blk image_data.resize(config.avatar_max_size, config.avatar_max_size);
    };
    defer smol.deinit();

    return try image_allocator.dupe(u8, smol.data);
}

pub fn start_discord_conn
(
    http_allocator: std.mem.Allocator,
    image_allocator: ?std.mem.Allocator,
    heap_allocator: std.mem.Allocator,
)
!void
{
    if (conn != null) return;

    const config = try plugin_config.make_or_fetch_config(null);

    const simple_client = blk: {
        if (builtin.zig_backend == .stage2_x86_64)
        {
            var curl = try simple.init_curl_client();
            break :blk curl.as_simple_client();
        }

        http_client = try simple.init_std_http_client
        (
            .{
                .bundle = .{
                    .allocate_new = .{
                        .creation_allocator = heap_allocator,
                        .final_cert_allocator = http_allocator,
                    },
                },
                .http_allocator = http_allocator,
            },
        );
        break :blk http_client.?.as_simple_client();
    };

    conn = try disc.DiscordWsConn.init
    (
        simple_client,
        heap_allocator,
        image_allocator,
        &load_smol_image,
        if (config.*.logging_path) |*lp|
            .{ .output_path = lp.constSlice(), }
        else
            null,
    );

    if ((!builtin.single_threaded) and config.use_background_network_thread)
    {
        @atomicStore(bool, &thread_should_run, true, .Release);
        message_thread = try std.Thread.spawn(.{}, handle_message_thread, .{});

        std.log.scoped(.ATHENAPLUGIN).info("Started background discord message thread", .{});
    }
    else
    {
        const connection_uri = try conn.?.connect();
        std.log.scoped(.ATHENAPLUGIN).info
        (
            "Connection Success: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
            .{ connection_uri.? },
        );
    }
}

pub fn handle_one_message() !void
{
    _ = conn.?.receive_next_msg(timeout)
        catch |err| switch (err)
        {
            std.net.Stream.ReadError.WouldBlock => {},
            else => return err,
        };
}

pub fn handle_message_thread() void
{
    while (true) start_of_thread: {
        const max_sleep = std.time.ns_per_s * 8;
        var start_sleep: u64 = std.time.ns_per_s * 1;

        while (@atomicLoad(bool, &thread_should_run, .Acquire))
        {
            if (conn == null)
            {
                std.log.scoped(.ATHENAPLUGIN).err
                (
                    "Connection details were not setup, could not connect to discord. Closing thread...",
                    .{},
                );
                return;
            }

            const connection_uri = conn.?.connect()
                catch |err| switch (err)
                {
                    else =>
                    {
                        std.log.scoped(.ATHENAPLUGIN).warn
                        (
                            "Failed to connect to Discord, sleeping for {d} seconds before trying again...",
                            .{ start_sleep / std.time.ns_per_s, },
                        );

                        var now: u64 = @intCast(std.time.nanoTimestamp());
                        const until: u64 = now + start_sleep;
                        while (now < until)
                        {
                            if (!@atomicLoad(bool, &thread_should_run, .Acquire))
                            {
                                return;
                            }

                            std.time.sleep(sleep_interval);
                            now = @intCast(std.time.nanoTimestamp());
                        }

                        start_sleep = start_sleep * 2;
                        if (start_sleep > max_sleep) start_sleep = max_sleep;
                        continue;
                    }
                };

            if (connection_uri == null) continue;
            std.log.scoped(.ATHENAPLUGIN).info
            (
                "Connection Success: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                .{ connection_uri.? },
            );

            break;
        }

        while (@atomicLoad(bool, &thread_should_run, .Acquire))
        {
            handle_one_message()
                catch |err| switch (err)
                {
                    error.EndOfStream =>
                    {
                        std.log.scoped(.ATHENAPLUGIN).warn
                        (
                            "Lost connection to Discord, attempting to reconnect...",
                            .{},
                        );
                        conn.?.ws_conn.?.deinit();
                        conn.?.ws_conn = null;
                        conn.?.state.current_channel.reset();
                        break :start_of_thread;
                    },
                    else =>
                    {
                        std.log.err("{s}", .{ @errorName(err) });
                        if (stacktrace)
                        {
                            if (@errorReturnTrace()) |trace|
                            {
                                std.debug.dumpStackTrace(trace.*);
                            }
                        }

                        @panic("Unexpected error, aborting...");
                    },
                };
        }

        return;
    }
}

pub fn stop_discord_conn() void
{
    if (message_thread != null)
    {
        std.log.scoped(.ATHENAPLUGIN).info("Received shutdown command, shutting down message thread...", .{});
        @atomicStore(bool, &thread_should_run, false, .Release);
        message_thread.?.join();
        message_thread = null;
    }

    if (conn == null)
    {
        std.log.scoped(.ATHENAPLUGIN).warn("Discord connection was not started, could not close.", .{});
        return;
    }

    std.log.scoped(.ATHENAPLUGIN).info("Closing connection to discord...", .{});
    conn.?.close();
    conn = null;
    http_client = null;
    std.log.scoped(.ATHENAPLUGIN).info("Connection closed.", .{});
}
