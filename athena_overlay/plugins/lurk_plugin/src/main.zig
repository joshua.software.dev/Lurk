const builtin = @import("builtin");
const std = @import("std");

const disch = @import("discord_conn_holder.zig");
const plugin_config = @import("plugin_config.zig");
const plugin_types = @import("plugin_types.zig");

const zimgui = @import("Zig-ImGui");
const zstbi = @import("zstbi");


const LoadImageHookFunction = fn ([]const u8, u32, u32, ?[]const u8) anyerror!?zimgui.TextureID;
var load_image_hook: ?*const LoadImageHookFunction = null;
var needs_unlock = false;
var this_plugin_init_complete = false;

pub fn init(init_config: anytype) !void
{
    const short_allocator: std.mem.Allocator = init_config.short_lived_allocator;
    const long_allocator: std.mem.Allocator = init_config.long_lived_allocator;
    const heap_allocator: std.mem.Allocator = init_config.unbounded_heap_backed_allocator;
    const load_image_hook_fn: @TypeOf(load_image_hook) = init_config.load_image_hook_fn;

    const config = try plugin_config.make_or_fetch_config(short_allocator);
    if (!config.enable_plugin) return;

    if (config.show_user_avatars and load_image_hook == null)
    {
        load_image_hook = load_image_hook_fn;

        // Internal logic makes connecting multiple times idempotent
        try disch.start_discord_conn(short_allocator, long_allocator, heap_allocator);
    }
    else
    {
        // Internal logic makes connecting multiple times idempotent
        try disch.start_discord_conn(short_allocator, null, heap_allocator);
    }

    this_plugin_init_complete = true;
}

pub fn deinit() void
{
    if (load_image_hook != null)
    {
        load_image_hook = null;
    }

    if (this_plugin_init_complete)
    {
        disch.stop_discord_conn();
    }
}

pub fn plugin_requires_large_fonts() !bool
{
    return false;
}

pub fn ready_to_draw_frame() !?bool
{
    const config = try plugin_config.make_or_fetch_config(null);
    if (!config.enable_plugin) return null;

    if (disch.conn != null and disch.conn.?.ws_conn != null)
    {
        if (!config.use_background_network_thread)
        {
            try disch.handle_one_message();
            return true;
        }
        else
        {
            if (disch.conn.?.state.users_map_lock.tryLock())
            {
                needs_unlock = true;
                return true;
            }
        }

        // Reuse previous frame if lock cannot be acquired
        return false;
    }

    // Do not delay drawing other plugins, but skip drawing this one this frame
    return null;
}

fn set_window_position
(
    display_x: u32,
    display_y: u32,
    window_x: f32,
    window_y: f32,
    position: plugin_types.WindowPosition,
    margin: f32,
)
void
{
    switch (position)
    {
        .TOP_LEFT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init(margin, margin)
            ),
        .TOP_RIGHT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init(@as(f32, @floatFromInt(display_x)) - window_x - margin, margin)
            ),
        .BOTTOM_LEFT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init(margin, @as(f32, @floatFromInt(display_y)) - window_y - margin)
            ),
        .BOTTOM_RIGHT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init
                (
                    @as(f32, @floatFromInt(display_x)) - window_x - margin,
                    @as(f32, @floatFromInt(display_y)) - window_y - margin
                )
            ),
    }
}

fn draw_frame_contents() !void
{
    const config = try plugin_config.make_or_fetch_config(null);

    var alloc_buf: [512]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&alloc_buf);

    if (config.show_header_line) zimgui.Separator();

    if (disch.conn) |*conn|
    {
        for (conn.state.users_map.map.values()) |*user|
        {
            fba.reset();

            const avatar_max_size: f32 = @floatFromInt(config.avatar_max_size);
            const cursor = zimgui.GetCursorScreenPos();
            var draw_list = zimgui.GetWindowDrawList().?;

            if (config.show_user_avatars)
            {
                zimgui.Dummy(zimgui.Vec2.init(avatar_max_size, avatar_max_size));
                zimgui.SameLine();

                if (load_image_hook != null)
                {
                    const maybe_texture_pointer: ?zimgui.TextureID = blk: {
                        const existing_texture = try load_image_hook.?(user.user_id.constSlice(), 0, 0, null);
                        if (existing_texture != null) break :blk existing_texture.?;

                        if (user.avatar_image) |avatar_image|
                        {
                            std.log.scoped(.ATHENAPLUGIN).debug
                            (
                                "Loading user image: {s}|{s}",
                                .{
                                    user.user_id.constSlice(),
                                    avatar_image.avatar_id.constSlice(),
                                },
                            );

                            const result = try load_image_hook.?
                            (
                                user.user_id.constSlice(),
                                config.avatar_max_size,
                                config.avatar_max_size,
                                avatar_image.image_bytes,
                            );

                            break :blk result.?;
                        }

                        break :blk null;
                    };

                    if (maybe_texture_pointer) |texture_pointer|
                    {
                        if (config.round_avatar_images)
                        {
                            draw_list.AddImageRounded
                            (
                                texture_pointer,
                                cursor,
                                zimgui.Vec2.init(cursor.x + avatar_max_size, cursor.y + avatar_max_size),
                                zimgui.Vec2.init(0.0, 0.0),
                                zimgui.Vec2.init(1.0, 1.0),
                                0xFFFFFFFF,
                                avatar_max_size * 0.5,
                            );
                        }
                        else
                        {
                            draw_list.AddImage
                            (
                                texture_pointer,
                                cursor,
                                zimgui.Vec2.init(cursor.x + avatar_max_size, cursor.y + avatar_max_size),
                            );
                        }
                    }
                }
            }

            zimgui.PushStyleColor_Vec4(.Button, zimgui.Vec4.init(0.0, 0.0, 0.0, config.username_opacity));
            var style_count: u32 = 1;
            defer zimgui.PopStyleColorExt(@intCast(style_count));

            // the fba.reset() handles this deallocation
            var safe_name = try fba.allocator().allocSentinel(u8, user.nickname.?.constSlice().len, 0);
            for (user.nickname.?.constSlice(), 0..) |chr, i|
            {
                safe_name[i] = switch (chr)
                {
                    '\x00' => ' ',
                    '#' => '+',
                    '%' => '+',
                    else => chr,
                };
            }

            const color = blk: {
                if (user.muted and user.deafened) { break :blk config.username_deafened_muted_color; }
                else if (user.muted) { break :blk config.username_muted_color; }
                else if (user.deafened) { break :blk config.username_deafened_color; }
                else if (user.speaking) { break :blk config.username_speaking_color; }

                break :blk config.username_default_color;
            };
            zimgui.PushStyleColor_U32(.Text, color);
            style_count += 1;
            if (config.show_user_avatars and (user.deafened or user.muted or user.speaking))
            {
                if (config.round_avatar_images)
                {
                    draw_list.AddCircleExt
                    (
                        zimgui.Vec2.init
                        (
                            cursor.x + (avatar_max_size * 0.5),
                            cursor.y + (avatar_max_size * 0.5),
                        ),
                        (avatar_max_size - 1.0) * 0.5,
                        color,
                        0,
                        3.0,
                    );
                }
                else
                {
                    draw_list.AddRectExt
                    (
                        zimgui.Vec2.init(cursor.x + 1.0, cursor.y + 1.0),
                        zimgui.Vec2.init(cursor.x + avatar_max_size - 1.0, cursor.y + avatar_max_size - 1.0),
                        color,
                        0.0,
                        .{},
                        3.0,
                    );
                }
            }

            _ = zimgui.ButtonExt
            (
                safe_name[0..].ptr,
                zimgui.Vec2.init
                (
                    0.0,
                    @floatFromInt(config.username_row_height),
                ),
            );
        }
    }
}

pub fn draw_frame(_: anytype, display_x: u32, display_y: u32) !void
{
    const config = try plugin_config.make_or_fetch_config(null);

    zimgui.SetNextWindowBgAlpha(config.window_background_opacity);

    zimgui.PushStyleVar_Float(.WindowBorderSize, 0);
    defer zimgui.PopStyleVarExt(1);

    const window_size = zimgui.Vec2.init(400, @floatFromInt(config.ui_max_height orelse display_y));
    zimgui.SetNextWindowSize(window_size);
    set_window_position
    (
        display_x,
        display_y,
        window_size.x,
        window_size.y,
        config.window_position,
        @floatFromInt(config.screen_margin),
    );

    var show_window = true;
    if
    (
        zimgui.BeginExt
        (
            "Lurk",
            &show_window,
            .{
                .NoCollapse = true,
                .NoResize = true,
                .NoScrollbar = true,
                .NoTitleBar = true,
            }
        )
    )
    {
        defer zimgui.End();
        try draw_frame_contents();
    }
}

pub fn post_draw_frame() void
{
    if (needs_unlock)
    {
        needs_unlock = false;
        disch.conn.?.state.users_map_lock.unlock();
    }
}
