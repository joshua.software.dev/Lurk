const builtin = @import("builtin");
const std = @import("std");

const plugin_types = @import("plugin_types.zig");

const known_folders = @import("known-folders");
const simple = @import("simple");


const true_false_map = std.ComptimeStringMap
(
    bool,
    .{
        .{ "true", true, },
        .{ "false", false, },
    }
);

var config: ?plugin_types.PluginConfigDiscord = null;

fn parse_css6_color(input_color: []const u8) !u32
{
    var temp_color = try simple.BoundedArray(u8, 8).init(0);
    try temp_color.appendSlice("FF");

    const trimmed = std.mem.trimLeft(u8, input_color, "#");
    try temp_color.appendSlice(trimmed[4..6]);
    try temp_color.appendSlice(trimmed[2..4]);
    try temp_color.appendSlice(trimmed[0..2]);

    return try std.fmt.parseInt(u32, temp_color.constSlice(), 16);
}

fn parse_jsonc_config
(
    config_file_fd: *const std.fs.File,
    allocator: std.mem.Allocator,
)
!plugin_types.PluginConfigDiscord
{
    const file_reader = config_file_fd.reader();
    var jsonc_reader = simple.jsonc.reader(allocator, file_reader);

    const parsed = try std.json.parseFromTokenSource
    (
        plugin_types.PluginConfigDiscordRaw,
        allocator,
        &jsonc_reader,
        .{ .ignore_unknown_fields = true },
    );
    defer parsed.deinit();

    return .{
        .config_version = parsed.value.config_version,
        .enable_plugin = parsed.value.enable_plugin,

        // UI Details
        .show_header_line = parsed.value.show_header_line,
        .screen_margin = parsed.value.screen_margin,
        .window_position = parsed.value.window_position,
        .window_background_opacity = parsed.value.window_background_opacity,
        .ui_max_height = if (parsed.value.ui_max_height < 0) null else @intCast(parsed.value.ui_max_height),
        .show_user_avatars = parsed.value.show_user_avatars,
        .round_avatar_images = parsed.value.round_avatar_images,
        .avatar_max_size = parsed.value.avatar_max_size,
        .username_row_height = parsed.value.username_row_height,
        .username_opacity = parsed.value.username_opacity,
        .username_default_color = try parse_css6_color(parsed.value.username_default_color),
        .username_speaking_color = try parse_css6_color(parsed.value.username_speaking_color),
        .username_muted_color = try parse_css6_color(parsed.value.username_muted_color),
        .username_deafened_color = try parse_css6_color(parsed.value.username_deafened_color),
        .username_deafened_muted_color = try parse_css6_color(parsed.value.username_deafened_muted_color),

        // Advanced
        .use_background_network_thread = parsed.value.use_background_network_thread,
        .logging_path =
            if (parsed.value.logging_path) |lp|
                try simple.BoundedArray(u8, std.fs.MAX_PATH_BYTES - 1).fromSlice(lp)
            else
                null,
    };
}

fn parse_yaml_config
(
    config_file_fd: *const std.fs.File,
    allocator: std.mem.Allocator,
)
!plugin_types.PluginConfigDiscord
{
    const zig_yaml = @import("zig_yaml");

    var yaml_reader = blk: {
        const config_text = try config_file_fd.readToEndAlloc(allocator, std.math.maxInt(usize));
        defer allocator.free(config_text);

        break :blk try zig_yaml.Yaml.load(allocator, config_text);
    };
    defer yaml_reader.deinit();

    const config_version = yaml_reader.docs.items[0].map.get("config_version")
        orelse return error.MissingField;
    const enable_plugin = yaml_reader.docs.items[0].map.get("enable_plugin")
        orelse return error.MissingField;

    // UI Details
    const show_header_line = yaml_reader.docs.items[0].map.get("show_header_line")
        orelse return error.MissingField;
    const screen_margin = yaml_reader.docs.items[0].map.get("screen_margin")
        orelse return error.MissingField;
    const window_position = yaml_reader.docs.items[0].map.get("window_position")
        orelse return error.MissingField;
    const window_background_opacity = yaml_reader.docs.items[0].map.get("window_background_opacity")
        orelse return error.MissingField;
    const ui_max_height = yaml_reader.docs.items[0].map.get("ui_max_height")
        orelse return error.MissingField;
    const show_user_avatars = yaml_reader.docs.items[0].map.get("show_user_avatars")
        orelse return error.MissingField;
    const round_avatar_images = yaml_reader.docs.items[0].map.get("round_avatar_images")
        orelse return error.MissingField;
    const avatar_max_size = yaml_reader.docs.items[0].map.get("avatar_max_size")
        orelse return error.MissingField;
    const username_row_height = yaml_reader.docs.items[0].map.get("username_row_height")
        orelse return error.MissingField;
    const username_opacity = yaml_reader.docs.items[0].map.get("username_opacity")
        orelse return error.MissingField;
    const username_default_color = yaml_reader.docs.items[0].map.get("username_default_color")
        orelse return error.MissingField;
    const username_speaking_color = yaml_reader.docs.items[0].map.get("username_speaking_color")
        orelse return error.MissingField;
    const username_muted_color = yaml_reader.docs.items[0].map.get("username_muted_color")
        orelse return error.MissingField;
    const username_deafened_color = yaml_reader.docs.items[0].map.get("username_deafened_color")
        orelse return error.MissingField;
    const username_deafened_muted_color = yaml_reader.docs.items[0].map.get("username_deafened_muted_color")
        orelse return error.MissingField;

    // Advanced
    const use_background_network_thread = yaml_reader.docs.items[0].map.get("use_background_network_thread")
        orelse return error.MissingField;
    const logging_path = yaml_reader.docs.items[0].map.get("logging_path")
        orelse return error.MissingField;

    return .{
        .config_version = @intCast(config_version.int),
        .enable_plugin = true_false_map.get(enable_plugin.string)
            orelse return error.InvalidValue,

        // UI Details
        .show_header_line = true_false_map.get(show_header_line.string)
            orelse return error.InvalidValue,
        .screen_margin = @intCast(screen_margin.int),
        .window_position = std.meta.stringToEnum(plugin_types.WindowPosition, window_position.string)
            orelse return error.InvalidWindowPosition,
        .window_background_opacity = @floatCast(window_background_opacity.float),
        .ui_max_height =
            if (ui_max_height.int < 0)
                null
            else
                @intCast(ui_max_height.int),
        .show_user_avatars = true_false_map.get(show_user_avatars.string)
            orelse return error.InvalidValue,
        .round_avatar_images = true_false_map.get(round_avatar_images.string)
            orelse return error.InvalidValue,
        .avatar_max_size = @intCast(avatar_max_size.int),
        .username_row_height = @intCast(username_row_height.int),
        .username_opacity = @floatCast(username_opacity.float),
        .username_default_color = try parse_css6_color(username_default_color.string),
        .username_speaking_color = try parse_css6_color(username_speaking_color.string),
        .username_muted_color = try parse_css6_color(username_muted_color.string),
        .username_deafened_color = try parse_css6_color(username_deafened_color.string),
        .username_deafened_muted_color = try parse_css6_color(username_deafened_muted_color.string),

        // Advanced
        .use_background_network_thread = true_false_map.get(use_background_network_thread.string)
            orelse return error.InvalidValue,
        .logging_path =
            if (logging_path != .empty)
                try simple.BoundedArray(u8, std.fs.MAX_PATH_BYTES - 1).fromSlice(logging_path.string)
            else
                null,
    };
}

fn parse_libyaml_config
(
    config_file_fd: *const std.fs.File,
    allocator: std.mem.Allocator,
)
!plugin_types.PluginConfigDiscord
{
    const libyaml_zig = @import("libyaml_zig");

    const config_text = try config_file_fd.readToEndAlloc(allocator, std.math.maxInt(usize));
    defer allocator.free(config_text);
    var document = try libyaml_zig.parse(allocator, config_text);

    const config_version = document.mapping.get("config_version")
        orelse return error.MissingField;
    const enable_plugin = document.mapping.get("enable_plugin")
        orelse return error.MissingField;

    // UI Details
    const show_header_line = document.mapping.get("show_header_line")
        orelse return error.MissingField;
    const screen_margin = document.mapping.get("screen_margin")
        orelse return error.MissingField;
    const window_position = document.mapping.get("window_position")
        orelse return error.MissingField;
    const window_background_opacity = document.mapping.get("window_background_opacity")
        orelse return error.MissingField;
    const ui_max_height = document.mapping.get("ui_max_height")
        orelse return error.MissingField;
    const show_user_avatars = document.mapping.get("show_user_avatars")
        orelse return error.MissingField;
    const round_avatar_images = document.mapping.get("round_avatar_images")
        orelse return error.MissingField;
    const avatar_max_size = document.mapping.get("avatar_max_size")
        orelse return error.MissingField;
    const username_row_height = document.mapping.get("username_row_height")
        orelse return error.MissingField;
    const username_opacity = document.mapping.get("username_opacity")
        orelse return error.MissingField;
    const username_default_color = document.mapping.get("username_default_color")
        orelse return error.MissingField;
    const username_speaking_color = document.mapping.get("username_speaking_color")
        orelse return error.MissingField;
    const username_muted_color = document.mapping.get("username_muted_color")
        orelse return error.MissingField;
    const username_deafened_color = document.mapping.get("username_deafened_color")
        orelse return error.MissingField;
    const username_deafened_muted_color = document.mapping.get("username_deafened_muted_color")
        orelse return error.MissingField;

    // Advanced
    const use_background_network_thread = document.mapping.get("use_background_network_thread")
        orelse return error.MissingField;
    const logging_path = document.mapping.get("logging_path")
        orelse return error.MissingField;

    return .{
        .config_version = try std.fmt.parseInt(u32, config_version.string, 10),
        .enable_plugin = true_false_map.get(enable_plugin.string)
            orelse return error.InvalidValue,

        // UI Details
        .show_header_line = true_false_map.get(show_header_line.string)
            orelse return error.InvalidValue,
        .screen_margin = try std.fmt.parseInt(u32, screen_margin.string, 10),
        .window_position = std.meta.stringToEnum(plugin_types.WindowPosition, window_position.string)
            orelse return error.InvalidWindowPosition,
        .window_background_opacity = try std.fmt.parseFloat(f32, window_background_opacity.string),
        .ui_max_height = blk: {
            const ui_max_height_int = try std.fmt.parseInt(i32, ui_max_height.string, 10);

            if (ui_max_height_int < 0) break :blk null;
            break :blk @intCast(ui_max_height_int);
        },
        .show_user_avatars = true_false_map.get(show_user_avatars.string)
            orelse return error.InvalidValue,
        .round_avatar_images = true_false_map.get(round_avatar_images.string)
            orelse return error.InvalidValue,
        .avatar_max_size = try std.fmt.parseInt(u32, avatar_max_size.string, 10),
        .username_row_height = try std.fmt.parseInt(u32, username_row_height.string, 10),
        .username_opacity = try std.fmt.parseFloat(f32, username_opacity.string),
        .username_default_color = try parse_css6_color(username_default_color.string),
        .username_speaking_color = try parse_css6_color(username_speaking_color.string),
        .username_muted_color = try parse_css6_color(username_muted_color.string),
        .username_deafened_color = try parse_css6_color(username_deafened_color.string),
        .username_deafened_muted_color = try parse_css6_color(username_deafened_muted_color.string),

        // Advanced
        .use_background_network_thread = true_false_map.get(use_background_network_thread.string)
            orelse return error.InvalidValue,
        .logging_path =
            if (std.mem.eql(u8, "null", logging_path.string))
                null
            else
                try simple.BoundedArray(u8, std.fs.MAX_PATH_BYTES - 1).fromSlice(logging_path.string),
    };
}

pub fn make_or_fetch_config(allocator: ?std.mem.Allocator) !*plugin_types.PluginConfigDiscord
{
    if (config) |*c| return c;

    var arena = std.heap.ArenaAllocator.init(allocator.?);
    defer arena.deinit();

    const athena_overlay_cache_path = blk: {
        const maybe_path = try known_folders.getPath(arena.allocator(), .cache);
        if (maybe_path == null) return error.FailedToFindCacheFolder;
        const cache_folder_path = maybe_path.?;
        defer arena.allocator().free(cache_folder_path);

        const athena_overlay_cache_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                cache_folder_path,
                "athena_overlay",
            },
        );

        std.fs.makeDirAbsolute(athena_overlay_cache_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        break :blk athena_overlay_cache_path;
    };
    defer arena.allocator().free(athena_overlay_cache_path);

    var config_extension: ?plugin_types.ConfigType = null;
    const config_file_fd = blk: {
        const config_folder_path = (
            try known_folders.getPath
            (
                arena.allocator(),
                .local_configuration,
            )
        )
            orelse return error.FailedToFindConfig;
        defer arena.allocator().free(config_folder_path);

        const athena_overlay_config_folder_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                config_folder_path,
                "athena_overlay",
            }
        );
        defer arena.allocator().free(athena_overlay_config_folder_path);

        std.fs.makeDirAbsolute(athena_overlay_config_folder_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        const file_extensions: []const plugin_types.ConfigType = &.{ .json, .jsonc, .yaml, };
        for (file_extensions) |ext|
        {
            config_extension = ext;
            const lurk_config_path = try std.fs.path.join
            (
                arena.allocator(),
                &.{
                    athena_overlay_config_folder_path,
                    switch (ext)
                    {
                        .json => "lurk_config.json",
                        .jsonc => "lurk_config.jsonc",
                        .yaml => "lurk_config.yaml",
                    },
                },
            );
            defer arena.allocator().free(lurk_config_path);

            break :blk std.fs.openFileAbsolute(lurk_config_path, .{})
                catch |err| switch (err)
                {
                    error.FileNotFound => continue,
                    else => return err,
                };
        }

        // create file if not found
        const lurk_config_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                athena_overlay_config_folder_path,
                "lurk_config.jsonc",
            }
        );
        defer arena.allocator().free(lurk_config_path);

        config_extension = .jsonc;
        const fd = try std.fs.createFileAbsolute(lurk_config_path, .{ .read = true, .truncate = false, });
        const file_info = try fd.stat();
        if (file_info.size == 0)
        {
            try fd.writeAll(@embedFile("default_config.jsonc"));
            try fd.seekTo(0);
        }

        break :blk fd;
    };
    defer config_file_fd.close();

    config = switch (config_extension.?)
    {
        .json, .jsonc => try parse_jsonc_config(&config_file_fd, arena.allocator()),
        .yaml => try parse_libyaml_config(&config_file_fd, arena.allocator()),
    };

    return &config.?;
}
