const std = @import("std");

const simple = @import("simple");


pub const ConfigType = enum
{
    json,
    jsonc,
    yaml,
};

pub const WindowPosition = enum
{
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
};

pub const PluginConfigDiscord = struct
{
    config_version: u32,
    enable_plugin: bool,

    // UI Details
    show_header_line: bool,
    screen_margin: u32,
    window_position: WindowPosition,
    window_background_opacity: f32,
    ui_max_height: ?u32,
    show_user_avatars: bool,
    round_avatar_images: bool,
    avatar_max_size: u32,
    username_row_height: u32,
    username_opacity: f32,
    username_default_color: u32,
    username_speaking_color: u32,
    username_muted_color: u32,
    username_deafened_color: u32,
    username_deafened_muted_color: u32,

    // Advanced
    use_background_network_thread: bool,
    logging_path: ?simple.BoundedArray(u8, std.fs.MAX_PATH_BYTES - 1),
};

pub const PluginConfigDiscordRaw = struct
{
    config_version: u32,
    enable_plugin: bool,

    // UI Details
    show_header_line: bool,
    screen_margin: u32,
    window_position: WindowPosition,
    window_background_opacity: f32,
    ui_max_height: i32,
    show_user_avatars: bool,
    round_avatar_images: bool,
    avatar_max_size: u32,
    username_row_height: u32,
    username_opacity: f32,
    username_default_color: []const u8,
    username_speaking_color: []const u8,
    username_muted_color: []const u8,
    username_deafened_color: []const u8,
    username_deafened_muted_color: []const u8,

    // Advanced
    use_background_network_thread: bool,
    logging_path: ?[]const u8,
};
