const builtin = @import("builtin");

const zimgui = @import("Zig-ImGui");


pub fn init(init_config: anytype) !void
{
    _ = init_config;
}

pub fn deinit() void
{

}

pub fn plugin_requires_large_fonts() !bool
{
    return false;
}

pub fn ready_to_draw_frame() !?bool
{
    return true;
}

pub fn draw_frame(_: anytype, display_x: u32, display_y: u32) !void
{
    zimgui.SetNextWindowBgAlpha(0.0);

    zimgui.PushStyleVar_Float(.WindowBorderSize, 0);
    defer zimgui.PopStyleVarExt(1);

    const screen_margin = 20.0;
    const window_size = zimgui.Vec2.init(400, @floatFromInt(display_y));
    zimgui.SetNextWindowSize(window_size);
    // TOP_RIGHT
    zimgui.SetNextWindowPos
    (
        zimgui.Vec2.init
        (
            @as(f32, @floatFromInt(display_x)) - 400.0 - screen_margin,
            0.0,
        )
    );

    var show_window = true;
    if
    (
        zimgui.BeginExt
        (
            "Debug Plugin",
            &show_window,
            .{
                .NoCollapse = true,
                .NoResize = true,
                .NoScrollbar = true,
                .NoTitleBar = true,
            }
        )
    )
    {
        defer zimgui.End();
        zimgui.SeparatorText
        (
            switch (builtin.mode)
            {
                .Debug => "MODE: Debug 🖥️ デバッグ",
                .ReleaseFast => "MODE: ReleaseFast",
                .ReleaseSafe => "MODE: ReleaseSafe",
                .ReleaseSmall => "MODE: ReleaseSmall",
            },
        );
    }
}

pub fn post_draw_frame() void
{

}
