const std = @import("std");

const plugin_types = @import("plugin_types.zig");

const simple = @import("simple");
const zimgui = @import("Zig-ImGui");


fn draw_outlined_text
(
    text: [:0]const u8,
    pos: zimgui.Vec2,
    color: u32,
    thickness: f32
)
void
{
    const offsets: []const []const f32 = &.{
        &.{  1,  1, },
        &.{ -1, -1, },
        &.{  1, -1, },
        &.{ -1,  1, },
        &.{  1,  0, },
        &.{ -1,  0, },
        &.{  0, -1, },
        &.{  0,  1, },
    };

    const draw_list = zimgui.GetWindowDrawList().?;
    for (offsets) |offset|
    {
        draw_list.AddText_Vec2
        (
            zimgui.Vec2.init
            (
                pos.x + (offset[0] * thickness),
                pos.y + (offset[1] * thickness),
            ),
            0xFF000000, // abgr(255, 0, 0, 0) aka rgba(0, 0, 0, 255) aka black
            text[0..].ptr,
        );
    }

    draw_list.AddText_Vec2(pos, color, text[0..].ptr);
}

pub fn draw_notification
(
    window_label: [:0]const u8,
    window_width: f32,
    color: u32,
    notification: *plugin_types.CactbotNotification,
    large_font: *zimgui.Font
)
void
{
    if (notification.text.len < 1) return;

    zimgui.SetNextWindowBgAlpha(0.0);
    zimgui.SetNextWindowSize(zimgui.Vec2.init(window_width, 100.0));

    zimgui.PushStyleVar_Vec2(.WindowPadding, zimgui.Vec2.init(0.0, 0.0));
    zimgui.PushStyleVar_Float(.WindowBorderSize, 0.0);
    defer zimgui.PopStyleVarExt(2);
    zimgui.PushStyleColor_U32(.Text, color);
    defer zimgui.PopStyleColor();

    var show_window = true;
    if
    (
        zimgui.BeginExt
        (
            window_label[0..].ptr,
            &show_window,
            .{
                .NoCollapse = true,
                .NoResize = true,
                .NoScrollbar = true,
                .NoTitleBar = true,
            }
        )
    )
    {
        defer zimgui.End();

        var buf: [1025]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&buf);
        const text: [:0]const u8 = fba.allocator().dupeZ(u8, notification.text.constSlice())
            catch unreachable;

        zimgui.PushFont(large_font);
        defer zimgui.PopFont();
        const window_pos = zimgui.GetWindowPos();
        draw_outlined_text
        (
            text,
            zimgui.Vec2.init
            (
                window_pos.x + ((window_width - zimgui.CalcTextSize(text[0..].ptr).x) * 0.5),
                window_pos.y,
            ),
            color,
            2.0,
        );
    }
}

pub fn draw_timeline_events
(
    window_label: [:0]const u8,
    past: i64,
    events: *plugin_types.CactbotTimelineEvents
)
void
{
    zimgui.SetNextWindowBgAlpha(0.0);
    zimgui.SetNextWindowSize(zimgui.Vec2.init(300.0, 0.0));

    zimgui.PushStyleVar_Vec2(.WindowPadding, zimgui.Vec2.init(0.0, 0.0));
    zimgui.PushStyleVar_Float(.WindowBorderSize, 0.0);
    defer zimgui.PopStyleVarExt(2);

    var show_window = true;
    if
    (
        zimgui.BeginExt
        (
            window_label[0..].ptr,
            &show_window,
            .{
                .NoCollapse = true,
                .NoResize = true,
                .NoScrollbar = true,
                .NoTitleBar = true,
            }
        )
    )
    {
        defer zimgui.End();
        const width: f32 = zimgui.GetWindowSize().x;
        const now = std.time.milliTimestamp();
        var buf: [1024]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&buf);

        for (events.map.values()) |event|
        {
            if (now > event.ends_at) continue;

            zimgui.PushStyleColor_U32(.PlotHistogram, event.color_abgr);
            defer zimgui.PopStyleColor();

            // subtract a time in the past relative to both timestamps to keep
            // the numbers small and in f32 representation range
            const progress: f32 =
                (
                    @as(f32, @floatFromInt(now - past)) /
                    std.time.ms_per_s
                ) /
                (
                    @as(f32, @floatFromInt(event.ends_at - past)) /
                    std.time.ms_per_s
                );
            const remaining_time: f32 = @as(f32, @floatFromInt(event.ends_at - now)) / std.time.ms_per_s;
            var remaining_seconds: u32 = @intFromFloat(remaining_time);
            const hours: u32 = remaining_seconds / 3600;
            remaining_seconds = remaining_seconds % 3600;
            const minutes: u32 = remaining_seconds / 60;
            remaining_seconds = remaining_seconds % 60;

            fba.reset();
            const label = std.fmt.allocPrintZ
            (
                fba.allocator(),
                "{s} - {d:0>2}:{d:0>2}:{d:0>2}.{d:0>2}",
                .{
                    event.text.constSlice(),
                    hours,
                    minutes,
                    remaining_seconds,
                    @as(u8, @intFromFloat((remaining_time - @trunc(remaining_time)) * 100.0))
                }
            )
                catch unreachable;
            zimgui.ProgressBarExt
            (
                if (event.fill)
                    1.0 - progress
                else
                    progress,
                zimgui.Vec2.init(width, 0.0),
                label[0..].ptr,
            );
        }
    }
}