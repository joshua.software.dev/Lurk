const builtin = @import("builtin");
const std = @import("std");

const act_helper = @import("act_helper.zig");
const act_thread = @import("act_message_thread.zig");
const cactbot_helper = @import("cactbot_helper.zig");
const cactbot_thread = @import("cactbot_message_thread.zig");
const plugin_config = @import("plugin_config.zig");
const plugin_state = @import("plugin_state.zig");
const plugin_types = @import("plugin_types.zig");

const simple = @import("simple");
const zimgui = @import("Zig-ImGui");


pub fn init(init_config: anytype) !void
{
    if (builtin.target.ptrBitWidth() != 64) return;

    const is_ffxiv = try simple.does_process_name_match("ffxiv_dx11.exe");
    if (!(builtin.mode == .Debug or is_ffxiv)) return;

    const short_allocator: std.mem.Allocator = init_config.short_lived_allocator;
    const long_allocator: std.mem.Allocator = init_config.long_lived_allocator;
    const heap_allocator: std.mem.Allocator = init_config.unbounded_heap_backed_allocator;
    const load_image_hook_fn: @TypeOf(plugin_state.LoadImageHook) = init_config.load_image_hook_fn;

    const config = try plugin_config.make_or_fetch_config(short_allocator);
    if (config.enable_plugin)
    {
        plugin_state.IconArena = std.heap.ArenaAllocator.init(long_allocator);
        plugin_state.JobIcons = try plugin_types.JobIconsMap.init(plugin_state.IconArena.?.allocator());
        plugin_state.CactbotTimelineEvents = undefined;
        try plugin_state.CactbotTimelineEvents.?.init();

        if (!builtin.single_threaded and config.load_job_icons_in_background_thread)
        {
            try act_helper.load_job_icons_background
            (
                heap_allocator,
                plugin_state.IconArena.?.allocator(),
                config.job_icon_cache_path,
                &plugin_state.JobIcons.?,
                is_ffxiv,
            );
        }
        else
        {
            try act_helper.load_job_icons
            (
                heap_allocator,
                plugin_state.IconArena.?.allocator(),
                config.job_icon_cache_path,
                &plugin_state.JobIcons.?,
                is_ffxiv,
            );
        }
    }
    else
    {
        return;
    }

    if (plugin_state.LoadImageHook == null) plugin_state.LoadImageHook = load_image_hook_fn;

    plugin_state.ActMessageArena = std.heap.ArenaAllocator.init(heap_allocator);
    try act_thread.start_act_message_thread();
    try cactbot_thread.start_cactbot_message_thread(heap_allocator);

    plugin_state.InitTime = std.time.milliTimestamp();
}

pub fn deinit() void
{
    if (plugin_state.LoadImageHook != null) plugin_state.LoadImageHook = null;

    if (plugin_state.InitTime != null)
    {
        if (plugin_state.IconArena != null)
        {
            plugin_state.IconArena.?.deinit();
            plugin_state.IconArena = null;
            plugin_state.JobIcons = null;
        }

        act_thread.stop_act_message_thread();
        plugin_state.ActCombatants.len = 0;
        plugin_state.ActLastMessage = null;
        plugin_state.ActMessageArena.?.deinit();

        cactbot_thread.stop_cactbot_message_thread();
        plugin_state.CactbotTimelineEvents = null;
        plugin_state.CactbotAlarmNotification.state_update = false;
        plugin_state.CactbotAlarmNotification.text.len = 0;
        plugin_state.CactbotAlertNotification.state_update = false;
        plugin_state.CactbotAlertNotification.text.len = 0;
        plugin_state.CactbotInfoNotification.state_update = false;
        plugin_state.CactbotInfoNotification.text.len = 0;

        plugin_state.InitTime = null;
    }
}

pub fn plugin_requires_large_fonts() !bool
{
    return builtin.mode == .Debug or (try simple.does_process_name_match("ffxiv_dx11.exe"));
}

pub fn ready_to_draw_frame() !?bool
{
    const config = try plugin_config.make_or_fetch_config(null);
    if (!config.enable_plugin) return null;

    if (plugin_state.IconThread != null)
    {
        if (@atomicLoad(bool, &plugin_state.IconThreadFinished, .Acquire))
        {
            std.log.scoped(.ATHENAPLUGIN).debug("Closing job icon thread before first draw...", .{});
            plugin_state.IconThread.?.join();
            plugin_state.IconThread = null;
        }
        else
        {
            return null;
        }
    }

    if (plugin_state.ActWebSocketConnection != null)
    {
        if (!config.use_background_network_thread)
        {
            try act_thread.handle_one_message();
            return true;
        }
        else
        {
            if (plugin_state.ActDataMutex.tryLock())
            {
                if (plugin_state.CactbotDataMutex.tryLock())
                {
                    plugin_state.AfterThisFrameUnlockMutex = true;
                    return true;
                }

                plugin_state.ActDataMutex.unlock();
            }
        }

        // Reuse previous frame if lock cannot be acquired
        return false;
    }

    // Do not delay drawing other plugins, but skip drawing this one this frame
    return null;
}

fn set_window_position
(
    display_x: u32,
    display_y: u32,
    window_x: f32,
    window_y: f32,
    position: plugin_types.WindowPosition,
    offset_x: f32,
    offset_y: f32,
)
void
{
    switch (position)
    {
        .TOP_LEFT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init(offset_x, offset_y)
            ),
        .TOP_RIGHT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init(@as(f32, @floatFromInt(display_x)) - window_x - offset_x, offset_y)
            ),
        .BOTTOM_LEFT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init(offset_x, @as(f32, @floatFromInt(display_y)) - window_y - offset_y)
            ),
        .BOTTOM_RIGHT =>
            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init
                (
                    @as(f32, @floatFromInt(display_x)) - window_x - offset_x,
                    @as(f32, @floatFromInt(display_y)) - window_y - offset_y
                )
            ),
        .CENTER =>
        {
            const window_x_u32: u32 = @intFromFloat(window_x);
            const center_x: u32 =
                if (window_x_u32 >= display_x)
                    0
                else
                    (display_x - window_x_u32) / 2;
            const window_y_u32: u32 = @intFromFloat(window_y);
            const center_y: u32 =
                if (window_y_u32 >= display_y)
                    0
                else
                    (display_y - window_y_u32) / 2;

            zimgui.SetNextWindowPos
            (
                zimgui.Vec2.init
                (
                    @as(f32, @floatFromInt(center_x)) + offset_x,
                    @as(f32, @floatFromInt(center_y)) + offset_y,
                ),
            );
        },
    }
}

pub fn draw_frame(font_table: anytype, display_x: u32, display_y: u32) !void
{
    const primary_font_small: ?*zimgui.Font = font_table.primary_font_small;
    _ = primary_font_small;
    const primary_font_large: ?*zimgui.Font = font_table.primary_font_large;
    const emoji_font_small: ?*zimgui.Font = font_table.emoji_font_small;
    _ = emoji_font_small;
    const emoji_font_large: ?*zimgui.Font = font_table.emoji_font_large;
    _ = emoji_font_large;
    const monospace_font: ?*zimgui.Font = font_table.monospace_font;

    const config = try plugin_config.make_or_fetch_config(null);
    if
    (
        !config.hide_outside_combat or
        (
            config.hide_outside_combat and
            plugin_state.ActLastMessage != null and
            !plugin_state.ActLastMessage.?.isActive.value
        )
    )
    {
        if (plugin_state.ActCombatants.len > 0)
        {
            zimgui.SetNextWindowBgAlpha(config.window_background_opacity);
            const window_size = zimgui.Vec2.init(470, @floatFromInt(config.ui_max_height orelse display_y));
            zimgui.SetNextWindowSize(window_size);
            set_window_position
            (
                display_x,
                display_y,
                window_size.x,
                window_size.y,
                config.window_position,
                @floatFromInt(config.screen_margin),
                @floatFromInt(config.screen_margin),
            );

            zimgui.PushStyleVar_Vec2(.WindowPadding, zimgui.Vec2.init(0.0, 0.0));
            zimgui.PushStyleVar_Float(.WindowBorderSize, 2.0);
            defer zimgui.PopStyleVarExt(2);

            var show_window = true;
            if
            (
                zimgui.BeginExt
                (
                    "ACT WebSocket Renderer",
                    &show_window,
                    .{
                        .NoCollapse = true,
                        .NoResize = true,
                        .NoScrollbar = true,
                        .NoTitleBar = true,
                    }
                )
            )
            {
                defer zimgui.End();
                try act_helper.draw_overlay
                (
                    monospace_font.?,
                    window_size,
                    plugin_state.LoadImageHook,
                    plugin_state.ActLastMessage.?,
                    plugin_state.JobIcons.?,
                    plugin_state.ActCombatants,
                );
            }
        }

        set_window_position
        (
            display_x,
            display_y,
            1000.0,
            0.0,
            config.alarm_offset_position,
            @floatFromInt(config.alarm_screen_offset_x),
            @floatFromInt(config.alarm_screen_offset_y),
        );
        cactbot_helper.draw_notification
        (
            "Cactbot Alarm Notifications",
            1000.0,
            0xFF0000FF, // abgr(255, 0, 0, 255) aka rgba(255, 0, 0, 255)
            &plugin_state.CactbotAlarmNotification,
            primary_font_large.?,
        );

        set_window_position
        (
            display_x,
            display_y,
            1000.0,
            0.0,
            config.alert_offset_position,
            @floatFromInt(config.alert_screen_offset_x),
            @floatFromInt(config.alert_screen_offset_y),
        );
        cactbot_helper.draw_notification
        (
            "Cactbot Alert Notifications",
            1000.0,
            0xFF00FFFF, // abgr(255, 0, 255, 255) aka rgba(255, 255, 0, 255)
            &plugin_state.CactbotAlertNotification,
            primary_font_large.?,
        );

        set_window_position
        (
            display_x,
            display_y,
            1000.0,
            0.0,
            config.info_offset_position,
            @floatFromInt(config.info_screen_offset_x),
            @floatFromInt(config.info_screen_offset_y),
        );
        cactbot_helper.draw_notification
        (
            "Cactbot Info Notifications",
            1000.0,
            0xFF00FF00, // abgr(255, 0, 255, 0) aka rgba(0, 255, 0, 255)
            &plugin_state.CactbotInfoNotification,
            primary_font_large.?,
        );

        set_window_position
        (
            display_x,
            display_y,
            300.0,
            0.0,
            config.timeline_offset_position,
            @floatFromInt(config.timeline_screen_offset_x),
            @floatFromInt(config.timeline_screen_offset_y),
        );
        cactbot_helper.draw_timeline_events
        (
            "Cactbot Timeline Events",
            plugin_state.InitTime.?,
            &plugin_state.CactbotTimelineEvents.?,
        );
    }
}

pub fn post_draw_frame() void
{
    if (plugin_state.AfterThisFrameUnlockMutex)
    {
        plugin_state.AfterThisFrameUnlockMutex = false;
        plugin_state.ActDataMutex.unlock();
        plugin_state.CactbotDataMutex.unlock();
    }
}
