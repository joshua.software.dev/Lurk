const std = @import("std");

const plugin_types = @import("plugin_types.zig");

const zig_xml = @import("zig-xml");


const BODY_START = "<body>";
const BODY_END = "</body>";

const TimelineEvent = struct
{
    id: u32,
    duration: f64,
    remaining: f64,
    fill: bool,
    force_remove: bool,
    color_abgr: u32,
    text: []const u8,
};

const CactbotTimelineEventSortContext = struct
{
    keys: []u32,

    pub fn lessThan(ctx: @This(), a_index: usize, b_index: usize) bool {
        return ctx.keys[a_index] < ctx.keys[b_index];
    }
};

const OwnedNodeSearch = struct
{
    located: std.ArrayList(zig_xml.Node.Element),
    arena: std.heap.ArenaAllocator,

    pub fn deinit(self: @This()) void
    {
        self.arena.deinit();
    }
};

fn find_tags_by_attribute_recursive
(
    located: *std.ArrayList(zig_xml.Node.Element),
    attr_key: ?[]const u8,
    attr_value: ?[]const u8,
    element: zig_xml.Node.Element,
    first_only: bool,
)
!void
{
    for (element.children) |child|
    {
        switch (child)
        {
            .element => |e|
            {
                try find_tags_by_attribute_recursive(located, attr_key, attr_value, e, first_only);
            },
            .attribute => |a|
            {
                if (attr_key) |key|
                {
                    if (std.mem.eql(u8, key, a.name.local))
                    {
                        if (attr_value == null)
                        {
                            try located.append(element);
                            if (first_only) return error.FirstResultFound;
                        }
                        else if (std.mem.eql(u8, attr_value.?, a.value))
                        {
                            try located.append(element);
                            if (first_only) return error.FirstResultFound;
                        }
                    }
                }
                else if (attr_value) |value|
                {
                    if (std.mem.eql(u8, value, a.value))
                    {
                        try located.append(element);
                        if (first_only) return error.FirstResultFound;
                    }
                }
            },
            else => {},
        }
    }
}

fn find_tags_by_attribute_allocator
(
    allocator: std.mem.Allocator,
    attr_key: ?[]const u8,
    attr_value: ?[]const u8,
    element: zig_xml.Node.Element,
)
!OwnedNodeSearch
{
    var owned: OwnedNodeSearch = .{
        .located = undefined,
        .arena = std.heap.ArenaAllocator.init(allocator),
    };
    owned.located = std.ArrayList(zig_xml.Node.Element).init(owned.arena.allocator());

    try find_tags_by_attribute_recursive(&owned.located, attr_key, attr_value, element, false);

    return owned;
}

fn find_tag_by_attribute
(
    attr_key: ?[]const u8,
    attr_value: ?[]const u8,
    element: zig_xml.Node.Element,
)
!?zig_xml.Node.Element
{
    var buf: [1]zig_xml.Node.Element = undefined;
    var located: std.ArrayList(zig_xml.Node.Element) = .{
        .items = buf[0..0],
        .capacity = buf.len,
        .allocator = undefined,
    };

    find_tags_by_attribute_recursive(&located, attr_key, attr_value, element, true)
        catch |err| switch (err)
        {
            error.FirstResultFound => return located.items[0],
            else => return err,
        };

    return null;
}

fn element_to_text_recursive
(
    strings_list: *std.ArrayList([]const u8),
    element: zig_xml.Node.Element,
    trim_whitespace: bool,
)
!void
{
    for (element.children) |child|
    {
        switch (child)
        {
            .element => |e|
            {
                try element_to_text_recursive(strings_list, e, trim_whitespace);
            },
            .text => |t|
            {
                const text =
                    if (trim_whitespace)
                        std.mem.trim(u8, t.content, &std.ascii.whitespace)
                    else
                        t.content;

                if (text.len > 0) try strings_list.append(text);
            },
            else => {},
        }
    }
}

fn element_to_text_allocator
(
    allocator: std.mem.Allocator,
    element: zig_xml.Node.Element,
    deduplicate: bool,
    trim_whitespace: bool,
)
![]const u8
{
    var strings_list = std.ArrayList([]const u8).init(allocator);
    defer strings_list.deinit();

    try element_to_text_recursive(&strings_list, element, trim_whitespace);

    var length: usize = 0;
    var map: ?std.StringArrayHashMap(void) = blk: {
        if (!deduplicate)
        {
            for (strings_list.items) |string|
            {
                length += string.len;
            }

            switch (strings_list.items.len)
            {
                0, 1 => {},
                else => length += strings_list.items.len - 1
            }

            break :blk null;
        }

        var map = std.StringArrayHashMap(void).init(allocator);
        for (strings_list.items) |string|
        {
            const result = try map.getOrPut(string);
            if (!result.found_existing) length += string.len;
        }

        switch (map.count())
        {
            0, 1 => {},
            else => length += map.count() - 1
        }

        break :blk map;
    };
    defer if (map != null) map.?.deinit();

    var buf = try allocator.alloc(u8, length);
    var final: std.ArrayListUnmanaged(u8) = .{
        .items = buf[0..0],
        .capacity = buf.len,
    };

    const strings: []const []const u8 =
        if (deduplicate)
            map.?.keys()
        else
            strings_list.items;

    for (strings, 0..) |string, i|
    {
        final.appendSliceAssumeCapacity(string);
        if (i != strings.len - 1) final.appendAssumeCapacity('\n');
    }

    return buf;
}

fn get_timeline_events
(
    allocator: std.mem.Allocator,
    element: zig_xml.Node.Element,
    events: *plugin_types.CactbotTimelineEvents,
)
!void
{
    var inactive_ids = std.AutoArrayHashMap(u32, void).init(allocator);
    defer inactive_ids.deinit();
    for (events.map.keys()) |key|
    {
        try inactive_ids.put(key, {});
    }

    {
        const timers = try find_tags_by_attribute_allocator(allocator, "class", "timer-bar", element);
        defer timers.deinit();
        for (timers.located.items) |tl|
        {
            const timeline_event: TimelineEvent = blk: {
                var id: ?u32 = null;
                var duration: ?f64 = null;
                var remaining: ?f64 = null;
                var fill: ?bool = null;
                var force_remove: ?bool = null;
                var color_abgr: ?u32 = null;
                var text: ?[]const u8 = null;

                for (tl.children) |child|
                {
                    switch (child)
                    {
                        .attribute => |a|
                        {
                            if (std.mem.eql(u8, "class", a.name.local))
                            {
                                force_remove = std.mem.indexOf(u8, a.value, "removed") != null;
                            }
                            else if (std.mem.eql(u8, a.name.local, "style"))
                            {
                                id = try std.fmt.parseInt
                                (
                                    u32,
                                    std.mem.trimLeft
                                    (
                                        u8,
                                        a.value[0..a.value.len - 1], // -1 in order to trim trailing ';'
                                        "order: ",
                                    ),
                                    10,
                                );
                            }
                        },
                        .element => |e|
                        {
                            for (e.children) |child2|
                            {
                                switch (child2)
                                {
                                    .attribute => |a|
                                    {
                                        if (std.mem.eql(u8, "duration", a.name.local))
                                        {
                                            duration = try std.fmt.parseFloat(f64, a.value);
                                        }
                                        else if (std.mem.eql(u8, "fg", a.name.local))
                                        {
                                            const rgb_text = blk2: {
                                                const start = std.mem.indexOf(u8, a.value, "rgb(").?;
                                                const end = std.mem.indexOf(u8, a.value, ")").?;
                                                break :blk2 a.value[start + 4 .. end];
                                            };

                                            var rgb: [3]u32 = .{ 0, 0, 0, };
                                            var i: usize = 0;
                                            var iter = std.mem.splitScalar(u8, rgb_text, ',');
                                            while (iter.next()) |num_str|
                                            {
                                                rgb[i] = try std.fmt.parseInt
                                                (
                                                    u32,
                                                    std.mem.trim(u8, num_str, &std.ascii.whitespace),
                                                    10,
                                                );
                                                i += 1;
                                            }

                                            color_abgr = 0xFF000000 | rgb[2] << 16 | rgb[1] << 8 | rgb[0];
                                        }
                                        else if (std.mem.eql(u8, "lefttext", a.name.local))
                                        {
                                            text = a.value;
                                        }
                                        else if (std.mem.eql(u8, "stylefill", a.name.local))
                                        {
                                            fill = std.mem.eql(u8, a.value, "fill");
                                        }
                                        else if (std.mem.eql(u8, "value", a.name.local))
                                        {
                                            remaining = try std.fmt.parseFloat(f64, a.value);
                                        }
                                    },
                                    else => {},
                                }
                            }
                        },
                        else => {},
                    }
                }

                break :blk .{
                    .id = id.?,
                    .duration = duration.?,
                    .remaining = remaining.?,
                    .fill = fill.?,
                    .force_remove = force_remove.?,
                    .color_abgr = color_abgr.?,
                    .text = text.?,
                };
            };

            if
            (
                !timeline_event.force_remove and
                inactive_ids.get(timeline_event.id) != null
            )
            {
                _ = inactive_ids.swapRemove(timeline_event.id);
            }

            const now: i64 = std.time.milliTimestamp();
            const result = events.map.getOrPutAssumeCapacity(timeline_event.id);
            result.value_ptr.color_abgr = timeline_event.color_abgr;
            result.value_ptr.fill = timeline_event.fill;
            result.value_ptr.text.len = 0;
            result.value_ptr.text.appendSliceAssumeCapacity(timeline_event.text);
            if (!result.found_existing)
            {
                result.value_ptr.id = timeline_event.id;
                result.value_ptr.duration = timeline_event.duration;
                result.value_ptr.ends_at =
                    now +
                    @as
                    (
                        i64,
                        @intFromFloat
                        (
                            @as(f64, @floatFromInt(std.time.ms_per_s)) *
                            timeline_event.remaining
                        )
                    );
            }
            else if (result.value_ptr.duration != timeline_event.duration)
            {
                result.value_ptr.duration = timeline_event.duration;
                result.value_ptr.ends_at =
                    now +
                    @as
                    (
                        i64,
                        @intFromFloat
                        (
                            @as(f64, @floatFromInt(std.time.ms_per_s)) *
                            timeline_event.remaining
                        )
                    );
            }
        }
    }

    for (inactive_ids.keys()) |id|
    {
        _ = events.map.swapRemove(id);
    }

    if (@hasDecl(@TypeOf(events.map.*), "sortUnstable"))
    {
        events.map.sortUnstable(CactbotTimelineEventSortContext{ .keys = events.map.keys() });
    }
    else
    {
        events.map.sort(CactbotTimelineEventSortContext{ .keys = events.map.keys() });
    }
}

pub fn parse_cactbot_html
(
    allocator: std.mem.Allocator,
    html: []const u8,
    alarm_out: *plugin_types.CactbotNotification,
    alert_out: *plugin_types.CactbotNotification,
    info_out: *plugin_types.CactbotNotification,
    events: *plugin_types.CactbotTimelineEvents,
)
!void
{
    const body = blk: {
        const body_start_loc = std.mem.indexOf(u8, html, BODY_START).?;
        const body_end_loc = std.mem.indexOf(u8, html, BODY_END).?;
        break :blk html[body_start_loc..(body_end_loc + BODY_END.len)];
    };

    var doc = blk: {
        var stream = std.io.fixedBufferStream(body);
        break :blk try zig_xml.readDocument(allocator, stream.reader(), .{});
    };
    defer doc.deinit();

    {
        const alarm = blk: {
            const alarm_container = try find_tag_by_attribute
            (
                "id",
                "popup-text-alarm",
                doc.value.children[0].element,
            );
            const alarm_holder = try find_tag_by_attribute
            (
                "class",
                "holder",
                alarm_container.?,
            );
            break :blk try element_to_text_allocator(allocator, alarm_holder.?, true, true);
        };
        defer allocator.free(alarm);

        if (alarm_out.text.len == 0 and alarm.len > 0)
        {
            for (alarm) |chr|
            {
                alarm_out.text.appendAssumeCapacity
                (
                    switch (chr)
                    {
                        '\x00' => ' ',
                        '#' => '+',
                        '%' => '+',
                        else => chr,
                    }
                );
            }

            alarm_out.state_update = true;
        }
        else if (alarm_out.text.len > 0 and alarm.len == 0)
        {
            alarm_out.text.len = 0;
            alarm_out.state_update = true;
        }
    }

    {
        const alert = blk: {
            const alert_container = try find_tag_by_attribute
            (
                "id",
                "popup-text-alert",
                doc.value.children[0].element,
            );
            const alert_holder = try find_tag_by_attribute
            (
                "class",
                "holder",
                alert_container.?,
            );
            break :blk try element_to_text_allocator(allocator, alert_holder.?, true, true);
        };
        defer allocator.free(alert);

        if (alert_out.text.len == 0 and alert.len > 0)
        {
            for (alert) |chr|
            {
                alert_out.text.appendAssumeCapacity
                (
                    switch (chr)
                    {
                        '\x00' => ' ',
                        '#' => '+',
                        '%' => '+',
                        else => chr,
                    }
                );
            }

            alert_out.state_update = true;
        }
        else if (alert_out.text.len > 0 and alert.len == 0)
        {
            alert_out.text.len = 0;
            alert_out.state_update = true;
        }
    }

    {
        const info = blk: {
            const info_container = try find_tag_by_attribute
            (
                "id",
                "popup-text-info",
                doc.value.children[0].element,
            );
            const info_holder = try find_tag_by_attribute
            (
                "class",
                "holder",
                info_container.?,
            );
            break :blk try element_to_text_allocator(allocator, info_holder.?, true, true);
        };
        defer allocator.free(info);

        if (info_out.text.len == 0 and info.len > 0)
        {
            for (info) |chr|
            {
                info_out.text.appendAssumeCapacity
                (
                    switch (chr)
                    {
                        '\x00' => ' ',
                        '#' => '+',
                        '%' => '+',
                        else => chr,
                    }
                );
            }

            info_out.state_update = true;
        }
        else if (info_out.text.len > 0 and info.len == 0)
        {
            info_out.text.len = 0;
            info_out.state_update = true;
        }
    }

    const timeline = try find_tag_by_attribute
    (
        "id",
        "timeline",
        doc.value.children[0].element,
    );
    try get_timeline_events(allocator, timeline.?, events);
}

test "Parse cactbot.html"
{
    var alarm_notification: plugin_types.CactbotNotification = .{};
    var alert_notification: plugin_types.CactbotNotification = .{};
    var info_notification: plugin_types.CactbotNotification = .{};
    var tl_events: plugin_types.CactbotTimelineEvents = undefined;
    try tl_events.init();

    try parse_cactbot_html
    (
        std.testing.allocator,
        @embedFile("cactbot.html"),
        &alarm_notification,
        &alert_notification,
        &info_notification,
        &tl_events,
    );

    try std.testing.expectEqualSlices(u8, "example", alarm_notification.text.constSlice());
    try std.testing.expectEqualSlices(u8, "BEHIND ORB 4", alert_notification.text.constSlice());
    try std.testing.expectEqualSlices(u8, "a", info_notification.text.constSlice());

    {
        const sixty_one = tl_events.map.get(61).?;
        try std.testing.expect(sixty_one.id == 61);
        try std.testing.expect(sixty_one.fill == true);
        try std.testing.expect(sixty_one.color_abgr == 0xFFFF8888);
        try std.testing.expectEqualSlices(u8, "Weight Of The Land", sixty_one.text.constSlice());
    }

    {
        const sixty_two = tl_events.map.get(62).?;
        try std.testing.expect(sixty_two.id == 62);
        try std.testing.expect(sixty_two.fill == true);
        try std.testing.expect(sixty_two.color_abgr == 0xFFFF8888);
        try std.testing.expectEqualSlices(u8, "Rock Throw", sixty_two.text.constSlice());
    }

    {
        const sixty_three = tl_events.map.get(63).?;
        try std.testing.expect(sixty_three.id == 63);
        try std.testing.expect(sixty_three.fill == true);
        try std.testing.expect(sixty_three.color_abgr == 0xFFFF8888);
        try std.testing.expectEqualSlices(u8, "Rock Buster", sixty_three.text.constSlice());
    }

    {
        const sixty_four = tl_events.map.get(64).?;
        try std.testing.expect(sixty_four.id == 64);
        try std.testing.expect(sixty_four.fill == true);
        try std.testing.expect(sixty_four.color_abgr == 0xFFFF8888);
        try std.testing.expectEqualSlices(u8, "Upheaval", sixty_four.text.constSlice());
    }
}
