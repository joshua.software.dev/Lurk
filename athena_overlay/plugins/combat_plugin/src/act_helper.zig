const builtin = @import("builtin");
const std = @import("std");

const act_types = @import("act_types.zig");
const plugin_state = @import("plugin_state.zig");
const plugin_types = @import("plugin_types.zig");

const simple = @import("simple");
const zimgui = @import("Zig-ImGui");
const zstbi = @import("zstbi");


const IconEntry = struct
{
    path: []const u8,
    uri: std.Uri,
    hash: [32]u8,
};

const icon_entries: []const IconEntry = blk: {
    @setEvalBranchQuota(2000);
    break :blk &.{
        .{
            .path = "0",
            .uri = std.Uri.parse
            (
                "https://gitlab.com/joshua.software.dev/lurk-files/-/raw/" ++
                "fb57553bf0db85225946344000f6f0fcaecab4a1/public/color_icons.tar.gz"
            )
                catch unreachable,
            .hash = simple.sha256sum_to_array("dd19af87065229940a466115b34fac5408b4d935adc4a3582faa52e9c5652587"),
        },
        .{
            .path = "1",
            .uri = std.Uri.parse
            (
                "https://gitlab.com/joshua.software.dev/lurk-files/-/raw/" ++
                "fb57553bf0db85225946344000f6f0fcaecab4a1/public/plain_icons.tar.gz"
            )
                catch unreachable,
            .hash = simple.sha256sum_to_array("fa5748bc79a42768b8fdbb8a8bfa1a1bfb6cf362d0a00add7b7bf5ee2d44c202"),
        },
    };
};

pub fn sort_combatants_by_dps
(
    last_message: *const act_types.RawCombatData,
    combatants: *@TypeOf(plugin_state.ActCombatants),
)
!void
{
    combatants.len = 0;
    for (last_message.Combatant.map.values()) |*combatant|
    {
        try combatants.append(combatant);
    }

    std.sort.pdq
    (
        *const act_types.RawCombatant,
        combatants.slice(),
        {},
        struct {
            pub fn f(_: void, a: *const act_types.RawCombatant, b: *const act_types.RawCombatant) bool
            {
                return a.DPS.value > b.DPS.value;
            }
        }.f,
    );
}

pub fn load_job_icons
(
    temp_allocator: std.mem.Allocator,
    final_allocator: std.mem.Allocator,
    cache_path: simple.RealPath,
    job_icon_map: *plugin_types.JobIconsMap,
    is_ffxiv: bool,
)
!void
{
    var client: ?simple.StdHttpClient = null;
    var simple_http_client = blk: {
        if (builtin.zig_backend == .stage2_x86_64)
        {
            var curl = try simple.init_curl_client();
            break :blk curl.as_simple_client();
        }

        client = simple.init_std_http_client(.{
            .bundle = .dynamically_generate,
            .http_allocator = temp_allocator,
        })
            catch unreachable; // cannot error with .dynamically_generate
        break :blk client.?.as_simple_client();
    };
    defer simple_http_client.deinit();

    if (!cache_path.exists)
    {
        std.fs.makeDirAbsolute(cache_path.path.constSlice())
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };
    }

    for (icon_entries) |icon_style|
    {
        const cache_subdir_path = try std.fs.path.join
        (
            temp_allocator,
            &.{
                cache_path.path.constSlice(),
                icon_style.path,
            },
        );
        defer temp_allocator.free(cache_subdir_path);
        std.fs.makeDirAbsolute(cache_subdir_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        var dir =
            if (@hasDecl(std.fs, "openIterableDirAbsolute"))
                try std.fs.openIterableDirAbsolute(cache_subdir_path, .{})
            else
                try std.fs.openDirAbsolute(cache_subdir_path, .{ .iterate = true, });
        defer dir.close();
        var iter = dir.iterateAssumeFirstIteration();
        var file_count: usize = 0;
        while (try iter.next()) |entry|
        {
            if (std.mem.endsWith(u8, entry.name, ".png")) file_count += 1;
        }
        std.log.scoped(.ATHENAPLUGIN).debug("Found {d} files for job icon style: {s}", .{ file_count, icon_style.path, });

        if (file_count == 0)
        {
            const download_path = try std.fs.path.join
            (
                temp_allocator,
                &.{
                    cache_subdir_path,
                    "icons.tar.gz",
                },
            );
            defer temp_allocator.free(download_path);

            std.log.scoped(.ATHENAPLUGIN).info
            (
                "Downloading job icon archive: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                .{ icon_style.uri, }
            );
            try simple_http_client.download_file
            (
                &.{},
                icon_style.uri,
                download_path,
                icon_style.hash,
            );

            {
                var archive_fd = try std.fs.openFileAbsolute(download_path, .{});
                defer archive_fd.close();

                var decompressor = try std.compress.gzip.decompress(temp_allocator, archive_fd.reader());
                defer decompressor.deinit();

                try std.tar.pipeToFileSystem(iter.dir, decompressor.reader(), .{ .mode_mode = .ignore });
            }

            try iter.dir.deleteFile("icons.tar.gz");
        }
        else if ((file_count < 39) and is_ffxiv)
        {
            @panic("Failed to load job icons");
        }

        if (!(is_ffxiv or builtin.mode == .Debug)) continue;

        iter.reset();
        while (try iter.next()) |entry|
        {
            if (!std.mem.endsWith(u8, entry.name, ".png")) continue;
            const job =
                if
                (
                    std.meta.stringToEnum
                    (
                        act_types.Job,
                        std.mem.trimRight
                        (
                            u8,
                            std.fs.path.basename(entry.name),
                            ".png",
                        )
                    )
                ) |result|
                    result
                else
                    continue;

            const image_path = try std.fs.path.joinZ(temp_allocator, &.{ cache_subdir_path, entry.name, });
            defer temp_allocator.free(image_path);
            var image = try zstbi.Image.loadFromFile(image_path, 4);
            var smol = image.resize(23, 23);
            defer smol.deinit();
            image.deinit();

            job_icon_map.map.putAssumeCapacity
            (
                try std.fmt.allocPrint(final_allocator, "{s}_{}", .{ icon_style.path, job, }),
                .{
                    .width = 23,
                    .height = 23,
                    .data = try final_allocator.dupe(u8, smol.data),
                },
            );
        }
    }

    @atomicStore(bool, &plugin_state.IconThreadFinished, true, .Release);
    std.log.scoped(.ATHENAPLUGIN).info("Job icon loading complete", .{});
}

pub fn load_job_icons_background
(
    temp_allocator: std.mem.Allocator,
    final_allocator: std.mem.Allocator,
    cache_path: simple.RealPath,
    job_icon_map: *plugin_types.JobIconsMap,
    is_ffxiv: bool,
)
!void
{
    if (plugin_state.IconThread == null)
    {
        std.log.scoped(.ATHENAPLUGIN).info("Started background job icon loading thread", .{});
        plugin_state.IconThread = try std.Thread.spawn
        (
            .{},
            load_job_icons,
            .{
                temp_allocator,
                final_allocator,
                cache_path,
                job_icon_map,
                is_ffxiv,
            },
        );
    }
}

pub fn draw_overlay
(
    monospace_font: *zimgui.Font,
    window_size: zimgui.Vec2,
    load_image_hook_fn: ?*const plugin_types.LoadImageHookFunction,
    last_message: act_types.RawCombatData,
    job_icon_map: plugin_types.JobIconsMap,
    combatants: @TypeOf(plugin_state.ActCombatants),
)
!void
{
    var buf: [512]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buf);
    if
    (
        zimgui.BeginTableExt
        (
            "Act Render Header",
            4,
            .{ .SizingFixedFit = true },
            zimgui.Vec2.init(window_size.x, 0.0),
            window_size.x
        )
    )
    {
        defer zimgui.EndTable();

        zimgui.TableSetupColumnExt("Duration", .{}, window_size.x * 0.3, 0);
        zimgui.TableSetupColumnExt("rDPS", .{}, window_size.x * 0.21, 1);
        zimgui.TableSetupColumnExt("rHPS", .{}, window_size.x * 0.21, 2);
        zimgui.TableSetupColumnExt("Deaths", .{}, window_size.x * 0.2, 3);

        _ = zimgui.TableNextColumn();
        zimgui.Text(" Duration:");
        zimgui.SameLine();
        {
            const duration = try std.fmt.allocPrintZ(fba.allocator(), "{s}", .{ last_message.Encounter.duration });
            zimgui.PushFont(monospace_font);
            defer zimgui.PopFont();
            zimgui.Text(duration[0..].ptr);
        }

        _ = zimgui.TableNextColumn();
        zimgui.Text("rDPS:");
        zimgui.SameLine();
        {
            const rdps = try std.fmt.allocPrintZ
            (
                fba.allocator(),
                "{d: >6}",
                .{ last_message.Encounter.ENCDPS.value },
            );
            zimgui.PushFont(monospace_font);
            defer zimgui.PopFont();
            zimgui.Text(rdps[0..].ptr);
        }

        _ = zimgui.TableNextColumn();
        zimgui.Text("rHPS:");
        zimgui.SameLine();
        {
            const rhps = try std.fmt.allocPrintZ
            (
                fba.allocator(),
                "{d: >6}",
                .{ last_message.Encounter.ENCHPS.value },
            );
            zimgui.PushFont(monospace_font);
            defer zimgui.PopFont();
            zimgui.Text(rhps[0..].ptr);
        }

        _ = zimgui.TableNextColumn();
        zimgui.Text("Deaths:");
        zimgui.SameLine();
        {
            const deaths = try std.fmt.allocPrintZ
            (
                fba.allocator(),
                "{d: >4}",
                .{ last_message.Encounter.deaths.value },
            );
            zimgui.PushFont(monospace_font);
            defer zimgui.PopFont();
            zimgui.Text(deaths[0..].ptr);
        }
    }

    if
    (
        zimgui.BeginTableExt
        (
            "Act Render Body",
            2,
            .{
                .BordersInnerH = true,
                .BordersInnerV = true,
                .BordersOuterH = true,
                .BordersOuterV = true,
                .SizingFixedFit = true,
            },
            zimgui.Vec2.init(window_size.x, 0.0),
            window_size.x
        )
    )
    {
        defer zimgui.EndTable();

        zimgui.TableSetupColumnExt("Left", .{}, window_size.x * 0.4, 0);
        zimgui.TableSetupColumnExt("Right", .{}, window_size.x * 0.6, 1);

        for (combatants.constSlice()) |combatant|
        {
            fba.reset();
            const job = try std.fmt.allocPrintZ(fba.allocator(), "{}", .{ combatant.Job.value });
            const name = try std.fmt.allocPrintZ(fba.allocator(), "{s}", .{ combatant.name });

            _ = zimgui.TableNextColumn();
            if (load_image_hook_fn != null)
            {
                // grab the long lived memory from the existing hashmap key for
                // reuse in the gpu loaded images hashmap since there is no
                // chance of it being freed before the gpu images are freed
                const job_key = blk: {
                    var temp_key_buf: [5]u8 = undefined;
                    const temp_key = try std.fmt.bufPrint(&temp_key_buf, "0_{s}", .{ job, });
                    break :blk job_icon_map.map.getKey(temp_key).?;
                };
                const job_icon = job_icon_map.map.get(job_key).?;

                const texture_id = blk: {
                    const result = try load_image_hook_fn.?(job_key, 0, 0, null);
                    if (result) |existing_texture_id| break :blk existing_texture_id;

                    const maybe_new_texture_id = try load_image_hook_fn.?
                    (
                        job_key,
                        job_icon.width,
                        job_icon.height,
                        job_icon.data,
                    );
                    break :blk maybe_new_texture_id.?;
                };

                zimgui.Image
                (
                    texture_id,
                    zimgui.Vec2.init
                    (
                        @floatFromInt(job_icon.width),
                        @floatFromInt(job_icon.height),
                    )
                );
            }
            else
            {
                zimgui.Text(job[0..].ptr);
            }
            zimgui.SameLine();
            zimgui.Text(name[0..].ptr);

            {
                _ = zimgui.TableNextColumn();
                const pdps = try std.fmt.allocPrintZ
                (
                    fba.allocator(),
                    "{d: >6}",
                    .{ combatant.DPS.value },
                );
                zimgui.PushFont(monospace_font);
                defer zimgui.PopFont();
                zimgui.Text(pdps[0..].ptr);
            }
            zimgui.SameLine();
            zimgui.Text("DPS");
            zimgui.SameLineExt(0.0, 0.0);

            {
                const crit = try std.fmt.allocPrintZ
                (
                    fba.allocator(),
                    "{s: >4}%%",
                    .{ combatant.@"crithit%".value },
                );
                zimgui.PushFont(monospace_font);
                defer zimgui.PopFont();
                zimgui.Text(crit[0..].ptr);
            }
            zimgui.SameLine();
            zimgui.Text("Crit");
            zimgui.SameLineExt(0.0, 0.0);

            {
                const heal = try std.fmt.allocPrintZ
                (
                    fba.allocator(),
                    "{s: >4}%%",
                    .{ combatant.@"healed%".value },
                );
                zimgui.PushFont(monospace_font);
                defer zimgui.PopFont();
                zimgui.Text(heal[0..].ptr);
            }
            zimgui.SameLine();
            zimgui.Text("Heal");
        }
    }
}
