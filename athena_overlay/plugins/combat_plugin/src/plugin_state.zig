const std = @import("std");

const act_types = @import("act_types.zig");
const plugin_types = @import("plugin_types.zig");

const simple = @import("simple");
const ws = @import("ws");


pub var InitTime: ?i64 = null;
pub var LoadImageHook: ?*const plugin_types.LoadImageHookFunction = null;
pub var AfterThisFrameUnlockMutex = false;

pub var ActMessageArena: ?std.heap.ArenaAllocator = null;
pub var ActMessageThread: ?std.Thread = null;
pub var ActMessageThreadShouldRun: bool = false;
pub var ActWebSocketConnection: ?ws.UnbufferedConnection = null;
pub var ActDataMutex: std.Thread.Mutex = .{};
pub var ActCombatants = simple.BoundedArray(*const act_types.RawCombatant, 512).init(0)
    catch unreachable;
pub var ActLastMessage: ?act_types.RawCombatData = null;

pub var CactbotMessageThread: ?std.Thread = null;
pub var CactbotMessageThreadShouldRun: bool = false;
pub var CactbotDataMutex: std.Thread.Mutex = .{};
pub var CactbotAlarmNotification: plugin_types.CactbotNotification = .{};
pub var CactbotAlertNotification: plugin_types.CactbotNotification = .{};
pub var CactbotInfoNotification: plugin_types.CactbotNotification = .{};
pub var CactbotTimelineEvents: ?plugin_types.CactbotTimelineEvents = null;

pub var IconArena: ?std.heap.ArenaAllocator = null;
pub var IconThread: ?std.Thread = null;
pub var IconThreadFinished = false;
pub var JobIcons: ?plugin_types.JobIconsMap = null;
