const builtin = @import("builtin");
const std = @import("std");

const act_helper = @import("act_helper.zig");
const act_types = @import("act_types.zig");
const plugin_config = @import("plugin_config.zig");
const plugin_state = @import("plugin_state.zig");

const simple = @import("simple");
const ws = @import("ws");


const TIMEOUT: u64 = 0.5 * std.time.ns_per_ms;
const SLEEP_INTERVAL: u64 = 0.5 * std.time.ns_per_s;
const STACKTRACE = switch (builtin.mode)
{
    .Debug => true,
    else => false,
};
const SUBSCRIBE_MESSAGE = \\{"call":"subscribe","events":["CombatData"]}
;

pub fn start_act_message_thread() !void
{
    if (plugin_state.ActWebSocketConnection != null) return;

    const config = try plugin_config.make_or_fetch_config(null);
    const act_ws_uri = try std.Uri.parse(config.act_websocket_url.constSlice());

    if ((!builtin.single_threaded) and config.use_background_network_thread)
    {
        @atomicStore(bool, &plugin_state.ActMessageThreadShouldRun, true, .Release);
        plugin_state.ActMessageThread = try std.Thread.spawn
        (
            .{},
            handle_act_message_thread,
            .{ config.act_websocket_url.constSlice() },
        );
        std.log.scoped(.ATHENAPLUGIN).info("Started background ACT WebSocket message thread", .{});
    }
    else
    {
        var dns_buf: [1024]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&dns_buf);
        plugin_state.ActWebSocketConnection = try ws.connect_unbuffered(fba.allocator(), act_ws_uri, &.{});
        std.log.scoped(.ATHENAPLUGIN).info
        (
            "Connection Success: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
            .{ act_ws_uri },
        );

        std.log.scoped(.ATHENAPLUGIN).info("Sending subscribe message...", .{});
        try plugin_state.ActWebSocketConnection.?.send(.text, SUBSCRIBE_MESSAGE);
        std.log.scoped(.ATHENAPLUGIN).info("Sent.", .{});
    }
}

pub fn handle_one_message() !void
{
    const response = plugin_state.ActWebSocketConnection.?.receiveUnbuffered(0, 0.5 * std.time.ns_per_s)
        catch |err| switch (err)
        {
            std.net.Stream.ReadError.WouldBlock => return,
            error.EndOfStream =>
            {
                std.log.scoped(.ATHENAPLUGIN).warn("ACT WebSocket sent close message, shutting down connection...", .{});
                @atomicStore(bool, &plugin_state.ActMessageThreadShouldRun, false, .Release);
                return;
            },
            else => return err,
        };

    plugin_state.ActDataMutex.lock();
    defer plugin_state.ActDataMutex.unlock();
    if (plugin_state.ActLastMessage != null)
    {
        _ = plugin_state.ActMessageArena.?.reset(.retain_capacity);
        plugin_state.ActLastMessage = null;
    }

    switch (response.type)
    {
        .binary, .text =>
        {
            switch (response.data)
            {
                .slice => |slice|
                {
                    plugin_state.ActLastMessage = std.json.parseFromSliceLeaky
                    (
                        act_types.RawCombatData,
                        plugin_state.ActMessageArena.?.allocator(),
                        slice,
                        .{
                            .ignore_unknown_fields = true,
                            .allocate = .alloc_always,
                        },
                    )
                        catch
                        {
                            plugin_state.ActLastMessage = null;
                            plugin_state.ActCombatants.len = 0;
                            _ = plugin_state.ActMessageArena.?.reset(.retain_capacity);
                            return;
                        };
                },
                .reader => |r|
                {
                    // var limiter = std.io.limitedReader(r.stream_reader, r.message_length);
                    // var buffer = std.io.bufferedReader(limiter.reader());
                    // var json_reader = std.json.reader(plugin_state.ActMessageArena.?.allocator(), buffer.reader());
                    // defer json_reader.deinit();

                    // plugin_state.ActLastMessage = std.json.parseFromTokenSourceLeaky
                    // (
                    //     act_types.RawCombatData,
                    //     plugin_state.ActMessageArena.?.allocator(),
                    //     &json_reader,
                    //     .{
                    //         .ignore_unknown_fields = true,
                    //         .allocate = .alloc_always,
                    //     },
                    // )
                    //     catch
                    //     {
                    //         plugin_state.ActLastMessage = null;
                    //         plugin_state.Combatants.len = 0;
                    //         _ = plugin_state.ActMessageArena.?.reset(.retain_capacity);
                    //         return;
                    //     };

                    if (r.message_length > std.math.maxInt(usize)) return error.OutOfMemory;

                    const msg_buf = try plugin_state.ActMessageArena.?.allocator().alloc
                    (
                        u8,
                        @truncate(r.message_length),
                    );
                    defer plugin_state.ActMessageArena.?.allocator().free(msg_buf);
                    const msg_length = try r.stream_reader.read(msg_buf);

                    plugin_state.ActLastMessage = std.json.parseFromSliceLeaky
                    (
                        act_types.RawCombatData,
                        plugin_state.ActMessageArena.?.allocator(),
                        msg_buf[0..msg_length],
                        .{
                            .ignore_unknown_fields = true,
                            .allocate = .alloc_always,
                        },
                    )
                        catch
                        {
                            plugin_state.ActLastMessage = null;
                            plugin_state.ActCombatants.len = 0;
                            _ = plugin_state.ActMessageArena.?.reset(.retain_capacity);
                            return;
                        };
                },
                else => return error.UnexpectedMessageDataType,
            }
        },
        .close => return,
        else => return error.UnexpectedMessageDataType,
    }

    try act_helper.sort_combatants_by_dps(&plugin_state.ActLastMessage.?, &plugin_state.ActCombatants);
}

pub fn handle_act_message_thread(act_websocket_url: []const u8) void
{
    {
        const act_ws_uri = std.Uri.parse(act_websocket_url) catch unreachable;
        const max_sleep = std.time.ns_per_s * 8;
        var start_sleep: u64 = std.time.ns_per_s * 1;
        var dns_buf: [1024]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&dns_buf);

        while (@atomicLoad(bool, &plugin_state.ActMessageThreadShouldRun, .Acquire))
        {
            fba.reset();
            const attempted_conn = ws.connect_unbuffered(fba.allocator(), act_ws_uri, &.{})
                catch
                {
                    std.log.scoped(.ATHENAPLUGIN).warn
                    (
                        "Failed to connect to " ++
                        simple.URL_FORMAT_TEMPLATE_WITH_SCHEME ++
                        ", sleeping for {d} seconds before trying again...",
                        .{
                            act_ws_uri,
                            start_sleep / std.time.ns_per_s,
                        },
                    );

                    var now: u64 = @intCast(std.time.nanoTimestamp());
                    const until: u64 = now + start_sleep;
                    while (now < until)
                    {
                        if (!@atomicLoad(bool, &plugin_state.ActMessageThreadShouldRun, .Acquire))
                        {
                            return;
                        }

                        std.time.sleep(SLEEP_INTERVAL);
                        now = @intCast(std.time.nanoTimestamp());
                    }

                    start_sleep = start_sleep * 2;
                    if (start_sleep > max_sleep) start_sleep = max_sleep;
                    continue;
                };

            std.log.scoped(.ATHENAPLUGIN).info
            (
                "Connection Success: " ++ simple.URL_FORMAT_TEMPLATE_WITH_SCHEME,
                .{ act_ws_uri },
            );
            plugin_state.ActWebSocketConnection = attempted_conn;
            break;
        }
    }

    if (@atomicLoad(bool, &plugin_state.ActMessageThreadShouldRun, .Acquire))
    {
        std.log.scoped(.ATHENAPLUGIN).info("Sending subscribe message...", .{});
        plugin_state.ActWebSocketConnection.?.send(.text, SUBSCRIBE_MESSAGE)
            catch |err|
            {
                std.log.err("{s}", .{ @errorName(err) });
                if (STACKTRACE)
                {
                    if (@errorReturnTrace()) |trace|
                    {
                        std.debug.dumpStackTrace(trace.*);
                    }
                }

                @panic("Unexpected error, aborting...");
            };
        std.log.scoped(.ATHENAPLUGIN).info("Sent.", .{});
    }

    while (@atomicLoad(bool, &plugin_state.ActMessageThreadShouldRun, .Acquire))
    {
        handle_one_message()
            catch |err|
            {
                std.log.err("{s}", .{ @errorName(err) });
                if (STACKTRACE)
                {
                    if (@errorReturnTrace()) |trace|
                    {
                        std.debug.dumpStackTrace(trace.*);
                    }
                }

                @panic("Unexpected error, aborting...");
            };
    }
}

pub fn stop_act_message_thread() void
{
    if (plugin_state.ActMessageThread != null)
    {
        std.log.scoped(.ATHENAPLUGIN).info("Received shutdown command, shutting down ACT message thread...", .{});
        @atomicStore(bool, &plugin_state.ActMessageThreadShouldRun, false, .Release);
        plugin_state.ActMessageThread.?.join();
        plugin_state.ActMessageThread = null;
    }

    if (plugin_state.ActWebSocketConnection == null)
    {
        std.log.scoped(.ATHENAPLUGIN).warn("ACT WebSocket connection was not started, could not close.", .{});
        return;
    }

    std.log.scoped(.ATHENAPLUGIN).info("Closing connection to ACT...", .{});
    plugin_state.ActWebSocketConnection.?.deinit();
    plugin_state.ActWebSocketConnection = null;
    std.log.scoped(.ATHENAPLUGIN).info("Connection closed.", .{});
}
