const builtin = @import("builtin");
const std = @import("std");

const plugin_types = @import("plugin_types.zig");

const known_folders = @import("known-folders");
const simple = @import("simple");


const true_false_map = std.ComptimeStringMap
(
    bool,
    .{
        .{ "true", true, },
        .{ "false", false, },
    }
);

var config: ?plugin_types.PluginConfigCombat = null;

fn parse_jsonc_config
(
    config_file_fd: *const std.fs.File,
    allocator: std.mem.Allocator,
)
!plugin_types.PluginConfigCombat
{
    const file_reader = config_file_fd.reader();
    var jsonc_reader = simple.jsonc.reader(allocator, file_reader);

    const parsed = try std.json.parseFromTokenSource
    (
        plugin_types.PluginConfigCombatRaw,
        allocator,
        &jsonc_reader,
        .{ .ignore_unknown_fields = true },
    );
    defer parsed.deinit();

    return .{
        .config_version = parsed.value.config_version,
        .enable_plugin = parsed.value.enable_plugin,

        // Game Details
        .hide_outside_combat = parsed.value.hide_outside_combat,

        // ACT UI Details
        .screen_margin = parsed.value.screen_margin,
        .window_position = parsed.value.window_position,
        .window_background_opacity = parsed.value.window_background_opacity,
        .ui_max_height =
            if (parsed.value.ui_max_height < 0)
                null
            else
                @intCast(parsed.value.ui_max_height),
        .job_icon_cache_path = try simple.expand_realpath(allocator, parsed.value.job_icon_cache_path),
        .job_icon_style = std.meta.stringToEnum(plugin_types.JobIconStyle, parsed.value.job_icon_style)
            orelse return error.InvalidIconStyle,

        // Cactbot UI Details
        .alarm_offset_position = parsed.value.alarm_offset_position,
        .alarm_screen_offset_x = parsed.value.alarm_screen_offset_x,
        .alarm_screen_offset_y = parsed.value.alarm_screen_offset_y,
        .alert_offset_position = parsed.value.alert_offset_position,
        .alert_screen_offset_x = parsed.value.alert_screen_offset_x,
        .alert_screen_offset_y = parsed.value.alert_screen_offset_y,
        .info_offset_position = parsed.value.info_offset_position,
        .info_screen_offset_x = parsed.value.info_screen_offset_x,
        .info_screen_offset_y = parsed.value.info_screen_offset_y,
        .timeline_offset_position = parsed.value.timeline_offset_position,
        .timeline_screen_offset_x = parsed.value.timeline_screen_offset_x,
        .timeline_screen_offset_y = parsed.value.timeline_screen_offset_y,

        // Advanced
        .act_websocket_url = try simple.BoundedArray(u8, 4096).fromSlice(parsed.value.act_websocket_url),
        .cactbot_url = try simple.BoundedArray(u8, 4096).fromSlice(parsed.value.cactbot_url),
        .load_job_icons_in_background_thread = parsed.value.load_job_icons_in_background_thread,
        .use_background_network_thread = parsed.value.use_background_network_thread,
    };
}

fn parse_yaml_config
(
    config_file_fd: *const std.fs.File,
    allocator: std.mem.Allocator,
)
!plugin_types.PluginConfigCombat
{
    const zig_yaml = @import("zig_yaml");

    var yaml_reader = blk: {
        const config_text = try config_file_fd.readToEndAlloc(allocator, std.math.maxInt(usize));
        defer allocator.free(config_text);

        break :blk try zig_yaml.Yaml.load(allocator, config_text);
    };
    defer yaml_reader.deinit();

    const config_version = yaml_reader.docs.items[0].map.get("config_version")
        orelse return error.MissingField;
    const enable_plugin = yaml_reader.docs.items[0].map.get("enable_plugin")
        orelse return error.MissingField;

    // Game Details
    const hide_outside_combat = yaml_reader.docs.items[0].map.get("hide_outside_combat")
        orelse return error.MissingField;

    // ACT UI Details
    const screen_margin = yaml_reader.docs.items[0].map.get("screen_margin")
        orelse return error.MissingField;
    const window_position = yaml_reader.docs.items[0].map.get("window_position")
        orelse return error.MissingField;
    const window_background_opacity = yaml_reader.docs.items[0].map.get("window_background_opacity")
        orelse return error.MissingField;
    const ui_max_height = yaml_reader.docs.items[0].map.get("ui_max_height")
        orelse return error.MissingField;
    const job_icon_cache_path = yaml_reader.docs.items[0].map.get("job_icon_cache_path")
        orelse return error.MissingField;
    const job_icon_style = yaml_reader.docs.items[0].map.get("job_icon_style")
        orelse return error.MissingField;

    // Cactbot UI Details
    const alarm_offset_position = yaml_reader.docs.items[0].map.get("alarm_offset_position")
        orelse return error.MissingField;
    const alarm_screen_offset_x = yaml_reader.docs.items[0].map.get("alarm_screen_offset_x")
        orelse return error.MissingField;
    const alarm_screen_offset_y = yaml_reader.docs.items[0].map.get("alarm_screen_offset_y")
        orelse return error.MissingField;
    const alert_offset_position = yaml_reader.docs.items[0].map.get("alert_offset_position")
        orelse return error.MissingField;
    const alert_screen_offset_x = yaml_reader.docs.items[0].map.get("alert_screen_offset_x")
        orelse return error.MissingField;
    const alert_screen_offset_y = yaml_reader.docs.items[0].map.get("alert_screen_offset_y")
        orelse return error.MissingField;
    const info_offset_position = yaml_reader.docs.items[0].map.get("info_offset_position")
        orelse return error.MissingField;
    const info_screen_offset_x = yaml_reader.docs.items[0].map.get("info_screen_offset_x")
        orelse return error.MissingField;
    const info_screen_offset_y = yaml_reader.docs.items[0].map.get("info_screen_offset_y")
        orelse return error.MissingField;
    const timeline_offset_position = yaml_reader.docs.items[0].map.get("timeline_offset_position")
        orelse return error.MissingField;
    const timeline_screen_offset_x = yaml_reader.docs.items[0].map.get("timeline_screen_offset_x")
        orelse return error.MissingField;
    const timeline_screen_offset_y = yaml_reader.docs.items[0].map.get("timeline_screen_offset_y")
        orelse return error.MissingField;

    // Advanced
    const act_websocket_url = yaml_reader.docs.items[0].map.get("act_websocket_url")
        orelse return error.MissingField;
    const cactbot_url = yaml_reader.docs.items[0].map.get("cactbot_url")
        orelse return error.MissingField;
    const load_job_icons_in_background_thread = yaml_reader.docs.items[0].map.get("load_job_icons_in_background_thread")
        orelse return error.MissingField;
    const use_background_network_thread = yaml_reader.docs.items[0].map.get("use_background_network_thread")
        orelse return error.MissingField;

    return .{
        .config_version = @intCast(config_version.int),
        .enable_plugin = true_false_map.get(enable_plugin.string)
            orelse return error.InvalidValue,

        // Game Details
        .hide_outside_combat = true_false_map.get(hide_outside_combat.string)
            orelse return error.InvalidValue,

        // ACT UI Details
        .screen_margin = @intCast(screen_margin.int),
        .window_position = std.meta.stringToEnum(plugin_types.WindowPosition, window_position.string)
            orelse return error.InvalidWindowPosition,
        .window_background_opacity = @floatCast(window_background_opacity.float),
        .ui_max_height =
            if (ui_max_height.int < 0)
                null
            else
                @intCast(ui_max_height.int),
        .job_icon_cache_path = try simple.expand_realpath(allocator, job_icon_cache_path.string),
        .job_icon_style = std.meta.stringToEnum(plugin_types.JobIconStyle, job_icon_style.string)
            orelse return error.InvalidIconStyle,

        // Cactbot UI Details
        .alarm_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, alarm_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .alarm_screen_offset_x = @intCast(alarm_screen_offset_x.int),
        .alarm_screen_offset_y = @intCast(alarm_screen_offset_y.int),
        .alert_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, alert_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .alert_screen_offset_x = @intCast(alert_screen_offset_x.int),
        .alert_screen_offset_y = @intCast(alert_screen_offset_y.int),
        .info_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, info_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .info_screen_offset_x = @intCast(info_screen_offset_x.int),
        .info_screen_offset_y = @intCast(info_screen_offset_y.int),
        .timeline_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, timeline_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .timeline_screen_offset_x = @intCast(timeline_screen_offset_x.int),
        .timeline_screen_offset_y = @intCast(timeline_screen_offset_y.int),

        // Advanced
        .act_websocket_url = try simple.BoundedArray(u8, 4096).fromSlice(act_websocket_url.string),
        .cactbot_url = try simple.BoundedArray(u8, 4096).fromSlice(cactbot_url.string),
        .load_job_icons_in_background_thread = true_false_map.get(load_job_icons_in_background_thread.string)
            orelse return error.InvalidValue,
        .use_background_network_thread = true_false_map.get(use_background_network_thread.string)
            orelse return error.InvalidValue,
    };
}

fn parse_libyaml_config
(
    config_file_fd: *const std.fs.File,
    allocator: std.mem.Allocator,
)
!plugin_types.PluginConfigCombat
{
    const libyaml_zig = @import("libyaml_zig");

    const config_text = try config_file_fd.readToEndAlloc(allocator, std.math.maxInt(usize));
    defer allocator.free(config_text);
    var document = try libyaml_zig.parse(allocator, config_text);

    const config_version = document.mapping.get("config_version")
        orelse return error.MissingField;
    const enable_plugin = document.mapping.get("enable_plugin")
        orelse return error.MissingField;

    // Game Details
    const hide_outside_combat = document.mapping.get("hide_outside_combat")
        orelse return error.MissingField;

    // ACT UI Details
    const screen_margin = document.mapping.get("screen_margin")
        orelse return error.MissingField;
    const window_position = document.mapping.get("window_position")
        orelse return error.MissingField;
    const window_background_opacity = document.mapping.get("window_background_opacity")
        orelse return error.MissingField;
    const ui_max_height = document.mapping.get("ui_max_height")
        orelse return error.MissingField;
    const job_icon_cache_path = document.mapping.get("job_icon_cache_path")
        orelse return error.MissingField;
    const job_icon_style = document.mapping.get("job_icon_style")
        orelse return error.MissingField;

    // Cactbot UI Details
    const alarm_offset_position = document.mapping.get("alarm_offset_position")
        orelse return error.MissingField;
    const alarm_screen_offset_x = document.mapping.get("alarm_screen_offset_x")
        orelse return error.MissingField;
    const alarm_screen_offset_y = document.mapping.get("alarm_screen_offset_y")
        orelse return error.MissingField;
    const alert_offset_position = document.mapping.get("alert_offset_position")
        orelse return error.MissingField;
    const alert_screen_offset_x = document.mapping.get("alert_screen_offset_x")
        orelse return error.MissingField;
    const alert_screen_offset_y = document.mapping.get("alert_screen_offset_y")
        orelse return error.MissingField;
    const info_offset_position = document.mapping.get("info_offset_position")
        orelse return error.MissingField;
    const info_screen_offset_x = document.mapping.get("info_screen_offset_x")
        orelse return error.MissingField;
    const info_screen_offset_y = document.mapping.get("info_screen_offset_y")
        orelse return error.MissingField;
    const timeline_offset_position = document.mapping.get("timeline_offset_position")
        orelse return error.MissingField;
    const timeline_screen_offset_x = document.mapping.get("timeline_screen_offset_x")
        orelse return error.MissingField;
    const timeline_screen_offset_y = document.mapping.get("timeline_screen_offset_y")
        orelse return error.MissingField;

    // Advanced
    const act_websocket_url = document.mapping.get("act_websocket_url")
        orelse return error.MissingField;
    const cactbot_url = document.mapping.get("cactbot_url")
        orelse return error.MissingField;
    const load_job_icons_in_background_thread = document.mapping.get("load_job_icons_in_background_thread")
        orelse return error.MissingField;
    const use_background_network_thread = document.mapping.get("use_background_network_thread")
        orelse return error.MissingField;

    return .{
        .config_version = try std.fmt.parseInt(u32, config_version.string, 10),
        .enable_plugin = true_false_map.get(enable_plugin.string)
            orelse return error.InvalidValue,

        // Game Details
        .hide_outside_combat = true_false_map.get(hide_outside_combat.string)
            orelse return error.InvalidValue,

        // ACT UI Details
        .screen_margin = try std.fmt.parseInt(i32, screen_margin.string, 10),
        .window_position = std.meta.stringToEnum(plugin_types.WindowPosition, window_position.string)
            orelse return error.InvalidWindowPosition,
        .window_background_opacity = try std.fmt.parseFloat(f32, window_background_opacity.string),
        .ui_max_height = blk: {
            const ui_max_height_int = try std.fmt.parseInt(i32, ui_max_height.string, 10);

            if (ui_max_height_int < 0) break :blk null;
            break :blk @intCast(ui_max_height_int);
        },
        .job_icon_cache_path = try simple.expand_realpath(allocator, job_icon_cache_path.string),
        .job_icon_style = std.meta.stringToEnum(plugin_types.JobIconStyle, job_icon_style.string)
            orelse return error.InvalidIconStyle,

        // Cactbot UI Details
        .alarm_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, alarm_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .alarm_screen_offset_x = try std.fmt.parseInt(i32, alarm_screen_offset_x.string, 10),
        .alarm_screen_offset_y = try std.fmt.parseInt(i32, alarm_screen_offset_y.string, 10),
        .alert_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, alert_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .alert_screen_offset_x = try std.fmt.parseInt(i32, alert_screen_offset_x.string, 10),
        .alert_screen_offset_y = try std.fmt.parseInt(i32, alert_screen_offset_y.string, 10),
        .info_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, info_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .info_screen_offset_x = try std.fmt.parseInt(i32, info_screen_offset_x.string, 10),
        .info_screen_offset_y = try std.fmt.parseInt(i32, info_screen_offset_y.string, 10),
        .timeline_offset_position = std.meta.stringToEnum(plugin_types.WindowPosition, timeline_offset_position.string)
            orelse return error.InvalidWindowPosition,
        .timeline_screen_offset_x = try std.fmt.parseInt(i32, timeline_screen_offset_x.string, 10),
        .timeline_screen_offset_y = try std.fmt.parseInt(i32, timeline_screen_offset_y.string, 10),

        // Advanced
        .act_websocket_url = try simple.BoundedArray(u8, 4096).fromSlice(act_websocket_url.string),
        .cactbot_url = try simple.BoundedArray(u8, 4096).fromSlice(cactbot_url.string),
        .load_job_icons_in_background_thread = true_false_map.get(load_job_icons_in_background_thread.string)
            orelse return error.InvalidValue,
        .use_background_network_thread = true_false_map.get(use_background_network_thread.string)
            orelse return error.InvalidValue,
    };
}

pub fn make_or_fetch_config(allocator: ?std.mem.Allocator) !*plugin_types.PluginConfigCombat
{
    if (config) |*c|
    {
        return c;
    }
    else if (allocator == null)
    {
        config = .{
            .config_version = 0,
            .enable_plugin = false,

            // Game Details
            .hide_outside_combat = true,

            // ACT UI Details
            .screen_margin = 0,
            .window_position = .TOP_LEFT,
            .window_background_opacity = 0.0,
            .ui_max_height = null,
            .job_icon_cache_path = .{
                .exists = false,
                .path = try simple.BoundedArray(u8, std.fs.MAX_PATH_BYTES).init(0),
            },
            .job_icon_style = .COLOR,

            // Cactbot UI Details
            .alarm_offset_position = .TOP_LEFT,
            .alarm_screen_offset_x = 0,
            .alarm_screen_offset_y = 0,
            .alert_offset_position = .TOP_LEFT,
            .alert_screen_offset_x = 0,
            .alert_screen_offset_y = 0,
            .info_offset_position = .TOP_LEFT,
            .info_screen_offset_x = 0,
            .info_screen_offset_y = 0,
            .timeline_offset_position = .TOP_LEFT,
            .timeline_screen_offset_x = 0,
            .timeline_screen_offset_y = 0,

            // Advanced
            .act_websocket_url = try simple.BoundedArray(u8, 4096).init(0),
            .cactbot_url = try simple.BoundedArray(u8, 4096).init(0),
            .load_job_icons_in_background_thread = false,
            .use_background_network_thread = false,
        };
        return &config.?;
    }

    var arena = std.heap.ArenaAllocator.init(allocator.?);
    defer arena.deinit();

    const athena_overlay_cache_path = blk: {
        const maybe_path = try known_folders.getPath(arena.allocator(), .cache);
        if (maybe_path == null) return error.FailedToFindCacheFolder;
        const cache_folder_path = maybe_path.?;
        defer arena.allocator().free(cache_folder_path);

        const athena_overlay_cache_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                cache_folder_path,
                "athena_overlay",
            },
        );

        std.fs.makeDirAbsolute(athena_overlay_cache_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        break :blk athena_overlay_cache_path;
    };
    defer arena.allocator().free(athena_overlay_cache_path);

    var config_extension: ?plugin_types.ConfigType = null;
    const config_file_fd = blk: {
        const config_folder_path = (
            try known_folders.getPath
            (
                arena.allocator(),
                .local_configuration,
            )
        )
            orelse return error.FailedToFindConfig;
        defer arena.allocator().free(config_folder_path);

        const athena_overlay_config_folder_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                config_folder_path,
                "athena_overlay",
            }
        );
        defer arena.allocator().free(athena_overlay_config_folder_path);

        std.fs.makeDirAbsolute(athena_overlay_config_folder_path)
            catch |err| switch (err)
            {
                error.PathAlreadyExists => {},
                else => return err,
            };

        const file_extensions: []const plugin_types.ConfigType = &.{ .json, .jsonc, .yaml, };
        for (file_extensions) |ext|
        {
            config_extension = ext;
            const combat_config_path = try std.fs.path.join
            (
                arena.allocator(),
                &.{
                    athena_overlay_config_folder_path,
                    switch (ext)
                    {
                        .json => "combat_config.json",
                        .jsonc => "combat_config.jsonc",
                        .yaml => "combat_config.yaml",
                    },
                },
            );
            defer arena.allocator().free(combat_config_path);

            break :blk std.fs.openFileAbsolute(combat_config_path, .{})
                catch |err| switch (err)
                {
                    error.FileNotFound => continue,
                    else => return err,
                };
        }

        // create file if not found
        const combat_config_path = try std.fs.path.join
        (
            arena.allocator(),
            &.{
                athena_overlay_config_folder_path,
                "combat_config.jsonc",
            }
        );
        defer arena.allocator().free(combat_config_path);

        config_extension = .jsonc;
        const fd = try std.fs.createFileAbsolute(combat_config_path, .{ .read = true, .truncate = false, });
        const file_info = try fd.stat();
        if (file_info.size == 0)
        {
            try fd.writeAll(@embedFile("default_config.jsonc"));
            try fd.seekTo(0);
        }

        break :blk fd;
    };
    defer config_file_fd.close();

    config = switch (config_extension.?)
    {
        .json, .jsonc => try parse_jsonc_config(&config_file_fd, arena.allocator()),
        .yaml => try parse_libyaml_config(&config_file_fd, arena.allocator()),
    };

    return &config.?;
}
