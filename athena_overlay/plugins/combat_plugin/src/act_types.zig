const std = @import("std");


pub const ActBool = struct
{
    value: bool,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        _ = allocator;
        _ = options;
        const true_false_map = std.ComptimeStringMap
        (
            bool,
            .{
                .{ "true", true, },
                .{ "false", false, },
            },
        );

        const token: std.json.Token = try source.next();
        switch (token)
        {
            .allocated_string, .string => |input|
            {
                return .{
                    .value =
                        if (true_false_map.get(input)) |result|
                            result
                        else
                            false,
                };
            },
            else => {},
        }

        return .{ .value = false };
    }
};

pub const ActInt = struct
{
    value: u64,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        _ = allocator;
        _ = options;
        const replacement_table = std.ComptimeStringMap
        (
            u64,
            .{
                .{ "∞", 0, },
                .{ "NaN", 0, },
                .{ "---", 0, },
            },
        );

        const token: std.json.Token = try source.next();
        switch (token)
        {
            .allocated_number, .allocated_string, .number, .string => |input|
            {
                return .{
                    .value =
                        if (input.len < 1)
                            0
                        else if (replacement_table.get(input)) |replacement|
                            replacement
                        else
                            try std.fmt.parseInt(u64, input, 10)
                };
            },
            else => {},
        }

        return .{ .value = 0 };
    }
};

pub const ActFloat = struct
{
    value: f64,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        _ = allocator;
        _ = options;
        const replacement_table = std.ComptimeStringMap
        (
            f64,
            .{
                .{ "∞", 0.0, },
                .{ "NaN", 0.0, },
                .{ "---", 0.0, },
            },
        );

        const token: std.json.Token = try source.next();
        switch (token)
        {
            .allocated_number, .allocated_string, .number, .string => |input|
            {
                return .{
                    .value =
                        if (input.len < 1)
                            0.0
                        else if (replacement_table.get(input)) |replacement|
                            replacement
                        else
                            try std.fmt.parseFloat(f64, input),
                };
            },
            else => {},
        }

        return .{ .value = 0.0 };
    }
};

pub const Job = enum
{
    // Unknown
    UNK,

    // Tanks
    GLA,
    PLD,
    MRD,
    WAR,
    DRK,
    GNB,

    // Healers
    CNJ,
    SCH,
    AST,
    SGE,

    // Melee DPS
    PGL,
    MNK,
    LNC,
    DRG,
    ROG,
    NIN,
    SAM,
    RPR,

    // Phys Ranged DPS
    ARC,
    BRD,
    MCH,
    DNC,

    // Magic Ranged DPS
    THM,
    BLM,
    ACN,
    SMN,
    RDM,
    BLU,

    // Crafting
    CRP,
    BSM,
    ARM,
    GSM,
    LTW,
    WVR,
    ALC,
    CUL,

    // Gathering
    MIN,
    BTN,
    FSH,

    pub fn format
    (
        obj: @This(),
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    )
    !void
    {
        _ = options;
        _ = fmt_str;
        _ = try writer.write(@tagName(obj));
    }
};

pub const ActJob = struct
{
    value: Job,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        _ = allocator;
        _ = options;

        const token: std.json.Token = try source.next();
        switch (token)
        {
            .allocated_string, .string => |input|
            {
                return .{
                    .value = blk: {
                        if (input.len < 1 or input.len > 3) break :blk .UNK;

                        var job: [3]u8 = undefined;
                        for (0..input.len) |i|
                        {
                            job[i] = std.ascii.toUpper(input[i]);
                        }
                        if (std.meta.stringToEnum(Job, &job)) |result| break :blk result;

                        break :blk .UNK;
                    },
                };
            },
            else => {},
        }

        return .{ .value = .UNK };
    }

    pub fn format
    (
        obj: @This(),
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    )
    !void
    {
        _ = options;
        _ = fmt_str;
        _ = try writer.write(@tagName(obj.value));
    }
};

pub const SafeString = struct
{
    value: []const u8,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        _ = options;

        const token: std.json.Token = try source.next();
        switch (token)
        {
            .allocated_string, .string => |input|
            {
                if (input.len < 1) return .{ .value = input, };

                const output = try allocator.alloc(u8, std.mem.replacementSize(u8, input, "%", ""));
                _ = std.mem.replace(u8, input, "%", "", output);
                return .{ .value = output, };
            },
            else => {},
        }

        return error.UnexpectedToken;
    }
};

pub const RawEncounter = struct
{
    // title: []const u8,            // "Kacchapa",
    duration: []const u8,         // "02:56",
    DURATION: ActInt,             // "177",
    damage: ActInt,               // "3224",
    // @"damage-m": []const u8,      // "0.00",
    // @"damage-*": []const u8,      // "3.00K",
    // @"DAMAGE-k": []const u8,      // "3",
    // @"DAMAGE-m": []const u8,      // "0",
    // @"DAMAGE-b": []const u8,      // "0",
    // @"DAMAGE-*": []const u8,      // "3K",
    dps: ActFloat,                // "18.25",
    // @"dps-*": []const u8,         // "dps-*",
    DPS: ActInt,                  // "18",
    // @"DPS-k": []const u8,         // "0",
    // @"DPS-m": []const u8,         // "DPS-m",
    // @"DPS-*": []const u8,         // "18",
    encdps: ActFloat,             // "18.25",
    // @"encdps-*": []const u8,      // "18",
    ENCDPS: ActInt,               // "18",
    // @"ENCDPS-k": []const u8,      // "0",
    // @"ENCDPS-m": []const u8,      // "0",
    // @"ENCDPS-*": []const u8,      // "18",
    hits: ActInt,                 // "1",
    crithits: ActInt,             // "0",
    @"crithit%": []const u8,      // "0%",
    misses: ActInt,               // "0",
    hitfailed: ActInt,            // "0",
    swings: ActInt,               // "1",
    tohit: ActFloat,              // "100.00",
    TOHIT: ActInt,                // "100",
    // maxhit: []const u8,           // "YOU-Tomahawk-3224",
    // MAXHIT: []const u8,           // "YOU-3224",
    // @"maxhit-*": []const u8,      // "YOU-Tomahawk-3K",
    // @"MAXHIT-*": []const u8,      // "YOU-3.00K",
    healed: ActInt,               // "0",
    enchps: ActFloat,             // "0.00",
    // @"enchps-*": []const u8,      // "0",
    ENCHPS: ActInt,               // "0",
    // @"ENCHPS-k": []const u8,      // "0",
    // @"ENCHPS-m": []const u8,      // "0",
    // @"ENCHPS-*": []const u8,      // "0",
    heals: []const u8,            // "0",
    critheals: []const u8,        // "0",
    @"critheal%": []const u8,     // "NaN",
    cures: []const u8,            // "0",
    // maxheal: []const u8,          // "",
    // MAXHEAL: []const u8,          // "",
    // maxhealward: []const u8,      // "",
    // MAXHEALWARD: []const u8,      // "",
    // @"maxheal-*": []const u8,     // "",
    // @"MAXHEAL-*": []const u8,     // "",
    // @"maxhealward-*": []const u8, // "",
    // @"MAXHEALWARD-*": []const u8, // "",
    damagetaken: []const u8,      // "0",
    // @"damagetaken-*": []const u8, // "0",
    healstaken: []const u8,       // "0",
    // @"healstaken-*": []const u8,  // "0",
    powerdrain: []const u8,       // "0",
    // @"powerdrain-*": []const u8,  // "0",
    powerheal: []const u8,        // "0",
    // @"powerheal-*": []const u8,   // "0",
    kills: ActInt,                // "0",
    deaths: ActInt,               // "0",
    CurrentZoneName: []const u8,  // "Thavnair",
    // Last10DPS: []const u8,        // "0",
    // Last30DPS: []const u8,        // "0",
    // Last60DPS: []const u8,        // "0"

    pub fn format
    (
        message_fmt: @This(),
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    )
    !void
    {
        _ = options;
        _ = fmt_str;
        _ = try writer.write(@typeName(@This()));
        try std.json.stringify(message_fmt, .{}, writer);
    }
};

pub const RawCombatant = struct
{
    absorbHeal: []const u8,
    BlockPct: []const u8,
    CritDirectHitCount: ActInt,
    CritDirectHitPct: []const u8,
    @"critheal%": SafeString,
    critheals: []const u8,
    @"crithit%": SafeString,
    crithits: ActInt,
    cures: []const u8,
    // @"damage-*": []const u8,
    // @"DAMAGE-*": []const u8,
    // @"damage-b": []const u8,
    // @"DAMAGE-b": []const u8,
    // @"DAMAGE-k": []const u8,
    // @"damage-m": []const u8,
    // @"DAMAGE-m": []const u8,
    damage: ActInt,
    @"damage%": SafeString,
    damageShield: []const u8,
    // @"damagetaken-*": []const u8,
    damagetaken: []const u8,
    deaths: ActInt,
    DirectHitCount: []const u8,
    DirectHitPct: []const u8,
    // @"dps-*": []const u8,
    // @"DPS-*": []const u8,
    // @"DPS-k": []const u8,
    // @"DPS-m": []const u8,
    dps: ActFloat,
    DPS: ActInt,
    duration: []const u8,
    DURATION: []const u8,
    // @"encdps-*": []const u8,
    // @"ENCDPS-*": []const u8,
    // @"ENCDPS-k": []const u8,
    // @"ENCDPS-m": []const u8,
    encdps: ActFloat,
    ENCDPS: ActInt,
    // @"enchps-*": []const u8,
    // @"ENCHPS-*": []const u8,
    // @"ENCHPS-k": []const u8,
    // @"ENCHPS-m": []const u8,
    enchps: ActFloat,
    ENCHPS: ActInt,
    healed: ActInt,
    @"healed%": SafeString,
    heals: ActInt,
    // @"healstaken-*": []const u8,
    healstaken: ActInt,
    hitfailed: []const u8,
    hits: ActInt,
    IncToHit: []const u8,
    Job: ActJob,
    kills: ActInt,
    // Last10DPS: []const u8,
    // Last30DPS: []const u8,
    // Last60DPS: []const u8,
    // @"maxheal-*": []const u8,
    // @"MAXHEAL-*": []const u8,
    // maxheal: []const u8,
    MAXHEAL: []const u8,
    // @"maxhealward-*": []const u8,
    // @"MAXHEALWARD-*": []const u8,
    // maxhealward: []const u8,
    MAXHEALWARD: []const u8,
    // @"maxhit-*": []const u8,
    // @"MAXHIT-*": []const u8,
    // maxhit: []const u8,
    MAXHIT: []const u8,
    misses: ActInt,
    name: []const u8,
    overHeal: []const u8,
    OverHealPct: []const u8,
    ParryPct: []const u8,
    // @"powerdrain-*": []const u8,
    powerdrain: []const u8,
    // @"powerheal-*": []const u8,
    powerheal: []const u8,
    swings: ActInt,
    // threatdelta: []const u8,
    // threatstr: []const u8,
    tohit: ActFloat,
    TOHIT: ActInt,

    pub fn format
    (
        message_fmt: @This(),
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    )
    !void
    {
        _ = options;
        _ = fmt_str;
        _ = try writer.write(@typeName(@This()));
        try std.json.stringify(message_fmt, .{}, writer);
    }

    pub fn critical_hit_percent(self: @This()) u7
    {
        if (self.hits.value < 1) return 0;
        const percentage: f64 = @as(f64, @floatFromInt(self.crithits.value)) / @as(f64, @floatFromInt(self.hits.value));
        return @intFromFloat(@floor(percentage * 100.0 + 0.5));
    }

    pub fn healed_percent(self: @This()) u7
    {
        if (self.healstaken.value < 1) return 0;
        const percentage: f64 = @as(f64, @floatFromInt(self.healed.value)) / @as(f64, @floatFromInt(self.healstaken.value));
        return @intFromFloat(@floor(percentage * 100.0 + 0.5));
    }
};

pub const RawCombatData = struct
{
    @"type": []const u8,
    Encounter: RawEncounter,
    Combatant: std.json.ArrayHashMap(RawCombatant),
    isActive: ActBool,

    pub fn format
    (
        message_fmt: @This(),
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    )
    !void
    {
        _ = options;
        _ = fmt_str;
        _ = try writer.write(@typeName(@This()));
        try std.json.stringify(message_fmt, .{}, writer);
    }
};
