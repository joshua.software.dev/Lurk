const std = @import("std");

const act_types = @import("act_types.zig");

const simple = @import("simple");
const zimgui = @import("Zig-ImGui");


pub const ConfigType = enum
{
    json,
    jsonc,
    yaml,
};

pub const WindowPosition = enum
{
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
    CENTER,
};

pub const JobIconStyle = enum(usize)
{
    COLOR = 0,
    PLAIN = 1,
};

pub const PluginConfigCombatRaw = struct
{
    config_version: u32,
    enable_plugin: bool,

    // Game Details
    hide_outside_combat: bool,

    // ACT UI Details
    screen_margin: i32,
    window_position: WindowPosition,
    window_background_opacity: f32,
    ui_max_height: i32,
    job_icon_cache_path: []const u8,
    job_icon_style: []const u8,

    // Cactbot UI Details
    alarm_offset_position: WindowPosition,
    alarm_screen_offset_x: i32,
    alarm_screen_offset_y: i32,
    alert_offset_position: WindowPosition,
    alert_screen_offset_x: i32,
    alert_screen_offset_y: i32,
    info_offset_position: WindowPosition,
    info_screen_offset_x: i32,
    info_screen_offset_y: i32,
    timeline_offset_position: WindowPosition,
    timeline_screen_offset_x: i32,
    timeline_screen_offset_y: i32,

    // Advanced
    act_websocket_url: []const u8,
    cactbot_url: []const u8,
    load_job_icons_in_background_thread: bool,
    use_background_network_thread: bool,
};

pub const PluginConfigCombat = struct
{
    config_version: u32,
    enable_plugin: bool,

    // Game Details
    hide_outside_combat: bool,

    // ACT UI Details
    screen_margin: i32,
    window_position: WindowPosition,
    window_background_opacity: f32,
    ui_max_height: ?u32,
    job_icon_cache_path: simple.RealPath,
    job_icon_style: JobIconStyle,

    // Cactbot UI Details
    alarm_offset_position: WindowPosition,
    alarm_screen_offset_x: i32,
    alarm_screen_offset_y: i32,
    alert_offset_position: WindowPosition,
    alert_screen_offset_x: i32,
    alert_screen_offset_y: i32,
    info_offset_position: WindowPosition,
    info_screen_offset_x: i32,
    info_screen_offset_y: i32,
    timeline_offset_position: WindowPosition,
    timeline_screen_offset_x: i32,
    timeline_screen_offset_y: i32,

    // Advanced
    act_websocket_url: simple.BoundedArray(u8, 4096),
    cactbot_url: simple.BoundedArray(u8, 4096),
    load_job_icons_in_background_thread: bool,
    use_background_network_thread: bool,
};

pub const CactbotNotification = struct
{
    state_update: bool = false,
    text: simple.BoundedArray(u8, 1024) = simple.BoundedArray(u8, 1024).init(0) catch unreachable,
};
pub const CactbotTimelineEvent = struct
{
    id: u32,
    ends_at: i64,
    fill: bool,
    duration: f64,
    color_abgr: u32,
    text: simple.BoundedArray(u8, 512),
};
pub const CactbotTimelineEvents = simple.FixedSizeArrayHashMap
(
    std.AutoArrayHashMapUnmanaged(u32, CactbotTimelineEvent),
    128,
);

pub const JobIcon = struct
{
    width: u32,
    height: u32,
    data: []const u8,
};
pub const JobIconsMap = simple.FixedSizeArrayHashMapAllocator(std.StringArrayHashMapUnmanaged(JobIcon), 40 + 39);

pub const LoadImageHookFunction = fn ([]const u8, u32, u32, ?[]const u8) anyerror!?zimgui.TextureID;
