const builtin = @import("builtin");
const std = @import("std");

const cactbot_parser = @import("cactbot_parser.zig");
const plugin_config = @import("plugin_config.zig");
const plugin_state = @import("plugin_state.zig");

const simple = @import("simple");


const SLEEP_INTERVAL: u64 = std.time.ns_per_ms * (std.time.ms_per_s / 100);
const STACKTRACE = switch (builtin.mode)
{
    .Debug => true,
    else => false,
};

pub fn start_cactbot_message_thread(heap_allocator: std.mem.Allocator) !void
{
    if (plugin_state.CactbotMessageThread != null) return;

    const config = try plugin_config.make_or_fetch_config(null);
    _ = try std.Uri.parse(config.cactbot_url.constSlice());

    if ((!builtin.single_threaded) and config.use_background_network_thread)
    {
        @atomicStore(bool, &plugin_state.CactbotMessageThreadShouldRun, true, .Release);
        plugin_state.CactbotMessageThread = try std.Thread.spawn
        (
            .{},
            handle_cactbot_message_thread,
            .{
                heap_allocator,
                config.cactbot_url.constSlice(),
            },
        );
        std.log.scoped(.ATHENAPLUGIN).info("Started background Cactbot message thread", .{});
    }
    else
    {
        var http_client = simple.init_std_http_client(.{
            .bundle = .dynamically_generate,
            .http_allocator = heap_allocator,
        })
            catch unreachable; // cannot error with .dynamically_generate
        const simple_http_client = http_client.as_simple_client();
        defer simple_http_client.deinit();

        const verify_url = try std.fmt.allocPrint
        (
            heap_allocator,
            "{s}/tncef",
            .{ config.cactbot_url.constSlice() },
        );
        defer heap_allocator.free(verify_url);

        const response = simple_http_client.download_bytes
        (
            &.{},
            std.Uri.parse(verify_url)
                catch unreachable,
            heap_allocator,
            std.math.maxInt(usize),
        )
            catch |err| switch (err)
            {
                error.ConnectionRefused => return,
                else => return err,
            };
        defer heap_allocator.free(response);

        if
        (
            !std.mem.eql
            (
                u8,
                response,
                \\{"IsTotallyNotCef":true}
            )
        )
        {
            std.log.scoped(.ATHENAPLUGIN).warn("Cactbot server is not TotallyNotCef", .{});
        }
    }
}

pub fn handle_one_message
(
    allocator: std.mem.Allocator,
    http_client: simple.SimpleHttpClient,
    cactbot_http_uri: std.Uri,
)
!void
{
    const response = try http_client.download_bytes
    (
        &.{},
        cactbot_http_uri,
        allocator,
        std.math.maxInt(usize),
    );
    defer allocator.free(response);

    plugin_state.CactbotDataMutex.lock();
    defer plugin_state.CactbotDataMutex.unlock();

    try cactbot_parser.parse_cactbot_html
    (
        allocator,
        response,
        &plugin_state.CactbotAlarmNotification,
        &plugin_state.CactbotAlertNotification,
        &plugin_state.CactbotInfoNotification,
        &plugin_state.CactbotTimelineEvents.?,
    );
}

pub fn handle_cactbot_message_thread(allocator: std.mem.Allocator, cactbot_url: []const u8) void
{
    const cactbot_uri = std.Uri.parse(cactbot_url)
        catch unreachable;
    const verify_url = std.fmt.allocPrint(allocator, "{s}/tncef", .{ cactbot_url })
        catch @panic("oom");
    defer allocator.free(verify_url);

    var http_client = simple.init_std_http_client(.{
        .bundle = .dynamically_generate,
        .http_allocator = allocator,
    })
        catch unreachable; // cannot error with .dynamically_generate
    const simple_http_client = http_client.as_simple_client();
    defer simple_http_client.deinit();

    while (@atomicLoad(bool, &plugin_state.CactbotMessageThreadShouldRun, .Acquire)) start_of_thread: {
        const response = simple_http_client.download_bytes
        (
            &.{},
            std.Uri.parse(verify_url)
                catch unreachable,
            allocator,
            std.math.maxInt(usize),
        )
            catch |err| switch (err)
            {
                error.ConnectionRefused => break :start_of_thread,
                else =>
                {
                    std.log.err("{s}", .{ @errorName(err) });
                    if (STACKTRACE)
                    {
                        if (@errorReturnTrace()) |trace|
                        {
                            std.debug.dumpStackTrace(trace.*);
                        }
                    }

                    @panic("Unexpected error, aborting...");
                },
            };
        defer allocator.free(response);

        if
        (
            !std.mem.eql
            (
                u8,
                response,
                \\{"IsTotallyNotCef":true}
            )
        )
        {
            break :start_of_thread;
        }

        while (@atomicLoad(bool, &plugin_state.CactbotMessageThreadShouldRun, .Acquire))
        {
            handle_one_message(allocator, simple_http_client, cactbot_uri)
                catch |err| switch (err)
                {
                    error.ConnectionRefused => break :start_of_thread,
                    else =>
                    {
                        std.log.err("{s}", .{ @errorName(err) });
                        if (STACKTRACE)
                        {
                            if (@errorReturnTrace()) |trace|
                            {
                                std.debug.dumpStackTrace(trace.*);
                            }
                        }

                        @panic("Unexpected error, aborting...");
                    }
                };

            var now = std.time.milliTimestamp();
            const until = now + (std.time.ms_per_s / 10);
            while (now < until)
            {
                if (!@atomicLoad(bool, &plugin_state.CactbotMessageThreadShouldRun, .Acquire))
                {
                    return;
                }

                std.time.sleep(SLEEP_INTERVAL);
                now = std.time.milliTimestamp();
            }
        }
    }
}

pub fn stop_cactbot_message_thread() void
{
    if (plugin_state.CactbotMessageThread == null)
    {
        std.log.scoped(.ATHENAPLUGIN).warn("Cactbot connection was not started, could not close.", .{});
    }
    else
    {
        std.log.scoped(.ATHENAPLUGIN).info("Received shutdown command, shutting down Cactbot message thread...", .{});
        @atomicStore(bool, &plugin_state.CactbotMessageThreadShouldRun, false, .Release);
        plugin_state.CactbotMessageThread.?.join();
        plugin_state.CactbotMessageThread = null;
    }
}
